package com.radix.renaplayer.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static class PlayInfoScheme {
        public static final String TABLE_NAME = "playinfo";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_FILENAME = "filename";
        public static final String COLUMN_NAME_POS = "playpos_second";
        public static final String COLUMN_NAME_ORIENTATION = "orientation";
        public static final String COLUMN_NAME_SCREEN_RESOLUTION = "screen_resolution";
        public static final String SQL_CREATE_MEDIA_PLAY_INFO_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME_FILENAME + " TEXT NOT NULL, " +
                    COLUMN_NAME_ORIENTATION + " INTEGER, " +
                    COLUMN_NAME_SCREEN_RESOLUTION + " INTEGER, " +
                    COLUMN_NAME_POS + " INTEGER NOT NULL)";
        public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public class StoredPlayInfo {
        public String fileName_ = "";
        public long playPos_ = 0;
        public int orientation_ = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        public int screenResolution_ = 0;
    }

    public static class NetworkStreamTable {
        public static final String TABLE_NAME = "networkstream";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_URL = "url";
        public static final String SQL_CREATE_NETWORKSTREAM_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                  COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                  COLUMN_NAME_URL + " TEXT)";
        public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    private static final int dbVersion_  = 1;

    private long countFromPlayInfoTable() {
        SQLiteDatabase db = getReadableDatabase();
        return DatabaseUtils.queryNumEntries(db, PlayInfoScheme.TABLE_NAME);
    }

    public DatabaseHelper(Context context) {
        super(context, "renapl.db", null, dbVersion_);
    }

    public void savePlayInfo(StoredPlayInfo storedPlayInfo) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PlayInfoScheme.COLUMN_NAME_POS, storedPlayInfo.playPos_);
        values.put(PlayInfoScheme.COLUMN_NAME_ORIENTATION, storedPlayInfo.orientation_);
        values.put(PlayInfoScheme.COLUMN_NAME_SCREEN_RESOLUTION, storedPlayInfo.screenResolution_);
        String selection = PlayInfoScheme.COLUMN_NAME_FILENAME + "=?";
        String[] args = { storedPlayInfo.fileName_ };
        int count = db.update(PlayInfoScheme.TABLE_NAME, values, selection, args);
        if (count == 0) {
            long rowcount = countFromPlayInfoTable();
            if (rowcount >= 20) {
                db.execSQL("DELETE FROM " + PlayInfoScheme.TABLE_NAME + " WHERE " + PlayInfoScheme.COLUMN_NAME_ID +
                        "=(SELECT MIN(" + PlayInfoScheme.COLUMN_NAME_ID + ") FROM " + PlayInfoScheme.TABLE_NAME + ")");
            }
            values.put(PlayInfoScheme.COLUMN_NAME_FILENAME, storedPlayInfo.fileName_);
            db.insert(PlayInfoScheme.TABLE_NAME,null, values);
        }
    }

    public StoredPlayInfo getPlayInfo(String fileName) {
        StoredPlayInfo storedPlayInfo = new StoredPlayInfo();
        storedPlayInfo.fileName_ = fileName;
        SQLiteDatabase db = getReadableDatabase();
        String[] column = { PlayInfoScheme.COLUMN_NAME_POS, PlayInfoScheme.COLUMN_NAME_ORIENTATION, PlayInfoScheme.COLUMN_NAME_SCREEN_RESOLUTION };
        String selection = PlayInfoScheme.COLUMN_NAME_FILENAME + "=?";
        String[] args = { fileName };
        Cursor cursor = db.query(PlayInfoScheme.TABLE_NAME, column, selection, args, null, null, null);
        if (cursor == null)
            return storedPlayInfo;

        if (cursor.getCount() > 0) {
            cursor.moveToNext();
            storedPlayInfo.playPos_ = cursor.getInt(0);
            storedPlayInfo.orientation_ = cursor.getInt(1);
            storedPlayInfo.screenResolution_ = cursor.getInt(2);
        }
        cursor.close();

        return storedPlayInfo;
    }

    public void updateNetworkStreamUrl(String url) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NetworkStreamTable.COLUMN_NAME_URL, url);
        String selection = NetworkStreamTable.COLUMN_NAME_ID + "=1";
        int count = db.update(NetworkStreamTable.TABLE_NAME, values, selection, null);
        if (count == 0)
            db.insert(NetworkStreamTable.TABLE_NAME, null, values);
    }

    public String getNetworkStreamUrl() {
        SQLiteDatabase db = getReadableDatabase();
        String[] column = { NetworkStreamTable.COLUMN_NAME_URL };
        String selection = NetworkStreamTable.COLUMN_NAME_ID + "=1";
        Cursor cursor = db.query(NetworkStreamTable.TABLE_NAME, column, selection, null, null, null, null);
        if (cursor == null)
            return "";

        String ret = null;
        if (cursor.moveToNext())
            ret = cursor.getString(0);
        cursor.close();

        return ret;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PlayInfoScheme.SQL_CREATE_MEDIA_PLAY_INFO_TABLE);
        db.execSQL(NetworkStreamTable.SQL_CREATE_NETWORKSTREAM_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(PlayInfoScheme.SQL_DROP_TABLE);
        db.execSQL(NetworkStreamTable.SQL_DROP_TABLE);
        onCreate(db);
    }
}
