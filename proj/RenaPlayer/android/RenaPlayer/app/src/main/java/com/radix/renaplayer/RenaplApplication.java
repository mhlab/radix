package com.radix.renaplayer;

import android.app.Application;

import com.radix.renaplayer.db.DatabaseHelper;

public class RenaplApplication extends Application {
    private DatabaseHelper dbHelper_ = null;

    public void savePlayInfo(DatabaseHelper.StoredPlayInfo storedPlayInfo) {
        if (storedPlayInfo == null)
            return;
        dbHelper_.savePlayInfo(storedPlayInfo);
    }

    public DatabaseHelper.StoredPlayInfo getPlayInfo(String fileName) {
        return dbHelper_.getPlayInfo(fileName);
    }

    public void updateNetworkStreamUrl(String url) {
        dbHelper_.updateNetworkStreamUrl(url);
    }

    public String getNetworkStreamUrl() {
        return dbHelper_.getNetworkStreamUrl();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        dbHelper_ = new DatabaseHelper(getApplicationContext());
    }
}
