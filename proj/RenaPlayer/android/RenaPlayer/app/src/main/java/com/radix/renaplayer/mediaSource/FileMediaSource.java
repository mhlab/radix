package com.radix.renaplayer.mediaSource;

import Radix.Media.Demuxer.DemuxerDef;
import Radix.Media.Demuxer.DemuxerListener;
import Radix.Media.Demuxer.MediaExtractorDemuxer;
import Radix.Media.Demuxer.SyncDemuxer;

public class FileMediaSource extends BaseMediaSource implements DemuxerListener {
    private SyncDemuxer demuxer_ = null;

    @Override
    public void openMedia(String url) {
        demuxer_ = new SyncDemuxer(this);
        demuxer_.open(url);
    }

    @Override
    public void closeMedia() {
        if (demuxer_ != null) {
            demuxer_.destroy();
            demuxer_ = null;
        }
    }

    @Override
    public String getUrl() {
        if (demuxer_ == null)
            return "";

        return demuxer_.getFileName();
    }

    @Override
    public String getShortUrl() {
        String url = getUrl();
        return url.substring(url.lastIndexOf("/") + 1);
    }

    @Override
    public void play() {
        if (demuxer_ == null)
            return;

        demuxer_.play();
    }

    @Override
    public void stop() {
        if (demuxer_ == null)
            return;

        demuxer_.stop();
    }

    @Override
    public void pause() {
        if (demuxer_ == null)
            return;

        demuxer_.pause();
    }

    @Override
    public void seek(long seekSeconds) {
        if (demuxer_ == null)
            return;

        demuxer_.seek((int)seekSeconds);
    }


    @Override
    public void onCommandResult_Demuxer(int command, boolean ret, String commandAttr) {
        switch (command) {
            case DemuxerDef.DEMUXER_COMMAND_OPEN:
                callCommandResultMediaSource(demuxer_.getFileName(), MediaCommand.MEDIACOMMAND_OPEN, ret, commandAttr);
                break;
            case DemuxerDef.DEMUXER_COMMAND_SEEK:
                callCommandResultMediaSource(demuxer_.getFileName(), MediaCommand.MEDIACOMMAND_SEEK, ret, commandAttr);
                break;
            case DemuxerDef.DEMUXER_COMMAND_PLAY:
                callCommandResultMediaSource(demuxer_.getFileName(), MediaCommand.MEDIACOMMAND_PLAY, ret, commandAttr);
                break;
        }
    }

    @Override
    public void onCodecData_Demuxer(byte[] videoCodecData, byte[] audioCodecData) {
        callCodecDataMediaSource(demuxer_.getFileName(), videoCodecData, audioCodecData);
    }

    @Override
    public void onRecvStream_Demuxer(byte[] mediaData, int mediaSize, int codec, long presentationTime) {
        callRecvStreamMediaSource(demuxer_.getFileName(), mediaData, mediaSize, codec, presentationTime);
    }

    @Override
    public void onUpdatedPlayTime_Demuxer(long playTime) {
        callUpdatedPlayTime(demuxer_.getFileName(), playTime);
    }
}
