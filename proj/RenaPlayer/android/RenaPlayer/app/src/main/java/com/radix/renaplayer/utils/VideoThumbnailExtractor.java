package com.radix.renaplayer.utils;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;

public class VideoThumbnailExtractor {
    public interface VideoThumbnailExtractorListener {
        void onExtractVideoThumbnail(String videoFilePath, Bitmap thumbnail);
    }

    public class ThumbnailExtractorThread extends Thread {
        private String videoFilePath_ = null;

        public ThumbnailExtractorThread(String videoFilePath) {
            videoFilePath_ = videoFilePath;
        }

        @Override
        public void run() {
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(videoFilePath_, MediaStore.Video.Thumbnails.MICRO_KIND);
            if (listener_ != null)
                listener_.onExtractVideoThumbnail(videoFilePath_, bitmap);
        }
    }

    private VideoThumbnailExtractorListener listener_ = null;

    public VideoThumbnailExtractor(VideoThumbnailExtractorListener listener) {
        listener_ = listener;
    }

    public void putVideoFile(String videoFilePath) {
        new ThumbnailExtractorThread(videoFilePath).start();
    }
}
