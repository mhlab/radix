package com.radix.renaplayer.mediaSource;

import java.util.ArrayList;

public class MediaSourceContainer implements BaseMediaSource.MediaSourceListener {
    private static MediaSourceContainer instance_ = null;
    private ArrayList<BaseMediaSource> mediaSourceList_ = new ArrayList<>();
    private MediaSourceContainerListener listener_ = null;

    public interface MediaSourceContainerListener {
        void onCommandResultMediaSourceContainer(String url, BaseMediaSource.MediaCommand command, boolean ret, String commandAttr);
        void onRecvStreamMediaSourceContainer(String url, byte[] mediaData, int mediaSize, int codec, long presentationTime);
        void onCodecDataMediaSourceContainer(String url, byte[] videoCodecData, byte[] audioCodecData);
        void onUpdatedPlayTimeMediaSourceContainer(String url, long playTime);
    }

    private BaseMediaSource createMediaSource(String url) {
        if (url.indexOf("rtsp://") >= 0)
            return new RTSPMediaSource();

        return new FileMediaSource();
    }

    public static MediaSourceContainer getInstance() {
        if (instance_ == null)
            instance_ = new MediaSourceContainer();

        return instance_;
    }

    public void setListener(MediaSourceContainerListener listener) {
        listener_ = listener;
    }

    public void release() {
        instance_ = null;
    }

    public void openMediaSource(String url) {
        BaseMediaSource mediaSource = createMediaSource(url);
        mediaSource.setListener(this);
        mediaSourceList_.add(mediaSource);
        mediaSource.openMedia(url);
    }

    public void closeMediaSource(BaseMediaSource mediaSource) {
        if (mediaSource == null)
            return;

        mediaSource.setListener(null);
        mediaSource.closeMedia();
        mediaSourceList_.remove(mediaSource);
    }

    public void closeMediaSource(String url) {
        BaseMediaSource source = getMediaSource(url);
        if (source == null)
            return;
        closeMediaSource(source);
    }

    public BaseMediaSource getMediaSource(String url) {
        for (BaseMediaSource source : mediaSourceList_) {
            if (source.getUrl().equals(url))
                return source;
        }

        return null;
    }

    @Override
    public void onCommandResultMediaSource(String url, BaseMediaSource.MediaCommand command, boolean ret, String commandAttr) {
        if (listener_ != null)
            listener_.onCommandResultMediaSourceContainer(url, command, ret, commandAttr);
    }

    @Override
    public void onRecvStreamMediaSource(String url, byte[] mediaData, int mediaSize, int codec, long presentationTime) {
        if (listener_ != null)
            listener_.onRecvStreamMediaSourceContainer(url, mediaData, mediaSize, codec, presentationTime);
    }

    @Override
    public void onCodecDataMediaSource(String url, byte[] videoCodecData, byte[] audioCodecData) {
        if (listener_ != null)
            listener_.onCodecDataMediaSourceContainer(url, videoCodecData, audioCodecData);
    }

    @Override
    public void onUpdatedPlayTime(String url, long playTime) {
        if (listener_ != null)
            listener_.onUpdatedPlayTimeMediaSourceContainer(url, playTime);
    }
}
