package com.radix.renaplayer.mediaSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Radix.Media.MediaDef;
import Radix.Network.Rtsp.Live555Client;
import Radix.Network.Rtsp.RTSPClient;
import Radix.Network.Rtsp.RTSPDef.*;

import static Radix.Network.Rtsp.RTSPDef.RTSPCommand.COMMAND_DESCRIBE;
import static Radix.Network.Rtsp.RTSPDef.RTSPCommand.COMMAND_PLAY;

public class RTSPMediaSource extends BaseMediaSource implements RTSPClientListener {
    private Live555Client rtspClient_ = null;
    private ConnectionInfo connectionInfo_ = null;
    private MediaAttribution mediaAttribution_ = null;

    private class ConnectionInfo {
        public String ip_ = null;
        public int port_ = 554;
        public String abs_path_ = null;
        public String fullUrl_ = "";

        public ConnectionInfo(String url) {
            fullUrl_ = url;
            try {
                String protocol = "rtsp://";
                int portPos = url.indexOf(":", protocol.length());
                int absPathPos = url.indexOf("/", protocol.length());
                if (portPos > 0) {
                    port_ = Integer.parseInt(url.substring(portPos + 1, absPathPos));
                    ip_ = url.substring(protocol.length(), portPos);
                }
                else
                    ip_ = url.substring(protocol.length(), absPathPos);
                abs_path_ = url.substring(absPathPos + 1, url.length());
            }
            catch (Exception e) {
                port_ = 0;
                ip_ = null;
                abs_path_ = null;
            }
        }
    }

    private int rtspCodecToMediaCodec(int rtspCodec) {
        switch (rtspCodec) {
            case RTSPCodec.CODEC_H264:
                return MediaDef.MediaCodec.MEDIA_CODEC_H264;
            case RTSPCodec.CODEC_JPEG:
                return MediaDef.MediaCodec.MEDIA_CODEC_MJPEG;
        }

        return MediaDef.MediaCodec.MEDIA_CODEC_UNKNOWN;
    }

    private void createMediaAttributionFromJson(String json) {
        try {
            JSONArray jsonArray = new JSONArray(json);
            JSONObject jsonObject;
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);
                int codec = jsonObject.getInt("codec");
                codec = rtspCodecToMediaCodec(codec);
                if (MediaDef.MediaCodec.isVideoCodec(codec)) {
                    mediaAttribution_ = createMediaAttribution(null);
                    mediaAttribution_.video_codec_ = codec;
                    mediaAttribution_.video_width_ = jsonObject.getInt("video_width");
                    mediaAttribution_.video_height_ = jsonObject.getInt("video_height");
                }
            }
        }
        catch (JSONException e) {
        }
    }

    @Override
    public void openMedia(String url) {
        connectionInfo_ = new ConnectionInfo(url);
        if (connectionInfo_.ip_ == null || connectionInfo_.port_ == 0) {
            callCommandResultMediaSource(url, MediaCommand.MEDIACOMMAND_OPEN, false, "");
            return;
        }
        rtspClient_ = new Live555Client(this);
        rtspClient_.sendDescribe(0, url, null, null);
    }

    @Override
    public void closeMedia() {
        if (rtspClient_ != null) {
            rtspClient_.destroy();
            rtspClient_ = null;
        }
    }

    @Override
    public String getUrl() {
        if (connectionInfo_ == null)
            return "";
        return  connectionInfo_.fullUrl_;
    }

    @Override
    public String getShortUrl() {
        return getUrl();
    }

    @Override
    public void play() {
        if (rtspClient_ == null || connectionInfo_.ip_ == null || connectionInfo_.port_ == 0)
            return;

        rtspClient_.startLiveVideo(0, connectionInfo_.fullUrl_, null, null);
    }

    @Override
    public void onCommandResult_RTSPClient(int connectionId, RTSPCommand command, boolean ret, String param) {
        if (command == COMMAND_DESCRIBE) {
            if (ret)
                createMediaAttributionFromJson(param);
            String mediaJson = "";
            if (mediaAttribution_ != null)
                mediaJson = mediaAttribution_.toJson();
            callCommandResultMediaSource(connectionInfo_.fullUrl_, MediaCommand.MEDIACOMMAND_OPEN, ret, mediaJson);
        }
    }

    @Override
    public void onRecvMediaStream_RTSPClient(int connectionId, byte[] media, int size, int codec, long timestamp) {
        int mediaCodec = rtspCodecToMediaCodec(codec);
        if (mediaCodec == MediaDef.MediaCodec.MEDIA_CODEC_UNKNOWN)
            return;
        callRecvStreamMediaSource(connectionInfo_.fullUrl_, media, size, mediaCodec, timestamp);
    }
}
