package com.radix.renaplayer.mediaSource;

import org.json.JSONException;
import org.json.JSONObject;

import Radix.Media.MediaDef;

public class BaseMediaSource {
    public enum MediaCommand {
        MEDIACOMMAND_OPEN,
        MEDIACOMMAND_PLAY,
        MEDIACOMMAND_STOP,
        MEDIACOMMAND_PAUSE,
        MEDIACOMMAND_SEEK
    }

    public class MediaAttribution {
        public long duration_ = MediaDef.MediaCodec.MEDIA_CODEC_UNKNOWN;
        public int video_codec_ = 0;
        public int video_width_ = 0;
        public int video_height_ = 0;
        public double video_timebase_ = 0;
        public int video_bitrate_ = 0;
        public int audio_codec_ = MediaDef.MediaCodec.MEDIA_CODEC_UNKNOWN;
        public int audio_sample_rate_ = 0;
        public int audio_channelLayout_ = 0;
        public int audio_channels_ = 0;
        public int audio_profile_ = 0;
        public double audio_timebase_ = 0;

        public MediaAttribution() {
        }

        public MediaAttribution(String json) {
            try {
                JSONObject jsonObject = new JSONObject(json);
                duration_ = jsonObject.getInt("duration");
                video_codec_ = jsonObject.getInt("video_codec");
                video_width_ = jsonObject.getInt("video_width");
                video_height_ = jsonObject.getInt("video_height");
                int video_timebase_num = jsonObject.getInt("video_timebase_num");
                int video_timebase_den = jsonObject.getInt("video_timebase_den");
                video_bitrate_ = jsonObject.getInt("video_bitrate");
                video_timebase_ = video_timebase_num * 1.0f / video_timebase_den;
                audio_codec_ = jsonObject.getInt("audio_codec");
                audio_sample_rate_ = jsonObject.getInt("audio_sample_rate");
                audio_channelLayout_ = jsonObject.getInt("audio_channelLayout");
                audio_channels_ = jsonObject.getInt("audio_channels");
                audio_profile_ = jsonObject.getInt("audio_profile");
                int audio_timebase_num = jsonObject.getInt("audio_timebase_num");
                int audio_timebase_den = jsonObject.getInt("audio_timebase_den");
                audio_timebase_ = audio_timebase_num * 1.0f / audio_timebase_den;
            }
            catch (JSONException e) {
            }
        }

        public String toJson() {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("duration", duration_);
                jsonObject.put("video_codec", video_codec_);
                jsonObject.put("video_width", video_width_);
                jsonObject.put("video_height", video_height_);
                jsonObject.put("video_bitrate", video_bitrate_);
            }
            catch (JSONException e) {
            }

            return jsonObject.toString();
        }
    }

    public interface MediaSourceListener {
        void onCommandResultMediaSource(String url, MediaCommand command, boolean ret, String commandAttr);
        void onRecvStreamMediaSource(String url, byte[] mediaData, int mediaSize, int codec, long presentationTime);
        void onCodecDataMediaSource(String url, byte[] videoCodecData, byte[] audioCodecData);
        void onUpdatedPlayTime(String url, long playTime);
    }

    private MediaSourceListener listener_ = null;

    protected void callCommandResultMediaSource(String url, MediaCommand command, boolean ret, String commandAttr) {
        if (listener_ != null)
            listener_.onCommandResultMediaSource(url, command, ret, commandAttr);
    }

    protected void callRecvStreamMediaSource(String url, byte[] mediaData, int mediaSize, int codec, long presentationTime) {
        if (listener_ != null)
            listener_.onRecvStreamMediaSource(url, mediaData, mediaSize, codec, presentationTime);
    }

    protected void callCodecDataMediaSource(String url, byte[] videoCodecData, byte[] audioCodecData) {
        if (listener_ != null)
            listener_.onCodecDataMediaSource(url, videoCodecData, audioCodecData);
    }

    protected void callUpdatedPlayTime(String url, long playTime) {
        if (listener_ != null)
            listener_.onUpdatedPlayTime(url, playTime);
    }

    public void setListener(MediaSourceListener listener) {
        listener_ = listener;
    }

    public void openMedia(String url) {
    }

    public void closeMedia() {
    }

    public String getUrl() {
        return "";
    }

    public String getShortUrl() {
        return "";
    }

    public void play() {
    }

    public void stop() {
    }

    public void pause() {
    }

    public void seek(long seekSeconds) {
    }

    public MediaAttribution createMediaAttribution(String json) {
        if (json != null)
            return new MediaAttribution(json);
        else
            return new MediaAttribution();
    }
}
