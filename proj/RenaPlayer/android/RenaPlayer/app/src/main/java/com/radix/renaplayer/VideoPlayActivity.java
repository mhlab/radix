package com.radix.renaplayer;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.SurfaceTexture;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.radix.renaplayer.db.DatabaseHelper;
import com.radix.renaplayer.mediaSource.BaseMediaSource;
import com.radix.renaplayer.mediaSource.MediaSourceContainer;

import Radix.Log.Logger;
import Radix.Media.Decoder.MediaDecodeThread;
import Radix.Reader.SubtitleReader;
import Radix.Ui.Display.YuvTextureView;
import Radix.Ui.SeekBar.RangeSeekBar;

import static Radix.Media.MediaDef.MediaCodec.MEDIA_CODEC_UNKNOWN;

public class VideoPlayActivity extends Activity implements View.OnClickListener, MediaSourceContainer.MediaSourceContainerListener,
        TextureView.SurfaceTextureListener, MediaDecodeThread.MediaDecodeThreadListener, SubtitleReader.SubtitleReaderListener {
    public static final String INTENT_KEY_URL = "KEY_URL";
    public static final String INTENT_KEY_MEDIA_ATTR = "MEDIA_ATTR";
    public static final String INTENT_KEY_VIDEO_CODEC_DATA = "VIDEO_CODEC_DATA";
    public static final String INTENT_KEY_AUDIO_CODEC_DATA = "AUDIO_CODEC_DATA";

    private BaseMediaSource.MediaAttribution mediaAttribution_ = null;
    private BaseMediaSource mediaSource_ = null;
    private ImageView imgLogo_;
    private ImageView imgPlay_;
    private ImageView imgPause_;
    private ImageView imgStop_;
    private SurfaceTexture surfaceTexture_ = null;
    private TextView tvCurrentPlayTime_;
    private TextView tvSubtitle_;
    private RangeSeekBar seekBarMedia_;
    private RelativeLayout layoutVideoTopUi_;
    private LinearLayout layoutProgress_;
    private RelativeLayout layoutPlayMenu_;
    private YuvTextureView textureView_;
    private CheckedTextView ctUseHwDecode_;
    private AudioTrack audioTrack_ = null;
    private Button btnPlayRepeatOpt_;
    private MediaDecodeThread videoDecodeThread_ = null;
    private MediaDecodeThread audioDecodeThread_ = null;
    private byte[] videoCodecData_ = null;
    private byte[] audioCodecData_ = null;
    private long playTime_ = 0;
    private SubtitleReader subtitleReader_ = null;
    private boolean isCreated_ = true;
    private boolean isSeekbarTracking_ = false;
    private ActivityInternalManager activityManager_ = new ActivityInternalManager();
    private DatabaseHelper.StoredPlayInfo storedPlayInfo_ = null;

    private void initDurationView() {
        TextView tvDuration = findViewById(R.id.tvDuration);
        int hours = (int)(mediaAttribution_.duration_ / 3600);
        int miniutes = (int)((mediaAttribution_.duration_ % 3600) / 60);
        int seconds = (int)(mediaAttribution_.duration_ % 60);
        if (hours > 0)
            tvDuration.setText(String.format("%02d:%02d:%02d", hours, miniutes, seconds));
        else
            tvDuration.setText(String.format("%02d:%02d", miniutes, seconds));
    }

    private void updatePlayTimeView(double playTime) {
        int hours = (int)(playTime / 3600);
        int miniutes = (int)((playTime % 3600) / 60);
        int seconds = (int)(playTime % 60);
        if (hours > 0)
            tvCurrentPlayTime_.setText(String.format("%02d:%02d:%02d", hours, miniutes, seconds));
        else
            tvCurrentPlayTime_.setText(String.format("%02d:%02d", miniutes, seconds));
        seekBarMedia_.setProgress((int)playTime);
        playTime_ = (long)playTime;
    }

    private void updatePlayTime(final double playTime) {
        if (subtitleReader_ != null)
            subtitleReader_.progress(playTime);
        if (isSeekbarTracking_)
            return;
        long rangeMax = seekBarMedia_.getRangeMax();
        if (rangeMax > -1 && playTime >= rangeMax) {
            seekFrame(seekBarMedia_.getRangeMin());
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isPlayMode())
                    return;
                updatePlayTimeView(playTime);
            }
        });
    }

    private void stopView() {
        imgPause_.setVisibility(View.GONE);
        imgPlay_.setVisibility(View.VISIBLE);
        imgLogo_.setVisibility(View.VISIBLE);
        tvSubtitle_.setVisibility(View.GONE);
        seekBarMedia_.setProgress(0);
        tvCurrentPlayTime_.setText("00:00");
    }

    private void pauseView() {
        layoutVideoTopUi_.setVisibility(View.VISIBLE);
        layoutProgress_.setVisibility(View.VISIBLE);
        layoutPlayMenu_.setVisibility(View.VISIBLE);
        imgPause_.setVisibility(View.GONE);
        imgPlay_.setVisibility(View.VISIBLE);
    }

    private void playView() {
        imgPlay_.setVisibility(View.GONE);
        imgPause_.setVisibility(View.VISIBLE);
        layoutVideoTopUi_.setVisibility(View.GONE);
        layoutProgress_.setVisibility(View.GONE);
        layoutPlayMenu_.setVisibility(View.GONE);
        if (subtitleReader_ != null) {
            tvSubtitle_.setVisibility(View.VISIBLE);
            tvSubtitle_.setText("");
        }
        activityManager_.resizeTextureView();
    }

    private boolean isPlayMode() {
        return (imgPlay_.getVisibility() == View.GONE);
    }

    private void initDecoderThread() {
        if (mediaAttribution_ == null)
            return;
        if (mediaAttribution_.video_codec_ != MEDIA_CODEC_UNKNOWN && (videoDecodeThread_ == null)) {
            videoDecodeThread_ = new MediaDecodeThread();
            videoDecodeThread_.setListener(this);
            videoDecodeThread_.setName("VideoDecodeThread");
            videoDecodeThread_.setPriority(Thread.MAX_PRIORITY);
            videoDecodeThread_.start();
        }
        if (mediaAttribution_.audio_codec_ != MEDIA_CODEC_UNKNOWN && (audioDecodeThread_ == null)) {
            audioDecodeThread_ = new MediaDecodeThread();
            audioDecodeThread_.setListener(this);
            audioDecodeThread_.setName("AudioDecodeThread");
            audioDecodeThread_.setPriority(Thread.MAX_PRIORITY);
            audioDecodeThread_.start();
        }
        if (videoDecodeThread_ != null) {
            if (ctUseHwDecode_.isChecked())
                textureView_.stopRender();
            else
                textureView_.startRender();
            videoDecodeThread_.useHwDecode(ctUseHwDecode_.isChecked());
            if (!videoDecodeThread_.isOpenCodec())
                videoDecodeThread_.openVideoCodec(mediaAttribution_.video_codec_, videoCodecData_, mediaAttribution_.video_width_, mediaAttribution_.video_height_, new Surface(surfaceTexture_));
        }
        if (audioDecodeThread_ != null) {
            audioDecodeThread_.useHwDecode(ctUseHwDecode_.isChecked());
            if (!audioDecodeThread_.isOpenCodec())
                audioDecodeThread_.openAudioCodec(mediaAttribution_.audio_codec_, audioCodecData_, mediaAttribution_.audio_sample_rate_, mediaAttribution_.audio_channels_, mediaAttribution_.audio_profile_, mediaAttribution_.audio_channelLayout_);
        }
    }

    private void closeDecoder() {
        if (videoDecodeThread_ != null)
            videoDecodeThread_.closeCodec();
        if (audioDecodeThread_ != null)
            audioDecodeThread_.closeCodec();
    }

    private void initAudioTrack() {
        if (mediaAttribution_ == null || mediaAttribution_.audio_codec_ == MEDIA_CODEC_UNKNOWN)
            return;

        Logger.writeDebugMessageInDebugMode("[RenaPl]", "VideoPlayActivity initAudioTrack Start: " + mediaAttribution_.toString());
        if (audioTrack_ == null) {
            int minBufSize;
            int channelConfig = AudioFormat.CHANNEL_OUT_STEREO;
            switch (mediaAttribution_.audio_channels_) {
                case 1:
                    channelConfig = AudioFormat.CHANNEL_OUT_MONO;
                    break;
                case 2:
                    channelConfig = AudioFormat.CHANNEL_OUT_STEREO;
                    break;
                case 6:
                    channelConfig = AudioFormat.CHANNEL_OUT_5POINT1;
            }
            minBufSize = AudioTrack.getMinBufferSize(mediaAttribution_.audio_sample_rate_, channelConfig, AudioFormat.ENCODING_PCM_16BIT);
            audioTrack_ = new AudioTrack(AudioManager.STREAM_MUSIC, mediaAttribution_.audio_sample_rate_, channelConfig, AudioFormat.ENCODING_PCM_16BIT, minBufSize, AudioTrack.MODE_STREAM);
        }
        if (audioTrack_.getPlayState() != AudioTrack.PLAYSTATE_PLAYING)
            audioTrack_.play();
    }

    private void initSubtitleReader(String url) {
        int pos = url.lastIndexOf(".");
        String smiFile = url.replace(url.substring(pos + 1), "smi");
        subtitleReader_ = new SubtitleReader(this);
        if (!subtitleReader_.open(SubtitleReader.SubTitleFileType.SUBTITLE_FILE_SMI, smiFile)) {
            subtitleReader_.close();
            subtitleReader_ = null;
        }
    }

    private String contentUriToFileUri(Uri contentUri) {
        Context context = this;
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null)
            return null;

        cursor.moveToNext();
        String ret = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA));
        cursor.close();

        return ret;
    }

    private void playAfterOnCreate() {
        if (surfaceTexture_ != null && isCreated_) {
            isCreated_ = false;
            initDecoderThread();
            initAudioTrack();
            if (storedPlayInfo_ != null && storedPlayInfo_.playPos_ > 0)
                seekFrame((int)storedPlayInfo_.playPos_);
            else
                onClick(imgPlay_);
        }
    }

    private void seekFrame(long seekTime) {
        mediaSource_.seek(seekTime);
        if (videoDecodeThread_ != null)
            videoDecodeThread_.clearFrames();
        if (audioDecodeThread_ != null)
            audioDecodeThread_.clearFrames();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.writeDebugMessageInDebugMode("[RenaPl]", "VideoPlayActivity onCreate Start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoplay);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        MediaSourceContainer.getInstance().setListener(this);

        ImageView imgBack = findViewById(R.id.imgBackToMain);
        imgBack.setOnClickListener(this);
        ImageView imgRotation = findViewById(R.id.imgRotation);
        imgRotation.setOnClickListener(this);
        ImageView imgScreenRatio = findViewById(R.id.imgScreenRatio);
        imgScreenRatio.setOnClickListener(this);
        seekBarMedia_ = findViewById(R.id.prgBarMedia);
        imgPlay_ = findViewById(R.id.imgPlay);
        imgPlay_.setOnClickListener(this);
        imgPlay_.setEnabled(false);
        imgPause_ = findViewById(R.id.imgPause);
        imgPause_.setOnClickListener(this);
        imgPause_.setVisibility(View.GONE);
        imgStop_ = findViewById(R.id.imgStop);
        imgStop_.setOnClickListener(this);
        imgLogo_ = findViewById(R.id.imgLogo);
        imgLogo_.setVisibility(View.GONE);
        ImageView imgPrev = findViewById(R.id.imgPrev);
        imgPrev.setOnClickListener(this);
        ImageView imgNext = findViewById(R.id.imgNext);
        imgNext.setOnClickListener(this);
        tvCurrentPlayTime_ = findViewById(R.id.tvCurrentPlayTime);
        layoutVideoTopUi_ = findViewById(R.id.layoutTopUI);
        layoutProgress_ = findViewById(R.id.layoutProgress);
        layoutPlayMenu_ = findViewById(R.id.layoutPlayMenu);
        tvSubtitle_ = findViewById(R.id.tvSubtitle);
        tvSubtitle_.setVisibility(View.GONE);
        FrameLayout layoutWholeFrame = findViewById(R.id.layoutWholeFrame);
        layoutWholeFrame.setOnClickListener(this);
        textureView_ = findViewById(R.id.textureView);
        textureView_.setGLTextureViewListener(this);
        ctUseHwDecode_ = findViewById(R.id.checkedUseHwDecode);
        ctUseHwDecode_.setOnClickListener(this);
        ctUseHwDecode_.setChecked(true);
        btnPlayRepeatOpt_ = findViewById(R.id.btnPlayRepeatOpt);
        btnPlayRepeatOpt_.setOnClickListener(this);
        btnPlayRepeatOpt_.setText("-");

        Uri uri = getIntent().getData();
        if (uri == null) { // Created from MainActivity
            final String mediaAttr = getIntent().getStringExtra(INTENT_KEY_MEDIA_ATTR);
            final String sourceUrl = getIntent().getStringExtra(INTENT_KEY_URL);
            videoCodecData_ = getIntent().getByteArrayExtra(INTENT_KEY_VIDEO_CODEC_DATA);
            audioCodecData_ = getIntent().getByteArrayExtra(INTENT_KEY_AUDIO_CODEC_DATA);
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    onCommandResultMediaSourceContainer(sourceUrl, BaseMediaSource.MediaCommand.MEDIACOMMAND_OPEN, true, mediaAttr);
                }
            });
        }
        else { // Called from System
            String mediaFilePath = uri.toString();
            if (uri.getScheme().equals("content"))
                mediaFilePath = contentUriToFileUri(uri);
            MediaSourceContainer.getInstance().openMediaSource(mediaFilePath);
        }

        seekBarMedia_.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updatePlayTimeView(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isSeekbarTracking_ = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isSeekbarTracking_ = false;
                int seekSeconds = seekBar.getProgress();
                seekFrame(seekSeconds);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.writeDebugMessageInDebugMode("[RenaPl]", "VideoPlayActivity OnDestroy Start");
        if (storedPlayInfo_ != null) {
            storedPlayInfo_.playPos_ = playTime_;
            storedPlayInfo_.orientation_ = getRequestedOrientation();
        }
        if (mediaSource_ != null) {
            ((RenaplApplication) getApplication()).savePlayInfo(storedPlayInfo_);
            MediaSourceContainer.getInstance().closeMediaSource(mediaSource_);
        }
        if (videoDecodeThread_ != null)
            videoDecodeThread_.terminate();
        if (audioDecodeThread_ != null)
            audioDecodeThread_.terminate();
        try {
            if (videoDecodeThread_ != null)
                videoDecodeThread_.join();
            if (audioDecodeThread_ != null)
                audioDecodeThread_.join();
        }
        catch (InterruptedException e) {
            Logger.writeDebugMessageInDebugMode("[RenaPl]", String.format("VideoPlayActivity OnDestroy Exception: %s", e.getMessage()));
        }
        if (audioTrack_ != null) {
            audioTrack_.stop();
            audioTrack_.release();
        }
        if (subtitleReader_ != null) {
            subtitleReader_.close();
            subtitleReader_ = null;
        }
        Logger.writeDebugMessageInDebugMode("[RenaPl]", "VideoPlayActivity OnDestroy Finish");
    }

    @Override
    public void onResume() {
        if (surfaceTexture_ != null) {
            textureView_.setSurfaceTexture(surfaceTexture_);
        }
        super.onResume();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                                  View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        final FrameLayout wholeFrameLayout = findViewById(R.id.layoutWholeFrame);
        ViewTreeObserver viewTreeObserver = wholeFrameLayout.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                activityManager_.resizeTextureView();
                wholeFrameLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgPrev:
                if (playTime_ > 10)
                    seekFrame(playTime_ - 10);
                break;
            case R.id.imgNext:
                if (playTime_ < (seekBarMedia_.getMax() - 10))
                    seekFrame(playTime_ + 10);
                break;
            case R.id.imgBackToMain:
                finish();
                break;
            case R.id.imgPlay:
                initDecoderThread();
                initAudioTrack();
                mediaSource_.play();
                break;
            case R.id.imgStop:
                mediaSource_.stop();
                closeDecoder();
                if (subtitleReader_ != null)
                    subtitleReader_.reset();
                stopView();
                break;
            case R.id.imgPause:
                mediaSource_.pause();
                pauseView();
                break;
            case R.id.layoutWholeFrame:
                if (isPlayMode()) {
                    if (layoutVideoTopUi_.getVisibility() == View.GONE) {
                        layoutVideoTopUi_.setVisibility(View.VISIBLE);
                        layoutProgress_.setVisibility(View.VISIBLE);
                        layoutPlayMenu_.setVisibility(View.VISIBLE);
                    }
                    else {
                        layoutVideoTopUi_.setVisibility(View.GONE);
                        layoutProgress_.setVisibility(View.GONE);
                        layoutPlayMenu_.setVisibility(View.GONE);
                    }
                }
                break;
            case R.id.imgRotation:
                activityManager_.rotate();
                break;
            case R.id.imgScreenRatio:
                if (storedPlayInfo_.screenResolution_ == 0)
                    storedPlayInfo_.screenResolution_ = 1;
                else
                    storedPlayInfo_.screenResolution_ = 0;
                activityManager_.resizeTextureView();
                break;
            case R.id.checkedUseHwDecode:
                ctUseHwDecode_.setChecked(!ctUseHwDecode_.isChecked());
                if (videoDecodeThread_ != null)
                    videoDecodeThread_.useHwDecode(ctUseHwDecode_.isChecked());
                if (audioDecodeThread_ != null)
                    audioDecodeThread_.useHwDecode(ctUseHwDecode_.isChecked());
                break;
            case R.id.btnPlayRepeatOpt:
                if (seekBarMedia_.getRangeMin() == -1) {
                    seekBarMedia_.setRangeMin((int)playTime_);
                    btnPlayRepeatOpt_.setText("A-");
                }
                else if (seekBarMedia_.getRangeMax() == -1) {
                    seekBarMedia_.setRangeMax((int)playTime_);
                    btnPlayRepeatOpt_.setText("A-B");
                    mediaSource_.seek(seekBarMedia_.getRangeMin());
                }
                else {
                    seekBarMedia_.setRangeMin(-1);
                    seekBarMedia_.setRangeMax(-1);
                    btnPlayRepeatOpt_.setText("-");
                }
                break;
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        surfaceTexture_ = surface;
        imgPlay_.setEnabled(true);

        if (isCreated_) {
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    playAfterOnCreate();
                }
            });
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        if (isPlayMode()) {
            onClick(imgPause_);
            pauseView();
        }
        if (isFinishing())
            return true;

        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        surfaceTexture_ = surface;
        imgPlay_.setEnabled(true);
    }

    @Override
    public void onCommandResultMediaSourceContainer(final String url, BaseMediaSource.MediaCommand command, boolean ret, String commandAttr) {
        Logger.writeDebugMessageInDebugMode("VideoPlayActivity-CommandResult", String.format("url: %s, cmd: %s, ret: %s", url, command.toString(), Boolean.toString(ret)));
        if (command == BaseMediaSource.MediaCommand.MEDIACOMMAND_OPEN) {
            if (!ret)
                return;
            mediaSource_ = MediaSourceContainer.getInstance().getMediaSource(url);
            if (mediaSource_ == null)
                return;
            mediaAttribution_ = mediaSource_.createMediaAttribution(commandAttr);
            storedPlayInfo_ = ((RenaplApplication)getApplication()).getPlayInfo(url);
            setRequestedOrientation(storedPlayInfo_.orientation_);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView tvMediaFileName = findViewById(R.id.tvMediaFileName);
                    tvMediaFileName.setText(mediaSource_.getShortUrl());
                    seekBarMedia_.setMax((int)mediaAttribution_.duration_);
                    initDurationView();
                    initSubtitleReader(url);
                    playAfterOnCreate();
                }
            });
        }
        else if (command == BaseMediaSource.MediaCommand.MEDIACOMMAND_SEEK) {
            if (videoDecodeThread_ != null) {
//                videoDecodeThread_.needKeyFrame();  // Comment because of repeat playing option.
                videoDecodeThread_.clearFrames();
            }
            if (audioDecodeThread_ != null) {
                audioDecodeThread_.clearFrames();
            }
            if (subtitleReader_ != null)
                subtitleReader_.reset();
            mediaSource_.play();
        }
        else if (command == BaseMediaSource.MediaCommand.MEDIACOMMAND_PLAY) {
            if (ret) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        playView();
                    }
                });
            }
        }
    }

    @Override
    public void onRecvStreamMediaSourceContainer(String url, byte[] mediaData, int mediaSize, int codec, long presentationTime) {
        if (mediaSize == 0)
            return;
        if (videoDecodeThread_ != null && videoDecodeThread_.isVideoCodec(codec)) {
            videoDecodeThread_.sendMediaFrame(mediaData, mediaSize, codec, presentationTime);
        }
        else if (audioDecodeThread_ != null && audioDecodeThread_.isAudioCodec(codec)) {
            audioDecodeThread_.sendMediaFrame(mediaData, mediaSize, codec, presentationTime);
        }
    }

    @Override
    public void onCodecDataMediaSourceContainer(String url, byte[] videoCodecData, byte[] audioCodecData) {
        if (videoCodecData != null)
            videoCodecData_ = videoCodecData.clone();
        if (audioCodecData != null)
            audioCodecData_ = audioCodecData.clone();
    }

    @Override
    public void onUpdatedPlayTimeMediaSourceContainer(String url, final long playTime) {
    }

    @Override
    public void onDecodedFrame(boolean isVideo, byte[] decodeStream, long presentationTime) {
        if (isFinishing())
            return;

        if (isVideo) {
            if (imgLogo_.getVisibility() == View.VISIBLE && isPlayMode()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // After updated video, hides logo imageview.
                        try {
                            Thread.sleep(100);
                        }
                        catch (InterruptedException e) {
                        }
                        imgLogo_.setVisibility(View.GONE);
                    }
                });
            }
        }
        else {
            if (audioTrack_ == null || decodeStream == null)
                return;
            audioTrack_.write(decodeStream, 0, decodeStream.length);
        }
        if (!isVideo || (isVideo && mediaAttribution_.audio_codec_ == MEDIA_CODEC_UNKNOWN)) {
            double curPlayTime = 0;
            if (!isVideo)
                curPlayTime = presentationTime * mediaAttribution_.audio_timebase_;
            else
                curPlayTime = presentationTime * mediaAttribution_.video_timebase_;
            updatePlayTime(curPlayTime);
        }
    }

    @Override
    public void onDecodedFrame(byte[] ybuf, byte[] ubuf, byte[] vbuf, int yuvWidth, int yuvHeight, long presentationTime) {
        if (imgLogo_.getVisibility() == View.VISIBLE && isPlayMode()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // After updated video, hides logo imageview.
                    try {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException e) {
                    }
                    imgLogo_.setVisibility(View.GONE);
                }
            });
        }
        textureView_.setYuv(ybuf, ubuf, vbuf, mediaAttribution_.video_width_, mediaAttribution_.video_height_);
        if (mediaAttribution_.audio_codec_ == MEDIA_CODEC_UNKNOWN) {
            double curPlayTime = presentationTime * mediaAttribution_.video_timebase_;
            updatePlayTime(curPlayTime);
        }
    }

    @Override
    public void onChangedAudioOutputFormat(MediaFormat format) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (audioTrack_ != null)
                audioTrack_.setPlaybackRate(format.getInteger(MediaFormat.KEY_SAMPLE_RATE));
        }
    }

    @Override
    public void onRecvSubtitle(final String subtitle) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String newSubTitle = subtitle.replace("<br>", "\n");
                newSubTitle = newSubTitle.replaceAll("<(.*?)\\>", "");
                tvSubtitle_.setText(newSubTitle);
            }
        });
    }

    @Override
    public void onChangeDecodeMode(boolean isVideoCodec, boolean useHwDecode) {
        if (isFinishing())
            return;
        if (isVideoCodec && !useHwDecode) // if current decode mode is sw decode, stop rendering on textureview because hw decode uses surface
            textureView_.stopRender();
    }

    @Override
    public void onChangedDecodeMode(boolean isVideoCodec, boolean useHwDecode) {
        if (isFinishing())
            return;
        if (isVideoCodec && !useHwDecode)
            textureView_.startRender();
    }

    private class ActivityInternalManager {
        private int direction[] = {ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,
            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE, ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT};

        public void rotate() {
            int orientation = getRequestedOrientation();
            for (int i = 0; i < direction.length; i++) {
                if (direction[i] == orientation) {
                    int newOrientation;
                    if (i + 1 >= direction.length)
                        newOrientation = direction[0];
                    else
                        newOrientation = direction[i + 1];
                    setRequestedOrientation(newOrientation);
                    break;
                }
            }
        }

        public void resizeTextureView() {
            if (mediaAttribution_ == null || mediaAttribution_.video_width_ == 0 || mediaAttribution_.video_height_ == 0)
                return;
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)textureView_.getLayoutParams();
            if (storedPlayInfo_.screenResolution_ == 1) {
                layoutParams.setMargins(0, 0, 0, 0);
                textureView_.setLayoutParams(layoutParams);
            }
            else {
                int windowWidth = getWindow().getDecorView().getWidth();
                int windowHeight = getWindow().getDecorView().getHeight();
                double videoRatio = mediaAttribution_.video_width_ * 1.0f / mediaAttribution_.video_height_;
                double widthRatio = windowWidth * 1.0f / mediaAttribution_.video_width_;
                double heightRatio = windowHeight * 1.0f / mediaAttribution_.video_height_;
                if (widthRatio <= heightRatio) {
                    int newHeight = (int) (windowWidth * (1 / videoRatio));
                    int heightMargin = (windowHeight - newHeight) / 2;
                    layoutParams.setMargins(0, heightMargin, 0, heightMargin);
                    textureView_.setLayoutParams(layoutParams);
                } else {
                    int newWidth = (int) (windowHeight * videoRatio);
                    int widthMargin = (windowWidth - newWidth) / 2;
                    layoutParams.setMargins(widthMargin, 0, widthMargin, 0);
                    textureView_.setLayoutParams(layoutParams);
                }
            }
        }
    }
}
