package com.radix.renaplayer;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.radix.renaplayer.mediaSource.BaseMediaSource;
import com.radix.renaplayer.mediaSource.MediaSourceContainer;
import com.radix.renaplayer.utils.VideoThumbnailExtractor;

import java.io.File;
import java.util.ArrayList;

import Radix.Media.Demuxer.SyncDemuxer;
import Radix.Ui.FileExplorer.FileExploreSource;
import Radix.Ui.FileExplorer.FileExploreView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FileExploreView.FileExploreViewListener,
        MediaSourceContainer.MediaSourceContainerListener, VideoThumbnailExtractor.VideoThumbnailExtractorListener {
    private final static int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 300;
    private final static int REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = 301;
    private FileExploreView exploreView_ = null;
    private byte[] videoCodecData_ = null;
    private byte[] audioCodecData_ = null;
    private SyncDemuxer demuxer_ = new SyncDemuxer(null);
    private VideoThumbnailExtractor thumbnailExtractor_ = null;

    private void checkPermission(String permission, int reqCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) == PackageManager.PERMISSION_GRANTED)
            return;
        if (!ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission))
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, reqCode);
    }

    private void exploreInitDirectory() {
        File exploreFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if (exploreFile == null || !exploreFile.exists())
            exploreFile = Environment.getExternalStorageDirectory();
        if (exploreFile == null ||  !exploreFile.exists())
            exploreFile = new File("/");

        exploreView_.setDirectory(exploreFile.getAbsolutePath());
    }

    private void showNetworkStreamDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.msg_input_network_address);
        final EditText editText = new EditText(this);
        editText.setText(((RenaplApplication)getApplication()).getNetworkStreamUrl());
        builder.setView(editText);
        builder.setPositiveButton(R.string.caption_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String url = editText.getText().toString();
                ((RenaplApplication)getApplication()).updateNetworkStreamUrl(url);
                MediaSourceContainer.getInstance().openMediaSource(url);
                dialog.cancel();
            }
        });
        builder.setNegativeButton(R.string.caption_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        exploreView_ = (FileExploreView)findViewById(R.id.fileExploreView);
        exploreView_.setListener(this);
        exploreView_.setFolderResourceId(R.drawable.folder_48);
        exploreView_.setFileResourceId(R.drawable.media_file_48);
        thumbnailExtractor_ = new VideoThumbnailExtractor(this);
        ImageView imgClose = (ImageView)findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);
        ImageView imgMain = (ImageView)findViewById(R.id.imgMenu);
        imgMain.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        videoCodecData_ = null;
        audioCodecData_ = null;

        checkPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE, REQUEST_PERMISSION_READ_EXTERNAL_STORAGE);
        if (BuildConfig.DEBUG)
            checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE);
        exploreInitDirectory();
    }

    @Override
    protected void onDestroy() {
        MediaSourceContainer.getInstance().release();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        MediaSourceContainer.getInstance().setListener(this);
        super.onStart();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                exploreInitDirectory();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_main_NetworkStream:
                showNetworkStreamDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgClose:
                finish();
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onSelected(File selected) {
        if (selected == null)
            return;

        MediaSourceContainer.getInstance().openMediaSource(selected.getAbsolutePath());
    }

    @Override
    public void onUpdatedFileList(ArrayList<FileExploreSource.FileItemInfo> fileItemInfoList) {
        if (fileItemInfoList.size() == 0)
            return;

        FileExploreSource.FileItemInfo fileItemInfo;
        for (int i = fileItemInfoList.size() - 1; i >= 0; i--) {
            fileItemInfo = fileItemInfoList.get(i);
            if (fileItemInfo.file_.isDirectory()) {
                continue;
            }
            else {
                if (!demuxer_.isSupportFile(fileItemInfo.file_.getAbsolutePath())) {
                    fileItemInfoList.remove(i);
                }
                else
                    thumbnailExtractor_.putVideoFile(fileItemInfo.file_.getAbsolutePath());
            }
        }
    }

    @Override
    public void onCommandResultMediaSourceContainer(String url, BaseMediaSource.MediaCommand command, boolean ret, String commandAttr) {
        if (command == BaseMediaSource.MediaCommand.MEDIACOMMAND_OPEN) {
            if (!ret) {
                MediaSourceContainer.getInstance().closeMediaSource(url);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, R.string.open_fail, Toast.LENGTH_SHORT).show();
                    }
                });
            }
            else {
                final String urlForIntent = url;
                final String mediaAttrForIntent = commandAttr;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, VideoPlayActivity.class);
                        intent.putExtra(VideoPlayActivity.INTENT_KEY_URL, urlForIntent);
                        intent.putExtra(VideoPlayActivity.INTENT_KEY_MEDIA_ATTR, mediaAttrForIntent);
                        if (videoCodecData_ != null)
                            intent.putExtra(VideoPlayActivity.INTENT_KEY_VIDEO_CODEC_DATA, videoCodecData_);
                        if (audioCodecData_ != null)
                            intent.putExtra(VideoPlayActivity.INTENT_KEY_AUDIO_CODEC_DATA, audioCodecData_);
                        startActivity(intent);
                    }
                });
            }
        }
    }

    @Override
    public void onRecvStreamMediaSourceContainer(String url, byte[] mediaData, int mediaSize, int codec, long presentationTime) {
    }

    @Override
    public void onCodecDataMediaSourceContainer(String url, byte[] videoCodecData, byte[] audioCodecData) {
        if (videoCodecData != null)
            videoCodecData_ = videoCodecData.clone();
        if (audioCodecData != null)
            audioCodecData_ = audioCodecData.clone();
    }

    @Override
    public void onUpdatedPlayTimeMediaSourceContainer(String url, long playTime) {
    }

    @Override
    public void onExtractVideoThumbnail(final String videoFilePath, final Bitmap thumbnail) {
        if (thumbnail == null)
            return;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                exploreView_.setThumbnail(videoFilePath, thumbnail);
            }
        });
    }
}
