﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Renapl.App.MediaSource;
using Radix.Ui.Forms;
using Radix.Media;

namespace Renapl {
    public partial class formMain : BaseForm, MediaSourceList.MediaSourceListListener {
        private MediaSourceList mediaSourceList_ = new MediaSourceList();
        private String mainUrl_ = null;

        private void formMain_Load(object sender, EventArgs e) {
            addControlForMovableForm(pnlCaptionBar);
            addControlForRestoreForm(pnlCaptionBar);
            mediaSourceList_.setListener(this);
        }

        private void formMain_FormClosed(object sender, FormClosedEventArgs e) {
            mediaSourceList_.Dispose();
        }

        private void formMain_Resize(object sender, EventArgs e) {
            addResizeAreaPoint(Width - 10, Height - 10);
        }

        private void btnClose_Click(object sender, EventArgs e) {
            Close();
        }

        private void btnRestoreWindow_Click(object sender, EventArgs e) {
            internalDoubleClickForm();
        }

        private void btnMinimizeWindow_Click(object sender, EventArgs e) {
            WindowState = FormWindowState.Minimized;
        }

        private void videoView_DragEnter(object sender, DragEventArgs e) {
            e.Effect = DragDropEffects.None;
            if (!e.Data.GetDataPresent(DataFormats.FileDrop))
                return;

            string[] dragFiles = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (dragFiles == null)
                return;

            foreach (string dragFile in dragFiles) {
                if (mediaSourceList_.isSupportFile(dragFile)) {
                    e.Effect = DragDropEffects.Link;
                    break;
                }
            }
        }

        private void videoView_DragDrop(object sender, DragEventArgs e) {
            string[] dragFiles = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (dragFiles == null)
                return;

            if (mainUrl_ != null)
                mediaSourceList_.close(mainUrl_);
            mainUrl_ = dragFiles[0];
            mediaSourceList_.openFile(dragFiles[0]);
        }

        private void pnlBottom_Resize(object sender, EventArgs e) {
            int margin = (pnlBottom.Width - pnlPlayMenu.Width) / 2;
            pnlPlayMenu.Left = margin;
        }

        private void btnPlayPause_Click(object sender, EventArgs e) {
            BaseMediaSource mediaSource = mediaSourceList_.getMediaSource(mainUrl_);
            if (mediaSource == null)
                return;

            if (!mediaSource.isPlaying())
                mediaSource.play();
            else
                mediaSource.pause();
        }

        private void btnStop_Click(object sender, EventArgs e) {
            mediaSourceList_.stop(mainUrl_);
        }

        private void btnNextTenSeconds_Click(object sender, EventArgs e) {
            if (progMediaTime.Value + 10 <= progMediaTime.Maximum)
                mediaSourceList_.seek(mainUrl_, progMediaTime.Value + 10);
            else
                mediaSourceList_.seek(mainUrl_, progMediaTime.Maximum);
        }

        private void btnPrevTenSeconds_Click(object sender, EventArgs e) {
            if (progMediaTime.Value - 10 >= 0)
                mediaSourceList_.seek(mainUrl_, progMediaTime.Value - 10);
            else
                mediaSourceList_.seek(mainUrl_, 0);
        }

        private void progMediaTime_MouseUp(object sender, MouseEventArgs e) {
            BaseMediaSource mediaSource = mediaSourceList_.getMediaSource(mainUrl_);
            if (mediaSource == null)
                return;

            int seekSecond = (e.X * progMediaTime.Maximum) / progMediaTime.Width;
            mediaSource.seek(seekSecond);
        }

        private String secondsToFormattedTime(uint seconds) {
            TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);
            return String.Format("{0:D2}:{1:D2}:{2:D2}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
        }

        public formMain() {
            InitializeComponent();
        }

        // Interface of MediaSourceList
        public void onCommandResultMediaSourceList(String url, BaseMediaSource.MediaSourceCommand command, bool ret, object param) {
            if (command == BaseMediaSource.MediaSourceCommand.MEDIASOURCE_COMMAND_OPEN) {
                if (ret) {
                    MediaAttribution mediaAttr = mediaSourceList_.mediaAttribution(url);
                    mediaPlayer.initAudio(mediaAttr.audio_sample_rate, mediaAttr.audio_channels);
                    mediaPlayer.openSubtitleReader(url);
                    mediaSourceList_.play(url);
                    BeginInvoke(new Action(delegate() {
                        progMediaTime.Maximum = (int)mediaAttr.duration;
                        lblFinishTime.Text = secondsToFormattedTime(mediaAttr.duration);
                        lblCurrentTime.Text = secondsToFormattedTime(0);
                    }));
                }
                else {
                    BeginInvoke(new Action(delegate() {
                        MessageBox.Show(String.Format("Fail to open %s", url));
                    }));
                }
            }
            else if (command == BaseMediaSource.MediaSourceCommand.MEDIASOURCE_COMMAND_PLAY) {
                BeginInvoke(new Action(delegate() {
                    btnPlayPause.BackgroundImage = Properties.Resources.player_pause;
                }));
            }
            else if (command == BaseMediaSource.MediaSourceCommand.MEDIASOURCE_COMMAND_PAUSE) {
                BeginInvoke(new Action(delegate() {
                    btnPlayPause.BackgroundImage = Properties.Resources.player_play;
                }));
            }
            else if (command == BaseMediaSource.MediaSourceCommand.MEDIASOURCE_COMMAND_STOP) {
                BeginInvoke(new Action(delegate() {
                    btnPlayPause.BackgroundImage = Properties.Resources.player_play;
                    progMediaTime.Value = 0;
                    lblCurrentTime.Text = secondsToFormattedTime(0);
                    mediaPlayer.resetScreen();
                }));
            }
            else if (command == BaseMediaSource.MediaSourceCommand.MEDIASOURCE_COMMAND_SEEK) {
                mediaSourceList_.play(url);
            }
        }

        // Interface of MediaSourceList
        public void onRecvMediaStreamSourceList(String url, IntPtr media, int mediaSize, MediaCodec mediaCodec, UInt64 presentationTime, IntPtr obj) {
        }

        // Interface of MediaSourceList
        public void onDecodedFrameSourceList(String url, byte[] ybuf, byte[] ubuf, byte[] vbuf, int videoWidth, int videoHeight, UInt64 presentationTime) {
            mediaPlayer.setYuv(ybuf, ubuf, vbuf, videoWidth, videoHeight);
            MediaAttribution mediaAttr = mediaSourceList_.mediaAttribution(url);
            if (mediaAttr.audio_codec == (int)MediaCodec.MEDIA_CODEC_UNKNOWN) {
                BeginInvoke(new Action(delegate() {
                    int secondPts = (int)(presentationTime * mediaAttr.video_timebase);
                    progMediaTime.Value = secondPts;
                    lblCurrentTime.Text = secondsToFormattedTime((uint)secondPts);
                    mediaPlayer.progressSubtitle((uint)secondPts);
                }));
            }
        }

        // Interface of MediaSourceList
        public void onDecodedFrameSourceList(String url, MediaCodec codec, byte[] decodeFrame, int decodeFrameSize, UInt64 presentationTime) {
            if (!MediaDef.isVideoCodec(codec)) {
                mediaPlayer.writeAudio(decodeFrame, decodeFrameSize);
                BeginInvoke(new Action(delegate() {
                    MediaAttribution mediaAttr = mediaSourceList_.mediaAttribution(url);
                    int secondPts = (int)(presentationTime * mediaAttr.audio_timebase);
                    progMediaTime.Value = secondPts;
                    lblCurrentTime.Text = secondsToFormattedTime((uint)secondPts);
                    mediaPlayer.progressSubtitle((uint)secondPts);
                }));
            }
        }
    }
}
