﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Radix.Base;
using Radix.Media;

namespace Renapl.App.MediaSource
{
    public class MediaSourceList : BaseDisposeObject, BaseMediaSource.MediaSourceListener {
        public interface MediaSourceListListener {
            void onCommandResultMediaSourceList(String url, BaseMediaSource.MediaSourceCommand command, bool ret, object param);
            void onRecvMediaStreamSourceList(String url, IntPtr media, int mediaSize, MediaCodec mediaCodec, UInt64 presentationTime, IntPtr obj);
            void onDecodedFrameSourceList(String url, byte[] ybuf, byte[] ubuf, byte[] vbuf, int videoWidth, int videoHeight, UInt64 presentationTime);
            void onDecodedFrameSourceList(String url, MediaCodec codec, byte[] decodeFrame, int decodeFrameSize, UInt64 presentationTime);
        }

        private List<BaseMediaSource> mediaSourceList_ = new List<BaseMediaSource>();
        private FileMediaSource sourceForCheck_ = new FileMediaSource();
        private MediaSourceListListener listener_ = null;

        protected override void internalDispose() {
            sourceForCheck_.Dispose();
            foreach (BaseMediaSource source in mediaSourceList_)
                source.Dispose();
            mediaSourceList_.Clear();
        }

        public void setListener(MediaSourceListListener listener) {
            listener_ = listener;
        }

        public void openFile(String url) {
            BaseMediaSource mediaSource = new FileMediaSource();
            mediaSource.setListener(this);
            mediaSourceList_.Add(mediaSource);
            mediaSource.open(url);
        }

        public void close(String url) {
            BaseMediaSource mediaSource = getMediaSource(url);
            if (mediaSource == null)
                return;

            mediaSource.Dispose();
            mediaSourceList_.Remove(mediaSource);
        }

        public BaseMediaSource getMediaSource(String url) {
            foreach (BaseMediaSource source in mediaSourceList_) {
                if (source.url().CompareTo(url) == 0)
                    return source;
            }

            return null;
        }

        public MediaAttribution mediaAttribution(String url) {
            BaseMediaSource source = getMediaSource(url);
            if (source == null)
                return new MediaAttribution();

            return source.mediaAttribution();
        }

        public void play(String url) {
            BaseMediaSource mediaSource = getMediaSource(url);
            if (mediaSource == null)
                return;
            mediaSource.play();
        }

        public void pause(String url) {
            BaseMediaSource mediaSource = getMediaSource(url);
            if (mediaSource == null)
                return;
            mediaSource.pause();
        }

        public void stop(String url) {
            BaseMediaSource mediaSource = getMediaSource(url);
            if (mediaSource == null)
                return;
            mediaSource.stop();
        }

        public void seek(String url, int seekSeconds) {
            BaseMediaSource mediaSource = getMediaSource(url);
            if (mediaSource == null)
                return;
            mediaSource.seek(seekSeconds);
        }

        public bool isSupportFile(String fileUrl) {
            return sourceForCheck_.isSupportFile(fileUrl);
        }

        /* BaseMediaSource.MediaSourceListener Interface */
        public void onCommandResultMediaSource(String url, BaseMediaSource.MediaSourceCommand command, bool ret, object param) {
            if (listener_ != null)
                listener_.onCommandResultMediaSourceList(url, command, ret, param);
        }

        /* BaseMediaSource.MediaSourceListener Interface */
        public void onRecvMediaStreamMediaSource(String url, IntPtr media, int mediaSize, MediaCodec mediaCodec, UInt64 presentationTime, IntPtr obj) {
        }

        /* BaseMediaSource.MediaSourceListener Interface */
        public void onDecodedFrameMeidaSource(String url, byte[] ybuf, byte[] ubuf, byte[] vbuf, int videoWidth, int videoHeight, UInt64 presentationTime) {
            if (listener_ != null)
                listener_.onDecodedFrameSourceList(url, ybuf, ubuf, vbuf, videoWidth, videoHeight, presentationTime);
        }

        /* BaseMediaSource.MediaSourceListener Interface */
        public void onDecodedFrameMeidaSource(String url, MediaCodec codec, byte[] decodeFrame, int decodeFrameSize, UInt64 presentationTime) {
            if (listener_ != null)
                listener_.onDecodedFrameSourceList(url, codec, decodeFrame, decodeFrameSize, presentationTime);
        }
    }
}
