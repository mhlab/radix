﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using Radix.Media.Demuxer;
using Radix.Media;

namespace Renapl.App.MediaSource {
    public class FileMediaSource: BaseMediaSource, SyncDemuxer.SyncDemuxerListener {
        private SyncDemuxer syncDemuxer_ = null;

        private MediaSourceCommand toMediaSourceCommand(SyncDemuxer.DemuxerCommand demuxerCommand) {
            return (MediaSourceCommand)demuxerCommand;
        }

        protected override void internalDispose() {
            base.internalDispose();
            syncDemuxer_.Dispose();
        }

        public FileMediaSource():
            base() {
            syncDemuxer_ = new SyncDemuxer(this);
        }

        public override void open(String url) {
            base.open(url);
            syncDemuxer_.open(url);
        }

        public bool isSupportFile(String url) {
            return syncDemuxer_.isSupportFile(url);
        }

        public override void play() {
            syncDemuxer_.play();
        }

        public override void pause() {
            syncDemuxer_.pause();
        }

        public override void stop() {
            syncDemuxer_.stop();
            base.stop();
        }

        public override void seek(int seekSecond) {
            syncDemuxer_.seek(seekSecond);
        }

        // Interface of SyncDemuxer
        public void onCommandResultSyncDemuxer(int command, bool ret, IntPtr param, IntPtr obj) {
            SyncDemuxer.DemuxerCommand demuxerCommand = (SyncDemuxer.DemuxerCommand)command;
            switch (demuxerCommand)
            {
                case SyncDemuxer.DemuxerCommand.DEMUXER_COMMAND_OPEN:
                    {
                        if (ret) {
                            if (param != null) {
                                mediaAttr_ = (MediaAttribution)Marshal.PtrToStructure(param, mediaAttr_.GetType());
                                mediaAttr_.init();
                            }
                        }
                        break;
                    }
            }
            callListenerCommandResult(toMediaSourceCommand(demuxerCommand), ret, mediaAttr_);
        }

        // Interface of SyncDemuxer
        public void onRecvMediaStreamSyncDemuxer(IntPtr media, int mediaSize, int mediaCodec, UInt64 presentationTime, IntPtr obj) {
            callListenerRecvMediaStream(media, mediaSize, (MediaCodec)mediaCodec, presentationTime, obj);
        }
    }
}
