﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Radix.Base;
using Radix.Media;
using Radix.Media.Decoder;

namespace Renapl.App.MediaSource {
    public class BaseMediaSource: BaseDisposeObject, MediaDecodeThread.MediaDecodeThreadListener {
        public interface MediaSourceListener {
            void onCommandResultMediaSource(String url, MediaSourceCommand command, bool ret, object param);
            void onRecvMediaStreamMediaSource(String url, IntPtr media, int mediaSize, MediaCodec mediaCodec, UInt64 presentationTime, IntPtr obj);
            void onDecodedFrameMeidaSource(String url, byte[] ybuf, byte[] ubuf, byte[] vbuf, int videoWidth, int videoHeight, UInt64 presentationTime);
            void onDecodedFrameMeidaSource(String url, MediaCodec codec, byte[] decodeFrame, int decodeFrameSize, UInt64 presentationTime);
        }

        public enum MediaSourceCommand {
            MEDIASOURCE_COMMAND_NONE = -1,
            MEDIASOURCE_COMMAND_OPEN = 0,
            MEDIASOURCE_COMMAND_PLAY = 1,
            MEDIASOURCE_COMMAND_STOP = 2,
            MEDIASOURCE_COMMAND_PAUSE = 3,
            MEDIASOURCE_COMMAND_SEEK = 4
        }

        private String url_ = "";
        private MediaSourceListener listener_ = null;
        private MediaDecodeThread videoDecodeThread_ = null;
        private MediaDecodeThread audioDecodeThread_ = null;
        private MediaSourceCommand currentCommand_ = MediaSourceCommand.MEDIASOURCE_COMMAND_NONE;

        protected MediaAttribution mediaAttr_ = new MediaAttribution(); 

        protected override void internalDispose() {
            base.internalDispose();
            videoDecodeThread_.Dispose();
            audioDecodeThread_.Dispose();
        }

        protected void callListenerCommandResult(MediaSourceCommand command, bool ret, object param) {
            if (command == MediaSourceCommand.MEDIASOURCE_COMMAND_OPEN) {
                videoDecodeThread_.closeCodec();
                audioDecodeThread_.closeCodec();
                if (ret) {
                    if (mediaAttr_.video_codec != (int)MediaCodec.MEDIA_CODEC_UNKNOWN)
                        videoDecodeThread_.openVideoCodec((MediaCodec)mediaAttr_.video_codec);
                    if (mediaAttr_.audio_codec != (int)MediaCodec.MEDIA_CODEC_UNKNOWN)
                        audioDecodeThread_.openAudioCodec((MediaCodec)mediaAttr_.audio_codec, mediaAttr_.audio_sample_rate, mediaAttr_.audio_channels, mediaAttr_.audio_channelLayout);
                }
            }
            currentCommand_ = command;
            if (listener_ != null)
                listener_.onCommandResultMediaSource(url_, command, ret, param);
        }

        protected void callListenerRecvMediaStream(IntPtr media, int mediaSize, MediaCodec mediaCodec, UInt64 presentationTime, IntPtr obj) {
            if (mediaCodec != MediaCodec.MEDIA_CODEC_UNKNOWN) {
                if ((int)mediaCodec == mediaAttr_.video_codec)
                    videoDecodeThread_.putMediaFrame(media, mediaSize, mediaCodec, presentationTime);
                else if ((int)mediaCodec == mediaAttr_.audio_codec)
                    audioDecodeThread_.putMediaFrame(media, mediaSize, mediaCodec, presentationTime);
            }
            if (listener_ != null)
                listener_.onRecvMediaStreamMediaSource(url_, media, mediaSize, mediaCodec, presentationTime, obj);
        }

        public BaseMediaSource() {
            videoDecodeThread_ = new MediaDecodeThread(this);
            audioDecodeThread_ = new MediaDecodeThread(this);
        }

        public void setListener(MediaSourceListener listener) {
            listener_ = listener;
        }

        public virtual void open(String url) {
            url_ = url;
        }

        public String url() {
            return url_;
        }

        public MediaAttribution mediaAttribution() {
            return mediaAttr_;
        }

        public virtual void play() {
        }

        public virtual void pause() {
        }

        public virtual void stop() {
            videoDecodeThread_.removeAllFrames();
            audioDecodeThread_.removeAllFrames();
        }

        public virtual void seek(int seekSecond) {
        }

        public bool isPlaying() {
            return (currentCommand_ == MediaSourceCommand.MEDIASOURCE_COMMAND_PLAY);
        }

        // Interface of MediaDecodeThreadListener
        public void onDecodedFrame(byte[] ybuf, byte[] ubuf, byte[] vbuf, int videoWidth, int videoHeight, UInt64 presentationTime) {
            if (listener_ != null)
                listener_.onDecodedFrameMeidaSource(url_, ybuf, ubuf, vbuf, videoWidth, videoHeight, presentationTime);
        }

        // Interface of MediaDecodeThreadListener
        public void onDecodedFrame(MediaCodec codec, byte[] decodeFrame, int decodeFrameSize, UInt64 presentationTime) {
            if (listener_ != null)
                listener_.onDecodedFrameMeidaSource(url_, codec, decodeFrame, decodeFrameSize, presentationTime);
        }
    }
}
