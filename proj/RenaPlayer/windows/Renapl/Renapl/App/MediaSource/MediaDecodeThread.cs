﻿//#define _DEBUG_DUMP_FRAME

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.InteropServices;

using Radix.Base;
using Radix.Media;
using Radix.Media.Decoder;
using Radix.Memory;

namespace Renapl.App.MediaSource
{
    public class MediaDecodeThread: BaseDisposeObject {
        public interface MediaDecodeThreadListener {
            void onDecodedFrame(byte[] ybuf, byte[] ubuf, byte[] vbuf, int videoWidth, int videoHeight, UInt64 presentationTime);
            void onDecodedFrame(MediaCodec codec, byte[] decodeFrame, int decodeFrameSize, UInt64 presentationTime);
        }
       
        public enum QueueItemCommand {
            QUEUEITEM_COMMAND_OPENCODEC,
            QUEUEITEM_COMMAND_CLOSECODEC,
            QUEUEITEM_COMMAND_DECODE,
            QUEUEITEM_COMMAND_REMOVEALLFRAMES
        }

        public class QueueItem {
            public QueueItemCommand command_ = QueueItemCommand.QUEUEITEM_COMMAND_DECODE;
            public MediaCodec codec_ = MediaCodec.MEDIA_CODEC_UNKNOWN;
        }

        public class MediaQueueItem: QueueItem {
            public UInt64 presentationTime_ = 0;
            public MemoryPool.MemoryPoolItem poolItem_ = null;
        }

        public class CodecQueueItem: QueueItem {
            public int channels_ = 0;
            public int channelLayout_ = 0;
            public int samplerate_ = 0;
        }

        private ConcurrentQueue<QueueItem> queue_ = new ConcurrentQueue<QueueItem>();
        private MemoryPool framePool_ = new MemoryPool(5242880); //5MB
        private MediaDecoder videoDecoder_ = new MediaDecoder();
        private MediaDecoder audioDecoder_ = new MediaDecoder();
        private MediaDecodeThreadListener listener_ = null;
        
        private Thread decodeThread_ = null;
        private bool threadTerminated_ = false;
        private AutoResetEvent queueEvent_ = new AutoResetEvent(false);

        private void clearQueue() {
            QueueItem queueItem;
            while (queue_.TryDequeue(out queueItem)) {
                if (queueItem is MediaQueueItem)
                    if (((MediaQueueItem)queueItem).poolItem_ != null)
                        framePool_.dealloc(((MediaQueueItem)queueItem).poolItem_);
            }
        }

        private void deocdeThreadFunc() {
            QueueItem queueItem;
            int videoWidth = 0;
            int videoHeight = 0;
            int ysize = 0;
            int uvsize = 0;
            byte[] ybuf = null;
            byte[] ubuf = null;
            byte[] vbuf = null;
            byte[] audiobuf = null;
            int audioSize = 0;
            int audioChannels = -1;
            int audioSamplerate = -1;
            int audioChannelLayout = -1;
#if _DEBUG_DUMP_FRAME
            System.IO.FileStream encodeStream = new System.IO.FileStream(String.Format("encode_{0}.dat", GetHashCode()), System.IO.FileMode.OpenOrCreate);
            System.IO.FileStream yuvStream = new System.IO.FileStream(String.Format("yuv_{0}.dat", GetHashCode()), System.IO.FileMode.OpenOrCreate);
#endif
            while (!threadTerminated_) {
                queueEvent_.WaitOne();
                if (threadTerminated_)
                    break;
                if (!queue_.TryDequeue(out queueItem))
                    continue;
                switch (queueItem.command_) {
                    case QueueItemCommand.QUEUEITEM_COMMAND_CLOSECODEC: 
                        {
                            videoDecoder_.close();
                            audioDecoder_.close();
                            videoWidth = 0;
                            videoHeight = 0;
                            break;
                        }
                    case QueueItemCommand.QUEUEITEM_COMMAND_OPENCODEC: 
                        {
                            CodecQueueItem codecQueueItem = (CodecQueueItem)queueItem;
                            if (codecQueueItem.samplerate_ == 0)
                                videoDecoder_.openVideo(codecQueueItem.codec_);
                            else {
                                audioChannels = codecQueueItem.channels_;
                                audioSamplerate = codecQueueItem.samplerate_;
                                audioChannelLayout = codecQueueItem.channelLayout_;
                                audioDecoder_.openAudio(codecQueueItem.codec_, audioSamplerate, audioChannels, audioChannelLayout);
                            }
                            break;
                        }
                    case QueueItemCommand.QUEUEITEM_COMMAND_REMOVEALLFRAMES:
                        {
                            clearQueue();
                            break;
                        }
                    case QueueItemCommand.QUEUEITEM_COMMAND_DECODE:
                        {
                            MediaQueueItem mediaQueueItem = (MediaQueueItem)queueItem;
                            try {
                                if (videoDecoder_.getCodec() == queueItem.codec_) {
#if _DEBUG_DUMP_FRAME
                                    encodeStream.Write(mediaQueueItem.poolItem_.memory_, 0, (int)mediaQueueItem.poolItem_.memorySize_);
#endif
                                    if (!videoDecoder_.isOpen()) {
                                        if (!videoDecoder_.openVideo(mediaQueueItem.codec_))
                                            continue;
                                    }
                                    if (videoWidth == 0 && videoHeight == 0) {
                                        if (videoDecoder_.decodeVideoToYuv(mediaQueueItem.poolItem_.memory_, (int)mediaQueueItem.poolItem_.memorySize_, null, null, null, ref videoWidth, ref videoHeight, (int)MediaPixelFormat.MEDIA_PIXELFORMAT_YUV420P) != MediaDecoder.FFMPEG_DECODER_SUCCESS)
                                            break;
                                        if (videoWidth == 0 || videoHeight == 0)
                                            break;
                                        int widthRemainder = videoWidth % 16;
                                        int heightRemainder = videoHeight % 16;
                                        if (widthRemainder > 0)
                                            videoWidth = videoWidth - widthRemainder;
                                        if (heightRemainder > 0)
                                            videoHeight = videoHeight - heightRemainder;
                                        ysize = videoWidth * videoHeight;
                                        uvsize = (videoWidth / 2) * (videoHeight / 2);
                                        ybuf = new byte[ysize];
                                        ubuf = new byte[uvsize];
                                        vbuf = new byte[uvsize];
                                    }
                                    if (videoWidth > 0 && videoHeight > 0 && ysize > 0 && uvsize > 0) {
                                        if (videoDecoder_.decodeVideoToYuv(mediaQueueItem.poolItem_.memory_, (int)mediaQueueItem.poolItem_.memorySize_, ybuf, ubuf, vbuf, ref videoWidth, ref videoHeight, (int)MediaPixelFormat.MEDIA_PIXELFORMAT_YUV420P) != MediaDecoder.FFMPEG_DECODER_SUCCESS)
                                            continue;
#if _DEBUG_DUMP_FRAME
                                        yuvStream.Write(ybuf, 0, ybuf.Length);
                                        yuvStream.Write(ubuf, 0, ubuf.Length);
                                        yuvStream.Write(vbuf, 0, vbuf.Length);
#endif
                                        if (listener_ != null)
                                            listener_.onDecodedFrame(ybuf, ubuf, vbuf, videoWidth, videoHeight, mediaQueueItem.presentationTime_);
                                    }
                                }
                                else if (audioDecoder_.getCodec() == queueItem.codec_) {
                                    if (!audioDecoder_.isOpen()) {
                                        if (!audioDecoder_.openAudio(mediaQueueItem.codec_, audioSamplerate, audioChannels, audioChannelLayout))
                                            continue;
                                    }
                                    if (audioSize == 0) {
                                        audioDecoder_.decodeAudio(mediaQueueItem.poolItem_.memory_, (int)mediaQueueItem.poolItem_.memorySize_, null, ref audioSize, 2, (int)AudioChannelLayout.STEREO);
                                        if (audioSize > 0)
                                            audiobuf = new byte[audioSize];
                                    }
                                    if (audiobuf != null) {
                                        if (audioDecoder_.decodeAudio(mediaQueueItem.poolItem_.memory_, (int)mediaQueueItem.poolItem_.memorySize_, audiobuf, ref audioSize, 2, (int)AudioChannelLayout.STEREO) != MediaDecoder.FFMPEG_DECODER_SUCCESS)
                                            continue;
                                        if (listener_ != null)
                                            listener_.onDecodedFrame(mediaQueueItem.codec_, audiobuf, audiobuf.Length, mediaQueueItem.presentationTime_);
                                    }
                                }

                            }
                            finally {
                                if (mediaQueueItem.poolItem_ != null)
                                    framePool_.dealloc(mediaQueueItem.poolItem_);
                            }
                            break;
                        }
                }
                if (queue_.Count > 0)
                    queueEvent_.Set();
            }
            videoDecoder_.close();
            audioDecoder_.close();
#if _DEBUG_DUMP_FRAME
            encodeStream.Close();
            yuvStream.Close();
#endif
        }

        protected override void internalDispose() {
            base.internalDispose();
            threadTerminated_ = true;
            queueEvent_.Set();
            decodeThread_.Join();
            videoDecoder_.Dispose();
            audioDecoder_.Dispose();
        }

        public MediaDecodeThread(MediaDecodeThreadListener listener) {
            listener_ = listener;
            decodeThread_ = new Thread(deocdeThreadFunc);
            decodeThread_.Name = "MediaDecodeThread";
            decodeThread_.Priority = ThreadPriority.Highest;
            decodeThread_.Start();
        }

        public void openVideoCodec(MediaCodec codec) {
            CodecQueueItem queueItem = new CodecQueueItem();
            queueItem.command_ = QueueItemCommand.QUEUEITEM_COMMAND_OPENCODEC;
            queueItem.codec_ = codec;
            queue_.Enqueue(queueItem);
            queueEvent_.Set();
        }

        public void openAudioCodec(MediaCodec codec, int samplerate, int channels, int channelLayout) {
            CodecQueueItem queueItem = new CodecQueueItem();
            queueItem.command_ = QueueItemCommand.QUEUEITEM_COMMAND_OPENCODEC;
            queueItem.codec_ = codec;
            queueItem.channels_ = channels;
            queueItem.channelLayout_ = channelLayout;
            queueItem.samplerate_ = samplerate;
            queue_.Enqueue(queueItem);
            queueEvent_.Set();
        }

        public void closeCodec() {
            clearQueue();
            QueueItem queueItem = new QueueItem();
            queueItem.command_ = QueueItemCommand.QUEUEITEM_COMMAND_CLOSECODEC;
            queue_.Enqueue(queueItem);
            queueEvent_.Set();
        }

        public void removeAllFrames() {
            QueueItem queueItem = new QueueItem();
            queueItem.command_ = QueueItemCommand.QUEUEITEM_COMMAND_REMOVEALLFRAMES;
            queue_.Enqueue(queueItem);
            queueEvent_.Set();
        }

        public void putMediaFrame(IntPtr mediaFrame, int mediaFrameSize, MediaCodec codec, UInt64 pts) {
            MemoryPool.MemoryPoolItem poolItem = framePool_.alloc((UInt32)mediaFrameSize);
            if (poolItem == null)
                return;

            MediaQueueItem queueItem = new MediaQueueItem();
            Marshal.Copy(mediaFrame, poolItem.memory_, 0, mediaFrameSize);
            queueItem.poolItem_ = poolItem;
            queueItem.codec_ = codec;
            queueItem.presentationTime_ = pts;
            queue_.Enqueue(queueItem);
            queueEvent_.Set();
        }
    }
}
