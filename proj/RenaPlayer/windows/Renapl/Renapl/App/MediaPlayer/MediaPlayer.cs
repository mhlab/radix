﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;

using SharpGL;
using SharpGL.Shaders;
using NAudio.Wave;
using NAudio.CoreAudioApi;
using Radix.Media.Playback;
using Radix.Reader;
using Radix.Ui.Controls;

namespace Renapl.App.MediaPlayer {
    public class MediaPlayer : OpenGLControl, SubtitleReader.SubtitleReaderListener{
        private const String Vertex_Shader_Code_ = 
            "attribute vec4 vPosition;" +
            "attribute vec2 i_texCoord;" +
            "varying vec2 texCoord;" +
            "void main() {" +
            "  gl_Position = vPosition;" +
            "  texCoord = i_texCoord;" +
            "}";
        private const string Fragment_Shader_Codec_ =
            "precision mediump float;" +
            "varying vec2 texCoord;" +
            "uniform vec4 vColor;" +
            "uniform sampler2D tex_y;" +
            "uniform sampler2D tex_u;" +
            "uniform sampler2D tex_v;" +
            "const mat3 yuv2rgb = mat3(" +
            "	1, 0, 1.2802," +
            "	1, -0.214821, -0.380589," +
            "	1, 2.127982, 0" +
            ");" +
            "void main() {" +
            "	vec3 yuv = vec3(" +
            "	  1.1643 * (texture2D(tex_y, texCoord).r - 0.0627)," +
            "     texture2D(tex_u, texCoord).r - 0.5," +
            "	  texture2D(tex_v, texCoord).r - 0.5" +
            "	);" +
            "	vec3 rgb = yuv * yuv2rgb;" +
            "	gl_FragColor = vec4(rgb, 1.0);" +
            "}";

        private ShaderProgram program_ = new ShaderProgram();
        private IntPtr vertexPtr_ = IntPtr.Zero;
        private IntPtr coordPtr_ = IntPtr.Zero;
        private int vertexSize_ = 0;
        private int[] yuvTexture_ = { -1, -1, -1 };
        private uint[] vertexBuffer_ = new uint[1];
        private uint[] indexBuffer_ = new uint[1];

        private byte[] yBuf_ = null;
        private byte[] uBuf_ = null;
        private byte[] vBuf_ = null;
        private int yuvWidth_ = 0;
        private int yuvHeight_ = 0;
        private Object yuvLock_ = new Object();

        private BufferedWaveProvider bufferedWaveProvider_ = null;
        private VolumeWaveProvider16 volumeWaveProvider_ = null;
        private IWavePlayer wavePlayer_ = null;
        private MMDevice mmDevice_ = null;

        private SubtitleReader subtitleReader_ = null;
        private OutlineLabel lblSubtitle_ = null;

        private String shaderErrorLog(uint shaderId) {
            StringBuilder strBuilder = new StringBuilder(2048);
            OpenGL.GetShaderInfoLog(shaderId, 2048, IntPtr.Zero, strBuilder);
            return strBuilder.ToString();
        }

        private uint createShader(uint shaderType, String shaderCode) {
            uint shader = OpenGL.CreateShader(shaderType);
            OpenGL.ShaderSource(shader, shaderCode);
            OpenGL.CompileShader(shader);

            return shader;
        }

        private uint setYuvTexture(int pos, byte[] yuvData, int yuvWidth, int yuvHeight) {
            if (yuvTexture_[pos] == -1) {
                uint[] texture = new uint[1];
                OpenGL.GenTextures(1, texture);
                yuvTexture_[pos] = (int)texture[0];
            }
            OpenGL.BindTexture(OpenGL.GL_TEXTURE_2D, (uint)yuvTexture_[pos]);
            if (pos == 0)
                OpenGL.TexImage2D(OpenGL.GL_TEXTURE_2D, 0, OpenGL.GL_LUMINANCE, yuvWidth, yuvHeight, 0, OpenGL.GL_LUMINANCE, OpenGL.GL_UNSIGNED_BYTE, yuvData);
            else
                OpenGL.TexImage2D(OpenGL.GL_TEXTURE_2D, 0, OpenGL.GL_LUMINANCE, yuvWidth / 2, yuvHeight / 2, 0, OpenGL.GL_LUMINANCE, OpenGL.GL_UNSIGNED_BYTE, yuvData);
            OpenGL.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_MIN_FILTER, OpenGL.GL_LINEAR);
            OpenGL.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_MAG_FILTER, OpenGL.GL_LINEAR);
            OpenGL.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_WRAP_S, OpenGL.GL_CLAMP_TO_EDGE);
            OpenGL.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_WRAP_T, OpenGL.GL_CLAMP_TO_EDGE);

            return (uint)yuvTexture_[pos];
        }

        private void initVertexBuffer() {
            float[] squareCoords = {-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f};
            float[] textureCoords = {0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f};
            GCHandle handle = GCHandle.Alloc(squareCoords, GCHandleType.Pinned);
            vertexPtr_ = handle.AddrOfPinnedObject();
            handle = GCHandle.Alloc(textureCoords, GCHandleType.Pinned);
            coordPtr_ = handle.AddrOfPinnedObject();
            OpenGL.GenBuffers(1, vertexBuffer_);
            OpenGL.BindBuffer(OpenGL.GL_ARRAY_BUFFER, vertexBuffer_[0]);
            OpenGL.BufferData(OpenGL.GL_ARRAY_BUFFER, squareCoords.Length * sizeof(float), vertexPtr_, OpenGL.GL_STATIC_DRAW);
            OpenGL.GenBuffers(1, indexBuffer_);
            OpenGL.BindBuffer(OpenGL.GL_ARRAY_BUFFER, indexBuffer_[0]);
            OpenGL.BufferData(OpenGL.GL_ARRAY_BUFFER, textureCoords.Length * sizeof(float), coordPtr_, OpenGL.GL_STATIC_DRAW);
            vertexSize_ = 2;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            lblSubtitle_ = new OutlineLabel();
            lblSubtitle_.Parent = this;
            lblSubtitle_.BackColor = Color.Transparent;
            lblSubtitle_.ForeColor = Color.White;
            lblSubtitle_.Text = "";
            lblSubtitle_.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
            lblSubtitle_.TextAlign = ContentAlignment.MiddleCenter;
            lblSubtitle_.Font = new Font(lblSubtitle_.Font.Name, 20, FontStyle.Bold);
            lblSubtitle_.AutoSize = false;
            lblSubtitle_.SetBounds(0, Height - (lblSubtitle_.Height * 2) - 30, Width, lblSubtitle_.Height * 2 + 30);
            lblSubtitle_.OutlineColor = Color.Black;
            lblSubtitle_.OutlineWidth = 1;
        }

        protected override void DoOpenGLInitialized() {
            base.DoOpenGLInitialized();
            program_.Create(OpenGL, Vertex_Shader_Code_, Fragment_Shader_Codec_, null);
            initVertexBuffer();
        }

        protected override void DoOpenGLDraw(RenderEventArgs e) {
            base.DoOpenGLDraw(e);
            OpenGL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            OpenGL.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            if (yBuf_ == null || uBuf_ == null || vBuf_ == null)
                return;

            program_.Bind(OpenGL);
            uint positionHandle = (uint)program_.GetAttributeLocation(OpenGL, "vPosition");
            OpenGL.EnableVertexAttribArray(positionHandle);
            OpenGL.BindBuffer(OpenGL.GL_ARRAY_BUFFER, vertexBuffer_[0]);
            OpenGL.VertexAttribPointer(positionHandle, vertexSize_, OpenGL.GL_FLOAT, false, 0, IntPtr.Zero);
            uint coordHandle = (uint)program_.GetAttributeLocation(OpenGL, "i_texCoord");
            OpenGL.EnableVertexAttribArray(coordHandle);
            OpenGL.BindBuffer(OpenGL.GL_ARRAY_BUFFER, indexBuffer_[0]);
            OpenGL.VertexAttribPointer(coordHandle, vertexSize_, OpenGL.GL_FLOAT, false, 0, IntPtr.Zero);

            uint yTexture;
            uint uTexture;
            uint vTexture;
            Monitor.Enter(yuvLock_);
            try {
                yTexture = setYuvTexture(0, yBuf_, yuvWidth_, yuvHeight_);
                uTexture = setYuvTexture(1, uBuf_, yuvWidth_, yuvHeight_);
                vTexture = setYuvTexture(2, vBuf_, yuvWidth_, yuvHeight_);
            }
            finally {
                Monitor.Exit(yuvLock_);
            }
            OpenGL.ActiveTexture(OpenGL.GL_TEXTURE0);
            OpenGL.BindTexture(OpenGL.GL_TEXTURE_2D, yTexture);
            int yLocation = program_.GetUniformLocation(OpenGL, "tex_y");
            OpenGL.Uniform1(yLocation, 0);
            OpenGL.ActiveTexture(OpenGL.GL_TEXTURE1);
            OpenGL.BindTexture(OpenGL.GL_TEXTURE_2D, uTexture);
            int uLocation = program_.GetUniformLocation(OpenGL, "tex_u");
            OpenGL.Uniform1(uLocation, 1);
            OpenGL.ActiveTexture(OpenGL.GL_TEXTURE2);
            OpenGL.BindTexture(OpenGL.GL_TEXTURE_2D, vTexture);
            int vLocation = program_.GetUniformLocation(OpenGL, "tex_v");
            OpenGL.Uniform1(vLocation, 2);

            OpenGL.DrawArrays(OpenGL.GL_TRIANGLE_STRIP, 0, 8 / vertexSize_);
            OpenGL.DisableVertexAttribArray(positionHandle);
            OpenGL.DisableVertexAttribArray(coordHandle);
            OpenGL.UseProgram(0);
        }

        protected override void OnSizeChanged(EventArgs e) {
            base.OnSizeChanged(e);
            if (OpenGL.RenderContextProvider != null)
                OpenGL.Viewport(0, 0, OpenGL.RenderContextProvider.Width, OpenGL.RenderContextProvider.Height);
        }

        protected override void Dispose(bool disposing) {
            base.Dispose(disposing);
            if (wavePlayer_ != null)
                wavePlayer_.Stop();
        }

        public MediaPlayer():
            base() {
            mmDevice_ = new MMDeviceEnumerator().GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
        }

        public void setYuv(byte[] y, byte[] u, byte[] v, int yuvWidth, int yuvHeight) {
            if (y == null || u == null || v == null)
                return;
            if (!Monitor.TryEnter(yuvLock_))
                return;

            if (yBuf_ == null || yBuf_.Length != y.Length)
                yBuf_ = new byte[y.Length];
            y.CopyTo(yBuf_, 0);
            if (uBuf_ == null || uBuf_.Length != u.Length)
                uBuf_ = new byte[u.Length];
            u.CopyTo(uBuf_, 0);
            if (vBuf_ == null || vBuf_.Length != v.Length)
                vBuf_ = new byte[v.Length];
            v.CopyTo(vBuf_, 0);
            yuvWidth_ = yuvWidth;
            yuvHeight_ = yuvHeight;
            Monitor.Exit(yuvLock_);
            Invalidate();
        }

        public void initAudio(int samplerate, int channels) {
            if (wavePlayer_ != null)
                wavePlayer_.Stop();
            if (samplerate == 0 || channels == 0)
                return;

            bufferedWaveProvider_ = new BufferedWaveProvider(new WaveFormat(samplerate, 16, 2));
            volumeWaveProvider_ = new VolumeWaveProvider16(bufferedWaveProvider_);
            wavePlayer_ = new WasapiOut(mmDevice_, AudioClientShareMode.Shared, false, 100);
            wavePlayer_.Init(volumeWaveProvider_);
            wavePlayer_.Play();
        }

        public void writeAudio(byte[] audioStream, int audioStreamSize) {
            bufferedWaveProvider_.AddSamples(audioStream, 0, audioStreamSize);
        }

        public void resetScreen() {
            Monitor.Enter(yuvLock_);
            try {
                yBuf_ = null;
                uBuf_ = null;
                vBuf_ = null;
            }
            finally {
                Monitor.Exit(yuvLock_);
            }
            Invalidate();
        }

        public void openSubtitleReader(String url) {
            subtitleReader_ = SubtitleReader.createSubtitleReader(url, this);
            if (subtitleReader_ == null)
                return;
            subtitleReader_.open();
        }

        public void progressSubtitle(uint playTime) {
            if (subtitleReader_ == null)
                return;
            subtitleReader_.progress((double)playTime);
        }

        //Interface of SubtitleReader
        public void onOpenSubtitle(string subtitleFile, bool ret) {
            if (!ret) {
                subtitleReader_.Dispose();
                subtitleReader_ = null;
            }
        }

        //Interface of SubtitleReader
        public void onRecvSubtitle(string subtitle) {
            String newSubtitle = subtitle;
            if (newSubtitle.IndexOf("<br>") >= 0)
                newSubtitle = subtitle.Replace("<br>", Environment.NewLine);
            newSubtitle = Regex.Replace(newSubtitle, "<(.*?)\\>", "");
            lblSubtitle_.Text = newSubtitle;
        }

        private void InitializeComponent() {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MediaPlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.Name = "MediaPlayer";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
