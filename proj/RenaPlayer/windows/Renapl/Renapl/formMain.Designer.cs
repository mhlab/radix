﻿namespace Renapl
{
    partial class formMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCaptionBar = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRestoreWindow = new System.Windows.Forms.Button();
            this.btnMinimizeWindow = new System.Windows.Forms.Button();
            this.mediaPlayer = new Renapl.App.MediaPlayer.MediaPlayer();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.pnlTime = new System.Windows.Forms.Panel();
            this.lblFinishTime = new System.Windows.Forms.Label();
            this.progMediaTime = new System.Windows.Forms.ProgressBar();
            this.lblCurrentTime = new System.Windows.Forms.Label();
            this.pnlPlayMenu = new System.Windows.Forms.Panel();
            this.btnNextTenSeconds = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnPlayPause = new System.Windows.Forms.Button();
            this.btnPrevTenSeconds = new System.Windows.Forms.Button();
            this.pnlCaptionBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mediaPlayer)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.pnlTime.SuspendLayout();
            this.pnlPlayMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCaptionBar
            // 
            this.pnlCaptionBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlCaptionBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlCaptionBar.Controls.Add(this.label1);
            this.pnlCaptionBar.Controls.Add(this.btnClose);
            this.pnlCaptionBar.Controls.Add(this.btnRestoreWindow);
            this.pnlCaptionBar.Controls.Add(this.btnMinimizeWindow);
            this.pnlCaptionBar.Location = new System.Drawing.Point(0, 0);
            this.pnlCaptionBar.Name = "pnlCaptionBar";
            this.pnlCaptionBar.Size = new System.Drawing.Size(569, 25);
            this.pnlCaptionBar.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Silver;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "RenaPlayer";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackgroundImage = global::Renapl.Properties.Resources.close_window;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(548, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(17, 17);
            this.btnClose.TabIndex = 2;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRestoreWindow
            // 
            this.btnRestoreWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRestoreWindow.BackgroundImage = global::Renapl.Properties.Resources.restore_window;
            this.btnRestoreWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRestoreWindow.FlatAppearance.BorderSize = 0;
            this.btnRestoreWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRestoreWindow.Location = new System.Drawing.Point(530, 5);
            this.btnRestoreWindow.Name = "btnRestoreWindow";
            this.btnRestoreWindow.Size = new System.Drawing.Size(17, 17);
            this.btnRestoreWindow.TabIndex = 1;
            this.btnRestoreWindow.UseVisualStyleBackColor = true;
            this.btnRestoreWindow.Click += new System.EventHandler(this.btnRestoreWindow_Click);
            // 
            // btnMinimizeWindow
            // 
            this.btnMinimizeWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizeWindow.BackgroundImage = global::Renapl.Properties.Resources.minimize_window;
            this.btnMinimizeWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMinimizeWindow.FlatAppearance.BorderSize = 0;
            this.btnMinimizeWindow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizeWindow.Location = new System.Drawing.Point(513, 5);
            this.btnMinimizeWindow.Name = "btnMinimizeWindow";
            this.btnMinimizeWindow.Size = new System.Drawing.Size(17, 17);
            this.btnMinimizeWindow.TabIndex = 0;
            this.btnMinimizeWindow.UseVisualStyleBackColor = true;
            this.btnMinimizeWindow.Click += new System.EventHandler(this.btnMinimizeWindow_Click);
            // 
            // mediaPlayer
            // 
            this.mediaPlayer.AllowDrop = true;
            this.mediaPlayer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mediaPlayer.BackColor = System.Drawing.Color.Black;
            this.mediaPlayer.DrawFPS = false;
            this.mediaPlayer.Location = new System.Drawing.Point(5, 26);
            this.mediaPlayer.Name = "mediaPlayer";
            this.mediaPlayer.OpenGLVersion = SharpGL.Version.OpenGLVersion.OpenGL2_1;
            this.mediaPlayer.RenderContextType = SharpGL.RenderContextType.FBO;
            this.mediaPlayer.RenderTrigger = SharpGL.RenderTrigger.Manual;
            this.mediaPlayer.Size = new System.Drawing.Size(559, 207);
            this.mediaPlayer.TabIndex = 1;
            this.mediaPlayer.DragDrop += new System.Windows.Forms.DragEventHandler(this.videoView_DragDrop);
            this.mediaPlayer.DragEnter += new System.Windows.Forms.DragEventHandler(this.videoView_DragEnter);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBottom.Controls.Add(this.pnlTime);
            this.pnlBottom.Controls.Add(this.pnlPlayMenu);
            this.pnlBottom.Location = new System.Drawing.Point(5, 235);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(559, 88);
            this.pnlBottom.TabIndex = 3;
            this.pnlBottom.Resize += new System.EventHandler(this.pnlBottom_Resize);
            // 
            // pnlTime
            // 
            this.pnlTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTime.BackColor = System.Drawing.Color.Transparent;
            this.pnlTime.Controls.Add(this.lblFinishTime);
            this.pnlTime.Controls.Add(this.progMediaTime);
            this.pnlTime.Controls.Add(this.lblCurrentTime);
            this.pnlTime.Location = new System.Drawing.Point(0, 0);
            this.pnlTime.Name = "pnlTime";
            this.pnlTime.Size = new System.Drawing.Size(559, 22);
            this.pnlTime.TabIndex = 0;
            // 
            // lblFinishTime
            // 
            this.lblFinishTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFinishTime.AutoSize = true;
            this.lblFinishTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblFinishTime.Location = new System.Drawing.Point(509, 4);
            this.lblFinishTime.Name = "lblFinishTime";
            this.lblFinishTime.Size = new System.Drawing.Size(49, 12);
            this.lblFinishTime.TabIndex = 2;
            this.lblFinishTime.Text = "00:00:00\r\n";
            // 
            // progMediaTime
            // 
            this.progMediaTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progMediaTime.Location = new System.Drawing.Point(63, 7);
            this.progMediaTime.Name = "progMediaTime";
            this.progMediaTime.Size = new System.Drawing.Size(436, 6);
            this.progMediaTime.TabIndex = 1;
            this.progMediaTime.MouseUp += new System.Windows.Forms.MouseEventHandler(this.progMediaTime_MouseUp);
            // 
            // lblCurrentTime
            // 
            this.lblCurrentTime.AutoSize = true;
            this.lblCurrentTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblCurrentTime.Location = new System.Drawing.Point(7, 4);
            this.lblCurrentTime.Name = "lblCurrentTime";
            this.lblCurrentTime.Size = new System.Drawing.Size(49, 12);
            this.lblCurrentTime.TabIndex = 0;
            this.lblCurrentTime.Text = "00:00:00";
            // 
            // pnlPlayMenu
            // 
            this.pnlPlayMenu.BackColor = System.Drawing.Color.Black;
            this.pnlPlayMenu.Controls.Add(this.btnNextTenSeconds);
            this.pnlPlayMenu.Controls.Add(this.btnStop);
            this.pnlPlayMenu.Controls.Add(this.btnPlayPause);
            this.pnlPlayMenu.Controls.Add(this.btnPrevTenSeconds);
            this.pnlPlayMenu.Location = new System.Drawing.Point(153, 28);
            this.pnlPlayMenu.Name = "pnlPlayMenu";
            this.pnlPlayMenu.Size = new System.Drawing.Size(240, 56);
            this.pnlPlayMenu.TabIndex = 3;
            // 
            // btnNextTenSeconds
            // 
            this.btnNextTenSeconds.BackgroundImage = global::Renapl.Properties.Resources.player_forward;
            this.btnNextTenSeconds.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNextTenSeconds.Location = new System.Drawing.Point(179, 2);
            this.btnNextTenSeconds.Name = "btnNextTenSeconds";
            this.btnNextTenSeconds.Size = new System.Drawing.Size(59, 52);
            this.btnNextTenSeconds.TabIndex = 3;
            this.btnNextTenSeconds.UseVisualStyleBackColor = true;
            this.btnNextTenSeconds.Click += new System.EventHandler(this.btnNextTenSeconds_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackgroundImage = global::Renapl.Properties.Resources.player_stop;
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStop.Location = new System.Drawing.Point(120, 2);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(59, 52);
            this.btnStop.TabIndex = 2;
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnPlayPause
            // 
            this.btnPlayPause.BackgroundImage = global::Renapl.Properties.Resources.player_play;
            this.btnPlayPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlayPause.Location = new System.Drawing.Point(61, 2);
            this.btnPlayPause.Name = "btnPlayPause";
            this.btnPlayPause.Size = new System.Drawing.Size(59, 52);
            this.btnPlayPause.TabIndex = 1;
            this.btnPlayPause.UseVisualStyleBackColor = true;
            this.btnPlayPause.Click += new System.EventHandler(this.btnPlayPause_Click);
            // 
            // btnPrevTenSeconds
            // 
            this.btnPrevTenSeconds.BackgroundImage = global::Renapl.Properties.Resources.player_backward;
            this.btnPrevTenSeconds.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPrevTenSeconds.Location = new System.Drawing.Point(2, 2);
            this.btnPrevTenSeconds.Name = "btnPrevTenSeconds";
            this.btnPrevTenSeconds.Size = new System.Drawing.Size(59, 52);
            this.btnPrevTenSeconds.TabIndex = 0;
            this.btnPrevTenSeconds.UseVisualStyleBackColor = true;
            this.btnPrevTenSeconds.Click += new System.EventHandler(this.btnPrevTenSeconds_Click);
            // 
            // formMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(569, 327);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.mediaPlayer);
            this.Controls.Add(this.pnlCaptionBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "formMain";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.formMain_FormClosed);
            this.Load += new System.EventHandler(this.formMain_Load);
            this.Resize += new System.EventHandler(this.formMain_Resize);
            this.pnlCaptionBar.ResumeLayout(false);
            this.pnlCaptionBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mediaPlayer)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.pnlTime.ResumeLayout(false);
            this.pnlTime.PerformLayout();
            this.pnlPlayMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCaptionBar;
        private Renapl.App.MediaPlayer.MediaPlayer mediaPlayer;
        private System.Windows.Forms.Button btnMinimizeWindow;
        private System.Windows.Forms.Button btnRestoreWindow;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Panel pnlTime;
        private System.Windows.Forms.Label lblCurrentTime;
        private System.Windows.Forms.Label lblFinishTime;
        private System.Windows.Forms.ProgressBar progMediaTime;
        private System.Windows.Forms.Panel pnlPlayMenu;
        private System.Windows.Forms.Button btnPrevTenSeconds;
        private System.Windows.Forms.Button btnPlayPause;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnNextTenSeconds;
    }
}

