QT += quick
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    ../../../../src/media/demuxer/FFMpegDemuxer.cpp \
    ../../../../src/media/demuxer/SyncDemuxer.cpp \
    ../../../../src/thread/Lock.cpp \
    ../../../../src/thread/Signal.cpp \
    ../../../../src/thread/ThreadRunner.cpp \
    ../../../../src/datetime/DateTime.cpp

HEADERS += \
    ../../../../src/media/demuxer/FFMpegDemuxer.h \
    ../../../../src/media/demuxer/SyncDemuxer.h \
    ../../../../src/thread/Lock.h \
    ../../../../src/thread/Signal.h \
    ../../../../src/thread/ThreadRunner.h \
    StdAfx.h \
    ../../../../src/datetime/DateTime.h

INCLUDEPATH += \
    . \
    ../../../../include/ffmpeg \
    ../../../../src

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS += -L../../../../lib/ffmpeg/linux/ -lavformat -lavcodec -lavutil
LIBS += -ldl

#INCLUDEPATH += $$PWD/../../../../lib/ffmpeg/android
#DEPENDPATH += $$PWD/../../../../lib/ffmpeg/android

#unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../lib/ffmpeg/android/libavcodec.a
