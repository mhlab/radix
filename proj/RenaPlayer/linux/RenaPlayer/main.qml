import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 360
    title: qsTr("Rena Player")

    DropArea {
        id: dropArea
        anchors.fill: parent

        onEntered: {
            drag.accept(true)
        }

        onDropped: {
            console.log(drop.urls[0])
        }
    }

    Rectangle {
        anchors.fill: parent
    }
}
