#include "stdafx.h"
#include "media\decoder\FFMpegDecoder.h"

using Radix::Media::FFMpegDecoder;

extern "C" __declspec(dllexport) uint64_t __stdcall createInstance() {
	FFMpegDecoder* decoder = new FFMpegDecoder();
	return (uint64_t)decoder;
}

extern "C" __declspec(dllexport) void __stdcall destroyInstance(uint64_t instance) {
	FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
	delete decoder;
}

extern "C" __declspec(dllexport) bool __stdcall openVideo(uint64_t instance, int codec) {
	FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
	return decoder->openVideo((Radix::Media::MediaCodec)codec);
}

extern "C" __declspec(dllexport) bool __stdcall openAudio(uint64_t instance, int codec, int samplerate, int channels, int channelLayout) {
	FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
	return decoder->openAudio((Radix::Media::MediaCodec)codec, samplerate, channels, channelLayout);
}

extern "C" __declspec(dllexport) void __stdcall close(uint64_t instance) {
	FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
	decoder->close();
}

extern "C" __declspec(dllexport) int __stdcall decodeVideoToYuv(uint64_t instance, char* src, int srcSize, char* dstY, char* dstU, char* dstV, int* width, int* height, int pixelfmt) {
	FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
	return decoder->decodeVideoToYuv(src, srcSize, dstY, dstU, dstV, width, height, (Radix::Media::MediaPixelFormat)pixelfmt);
}

extern "C" __declspec(dllexport) int __stdcall decodeAudio(uint64_t instance, char* src, int srcSize, char* dst, int* dstSize, int newChannels, int newChannelLayout) {
	FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
	return decoder->decodeAudio(src, srcSize, dst, dstSize, newChannels, newChannelLayout);
}

extern "C" __declspec(dllexport) bool __stdcall isOpen(uint64_t instance) {
	FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
	return decoder->isOpen();
}