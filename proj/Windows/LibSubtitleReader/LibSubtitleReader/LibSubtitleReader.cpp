#include "stdafx.h"
#include "reader\SmiFileReader.h"
#include <stdint.h>

using Radix::Reader::BaseSubtitleReader;
using Radix::Reader::SmiFileReader;

extern "C" __declspec(dllexport) uint64_t __stdcall createInstance(int type) {
	BaseSubtitleReader* reader = BaseSubtitleReader::createSubtitleReader((Radix::Reader::SubTitleFileType)type);
	if (reader == NULL)
		return 0;

	return (uint64_t)reader;
}

extern "C" __declspec(dllexport) void __stdcall destroyInstance(uint64_t instance) {
	BaseSubtitleReader* reader = (BaseSubtitleReader*)instance;
	delete reader;
}

extern "C" __declspec(dllexport) void __stdcall setListener(uint64_t instance, Radix::Reader::FOnOpenSubtitle openSubtitle, Radix::Reader::FOnRecvSubtitle recvSubtitle) {
	BaseSubtitleReader* reader = (BaseSubtitleReader*)instance;
	reader->setListener(openSubtitle, recvSubtitle);
}

extern "C" __declspec(dllexport) void __stdcall open(uint64_t instance, char* subtitleFile) {
	BaseSubtitleReader* reader = (BaseSubtitleReader*)instance;
	reader->open(subtitleFile);
}

extern "C" __declspec(dllexport) void __stdcall progress(uint64_t instance, double playTime) {
	BaseSubtitleReader* reader = (BaseSubtitleReader*)instance;
	reader->progress(playTime);
}

extern "C" __declspec(dllexport) void __stdcall reset(uint64_t instance) {
	BaseSubtitleReader* reader = (BaseSubtitleReader*)instance;
	reader->reset();
}