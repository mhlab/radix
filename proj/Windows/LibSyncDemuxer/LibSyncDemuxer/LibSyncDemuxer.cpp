#include "stdafx.h"
#include "media/demuxer/SyncDemuxer.h"

using Radix::Media::SyncDemuxer;
using Radix::Media::FOnCommandResult;
using Radix::Media::FOnCodecData;
using Radix::Media::FOnRecvMediaStream;

extern "C" __declspec(dllexport) uint64_t __stdcall createInstance() {
	SyncDemuxer* syncDemuxer = new SyncDemuxer();
	return (long)syncDemuxer;
}

extern "C" __declspec(dllexport) void __stdcall destroyInstance(uint64_t instance) {
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)instance;
	delete syncDemuxer;
}

extern "C" __declspec(dllexport) void __stdcall setListener(uint64_t instance, FOnCommandResult cmdResult, FOnCodecData codecData,
	FOnRecvMediaStream videoRecvStream, FOnRecvMediaStream audioRecvStream) {
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)instance;
	syncDemuxer->setListener(cmdResult, videoRecvStream, audioRecvStream, codecData, NULL, NULL);
}

extern "C" __declspec(dllexport) void __stdcall open(uint64_t instance, char* fileName) {
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)instance;
	syncDemuxer->open(fileName);
}

extern "C" __declspec(dllexport) void __stdcall close(uint64_t instance) {
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)instance;
	syncDemuxer->close();
}

extern "C" __declspec(dllexport) void __stdcall play(uint64_t instance) {
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)instance;
	syncDemuxer->play();
}

extern "C" __declspec(dllexport) void __stdcall stop(uint64_t instance) {
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)instance;
	syncDemuxer->stop();
}

extern "C" __declspec(dllexport) void __stdcall pause(uint64_t instance) {
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)instance;
	syncDemuxer->pause();
}

extern "C" __declspec(dllexport) void __stdcall seek(uint64_t instance, int seekSeconds) {
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)instance;
	syncDemuxer->seek(seekSeconds);
}

extern "C" __declspec(dllexport) bool __stdcall isSupportFile(uint64_t instance, char* fileName) {
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)instance;
	return syncDemuxer->isSupportFile(fileName);
}