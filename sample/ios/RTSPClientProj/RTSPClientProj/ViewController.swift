import UIKit

class ViewController: UIViewController {

    var rtspClient_ = RTSPClientProxy()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickStartLiveVideo(_ sender: Any) {
        rtspClient_.startLiveVideo(0, ip: "192.168.1.135", port: 554, abs_path: "snl/live/1/1")
    }
    
    @IBAction func onClickStopLiveVideo(_ sender: Any) {
        rtspClient_.stopLiveVideo(0)
    }
}

