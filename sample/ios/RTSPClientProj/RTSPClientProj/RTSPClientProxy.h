#import <Foundation/Foundation.h>
//#import "network/rtsp/client/RTSPClient.h"


@interface RTSPClientProxy : NSObject {
}

- (void)startLiveVideo:(int)connectionId ip:(NSString*)ip port:(int)port abs_path:(NSString*)abs_path;
- (void)stopLiveVideo:(int)connectionId;

@end
