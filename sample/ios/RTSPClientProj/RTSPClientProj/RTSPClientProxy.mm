#import "RTSPClientProxy.h"
#import "network/rtsp/client/RTSPClient.h"


@interface RTSPClientProxy()
@property Radix::Network::RTSPClient* rtspClient_;
@end

@implementation RTSPClientProxy

- (instancetype)init {
    self = [super init];
    self.rtspClient_ = NULL;
    return self;
}

- (void)startLiveVideo:(int)connectionId ip:(NSString*)ip port:(int)port abs_path:(NSString*)abs_path {
    if (self.rtspClient_ == NULL)
        self.rtspClient_ = new Radix::Network::RTSPClient();
    self.rtspClient_->startLiveVideo(connectionId, [ip UTF8String], port, [abs_path UTF8String], false);
}

- (void)stopLiveVideo:(int)connectionId {
    if (self.rtspClient_ == NULL)
        return;
    
    self.rtspClient_->stopLiveVideo(connectionId);
}

@end
