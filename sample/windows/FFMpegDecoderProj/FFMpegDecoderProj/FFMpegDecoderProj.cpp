#include "stdafx.h"
#include "media/decoder/FFMpegDecoder.h"
#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	Radix::Media::FFMpegDecoder videoDecoder;
	if (!videoDecoder.openVideo(Radix::Media::MEDIA_CODEC_H264)) {
		std::cout << "Fail to open h.264 codec" << std::endl;
		std::cin.get();
		return 0;
	}

	Radix::Media::FFMpegDecoder audioDecoder;
	if (!audioDecoder.openAudio(Radix::Media::MEDIA_CODEC_AC3, 48000, 6, 1551)) {
		std::cout << "Fail to open aac codec" << std::endl;
		std::cin.get();
		return 0;
	}

	FILE* recordFile = fopen("raw_audio.dat", "rb");
	int readsize = 1;
	int framesize = 0;
	Radix::Media::MediaCodec codec;
	char encodebuf[100000];
	int videoWidth = 0;
	int videoHeight = 0;
	char* ybuf = NULL;
	char* ubuf = NULL;
	char* vbuf = NULL;
	int audiosize = 0;
	while (readsize == 1) {
		readsize = fread(&framesize, sizeof(framesize), 1, recordFile);
		if (readsize != 1)
			break;
		readsize = fread(&codec, sizeof(codec), 1, recordFile);
		if (readsize != 1)
			break;
		readsize = fread(encodebuf, framesize, 1, recordFile);
		if (readsize != 1)
			break;
		if (codec == Radix::Media::MEDIA_CODEC_H264) {
			if (videoWidth == 0 && videoHeight == 0) {
				videoDecoder.decodeVideoToYuv(encodebuf, framesize, NULL, NULL, NULL, &videoWidth, &videoHeight, Radix::Media::MEDIA_PIXELFORMAT_YUV420P);
				if (videoWidth != 0 && videoHeight != 0) {
					ybuf = new char[videoWidth * videoHeight];
					ubuf = new char[(videoWidth / 2) * (videoHeight / 2)];
					vbuf = new char[(videoWidth / 2) * (videoHeight / 2)];
				}
			}
			if (videoDecoder.decodeVideoToYuv(encodebuf, framesize, ybuf, ubuf, vbuf, &videoWidth, &videoHeight, Radix::Media::MEDIA_PIXELFORMAT_YUV420P) == Radix::Media::FFMPEG_DECODER_SUCCESS) {
				static int index = 0;
				index++;
				char outputFile[6] = {0, };
				sprintf(outputFile, "yuv%d", index);
				FILE* output = fopen(outputFile, "wb");
				fwrite(ybuf, videoWidth * videoHeight, 1, output);
				fwrite(ubuf, (videoWidth / 2) * (videoHeight / 2), 1, output);
				fwrite(vbuf, (videoWidth / 2) * (videoHeight / 2), 1, output);
				fclose(output);
			}
		}
		else {
			static char* output = NULL;
			if (output == NULL) {
				if (audioDecoder.decodeAudio(encodebuf, framesize, NULL, &audiosize) == Radix::Media::FFMPEG_DECODER_SUCCESS) {
					output = new char[audiosize];
				}
			}
			audioDecoder.decodeAudio(encodebuf, framesize, output, &audiosize);
		}
	}
/*
	// audio info
	// sample rate: 44100
	// channels: 2
	// channelLayout: 3
	FILE* recordFile = fopen("raw.h264", "rb");
	int framesize = 0;
	int readsize = 0;
	int videoWidth = 0;
	int videoHeight = 0;
	char encodesrc[100000];
	char* decodebuf = NULL;
	while (true) {
		readsize = fread(&framesize, sizeof(int), 1, recordFile);
		if (readsize != 1)
			break;
		readsize = fread(encodesrc, framesize, 1, recordFile);
		videoDecoder.decode(encodesrc, framesize, decodebuf, &videoWidth, &videoHeight, Radix::Media::MEDIA_PIXELFORMAT_YUV420P);
		if (readsize != 1)
			break;
		if (videoWidth != 0 && videoHeight != 0 && decodebuf == NULL)
			decodebuf = new char[(int)(videoWidth * videoHeight * 1.5)];
	}
	if (decodebuf != NULL)
		delete[] decodebuf;
	fclose(recordFile);
*/
LAST:
	std::cout << "Finish." << std::endl;
	std::cin.get();
	return 0;
}

