#include "stdafx.h"
#include <iostream>
#include "network/rtsp/client/RTSPClient.h"

void onCommandResult(int connectionId, Radix::Network::RTSPCommand command, bool ret, char* param) {
	if (param == NULL)
		std::cout << command << ": " << ret << std::endl;
	else
		std::cout << command << ": " << ret << ", Param: " << param << std::endl;
}

void onRecvMediaStream(int connectionId, const char* media, int size, Radix::Network::RTSPCodec codec, unsigned int timestamp) {
	std::cout << "Recv MediaStream: Size-" << size << std::endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	Radix::Network::RTSPClient rtspClient;
	rtspClient.setListener(onCommandResult, onRecvMediaStream);
	rtspClient.startLiveVideo(0, "140.254.23.91", 554, "media2/ufsap/ufsap.mov", false);
	std::cin.get();

	rtspClient.stopLiveVideo(0);
	std::cin.get();

	return 0;
}

