#include "stdafx.h"
#include <iostream>
#include "media/demuxer/SyncDemuxer.h"
#include "string/StringEx.h"
#include "reader/BaseSubtitleReader.h"
#include "string/IconvImpl.h"
#include <clocale>
//#define _DUMP_RAW_VIDEO
//#define _DUMP_RAW_AUDIO

Radix::Media::SyncDemuxer syncDemuxer;
Radix::Reader::BaseSubtitleReader* reader;
FILE* dumpRawVideo_ = NULL;
FILE* dumpRawAudio_ = NULL;

void commandResult(Radix::Media::DemuxerCommand command, bool ret, void* param, void* obj) {
	std::cout << command << std::endl;
	if (command == Radix::Media::DEMUXER_COMMAND_SEEK)
		syncDemuxer.play();
};

void recvVideoMediaStream(const char* media, int mediaSize, int mediaCodec, int64_t presentationTime, void* obj) {
	if (media == NULL) {
		std::cout << "End of File or Error" << std::endl;
		return;
	}
	if (Radix::Media::isVideoCodec((Radix::Media::MediaCodec)mediaCodec)) {
		bool isKey = Radix::Media::isVideoKeyFrame((Radix::Media::MediaCodec)mediaCodec, media, mediaSize);
		if (isKey) {
			Radix::String::format("dd\n");
		}
	}
	if (dumpRawVideo_ != NULL) {
		fwrite(&mediaSize, sizeof(int), 1, dumpRawVideo_);
		fwrite(&mediaCodec, sizeof(int), 1, dumpRawVideo_);
		fwrite(media, mediaSize, 1, dumpRawVideo_);
	}
	std::cout << "Video Codec: " << mediaCodec << ", PTS: " << presentationTime << std::endl;
};

void recvAudioMediaStream(const char* media, int mediaSize, int mediaCodec, int64_t presentationTime, void* obj) {
	if (media == NULL) {
		std::cout << "End of File or Error" << std::endl;
		return;
	}
	if (dumpRawAudio_ != NULL) {
		fwrite(&mediaSize, sizeof(int), 1, dumpRawAudio_);
		fwrite(&mediaCodec, sizeof(int), 1, dumpRawAudio_);
		fwrite(media, mediaSize, 1, dumpRawAudio_);
	}
	std::cout << "Audio Codec: " << mediaCodec << ", PTS: " << presentationTime << std::endl;
};

void codecData(const char* videoCodecData, int videoCodecDataSize, char* audioCodecData, int audioCodecDataSize, void* obj) {
	std::cout << "Called CodecData" << std::endl;
}

void updatedTime(long playTime) {
	std::cout << "Play Time: " << playTime << std::endl;
}

int get_utf16_length(const char* subtitle) {
	short* pos = (short*)subtitle;
	int ret = 0;
	while (1) {
		if (*pos == 0x0000)
			return ret;
		ret += 2;
		pos++;
	}
}

void onRecvSubtitle(const char* subtitle) {
	std::cout << "SubTitle Length: " << strlen(subtitle) << std::endl;
	std::cout << "SubTitle: " << subtitle << std::endl;
}

void onOpenSubtitle(const char* subtitleFile, bool ret) {
	std::cout << "[Subtitle Open Result]: " << ret << std::endl;
	if (ret) {
		for (double i = 206991 / 1000; i < 273182 / 1000; i++)
			reader->progress(i);
		reader->reset();
		for (int i = 30 * 60; i < 10000; i++)
			reader->progress(i);
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
//	reader = Radix::Reader::BaseSubtitleReader::createSubtitleReader(Radix::Reader::SUBTITLE_FILE_SMI);
//	reader->setListener(onOpenSubtitle, onRecvSubtitle);
//	reader->open("C:\\Users\\mjh\\Downloads\\K.u.n.g.F.u.P.a.n.d.a.2008.x264.AC3-Zoom.smi");
//	for (int i = 206991; i < 273182; i++)
//		reader->progress(i);
//	reader->move(60 * 60);

//	std::string videoFile = Radix::String::format("C:\\Users\\mjh\\Documents\\git\\lmh\\radix\\sample\\windows\\FFMpegDemuxerProj\\Debug\\rena.mp4");
//	std::string videoFile = Radix::String::format("C:\\Users\\mjh\\Music\\���Ը� - �������� (M. Li).mp3");
//	std::string videoFile = Radix::String::format("C:\\Users\\mjh\\Downloads\\Gods.of.Egypt.2016.720p.BRRip.X264.AC3-EVO.mkv");
//	std::string videoFile = Radix::String::format("C:\\Users\\mjh\\Downloads\\1486975945716.mp4");
//	std::string videoFile = Radix::String::format("C:\\Users\\mjh\\Downloads\\K.u.n.g.F.u.P.a.n.d.a.2008.x264.AC3-Zoom.avi");
	std::string videoFile = Radix::String::format("C:\\Users\\mjh\\Downloads\\rena_1.mp4");
//	std::string videoFile = Radix::String::format("C:\\Users\\mjh\\Downloads\\Luck-Key.2016.KOREAN.720p.BluRay.x264-FOXM.mp4");
//	std::string videoFile = Radix::String::format("C:\\Users\\mjh\\Downloads\\03 �� �׷� ����(Duet).mp3");

#ifdef _DUMP_RAW_VIDEO
	dumpRawVideo_ = fopen("raw_video.dat", "wb");
#endif
#ifdef _DUMP_RAW_AUDIO
	dumpRawAudio_ = fopen("raw_audio.dat", "wb");
#endif

	syncDemuxer.setListener(commandResult, recvVideoMediaStream, recvAudioMediaStream, codecData, updatedTime, NULL);
	syncDemuxer.isSupportFile(videoFile.c_str());
	syncDemuxer.open(videoFile.c_str());
	syncDemuxer.play();

	std::cin.get();
	if (dumpRawVideo_ != NULL)
		fclose(dumpRawVideo_);
	if (dumpRawAudio_ != NULL)
		fclose(dumpRawAudio_);
	return 0;
}

