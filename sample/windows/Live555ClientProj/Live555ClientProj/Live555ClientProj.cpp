#include "stdafx.h"
#include "network/rtsp/client_live555/Live555ClientContainer.h"
#include <iostream>

FILE* streamFile = NULL;

void onCommandResult(int streamNo, Radix::Network::RTSPCommand command, bool cmdResult, char* param) {
}

void onRecvMediaStream(int streamNo, const char* media, int mediaSize, Radix::Network::RTSPCodec codec, int64_t presentationTime) {
	if (streamFile == NULL)
		fopen_s(&streamFile, "h264_rawstream_320x240.raw", "wb");
	fwrite(media, mediaSize, 1, streamFile);
}


int _tmain(int argc, _TCHAR* argv[])
{
	Radix::Network::Live555ClientContainer clientContainer_;
	clientContainer_.setListener(onCommandResult, onRecvMediaStream);
	clientContainer_.startLiveVideo(0, "rtsp://140.254.23.91/media2/ufsap/ufsap.mov", NULL, NULL);
//	clientContainer_.startLiveVideo(0, "rtsp://freja.hiof.no:1935/rtplive/_definst_/hessdalen03.stream", NULL, NULL);
//	clientContainer_.startLiveVideo(0, "rtsp://freja.hiof.no:1935/rtplive/_definst_/hessdalen03.stream", NULL, NULL);
//	clientContainer_.stopLiveVideo(0);

	std::cin.get();
	if (streamFile != NULL)
		fclose(streamFile);
	return 0;
}

