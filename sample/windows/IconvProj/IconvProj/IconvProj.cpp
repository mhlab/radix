#include "stdafx.h"
#include "string\IconvImpl.h"
#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	try {
		Radix::String::IconvImpl iconvImpl;
		/*
		char* src_test_1 = "한글과 유니코드";
		size_t srclen = strlen(src_test_1);
		char* dst_test_2 = new char[srclen + 1];
		size_t dstlen = sizeof(dst_test_2);
		memset(dst_test_2, 0x00, dstlen);
		*/
		char* src = "123 한가지";
		size_t srclen = strlen(src);
		size_t dstlen = srclen * 2;
		char* dst = new char[dstlen];
		memset(dst, 0x00, dstlen);
		char* srcPtr = src;
		char* dstPtr = dst;

		if (!iconvImpl.convert("euc-kr", "utf-8", &srcPtr, srclen, &dstPtr, dstlen))
			throw "Error: Convert";

		std::cout << "Convert: " << dstPtr << std::endl;
	}
	catch (const char* err) {
		std::cout << err << std::endl;
	}

	std::cin.get();
	return 0;
}

