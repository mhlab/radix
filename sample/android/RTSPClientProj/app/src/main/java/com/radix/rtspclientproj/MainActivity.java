package com.radix.rtspclientproj;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private RTSPClient rtspClient_ = new RTSPClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnStartLiveVideo = (Button)findViewById(R.id.btnStartLiveVideo);
        btnStartLiveVideo.setOnClickListener(this);
        Button btnStopLiveVideo = (Button)findViewById(R.id.btnStopLiveVideo);
        btnStopLiveVideo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnStartLiveVideo:
                rtspClient_.startLiveVideo(0, "192.168.1.135", 554, "snl/live/1/1", false);
                break;
            case R.id.btnStopLiveVideo:
                rtspClient_.stopLiveVideo(0);
                break;
        }
    }
}
