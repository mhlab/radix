package com.radix.rtspclientproj;

/**
 * Created by lovechamisl on 2017-09-27.
 */

public class RTSPClient {
    private native long createInstance();
    private native void destroyInstance(long instance);
    private native void startLiveVideo(long instance, int connectionId, String ip, int port, String abs_path, boolean useRtpTcp);
    private native void stopLiveVideo(long instance, int connectionId);

    private long instance_ = 0;

    public RTSPClient() {
        instance_ = createInstance();
    }

    public void destroyInstance() {
        if (instance_ == 0)
            return;

        destroyInstance(instance_);
        instance_ = 0;
    }

    public void startLiveVideo(int connectionId, String ip, int port, String abs_path, boolean useRtpTcp) {
        if (instance_ == 0)
            return;

        startLiveVideo(instance_, connectionId, ip, port, abs_path, useRtpTcp);
    }

    public void stopLiveVideo(int connectionId) {
        if (instance_ == 0)
            return;

        stopLiveVideo(instance_, connectionId);
    }

    static {
        System.loadLibrary("RTSPClient");
    }
}
