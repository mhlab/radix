#include <jni.h>
#include "network/rtsp/client/RTSPClient.h"

extern "C" JNIEXPORT jlong JNICALL
Java_com_radix_rtspclientproj_RTSPClient_createInstance(JNIEnv *env, jobject instance) {
    Radix::Network::RTSPClient* rtspClient = new Radix::Network::RTSPClient();
    return (jlong)rtspClient;
}

extern "C" JNIEXPORT void JNICALL
Java_com_radix_rtspclientproj_RTSPClient_destroyInstance(JNIEnv *env, jobject instance_,
                                                            jlong instance) {
    Radix::Network::RTSPClient* rtspClient = (Radix::Network::RTSPClient*)instance;
    delete rtspClient;
}

extern "C" JNIEXPORT void JNICALL
Java_com_radix_rtspclientproj_RTSPClient_startLiveVideo(JNIEnv *env, jobject instance_, jlong instance,
                                                        jint connectionId, jstring ip_, jint port,
                                                        jstring abs_path_, jboolean useRtpTcp) {
    Radix::Network::RTSPClient* rtspClient = (Radix::Network::RTSPClient*)instance;

    const char *ip = env->GetStringUTFChars(ip_, 0);
    const char *abs_path = env->GetStringUTFChars(abs_path_, 0);

    rtspClient->startLiveVideo(connectionId, ip, port, abs_path, useRtpTcp);

    env->ReleaseStringUTFChars(ip_, ip);
    env->ReleaseStringUTFChars(abs_path_, abs_path);
}

extern "C" JNIEXPORT void JNICALL
Java_com_radix_rtspclientproj_RTSPClient_stopLiveVideo(JNIEnv *env, jobject instance_,
                                                           jlong instance, jint connectionId) {
    Radix::Network::RTSPClient* rtspClient = (Radix::Network::RTSPClient*)instance;
    rtspClient->stopLiveVideo(connectionId);
}