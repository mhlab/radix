package com.radix.mediacodecproj;

import android.annotation.TargetApi;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Surface;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import media.decoder.MediaCodecDecoder;

public class MainActivity extends AppCompatActivity implements VideoDisplay.VideoDisplayListener, MediaCodecDecoder.MediaCodecDecoderListener {
    private Surface videoSurface_ = null;
    private MediaCodecDecoder videoDecoder_ = null;
    private MediaCodecDecoder audioDecoder_ = null;
    private AudioTrack audioTrack_ = null;

    private static final int MEDIACODEC_H264 = 28;
    private static final int MEDIACODEC_AAC = 86018;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        VideoDisplay videoDisplay = (VideoDisplay)findViewById(R.id.surfaceView);
        videoDisplay.setListener(this);
    }

    @Override
    public void changedSurface(final Surface surface) {
        if (videoSurface_ == null && surface != null) {
            new Thread(new Runnable() {
                @TargetApi(Build.VERSION_CODES.M)
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void run() {
                    try {
                        File rawFile = new File("/user/raw_h264_aac.raw");
                        if (!rawFile.exists())
                            return;

                        FileInputStream inputStream = new FileInputStream(rawFile);
                        byte[] framesizeArr = new byte[4];
                        byte[] codecArr = new byte[4];
                        byte[] frame;
                        int size = 0;
                        int framesize;
                        int codec;

                        int bufsize = AudioTrack.getMinBufferSize(44100, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
                        audioTrack_ = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, bufsize, AudioTrack.MODE_STREAM);
                        /*
                        audioTrack_ = new AudioTrack.Builder()
                                .setAudioAttributes(new AudioAttributes.Builder()
                                    .setUsage(AudioAttributes.USAGE_MEDIA)
                                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                    .build())
                                .setAudioFormat(new AudioFormat.Builder()
                                    .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                                    .setSampleRate(44100)
                                    .setChannelIndexMask(AudioFormat.CHANNEL_OUT_STEREO)
                                    .build())
                                .setBufferSizeInBytes(bufsize)
                                .setTransferMode(AudioTrack.MODE_STREAM)
                                .build();
                        */
                        audioTrack_.play();

                        while (size != -1) {
                            size = inputStream.read(framesizeArr);
                            if (size == -1)
                                break;
                            framesize = ByteBuffer.wrap(framesizeArr).order(ByteOrder.LITTLE_ENDIAN) .getInt();
                            size = inputStream.read(codecArr);
                            if (size == -1)
                                break;
                            codec = ByteBuffer.wrap(codecArr).order(ByteOrder.LITTLE_ENDIAN).getInt();
                            frame = new byte[framesize];
                            size = inputStream.read(frame);
                            if (size == -1)
                                break;
                            if (codec == MEDIACODEC_H264) {
                                if (videoDecoder_ == null)
                                    videoDecoder_ = new MediaCodecDecoder();
                                if (!videoDecoder_.isOpen() && !videoDecoder_.openVideo(MediaCodecDecoder.MEDIA_CODEC_H264, frame, 1280, 720, videoSurface_))
                                    continue;
                                if (!videoDecoder_.push(frame, framesize, 0))
                                    continue;
                                videoDecoder_.decode(true);
                                Thread.sleep(33);
                            }
                            else {
                                if (audioDecoder_ == null) {
                                    audioDecoder_ = new MediaCodecDecoder();
                                    audioDecoder_.setListener(MainActivity.this);
                                }
                                if (!audioDecoder_.isOpen() && !audioDecoder_.openAudio(MediaCodecDecoder.MEDIA_CODEC_AAC, 44100, 2, 1))
                                    continue;
                                if (!audioDecoder_.push(frame, framesize, 0))
                                    continue;

                                ByteBuffer decodeBuf = audioDecoder_.decode();
                                if (decodeBuf == null)
                                    continue;

                                byte[] decodeStream = new byte[audioDecoder_.mediaBufferSize()];
                                decodeBuf.get(decodeStream);
                                decodeBuf.clear();
                                audioTrack_.write(decodeStream, 0, audioDecoder_.mediaBufferSize());
                                audioDecoder_.releaseBuffer(false);
                            }
                        }
                        inputStream.close();
                        audioTrack_.stop();
                        audioTrack_.release();
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        videoSurface_ = surface;
    }

    @Override
    public void onChangedOutputFormatChanged(MediaFormat format) {
        if (audioTrack_ != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                if (format.getString(MediaFormat.KEY_MIME).startsWith("audio"))
                    audioTrack_.setPlaybackRate(format.getInteger(MediaFormat.KEY_SAMPLE_RATE));
            }
        }
    }
}
