package com.radix.mediacodecproj;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.SurfaceHolder;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class VideoDisplay extends GLSurfaceView {
    public interface VideoDisplayListener {
        void changedSurface(Surface surface);
    }

    private VideoDisplayListener listener_ = null;

    private void notifyChangedHolder(SurfaceHolder holder) {
        if (listener_ == null)
            return;

        if (holder == null)
            listener_.changedSurface(null);
        else
            listener_.changedSurface(holder.getSurface());
    }

    private void init() {
        setEGLContextClientVersion(2);
        setRenderer(new Renderer() {
            @Override
            public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            }

            @Override
            public void onSurfaceChanged(GL10 gl, int width, int height) {
                gl.glViewport(0, 0, width, height);
            }

            @Override
            public void onDrawFrame(GL10 gl) {
            }
        });
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    public VideoDisplay(Context context) {
        super(context);
        init();
    }

    public VideoDisplay(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setListener(VideoDisplayListener listener) {
        listener_ = listener;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
//        super.surfaceCreated(holder);
        notifyChangedHolder(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
//        super.surfaceChanged(holder, format, w, h);
        notifyChangedHolder(holder);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
//        super.surfaceDestroyed(holder);
        notifyChangedHolder(holder);
    }
}
