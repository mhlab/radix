package com.radix.fileexplorerproj;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ui.FileExplorer.FileExploreView;

public class MainActivity extends AppCompatActivity {
    private FileExploreView fileExplorerView_ = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fileExplorerView_ = (FileExploreView)findViewById(R.id.fileExplorerView);
        fileExplorerView_.setDirectory("/user");
    }

    @Override
    protected void onDestroy() {
        fileExplorerView_.release();
        super.onDestroy();
    }
}
