﻿using System;

namespace Radix.Base {
    public class BaseDisposeObject: IDisposable {
        private bool disposed_ = false;

        protected virtual void Dispose(bool disposing) {
            if (disposed_)
                return;
            if (disposing) 
                internalDispose();
            disposed_ = true;
        }

        protected virtual void internalDispose() {
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~BaseDisposeObject() {
            Dispose(false);
        }
    }
}