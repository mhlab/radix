﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

using Radix.Base;

namespace Radix.Reader {
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void FOnOpenSubtitle(string subtitleFile, bool ret);
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void FOnRecvSubtitle(IntPtr subtitle, int length);

    public class SubtitleReader: BaseDisposeObject {
        public interface SubtitleReaderListener {
            void onOpenSubtitle(string subtitleFile, bool ret);
            void onRecvSubtitle(string subtitle);
        }

        public enum SubtitleFileType {
            SUBTITLE_FILE_SMI = 0,
            SUBTITLE_FILE_SRT = 1
        }

        [DllImport("LibSubtitleReader.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static ulong createInstance(int subtitleType);
        [DllImport("LibSubtitleReader.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void destroyInstance(ulong instance);
        [DllImport("LibSubtitleReader.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void setListener(ulong instance, FOnOpenSubtitle openSubtitle, FOnRecvSubtitle recvSubtitle);
        [DllImport("LibSubtitleReader.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void open(ulong instance, String fileName);
        [DllImport("LibSubtitleReader.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void progress(ulong instance, double playTime);
        [DllImport("LibSubtitleReader.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void reset(ulong instance);

        private ulong instance_ = 0;
        private String url_ = null;
        private SubtitleReaderListener listener_ = null;
        private GCHandle openSubtitleHandle_;
        private GCHandle recvSubtitleHandle_;

        private void onOpenSubtitle(string subtitleFile, bool ret) {
            if (listener_ != null)
                listener_.onOpenSubtitle(subtitleFile, ret);
        }

        private void onRecvSubtitle(IntPtr subtitle, int length) {
            byte[] arr = null;
            if (length > 0) {
                arr = new byte[length];
                Marshal.Copy(subtitle, arr, 0, length);
            }
            String decodeStr = "";
            if (arr != null)
                decodeStr = System.Text.Encoding.BigEndianUnicode.GetString(arr);
            if (listener_ != null)
                listener_.onRecvSubtitle(decodeStr);
        }

        protected override void internalDispose() {
            if (instance_ == 0)
                return;

            destroyInstance(instance_);
            instance_ = 0;
            openSubtitleHandle_.Free();
            recvSubtitleHandle_.Free();
        }

        public SubtitleReader(SubtitleFileType type, SubtitleReaderListener listener, String url) {
            instance_ = createInstance((int)type);
            listener_ = listener;
            url_ = url;
            if (instance_ == 0)
                return;

            FOnOpenSubtitle openSubtitle = new FOnOpenSubtitle(onOpenSubtitle);
            FOnRecvSubtitle recvSubtitle = new FOnRecvSubtitle(onRecvSubtitle);
            openSubtitleHandle_ = GCHandle.Alloc(openSubtitle);
            recvSubtitleHandle_ = GCHandle.Alloc(recvSubtitle);
            setListener(instance_, openSubtitle, recvSubtitle);
        }

        public void open() {
            if (instance_ == 0 || url_ == null)
                return;

            open(instance_, url_);
        }

        public void progress(double playTime) {
            if (instance_ == 0)
                return;

            progress(instance_, playTime);
        }

        public static SubtitleReader createSubtitleReader(String url, SubtitleReaderListener listener) {
            String[] fileTypes = { "smi", "srt" };
            String subtitleFile;
            for (int i = 0; i < fileTypes.Length; i++) {
                subtitleFile = url;
                subtitleFile = subtitleFile.Replace(subtitleFile.Substring(subtitleFile.LastIndexOf(".") + 1), fileTypes[i]);
                if (File.Exists(subtitleFile)) {
                    return new SubtitleReader((SubtitleFileType)i, listener, subtitleFile);
                }
            }

            return null;
        }
    }
}