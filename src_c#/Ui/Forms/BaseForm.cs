﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Radix.Ui.Forms {
    public class BaseForm: Form {
        [DllImportAttribute("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        private static extern bool ReleaseCapture();
        private const int WM_NCLBUTTONDOWN = 0xA1;
        private const int WM_NCHITTEST = 0x84;
        private const int HT_CAPTION = 0x2;
        private const int HT_BOTTOMRIGHT = 17;

        private Point mouseDownPt_ = new Point();
        private bool mouseDown_ = false;
        private Point formLocation_ = new Point();
        private Point resizeAreaPt_ = new Point(-1, -1);

        private void controlForMovableForm_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                mouseDownPt_ = MousePosition;
                formLocation_ = Location;
                mouseDown_ = true;
            }
        }

        private void controlForMovableForm_MouseMove(object sender, MouseEventArgs e) {
            if (mouseDown_) {
                int offsetX = MousePosition.X - mouseDownPt_.X;
                int offsetY = MousePosition.Y - mouseDownPt_.Y;
                Location = new Point(formLocation_.X + offsetX, formLocation_.Y + offsetY);
            }
        }

        private void controlForMovableForm_MouseUp(object sender, MouseEventArgs e) {
            mouseDown_ = false;
        }

        private void controlForRestoreForm_DoubleClick(object sender, EventArgs e) {
            if (WindowState == FormWindowState.Normal)
                WindowState = FormWindowState.Maximized;
            else if (WindowState == FormWindowState.Maximized)
                WindowState = FormWindowState.Normal;
        }

        protected override void WndProc(ref Message m) {
            if (m.Msg == WM_NCHITTEST && resizeAreaPt_.X > -1 && resizeAreaPt_.Y > -1) {
                Point curPt = new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16);
                curPt = PointToClient(curPt);
                if (curPt.X >= resizeAreaPt_.X && curPt.Y >= resizeAreaPt_.Y) {
                    m.Result = (IntPtr)HT_BOTTOMRIGHT;
                    return;
                }
            }
            base.WndProc(ref m);
        }

        protected void addControlForMovableForm(Control control) {
            control.MouseDown += controlForMovableForm_MouseDown;
            control.MouseMove += controlForMovableForm_MouseMove;
            control.MouseUp += controlForMovableForm_MouseUp;
        }

        protected void addControlForRestoreForm(Control control) {
            control.DoubleClick += controlForRestoreForm_DoubleClick;
        }

        protected void internalDoubleClickForm() {
            controlForRestoreForm_DoubleClick(this, null);
        }

        protected void addResizeAreaPoint(int x, int y) {
            resizeAreaPt_ = new Point(x, y);
        }

        protected void removeResizeAreaPoint() {
            resizeAreaPt_ = new Point(-1, -1);
        }
    }
}