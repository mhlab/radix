﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Radix.Ui.Controls {
    public class OutlineLabel: Label {
        private Brush backColorBrush_ = null;
        private Brush foreColorBrush_ = null;
        private Pen outlinePen_ = null;
        private float outlineWidth_ = 1.0f;
        private Color outlineColor_ = Color.Black;

        protected override void OnPaint(PaintEventArgs e) {
            if (backColorBrush_ != null)
                e.Graphics.FillRectangle(backColorBrush_, ClientRectangle);
            GraphicsPath gp = new GraphicsPath();
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            gp.AddString(Text, Font.FontFamily, (int)Font.Style, Font.Size, ClientRectangle, sf);
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            if (outlinePen_ != null)
                e.Graphics.DrawPath(outlinePen_, gp);
            if (foreColorBrush_ != null)
                e.Graphics.FillPath(foreColorBrush_, gp);
            sf.Dispose();
            gp.Dispose();
        }

        public override Color BackColor {
	        get { 
		       return base.BackColor;
	        }
	        set { 
		        base.BackColor = value;
                backColorBrush_ = new SolidBrush(value);
	        }
        }

        public override Color ForeColor {
            get {
                return base.ForeColor;
            }
            set
            {
                base.ForeColor = value;
                foreColorBrush_ = new SolidBrush(value);
            }
        }

        public Color OutlineColor { 
            get {
                return outlineColor_;
            }
            set {
                outlinePen_ = new Pen(value, outlineWidth_);
            }
        }
        public float OutlineWidth { 
            get {
                return outlineWidth_;
            }
            set {
                outlineWidth_ = value;
                outlinePen_ = new Pen(outlineColor_, outlineWidth_);
            }
        }
    }
}