﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace Radix.Ui.Controls {
    public class TransparentPanel: Panel {
        private const int WS_EX_TRANSPARENT = 0x20;
        private int opacity_ = 50;

        protected override CreateParams CreateParams {
            get {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= WS_EX_TRANSPARENT;
                return cp;
            }
        }

        protected override void OnPaint(PaintEventArgs e) {
            using (var brush = new SolidBrush(Color.FromArgb(opacity_ * 255 / 100, BackColor))) {
                e.Graphics.FillRectangle(brush, ClientRectangle);
            }
            base.OnPaint(e);
        }

        public TransparentPanel() {
            SetStyle(ControlStyles.Opaque, true);
        }

        public int Opacity {
            get {
                return opacity_;
            }
            set {
                if (value < 0 || value > 100)
                    return;
                opacity_ = value;
            }
        }
    }
}