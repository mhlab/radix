﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Radix.Memory
{
    public class MemoryPool {
        public class MemoryPoolItem {
            public byte[] memory_ = null;
            public UInt32 memorySize_ = 0;
            public bool alloced_ = false;
        }

        private UInt32 poolSize_ = 0;
        private UInt32 allocedSize_ = 0;
        private List<MemoryPoolItem> memoryPoolItemList_ = new List<MemoryPoolItem>();

        public MemoryPool(UInt32 poolSize) {
            poolSize_ = poolSize;
        }

        public MemoryPoolItem alloc(UInt32 allocSize) {
            MemoryPoolItem poolItem = null;
            for (int i = 0; i < memoryPoolItemList_.Count; i++) {
                poolItem = memoryPoolItemList_[i];
                if (!poolItem.alloced_ && allocSize <= poolItem.memory_.Length) {
                    poolItem.alloced_ = true;
                    poolItem.memorySize_ = allocSize;
                    return poolItem;
                }
            }
            if (allocedSize_ + allocSize > poolSize_)
                return null;

            allocedSize_ += allocSize;
            poolItem = new MemoryPoolItem();
            poolItem.memory_ = new byte[allocSize];
            poolItem.memorySize_ = allocSize;
            poolItem.alloced_ = true;
            memoryPoolItemList_.Add(poolItem);

            return poolItem;
        }

        public void dealloc(MemoryPoolItem poolItem) {
            poolItem.alloced_ = false;
        }
    }
}
