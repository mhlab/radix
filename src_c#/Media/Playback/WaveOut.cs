﻿using System;
using System.Threading;
using System.Runtime.InteropServices;

using Radix.Base;

// reference site
//http://www.kavenblog.com/2015/08/3970
//https://msdn.microsoft.com/en-us/library/windows/desktop/dd317593(v=vs.85).aspx
//https://www.codeproject.com/Articles/866347/Streaming-Audio-to-the-WaveOut-Device

namespace Radix.Media.Playback {
    public class WaveBuffer: BaseDisposeObject {
        private IntPtr waveOut_ = IntPtr.Zero;
        private WaveFormApi.WaveHdr waveHdr_ = new WaveFormApi.WaveHdr();
        private GCHandle headerDataHandle_;
        private int writtenSize_ = 0;
        private bool isWriting_ = false;

        protected override void internalDispose() {
            if (waveHdr_.lpData != IntPtr.Zero)
                headerDataHandle_.Free();
            if (waveOut_ != IntPtr.Zero)
                WaveFormApi.waveOutUnprepareHeader(waveOut_, ref waveHdr_, Marshal.SizeOf(waveHdr_));
        }

        public WaveBuffer() {
            waveHdr_.lpData = IntPtr.Zero;
        }

        public void init(IntPtr wave, int size) {
            waveOut_ = wave;
            byte[] headerData = new byte[size];
            headerDataHandle_ = GCHandle.Alloc(headerData, GCHandleType.Pinned);
            waveHdr_.lpData = headerDataHandle_.AddrOfPinnedObject();
            waveHdr_.dwBufferLength = size;
            waveHdr_.dwBytesRecorded = 0;
            waveHdr_.dwUser = (IntPtr)GCHandle.Alloc(this);
            waveHdr_.dwFlags = 0;
            waveHdr_.dwLoops = 0;
            waveHdr_.lpNext = IntPtr.Zero;
            waveHdr_.reserved = 0;
            WaveFormApi.waveOutPrepareHeader(wave, ref waveHdr_, Marshal.SizeOf(waveHdr_));
        }

        public bool write(byte[] data, int dataSize, int dataOffset, ref int writtenSize) {
            writtenSize = Math.Min(waveHdr_.dwBufferLength - writtenSize_, dataSize);
            Marshal.Copy(data, dataOffset, waveHdr_.lpData, writtenSize);
            writtenSize_ += writtenSize;
            if (writtenSize_ == (waveHdr_.dwBufferLength)) {
                writtenSize_ = 0;
                WaveFormApi.waveOutWrite(waveOut_, ref waveHdr_, Marshal.SizeOf(waveHdr_));
                isWriting_ = true;
                return true;
            }

            return false;
        }

        public void writtenCompletely() {
            isWriting_ = false;
        }

        public bool isWriting() {
            return isWriting_;
        }
    }

    public class WaveOut: BaseDisposeObject {
        private IntPtr waveOut_ = IntPtr.Zero;
        private WaveBuffer[] waveBuffers_ = null;
        private AutoResetEvent waveEvent_ = new AutoResetEvent(false);
        private bool noBuffer_ = false;
        private int curWaveBufferIndex_ = 0;

        private void waveOutDone(IntPtr hdrvr, int uMsg, int dwUser, ref WaveFormApi.WaveHdr wavhdr, int dwParam2) {
            if (uMsg == WaveFormApi.MM_WOM_DONE) {
                waveEvent_.Set();
                GCHandle handle = (GCHandle)wavhdr.dwUser;
                ((WaveBuffer)handle.Target).writtenCompletely();
            }
        }

        protected override void internalDispose() {
            if (waveBuffers_ != null)
                foreach (WaveBuffer buffer in waveBuffers_)
                    buffer.Dispose();
            if (waveOut_ != IntPtr.Zero)
                WaveFormApi.waveOutClose(waveOut_);
        }

        public WaveOut(WaveFormApi.WaveFormat waveFormat, int bufferCount, int bufferSize) {
            WaveFormApi.WaveDelegate waveDone = new WaveFormApi.WaveDelegate(waveOutDone);
            GCHandle.Alloc(waveDone);
            WaveFormApi.waveOutOpen(out waveOut_, -1, waveFormat, waveDone, 0, WaveFormApi.CALLBACK_FUNCTION);
            waveBuffers_ = new WaveBuffer[bufferCount];
            foreach (WaveBuffer buffer in waveBuffers_)
                buffer.init(waveOut_, bufferSize);
        }

        public void write(byte[] data, int dataSize) {
            int writtenSize;
            int dataOffset = 0;
            while (dataSize > 0) {
                if (noBuffer_) {
                    waveEvent_.WaitOne();
                    noBuffer_ = false;
                }
                writtenSize = 0;
                if (waveBuffers_[curWaveBufferIndex_].write(data, dataSize, dataOffset, ref writtenSize))
                {
                    noBuffer_ = true;
                    curWaveBufferIndex_ = (curWaveBufferIndex_ + 1) % waveBuffers_.Length;
                    dataSize -= writtenSize;
                    dataOffset += writtenSize;
                }
                else
                    break;
            }
        }
    }
}