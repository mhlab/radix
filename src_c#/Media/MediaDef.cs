﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Radix.Media
{
    [StructLayout(LayoutKind.Sequential)]
    public struct MediaAttribution {
		public uint duration;
        public int video_codec;
        public int video_width;
        public int video_height;
        public int video_timebase_den;
        public int video_timebase_num;
        public int video_bitrate;
        public int audio_codec;
        public int audio_sample_rate;
        public int audio_channelLayout;
        public int audio_channels;
        public int audio_profile;
        public int audio_timebase_den;
        public int audio_timebase_num;

        public double audio_timebase;
        public double video_timebase;

        public void init() {
            audio_timebase = audio_timebase_num * 1.0f / audio_timebase_den;
            video_timebase = video_timebase_num * 1.0f / video_timebase_den;
        }
    }

    public enum MediaCodec {
        MEDIA_CODEC_UNKNOWN = 0,
        MEDIA_CODEC_MJPEG = 8,
        MEDIA_CODEC_H264 = 28,
        MEDIA_CODEC_MP3 = 86017,
        MEDIA_CODEC_AAC = 86018,
        MEDIA_CODEC_AC3 = 86019
    };

    public enum MediaPixelFormat {
        MEDIA_PIXELFORMAT_YUV420P = 0
    };

    public enum AudioChannels {
        FRONT_LEFT = 0x00000001,
        FRONT_RIGHT = 0x00000002,
        FRONT_CENTER = 0x00000004
    }

    public enum AudioChannelLayout {
        MONO = AudioChannels.FRONT_CENTER,
        STEREO = AudioChannels.FRONT_LEFT | AudioChannels.FRONT_RIGHT
    }

    public class MediaDef {
        public static bool isVideoCodec(MediaCodec codec) {
            return (codec >= MediaCodec.MEDIA_CODEC_MJPEG && codec <= MediaCodec.MEDIA_CODEC_H264);
        }
    }
}
