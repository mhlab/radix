﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using Radix.Base;

namespace Radix.Media.Decoder {
    public class MediaDecoder: BaseDisposeObject {
        public static int FFMPEG_DECODER_SUCCESS = 0;
        public static int FFMPEG_DECODER_FAIL = -1;
        public static int FFMPEG_DECODER_NOT_SUPPORT_PIXELFORMAT = -2;
        public static int FFMPEG_DECODER_NOT_INITIALIZE = -3;
        public static int FFMPEG_DECODER_INVALID_ARGUMENTS = -4;

        [DllImport("LibMediaDecoder.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static ulong createInstance();
        [DllImport("LibMediaDecoder.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void destroyInstance(ulong instance);
        [DllImport("LibMediaDecoder.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static bool openVideo(ulong instance, int codec);
        [DllImport("LibMediaDecoder.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static bool openAudio(ulong instance, int codec, int samplerate, int channels, int channelLayout);
        [DllImport("LibMediaDecoder.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void close(ulong instance);
        [DllImport("LibMediaDecoder.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static int decodeVideoToYuv(ulong instance, byte[] src, int srcsize, byte[] dstY, byte[] dstU, byte[] dstV, ref int width, ref int height, int pixfmt);
        [DllImport("LibMediaDecoder.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static int decodeAudio(ulong instance, byte[] src, int srcsize, byte[] dst, ref int dstsize, int newChannels, int newChannelLayout);
        [DllImport("LibMediaDecoder.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static bool isOpen(ulong instance);

        private ulong instance_ = 0;
        private MediaCodec codec_ = MediaCodec.MEDIA_CODEC_UNKNOWN;

        protected override void internalDispose() {
            if (instance_ == 0)
                return;

            destroyInstance(instance_);
            instance_ = 0;
        }

        public MediaDecoder() {
            instance_ = createInstance();
        }

        public bool openVideo(MediaCodec codec) {
            if (instance_ == 0)
                return false;

            codec_ = codec;
            return openVideo(instance_, (int)codec);
        }

        public bool openAudio(MediaCodec codec, int samplerate, int channels, int channelLayout) {
            if (instance_ == 0)
                return false;

            codec_ = codec;
            return openAudio(instance_, (int)codec, samplerate, channels, channelLayout);
        }

        public void close() {
            if (instance_ == 0)
                return;

            codec_ = MediaCodec.MEDIA_CODEC_UNKNOWN;
            close(instance_);
        }

        public bool isOpen() {
            if (instance_ == 0)
                return false;

            return isOpen(instance_);
        }

        public MediaCodec getCodec() {
            return codec_;
        }

        public int decodeVideoToYuv(byte[] src, int srcsize, byte[] dstY, byte[] dstU, byte[] dstV, ref int width, ref int height, int pixfmt) {
            if (instance_ == 0)
                return 0;

            return decodeVideoToYuv(instance_, src, srcsize, dstY, dstU, dstV, ref width, ref height, pixfmt);
        }

        /*
        Params
         newChannel: new audio channel count. -1 uses original channel value.
         newChannelLayout: new audio channel layout. -1 use original channel layout.
        */
        public int decodeAudio(byte[] src, int srcsize, byte[] dst, ref int dstsize, int newChannels, int newChannelLayout) {
            if (instance_ == 0)
                return 0;

            return decodeAudio(instance_, src, srcsize, dst, ref dstsize, newChannels, newChannelLayout);
        }
    }
}
