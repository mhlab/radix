﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using Radix.Base;

namespace Radix.Media.Demuxer {
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void FOnCommandResult(int command, bool ret, IntPtr param, IntPtr obj);
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void FOnCodecData(IntPtr videoCodecData, int videoCodecDataSize, IntPtr audioCodecData, int audioCodecDataSize, IntPtr obj);
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void FOnRecvMediaStream(IntPtr media, int mediaSize, int mediaCodec, UInt64 presentationTime, IntPtr obj);

    public class SyncDemuxer: BaseDisposeObject {
        public interface SyncDemuxerListener {
            void onCommandResultSyncDemuxer(int command, bool ret, IntPtr param, IntPtr obj);
            void onRecvMediaStreamSyncDemuxer(IntPtr media, int mediaSize, int mediaCodec, UInt64 presentationTime, IntPtr obj);
        }

        public enum DemuxerCommand {
            DEMUXER_COMMAND_NONE = -1,
            DEMUXER_COMMAND_OPEN = 0,
            DEMUXER_COMMAND_PLAY = 1,
            DEMUXER_COMMAND_STOP = 2,
            DEMUXER_COMMAND_PAUSE = 3,
            DEMUXER_COMMAND_SEEK = 4
        }

        [DllImport("LibSyncDemuxer.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static ulong createInstance();
        [DllImport("LibSyncDemuxer.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void destroyInstance(ulong instance);
        [DllImport("LibSyncDemuxer.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void setListener(ulong instance, FOnCommandResult cmdResult, FOnCodecData codecData, FOnRecvMediaStream videoStream, FOnRecvMediaStream audioStream);
        [DllImport("LibSyncDemuxer.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void open(ulong instance, String fileName);
        [DllImport("LibSyncDemuxer.dll", CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I1)]
        extern private static bool isSupportFile(ulong instance, String fileName);
        [DllImport("LibSyncDemuxer.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void play(ulong instance);
        [DllImport("LibSyncDemuxer.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void stop(ulong instance);
        [DllImport("LibSyncDemuxer.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void pause(ulong instance);
        [DllImport("LibSyncDemuxer.dll", CallingConvention = CallingConvention.StdCall)]
        extern private static void seek(ulong instance, int seekSecond);

        private ulong instance_ = 0;
        private SyncDemuxerListener listener_ = null;
        private GCHandle cmdResultHandle_;
        private GCHandle recvMediaStreamHandle_;

        private void onCommandResult(int command, bool ret, IntPtr param, IntPtr obj) {
            if (listener_ != null)
                listener_.onCommandResultSyncDemuxer(command, ret, param, obj);
        }

        private void onRecvMediaStream(IntPtr media, int mediaSize, int mediaCodec, UInt64 presentationTime, IntPtr obj) {
            if (listener_ != null)
                listener_.onRecvMediaStreamSyncDemuxer(media, mediaSize, mediaCodec, presentationTime, obj);
        }

        protected override void internalDispose() {
            if (instance_ == 0)
                return;

            destroyInstance(instance_);
            instance_ = 0;
            cmdResultHandle_.Free();
            recvMediaStreamHandle_.Free();
        }

        public SyncDemuxer(SyncDemuxerListener listener) {
            listener_ = listener;
            instance_ = createInstance();
            if (instance_ > 0) {
                FOnCommandResult cmdResult = new FOnCommandResult(onCommandResult);
                FOnRecvMediaStream recvMediaStream = new FOnRecvMediaStream(onRecvMediaStream);
                cmdResultHandle_ = GCHandle.Alloc(cmdResult);
                recvMediaStreamHandle_ = GCHandle.Alloc(recvMediaStream);
                setListener(instance_, cmdResult, null, recvMediaStream, recvMediaStream);
            }
        }

        public void open(String fileName) {
            if (instance_ == 0)
                return;

            open(instance_, fileName);
        }

        public bool isSupportFile(String fileName) {
            if (instance_ == 0)
                return false;

            return isSupportFile((uint)instance_, fileName);
        }

        public void play() {
            if (instance_ == 0)
                return;

            play(instance_);
        }

        public void stop() {
            if (instance_ == 0)
                return;

            stop(instance_);
        }

        public void pause() {
            if (instance_ == 0)
                return;

            pause(instance_);
        }

        public void seek(int seekSecond) {
            if (instance_ == 0)
                return;

            seek(instance_, seekSecond);
        }
    }
}