#include "Radix_Network_Rtsp_RTSPClient.h"
#include "network/rtsp/client/RTSPClient.h"
#include "JNIDef.h"

using Radix::Network::RTSPClient;

struct CallbackParamEx : JniEnvHelper::CallbackParam {
    jmethodID onConnectionResult;
    jmethodID onRecvMediaStream;

    CallbackParamEx(JNIEnv* env, jobject jobj):
        JniEnvHelper::CallbackParam(env, jobj) {
    }
};

CallbackParamEx* cbParam_ = NULL;

void onConnectionResult(int connectionId, Radix::Network::RTSPCommand command, bool ret, char* param) {
    JniEnvHelper::Attach attach;
    JNIEnv* env = attach.attachToJvm();
    jstring paramstr = NULL;
    if (param != NULL)
        paramstr = env->NewStringUTF(param);
    else
        paramstr = env->NewStringUTF("");
    env->CallVoidMethod(cbParam_->cbObj_, cbParam_->onConnectionResult, connectionId, (int)command, ret, paramstr);
}

void onRecvMediaStream(int connectionId, const char* media, int size, Radix::Network::RTSPCodec codec, unsigned int timestamp) {
    JniEnvHelper::Attach attach;
    JNIEnv* env = attach.attachToJvm();
    jbyteArray stream = cbParam_->pointerToByteArray(env, (int)codec, (jbyte*)media, size);
    env->CallVoidMethod(cbParam_->cbObj_, cbParam_->onRecvMediaStream, connectionId, stream, size, (int)codec, (jlong)timestamp);
}

JNIEXPORT jlong JNICALL Java_Radix_Network_Rtsp_RTSPClient_create
  (JNIEnv* env, jobject jobj) {
    RTSPClient* client = new RTSPClient();
    client->setListener(onConnectionResult, onRecvMediaStream);
    jclass cls = env->GetObjectClass(jobj);
    cbParam_ = new CallbackParamEx(env, jobj);
    cbParam_->onConnectionResult = env->GetMethodID(cls, "onCommandResult", "(IIZLjava/lang/String;)V");
    cbParam_->onRecvMediaStream = env->GetMethodID(cls, "onRecvMediaStream", "(I[BIIJ)V");
    return (jlong)client;
}

JNIEXPORT void JNICALL Java_Radix_Network_Rtsp_RTSPClient_destroy
  (JNIEnv* env, jobject jobj, jlong instance) {
    RTSPClient* client = (RTSPClient*)instance;
    delete client;
    if (cbParam_ != NULL) {
        delete cbParam_;
        cbParam_ = NULL;
    }
}

JNIEXPORT void JNICALL Java_Radix_Network_Rtsp_RTSPClient_startLiveVideo
  (JNIEnv* env, jobject jobj, jlong instance, jint connectionId, jstring ip, jint port, jstring abs_path, jboolean useTcp) {
    RTSPClient* client = (RTSPClient*)instance;
    const char* absPath_cstr = env->GetStringUTFChars(abs_path, 0);
    const char* ip_cstr = env->GetStringUTFChars(ip, 0);
    client->startLiveVideo(connectionId, ip_cstr, port, absPath_cstr, useTcp);
    env->ReleaseStringUTFChars(abs_path, absPath_cstr);
    env->ReleaseStringUTFChars(ip, ip_cstr);
}

JNIEXPORT void JNICALL Java_Radix_Network_Rtsp_RTSPClient_stopLiveVideo
  (JNIEnv* env, jobject jobj, jlong instance, jint connectionId) {
    RTSPClient* client = (RTSPClient*)instance;
    client->stopLiveVideo(connectionId);
}
