LOCAL_PATH  := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := RTSPClient
LOCAL_SRC_FILES := Radix_Network_Rtsp_RTSPClient.cpp \
                   ../../../../../src/thread/Lock.cpp \
                   ../../../../../src/thread/Signal.cpp \
                   ../../../../../src/thread/ThreadRunner.cpp \
                   ../../../../../src/string/StringEx.cpp \
                   ../../../../../src/log/Log.cpp \
                   ../../../../../src/memory/MemoryBuffer.cpp \
                   ../../../../../src/network/TCPSocket.cpp \
                   ../../../../../src/network/SocketSelector.cpp \
                   ../../../../../src/network/rtp/BaseRtpTo.cpp \
                   ../../../../../src/network/rtp/RtpToH264Stream.cpp \
                   ../../../../../src/network/rtsp/client/BaseRtpSession.cpp \
                   ../../../../../src/network/rtsp/client/UdpRtpSession.cpp \
                   ../../../../../src/network/rtsp/client/RTSPPacketAgent.cpp \
                   ../../../../../src/network/rtsp/client/RTSPConnection.cpp \
                   ../../../../../src/network/rtsp/client/RTSPClient.cpp \
                   ../../../JNIDef.cpp
                   
LOCAL_C_INCLUDES := ../../../../../src \
                    ../../../

LOCAL_CFLAGS := -std=c++11 -D_RADIX_RTSP_DEBUG
LOCAL_LDLIBS := -llog

include $(BUILD_SHARED_LIBRARY)
