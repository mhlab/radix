#include "Radix_Network_Rtsp_Live555Client.h"
#include "network/rtsp/client_live555/Live555ClientContainer.h"
#include "JNIDef.h"

using Radix::Network::Live555ClientContainer;

struct CallbackParamEx : JniEnvHelper::CallbackParam {
    jmethodID onConnectionResult;
    jmethodID onRecvMediaStream;

    CallbackParamEx(JNIEnv* env, jobject jobj):
        JniEnvHelper::CallbackParam(env, jobj) {
    }
};

CallbackParamEx* cbParam_ = NULL;

void onConnectionResult(int connectionId, Radix::Network::RTSPCommand command, bool ret, char* param) {
    JniEnvHelper::Attach attach;
    JNIEnv* env = attach.attachToJvm();
    jstring paramstr = NULL;
    if (param != NULL)
        paramstr = env->NewStringUTF(param);
    else
        paramstr = env->NewStringUTF("");
    env->CallVoidMethod(cbParam_->cbObj_, cbParam_->onConnectionResult, connectionId, (int)command, ret, paramstr);
}

void onRecvMediaStream(int connectionId, const char* media, int size, Radix::Network::RTSPCodec codec, int64_t timestamp) {
    JniEnvHelper::Attach attach;
    JNIEnv* env = attach.attachToJvm();
    jbyteArray stream = cbParam_->pointerToByteArray(env, (int)codec, (jbyte*)media, size);
    env->CallVoidMethod(cbParam_->cbObj_, cbParam_->onRecvMediaStream, connectionId, stream, size, (int)codec, (jlong)timestamp);
}


JNIEXPORT jlong JNICALL Java_Radix_Network_Rtsp_Live555Client_create
  (JNIEnv* env, jobject jobj) {
    Live555ClientContainer* container = new Live555ClientContainer();
    container->setListener(onConnectionResult, onRecvMediaStream);
    jclass cls = env->GetObjectClass(jobj);
    cbParam_ = new CallbackParamEx(env, jobj);
    cbParam_->onConnectionResult = env->GetMethodID(cls, "onCommandResult", "(IIZLjava/lang/String;)V");
    cbParam_->onRecvMediaStream = env->GetMethodID(cls, "onRecvMediaStream", "(I[BIIJ)V");
    return (jlong)container;
}

JNIEXPORT void JNICALL Java_Radix_Network_Rtsp_Live555Client_destroy
  (JNIEnv* env, jobject, jlong instance) {
    Live555ClientContainer* container = (Live555ClientContainer*)instance;
    delete container;
    if (cbParam_ != NULL) {
        delete cbParam_;
        cbParam_ = NULL;
    }
}

JNIEXPORT void JNICALL Java_Radix_Network_Rtsp_Live555Client_startLiveVideo
  (JNIEnv* env, jobject jobj, jlong instance, jint connectionId, jstring url, jstring userId, jstring userPass) {
    Live555ClientContainer* container = (Live555ClientContainer*)instance;
    const char* url_cstr = env->GetStringUTFChars(url, 0);
    char* id_cstr = NULL;
    char* pass_cstr = NULL;
    if (userId != NULL)
        id_cstr = (char*)env->GetStringUTFChars(userId, 0);
    if (userPass != NULL)
        pass_cstr = (char*)env->GetStringUTFChars(userPass, 0);
    container->startLiveVideo(connectionId, url_cstr, id_cstr, pass_cstr);
    env->ReleaseStringUTFChars(url, url_cstr);
    if (id_cstr != NULL)
        env->ReleaseStringUTFChars(userId, id_cstr);
    if (pass_cstr != NULL)
        env->ReleaseStringUTFChars(userPass, pass_cstr);
}

JNIEXPORT void JNICALL Java_Radix_Network_Rtsp_Live555Client_stopLiveVideo
  (JNIEnv* env, jobject jobj, jlong instance, jint connectionId) {
   Live555ClientContainer* container = (Live555ClientContainer*)instance;
   container->stopLiveVideo(connectionId);
}

JNIEXPORT void JNICALL Java_Radix_Network_Rtsp_Live555Client_sendDescribe
  (JNIEnv* env, jobject jobj, jlong instance, jint connectionId, jstring url, jstring userId, jstring userPass) {
    Live555ClientContainer* container = (Live555ClientContainer*)instance;
    const char* url_cstr = env->GetStringUTFChars(url, 0);
    char* id_cstr = NULL;
    char* pass_cstr = NULL;
    if (userId != NULL)
        id_cstr = (char*)env->GetStringUTFChars(userId, 0);
    if (userPass != NULL)
        pass_cstr = (char*)env->GetStringUTFChars(userPass, 0);
    container->sendDescribe(connectionId, url_cstr, id_cstr, pass_cstr);
    env->ReleaseStringUTFChars(url, url_cstr);
    if (id_cstr != NULL)
        env->ReleaseStringUTFChars(userId, id_cstr);
    if (pass_cstr != NULL)
        env->ReleaseStringUTFChars(userPass, pass_cstr);
}