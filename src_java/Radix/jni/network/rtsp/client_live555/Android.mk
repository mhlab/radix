LOCAL_PATH  := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := libLive555Client
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../../../../lib/live555/android/armeabi/libLive555Client.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := Live555RtspClient
LOCAL_SRC_FILES := Radix_Network_Rtsp_Live555Client.cpp  \
                   ../../../../../../src/thread/Lock.cpp \
                   ../../../../../../src/thread/Signal.cpp \
                   ../../../../../../src/thread/ThreadRunner.cpp \
                   ../../../../../../src/string/StringEx.cpp \
                   ../../../../../../src/log/Log.cpp \
                   ../../../../../../src/memory/MemoryBuffer.cpp \
                   ../../../../../../src/network/rtsp/MediaFrameAssembler.cpp \
                   ../../../../../../src/network/rtsp/H264FrameAssembler.cpp \
                   ../../../../../../src/network/rtsp/client_live555/Live555Sink.cpp \
                   ../../../../../../src/network/rtsp/client_live555/Live555Client.cpp \
                   ../../../../../../src/network/rtsp/client_live555/Live555ClientContainer.cpp \
                   ../../../JNIDef.cpp
                   
LOCAL_C_INCLUDES := ../../../../../../src \
                    ../../../../../../include \
                    ../../../

LOCAL_CFLAGS := -std=c++11
LOCAL_LDLIBS := -llog
LOCAL_STATIC_LIBRARIES := libLive555Client

include $(BUILD_SHARED_LIBRARY)
