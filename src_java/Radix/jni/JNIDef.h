#include <jni.h>
#include <map>
#include <thread/Lock.h>

namespace JniEnvHelper {
    static JavaVM* jvm_ = NULL;

    static void setJvm(JavaVM* jvm);

    class Attach {
    private:
        bool isAttached_;

    public:
        Attach();
        ~Attach();

    public:
        JNIEnv* attachToJvm();
    };

    struct CallbackParam {
        jobject cbObj_;
        std::map<jint, jbyteArray> streamArrayMap_;
        Radix::Thread::Lock streamArrayMapLock_;

        CallbackParam(JNIEnv* env, jobject jobj) {
            cbObj_ = env->NewGlobalRef(jobj);
        }
        ~CallbackParam() {
            Attach attach;
            JNIEnv* env = attach.attachToJvm();
            env->DeleteGlobalRef(cbObj_);
            for (auto it = streamArrayMap_.begin(); it != streamArrayMap_.end(); it++)
                env->DeleteGlobalRef(it->second);
        }
        jbyteArray createByteArray(JNIEnv* env, int size) {
            jbyteArray tmpArr = env->NewByteArray(size);
            jbyteArray stream = (jbyteArray)env->NewGlobalRef(tmpArr);
            env->DeleteLocalRef(tmpArr);

            return stream;
        }
        jbyteArray pointerToByteArray(JNIEnv* env, int key, jbyte* media, int mediaSize) {
            jbyteArray stream = NULL;
            auto it = streamArrayMap_.find(key);
            if (it == streamArrayMap_.end()) {
                Radix::Thread::Locker locker(&streamArrayMapLock_);
                stream = createByteArray(env, mediaSize);
                streamArrayMap_[key] = stream;
            }
            else
                stream = it->second;
            int streamLength = env->GetArrayLength(stream);
            if (streamLength < mediaSize) {
                Radix::Thread::Locker locker(&streamArrayMapLock_);
                env->DeleteGlobalRef(stream);
                stream = createByteArray(env, mediaSize);
                streamArrayMap_[key] = stream;
            }
            env->SetByteArrayRegion(stream, 0, mediaSize, (jbyte*)media);

            return stream;
        }
    };
}


