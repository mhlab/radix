#include "JNIDef.h"
#include <stddef.h>

jint JNI_OnLoad(JavaVM* jvm, void* reserved) {
    JniEnvHelper::setJvm(jvm);
    JNIEnv* env;
    if (jvm->GetEnv((void**)&env, JNI_VERSION_1_4) != JNI_OK)
        return -1;

    return JNI_VERSION_1_4;
}

void JniEnvHelper::setJvm(JavaVM* jvm) {
    jvm_ = jvm;
}

JniEnvHelper::Attach::Attach() {
    isAttached_ = false;
}

JniEnvHelper::Attach::~Attach() {
    if (isAttached_ && jvm_ != NULL)
        jvm_->DetachCurrentThread();
}

JNIEnv* JniEnvHelper::Attach::attachToJvm() {
    if (jvm_ == NULL)
        return NULL;
    JNIEnv* env = NULL;
    jint rc = jvm_->GetEnv((void **)&env, JNI_VERSION_1_4);
    if (rc != JNI_OK) {
        isAttached_ = true;
       	jvm_->AttachCurrentThread((JNIEnv**)&env, NULL);
    }
    return env;
}
