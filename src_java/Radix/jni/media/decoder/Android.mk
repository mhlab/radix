LOCAL_PATH  := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := libavcodec
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../../../lib/ffmpeg/android/libavcodec.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libavutil
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../../../lib/ffmpeg/android/libavutil.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libavformat
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../../../lib/ffmpeg/android/libavformat.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libswscale
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../../../lib/ffmpeg/android/libswscale.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libswresample
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../../../lib/ffmpeg/android/libswresample.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := FFMpegDecoder
LOCAL_SRC_FILES := Radix_Media_Decoder_FFMpegDecoder.cpp \
                   ../../../../../src/media/decoder/FFMpegDecoder.cpp

LOCAL_C_INCLUDES := ../../../../../src \
                    ../../ \
                    ../../../../../include/ffmpeg

LOCAL_CFLAGS := -std=c++11
LOCAL_LDLIBS := -lz -llog
LOCAL_STATIC_LIBRARIES := libavformat libswscale libavcodec libavutil libswresample

include $(BUILD_SHARED_LIBRARY)
