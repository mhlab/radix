#include "Radix_Media_Decoder_FFMpegDecoder.h"
#include "media/decoder/FFMpegDecoder.h"

using Radix::Media::FFMpegDecoder;

JNIEXPORT jlong JNICALL Java_Radix_Media_Decoder_FFMpegDecoder_create
  (JNIEnv* env, jobject jobj) {
    FFMpegDecoder* decoder = new FFMpegDecoder();
    return (jlong)decoder;
}

JNIEXPORT void JNICALL Java_Radix_Media_Decoder_FFMpegDecoder_destroy
  (JNIEnv* env, jobject jobj, jlong instance) {
    FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
    delete decoder;
}

JNIEXPORT jboolean JNICALL Java_Radix_Media_Decoder_FFMpegDecoder_openVideoCodec
  (JNIEnv* env, jobject jobj, jlong instance, jint codec) {
    FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
    return decoder->openVideo((Radix::Media::MediaCodec)codec);
}

JNIEXPORT jboolean JNICALL Java_Radix_Media_Decoder_FFMpegDecoder_openAudioCodec
  (JNIEnv* env, jobject jobj, jlong instance, jint codec, jint samplerate, jint channels, jint channelLayout) {
    FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
    return decoder->openAudio((Radix::Media::MediaCodec)codec, samplerate, channels, channelLayout);
}

JNIEXPORT void JNICALL Java_Radix_Media_Decoder_FFMpegDecoder_closeCodec
  (JNIEnv* env, jobject jobj, jlong instance) {
    FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
    decoder->close();
}

JNIEXPORT jint JNICALL Java_Radix_Media_Decoder_FFMpegDecoder_decodeVideoToYuv
  (JNIEnv* env, jobject jobj, jlong instance, jbyteArray src, jint srcsize, jbyteArray dstY, jbyteArray dstU, jbyteArray dstV, jintArray resolution, jint pixformat) {
    FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
    jbyte* src_c = env->GetByteArrayElements(src, 0);
    jbyte* dstY_c = env->GetByteArrayElements(dstY, 0);
    jbyte* dstU_c = env->GetByteArrayElements(dstU, 0);
    jbyte* dstV_c = env->GetByteArrayElements(dstV, 0);
    int* videoWidth = NULL;
    int* videoHeight = NULL;
    jint* decodeResolution = NULL;
    if (resolution != NULL) {
        decodeResolution = env->GetIntArrayElements(resolution, 0);
        videoWidth = &decodeResolution[0];
        videoHeight = &decodeResolution[1];
    }

    jint ret = decoder->decodeVideoToYuv((const char*)src_c, srcsize, (char*)dstY_c, (char*)dstU_c, (char*)dstV_c, videoWidth, videoHeight, (Radix::Media::MediaPixelFormat)pixformat);
    if (decodeResolution != NULL) {
        decodeResolution[0] = (jint)videoWidth;
        decodeResolution[1]  = (jint)videoHeight;
    	env->ReleaseIntArrayElements(resolution, decodeResolution, 0);
    }
    env->ReleaseByteArrayElements(src, src_c, 0);
    env->ReleaseByteArrayElements(dstY, dstY_c, 0);
    env->ReleaseByteArrayElements(dstU, dstU_c, 0);
    env->ReleaseByteArrayElements(dstV, dstV_c, 0);

    return ret;
}

JNIEXPORT jint JNICALL Java_Radix_Media_Decoder_FFMpegDecoder_decodeAudio
  (JNIEnv* env, jobject jobj, jlong instance, jbyteArray src, jint srcsize, jbyteArray dst, jintArray dstsize) {
    FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
    jbyte* src_c = env->GetByteArrayElements(src, 0);
    jbyte* dst_c = NULL;
    if (dst != NULL)
        dst_c = env->GetByteArrayElements(dst, 0);
    jint* dstSizeArr = env->GetIntArrayElements(dstsize, 0);
    int* tmpDstSize = dstSizeArr;
    jint ret = decoder->decodeAudio((const char*)src_c, srcsize, (char*)dst_c, tmpDstSize);
    env->ReleaseByteArrayElements(src, src_c, 0);
    if (dst_c != NULL)
        env->ReleaseByteArrayElements(dst, dst_c, 0);
    env->ReleaseIntArrayElements(dstsize, dstSizeArr, 0);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_Radix_Media_Decoder_FFMpegDecoder_isOpen
  (JNIEnv* env, jobject jobj, jlong instance) {
    FFMpegDecoder* decoder = (FFMpegDecoder*)instance;
    return decoder->isOpen();
}
