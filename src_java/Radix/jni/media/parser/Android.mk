LOCAL_PATH  := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := VideoStreamParser
LOCAL_SRC_FILES := Radix_Media_Parser_VideoStreamParser.cpp \
                   ../../../../../src/media/parser/BaseVideoParser.cpp \
                   ../../../../../src/media/parser/H264VideoParser.cpp \
                   ../../../../../src/media/parser/JPEGVideoParser.cpp
LOCAL_C_INCLUDES := ../../../../../src \
                    ../../
LOCAL_CFLAGS := -std=c++11

include $(BUILD_SHARED_LIBRARY)
