#include "Radix_Media_Parser_VideoStreamParser.h"
#include "media/MediaDef.h"
#include "media/parser/H264VideoParser.h"
#include "media/parser/JPEGVideoParser.h"

JNIEXPORT void JNICALL Java_Radix_Media_Parser_VideoStreamParser_getVideoResolution
  (JNIEnv* env, jobject jobj, jbyteArray videoFrame, jint codec, jintArray videoResoultion) {
    int videoWidth = 0;
    int videoHeight = 0;
    int videoFrameLength = env->GetArrayLength(videoFrame);
    jbyte* videoFramePtr = env->GetByteArrayElements(videoFrame, NULL);
    jint* videoResultionPtr = env->GetIntArrayElements(videoResoultion, 0);
    if (codec == Radix::Media::MEDIA_CODEC_H264) {
        Radix::Media::H264VideoParser parser;
        parser.getResolution((void*)videoFramePtr, videoFrameLength, &videoWidth, &videoHeight);
    }
    else if (codec == Radix::Media::MEDIA_CODEC_MJPEG) {
        Radix::Media::JPEGVideoParser parser;
        parser.getResolution((void*)videoFramePtr, videoFrameLength, &videoWidth, &videoHeight);
    }
    videoResultionPtr[0] = videoWidth;
    videoResultionPtr[1] = videoHeight;
    env->ReleaseByteArrayElements(videoFrame, videoFramePtr, 0);
    env->ReleaseIntArrayElements(videoResoultion, videoResultionPtr, 0);
}