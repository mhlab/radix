#include "media_demuxer_FFMpegDemuxer.h"
#include "media/demuxer/FFMpegDemuxer.h"
#include "JNIDef.h"

static JavaVM* jvm_ = NULL;

using Radix::Media::FFMpegDemuxer;

jint JNI_OnLoad(JavaVM* jvm, void* reserved) {
    jvm_ = jvm;
    JNIEnv* env;
    if (jvm->GetEnv((void**)&env, JNI_VERSION_1_4) != JNI_OK)
        return -1;

    return JNI_VERSION_1_4;
}

struct CallbackParam {
    jobject obj_;
    jmethodID commandMethodId_;
    jmethodID streamMethodId_;
    jmethodID codecDataMethodId_;
    jbyteArray mediaData_;
    int mediaSize_;

    CallbackParam(JNIEnv* env, int size) {
        obj_ = NULL;
        commandMethodId_ = NULL;
        jbyteArray tmpArr = env->NewByteArray(size);
        mediaData_ = (jbyteArray)env->NewGlobalRef(tmpArr);
        env->DeleteLocalRef(tmpArr);
        mediaSize_ = size;
    }
    
    ~CallbackParam() {
        Attach attach(jvm_);
        JNIEnv* env = attach.attachToJvm();
        if (env == NULL)
            return;
        if (obj_ != NULL)
            env->DeleteGlobalRef(obj_);
        env->DeleteGlobalRef(mediaData_);
    }
};

CallbackParam* callbackParam_ = NULL;

void commandResult(Radix::Media::DemuxerCommand command, bool ret, void* param, void* obj) {
    Attach attach(jvm_);
    JNIEnv* env = attach.attachToJvm();
    if (env == NULL)
        return;
    
    jstring paramstr = NULL;
    if (command == Radix::Media::DEMUXER_COMMAND_OPEN && param != NULL) {
        Radix::Media::MediaFileInformation* mediaInfo = (Radix::Media::MediaFileInformation*)param;
        paramstr = env->NewStringUTF(mediaInfo->toJson().c_str());
    }
    else
        paramstr = env->NewStringUTF("");
    env->CallVoidMethod(callbackParam_->obj_, callbackParam_->commandMethodId_, (int)command, ret, paramstr);
}

void codecData(const char* videoCodecData, int videoCodecDataSize, char* audioCodecData, int audioCodecDataSize, void* obj) {
    Attach attach(jvm_);
    JNIEnv* env = attach.attachToJvm();
    if (env == NULL)
        return;

    jbyteArray videoCodecDataArr = NULL;
    jbyteArray audioCodecDataArr = NULL;
    if (videoCodecData != NULL && videoCodecDataSize > 0) {
        videoCodecDataArr = env->NewByteArray(videoCodecDataSize);
        env->SetByteArrayRegion(videoCodecDataArr, 0, videoCodecDataSize, (jbyte*)videoCodecData);
    }
    if (audioCodecData != NULL && audioCodecDataSize > 0) {
        audioCodecDataArr = env->NewByteArray(audioCodecDataSize);
        env->SetByteArrayRegion(audioCodecDataArr, 0, audioCodecDataSize, (jbyte*)audioCodecData);
    }
    env->CallVoidMethod(callbackParam_->obj_, callbackParam_->codecDataMethodId_, videoCodecDataArr, audioCodecDataArr);
}

void recvMediaStream(const char* media, int mediaSize, int codec, int64_t presentationTime, void* obj) {
    Attach attach(jvm_);
    JNIEnv* env = attach.attachToJvm();
    if (env == NULL)
        return;

    if (callbackParam_->mediaSize_ < mediaSize)
    {
        env->DeleteGlobalRef(callbackParam_->mediaData_);
        jbyteArray tmpArr = env->NewByteArray(mediaSize);
        callbackParam_->mediaData_ = (jbyteArray)env->NewGlobalRef(tmpArr);
        env->DeleteLocalRef(tmpArr);
    }
    env->SetByteArrayRegion(callbackParam_->mediaData_, 0, mediaSize, (jbyte*)media);
    env->CallVoidMethod(callbackParam_->obj_, callbackParam_->streamMethodId_, callbackParam_->mediaData_, mediaSize, (int)codec, presentationTime);
}

JNIEXPORT jlong JNICALL Java_Radix_Media_Demuxer_FFMpegDemuxer_initInstance
  (JNIEnv* env, jobject jobj) {
      FFMpegDemuxer* demuxer = new FFMpegDemuxer();
      demuxer->setListener(commandResult, recvMediaStream, codecData);
      
      if (callbackParam_ != NULL)
        delete callbackParam_;
      callbackParam_ = new CallbackParam(env, 1000000);
      jclass cls = env->GetObjectClass(jobj);
      callbackParam_->obj_ = env->NewGlobalRef(jobj);
      callbackParam_->commandMethodId_ = env->GetMethodID(cls, "onCommandResult", "(IZLjava/lang/String;)V");
      callbackParam_->streamMethodId_ = env->GetMethodID(cls, "onRecvMediaStream", "([BIIJ)V");
      callbackParam_->codecDataMethodId_ = env->GetMethodID(cls, "onCodecData", "([B[B)V");

      return (jlong)demuxer;
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_FFMpegDemuxer_freeInstance
  (JNIEnv* env, jobject jobj, jlong instance) {
      FFMpegDemuxer* demuxer = (FFMpegDemuxer*)instance;
      delete demuxer;
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_FFMpegDemuxer_open
  (JNIEnv* env, jobject jobj, jlong instance, jstring fileName) {
      FFMpegDemuxer* demuxer = (FFMpegDemuxer*)instance;
      const char* newFileName = env->GetStringUTFChars(fileName, 0);
      demuxer->open(newFileName);
	  env->ReleaseStringUTFChars(fileName, newFileName);
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_FFMpegDemuxer_close
  (JNIEnv* env, jobject jobj, jlong instance) {
      FFMpegDemuxer* demuxer = (FFMpegDemuxer*)instance;
      demuxer->close();
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_FFMpegDemuxer_play
  (JNIEnv* env, jobject jobj, jlong instance) {
      FFMpegDemuxer* demuxer = (FFMpegDemuxer*)instance;
      demuxer->play();
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_FFMpegDemuxer_stop
  (JNIEnv* env, jobject jobj, jlong instance) {
      FFMpegDemuxer* demuxer = (FFMpegDemuxer*)instance;
      demuxer->stop();
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_FFMpegDemuxer_pause
  (JNIEnv* env, jobject jobj, jlong instance) {
      FFMpegDemuxer* demuxer = (FFMpegDemuxer*)instance;
      demuxer->pause();
}

JNIEXPORT jboolean JNICALL Java_Radix_Media_Demuxerr_FFMpegDemuxer_isSupportFile
  (JNIEnv* env, jobject jobj, jlong instance, jstring fileName) {
      FFMpegDemuxer* demuxer = (FFMpegDemuxer*)instance;
      const char* newFileName = env->GetStringUTFChars(fileName, 0);
      bool ret = demuxer->isSupportFile(newFileName);
      env->ReleaseStringUTFChars(fileName, newFileName);

      return ret;
}