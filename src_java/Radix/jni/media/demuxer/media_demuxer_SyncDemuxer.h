/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class Radix_Media_Demuxer_SyncDemuxer */

#ifndef _Included_Radix_Media_Demuxer_SyncDemuxer
#define _Included_Radix_Media_Demuxer_SyncDemuxer
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     Radix_Media_Demuxer_SyncDemuxer
 * Method:    create
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_create
  (JNIEnv* env, jobject jobj);

/*
 * Class:     Radix_Media_Demuxer_SyncDemuxer
 * Method:    destroy
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_destroy
  (JNIEnv* env, jobject jobj, jlong instance);
/*
 * Class:     Radix_Media_Demuxer_SyncDemuxer
 * Method:    open
 * Signature: (JLjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_open
  (JNIEnv* env, jobject jobj, jlong instance, jstring fileName);

/*
 * Class:     Radix_Media_Demuxer_SyncDemuxer
 * Method:    play
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_play
  (JNIEnv* env, jobject jobj, jlong instance);

/*
 * Class:     Radix_Media_Demuxer_SyncDemuxer
 * Method:    stop
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_stop
  (JNIEnv* env, jobject jobj, jlong instance);

/*
 * Class:     Radix_Media_Demuxer_SyncDemuxer
 * Method:    pause
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_pause
  (JNIEnv* env, jobject jobj, jlong instance);

/*
 * Class:     Radix_Media_Demuxer_SyncDemuxer
 * Method:    isSupportFile
 * Signature: (JLjava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_isSupportFile
  (JNIEnv* env, jobject jobj, jlong instance, jstring fileName);

/*
 * Class:     Radix_Media_Demuxer_SyncDemuxer
 * Method:    seek
 * Signature: (JLI)Z
 */
JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_seek
  (JNIEnv* env, jobject jobj, jlong instance, jint seekSeconds);

#ifdef __cplusplus
}
#endif
#endif
