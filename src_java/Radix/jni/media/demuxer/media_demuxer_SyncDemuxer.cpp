#include "media_demuxer_SyncDemuxer.h"
#include "media/demuxer/SyncDemuxer.h"
#include "thread/Lock.h"
#include "JNIDef.h"
#include <map>

using Radix::Media::SyncDemuxer;

struct CallbackParam {
    jobject obj_;
    jmethodID commandMethodId_;
    jmethodID streamMethodId_;
    jmethodID codecDataMethodId_;
    jmethodID updatedPlayTimeMethodId_;
    jbyteArray mediaData_;
    int mediaSize_;
    std::map<jint, jbyteArray> mediaStreamMap_;
    Radix::Thread::Lock lock_;

    CallbackParam(JNIEnv* env, int size) {
        obj_ = NULL;
        commandMethodId_ = NULL;
        jbyteArray tmpArr = env->NewByteArray(size);
        mediaData_ = (jbyteArray)env->NewGlobalRef(tmpArr);
        env->DeleteLocalRef(tmpArr);
        mediaSize_ = size;
    }

    ~CallbackParam() {
        JniEnvHelper::Attach attach;
        JNIEnv* env = attach.attachToJvm();
        if (env == NULL)
            return;
        if (obj_ != NULL)
            env->DeleteGlobalRef(obj_);
        env->DeleteGlobalRef(mediaData_);
        for (auto it = mediaStreamMap_.begin(); it != mediaStreamMap_.end(); it++) {
           env->DeleteGlobalRef(it->second);
        }
    }

    jbyteArray createByteArray(JNIEnv* env, int size) {
        Radix::Thread::Locker locker(&lock_);

        jbyteArray tmpArr = env->NewByteArray(size);
        jbyteArray stream = (jbyteArray)env->NewGlobalRef(tmpArr);
        env->DeleteLocalRef(tmpArr);

        return stream;
    }

    jbyteArray mediaPointerToByteArray(JNIEnv* env, int codecId, jbyte* media, int mediaSize) {
        jbyteArray stream = NULL;
        auto it = mediaStreamMap_.find(codecId);
        if (it == mediaStreamMap_.end()) {
            stream = createByteArray(env, mediaSize);
            mediaStreamMap_[codecId] = stream;
        }
        else
            stream = it->second;
        int streamLength = env->GetArrayLength(stream);
        if (streamLength < mediaSize) {
            env->DeleteGlobalRef(stream);
            stream = createByteArray(env, mediaSize);
            mediaStreamMap_[codecId] = stream;
        }
        env->SetByteArrayRegion(stream, 0, mediaSize, (jbyte*)media);

        return stream;
    }
};

CallbackParam* callbackParam_ = NULL;

void commandResult(Radix::Media::DemuxerCommand command, bool ret, void* param, void* obj) {
    JniEnvHelper::Attach attach;
    JNIEnv* env = attach.attachToJvm();
    if (env == NULL)
        return;

    jstring paramstr = NULL;
    if (command == Radix::Media::DEMUXER_COMMAND_OPEN && param != NULL) {
        Radix::Media::MediaAttribution* mediaInfo = (Radix::Media::MediaAttribution*)param;
        paramstr = env->NewStringUTF(mediaInfo->toJson().c_str());
    }
    else
        paramstr = env->NewStringUTF("");
    env->CallVoidMethod(callbackParam_->obj_, callbackParam_->commandMethodId_, (int)command, ret, paramstr);
}

void codecData(const char* videoCodecData, int videoCodecDataSize, char* audioCodecData, int audioCodecDataSize, void* obj) {
    JniEnvHelper::Attach attach;
    JNIEnv* env = attach.attachToJvm();
    if (env == NULL)
        return;

    jbyteArray videoCodecDataArr = NULL;
    jbyteArray audioCodecDataArr = NULL;
    if (videoCodecData != NULL && videoCodecDataSize > 0) {
        videoCodecDataArr = env->NewByteArray(videoCodecDataSize);
        env->SetByteArrayRegion(videoCodecDataArr, 0, videoCodecDataSize, (jbyte*)videoCodecData);
    }
    if (audioCodecData != NULL && audioCodecDataSize > 0) {
        audioCodecDataArr = env->NewByteArray(audioCodecDataSize);
        env->SetByteArrayRegion(audioCodecDataArr, 0, audioCodecDataSize, (jbyte*)audioCodecData);
    }
    env->CallVoidMethod(callbackParam_->obj_, callbackParam_->codecDataMethodId_, videoCodecDataArr, audioCodecDataArr);
}

void recvVideoMediaStream(const char* media, int mediaSize, int codec, int64_t presentationTime, void* obj) {
    JniEnvHelper::Attach attach;
    JNIEnv* env = attach.attachToJvm();
    if (env == NULL)
        return;

    jbyteArray stream = callbackParam_->mediaPointerToByteArray(env, codec, (jbyte*)media, mediaSize);
    env->CallVoidMethod(callbackParam_->obj_, callbackParam_->streamMethodId_, stream, mediaSize, (int)codec, presentationTime);
}

void recvAudioMediaStream(const char* media, int mediaSize, int codec, int64_t presentationTime, void* obj) {
    JniEnvHelper::Attach attach;
    JNIEnv* env = attach.attachToJvm();
    if (env == NULL)
        return;

    jbyteArray stream = callbackParam_->mediaPointerToByteArray(env, codec, (jbyte*)media, mediaSize);
    env->CallVoidMethod(callbackParam_->obj_, callbackParam_->streamMethodId_, stream, mediaSize, (int)codec, presentationTime);
}

void updatedTime(long playTime) {
    JniEnvHelper::Attach attach;
    JNIEnv* env = attach.attachToJvm();
    if (env == NULL)
        return;

    env->CallVoidMethod(callbackParam_->obj_, callbackParam_->updatedPlayTimeMethodId_, (jlong)playTime);
}

JNIEXPORT jlong JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_create
  (JNIEnv* env, jobject jobj) {
    SyncDemuxer* demuxer = new SyncDemuxer();
    demuxer->setListener(commandResult, recvVideoMediaStream, recvAudioMediaStream, codecData, updatedTime, NULL);

    if (callbackParam_ != NULL)
        delete callbackParam_;
    callbackParam_ = new CallbackParam(env, 1000000);
    jclass cls = env->GetObjectClass(jobj);
    callbackParam_->obj_ = env->NewGlobalRef(jobj);
    callbackParam_->commandMethodId_ = env->GetMethodID(cls, "onCommandResult", "(IZLjava/lang/String;)V");
    callbackParam_->streamMethodId_ = env->GetMethodID(cls, "onRecvMediaStream", "([BIIJ)V");
    callbackParam_->codecDataMethodId_ = env->GetMethodID(cls, "onCodecData", "([B[B)V");
    callbackParam_->updatedPlayTimeMethodId_ = env->GetMethodID(cls, "onUpdatedPlayTime", "(J)V");

    return (jlong)demuxer;
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_destroy
  (JNIEnv* env, jobject jobj, jlong instance) {
    SyncDemuxer* demuxer = (SyncDemuxer*)instance;
    delete demuxer;
    if (callbackParam_ != NULL) {
        delete callbackParam_;
        callbackParam_ = NULL;
    }
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_open
  (JNIEnv* env, jobject jobj, jlong instance, jstring fileName) {
      SyncDemuxer* demuxer = (SyncDemuxer*)instance;
      const char* filename_cstr = env->GetStringUTFChars(fileName, 0);
      demuxer->open(filename_cstr);
      env->ReleaseStringUTFChars(fileName, filename_cstr);
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_play
  (JNIEnv* env, jobject jobj, jlong instance) {
      SyncDemuxer* demuxer = (SyncDemuxer*)instance;
      demuxer->play();
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_stop
  (JNIEnv* env, jobject jobj, jlong instance) {
      SyncDemuxer* demuxer = (SyncDemuxer*)instance;
      demuxer->stop();
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_pause
  (JNIEnv* env, jobject jobj, jlong instance) {
      SyncDemuxer* demuxer = (SyncDemuxer*)instance;
      demuxer->pause();
}

JNIEXPORT jboolean JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_isSupportFile
  (JNIEnv* env, jobject jobj, jlong instance, jstring fileName) {
      SyncDemuxer* demuxer = (SyncDemuxer*)instance;
      const char* newFileName = env->GetStringUTFChars(fileName, 0);
      bool ret = demuxer->isSupportFile(newFileName);
      env->ReleaseStringUTFChars(fileName, newFileName);

      return ret;
}

JNIEXPORT void JNICALL Java_Radix_Media_Demuxer_SyncDemuxer_seek
  (JNIEnv* env, jobject jobj, jlong instance, jint seekSeconds) {
      SyncDemuxer* demuxer = (SyncDemuxer*)instance;
      demuxer->seek(seekSeconds);
}