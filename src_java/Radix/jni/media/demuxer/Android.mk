LOCAL_PATH  := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := libavcodec
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../../../lib/ffmpeg/android/libavcodec.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libavutil
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../../../lib/ffmpeg/android/libavutil.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libavformat
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../../../lib/ffmpeg/android/libavformat.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := SyncDemuxer
LOCAL_SRC_FILES := media_demuxer_SyncDemuxer.cpp \
                   ../../../../../src/media/demuxer/FFMpegDemuxer.cpp \
                   ../../../../../src/media/demuxer/SyncDemuxer.cpp \
                   ../../../../../src/thread/Lock.cpp \
                   ../../../../../src/thread/Signal.cpp \
                   ../../../../../src/thread/ThreadRunner.cpp \
                   ../../../../../src/string/StringEx.cpp \
                   ../../../../../src/datetime/DateTime.cpp \
                   ../../../../../src/log/Log.cpp \
                   ../../JNIDef.cpp
                   
LOCAL_C_INCLUDES := ../../../../../src \
                    ../../ \
                    ../../../../../include/ffmpeg
LOCAL_CFLAGS := -std=c++11
LOCAL_LDLIBS := -lz -llog
LOCAL_STATIC_LIBRARIES := libavformat libavcodec libavutil

include $(BUILD_SHARED_LIBRARY)
