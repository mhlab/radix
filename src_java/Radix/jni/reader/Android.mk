LOCAL_PATH  := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libiconv
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../../lib/iconv/android/armeabi/libiconv.a

include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := SubtitleReader
LOCAL_SRC_FILES := reader_SubtitleReader.cpp \
                   ../../../../src/thread/Lock.cpp \
                   ../../../../src/thread/Signal.cpp \
                   ../../../../src/thread/ThreadRunner.cpp \
                   ../../../../src/string/StringEx.cpp \
                   ../../../../src/string/IconvImpl.cpp \
                   ../../../../src/file/FileEx.cpp \
                   ../../../../src/log/Log.cpp \
                   ../../../../src/reader/BaseSubtitleReader.cpp \
                   ../../../../src/reader/SmiFileReader.cpp \
                   ../JNIDef.cpp
                   
LOCAL_C_INCLUDES := ../../../../src \
                    ../ \
                    ../../../../include

LOCAL_CFLAGS := -std=c++11
LOCAL_LDLIBS := -lz -llog
LOCAL_STATIC_LIBRARIES := libiconv

include $(BUILD_SHARED_LIBRARY)
