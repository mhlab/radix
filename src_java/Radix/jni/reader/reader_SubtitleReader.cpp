#include "reader_SubtitleReader.h"
#include "reader/BaseSubtitleReader.h"
#include "log/Log.h"
#include "JNIDef.h"

using Radix::Reader::BaseSubtitleReader;

struct CallbackParam {
    jobject obj_;
    jmethodID callbackMethodId_;
    jbyteArray subtitle_;
    int subtitleLength_;

    CallbackParam(JNIEnv* env) {
        obj_ = NULL;
        subtitleLength_ = 2048;
        jbyteArray tmpArr = env->NewByteArray(subtitleLength_);
        subtitle_ = (jbyteArray)env->NewGlobalRef(tmpArr);
        env->DeleteLocalRef(tmpArr);
    }

    ~CallbackParam() {
        JniEnvHelper::Attach attach;
        JNIEnv* env = attach.attachToJvm();
        if (env == NULL)
            return;
        if (obj_ != NULL)
            env->DeleteGlobalRef(obj_);
        env->DeleteGlobalRef(subtitle_);
    }
};

CallbackParam* callbackParam_;

void onOpenSubtitle(const char* subtitle, bool ret) {
}

void onRecvSubtitle(const char* subtitle, int length) {
    JniEnvHelper::Attach attach;
    JNIEnv* env = attach.attachToJvm();
    if (env == NULL)
        return;

    if (callbackParam_->subtitleLength_ < length) {
        env->DeleteGlobalRef(callbackParam_->subtitle_);
        jbyteArray tmpArr = env->NewByteArray(length);
        callbackParam_->subtitle_ = (jbyteArray)env->NewGlobalRef(tmpArr);
        env->DeleteLocalRef(tmpArr);
    }
    env->SetByteArrayRegion(callbackParam_->subtitle_, 0, length, (jbyte*)subtitle);
    env->CallVoidMethod(callbackParam_->obj_, callbackParam_->callbackMethodId_, callbackParam_->subtitle_, length);
}

JNIEXPORT jlong JNICALL Java_Radix_Reader_SubtitleReader_create
  (JNIEnv* env, jobject jobj, int subtitleType) {
    BaseSubtitleReader* reader = BaseSubtitleReader::createSubtitleReader((Radix::Reader::SubTitleFileType)subtitleType);
    if (reader == NULL)
        return 0;

    jclass cls = env->GetObjectClass(jobj);
    callbackParam_ = new CallbackParam(env);
    callbackParam_->obj_ = env->NewGlobalRef(jobj);
    callbackParam_->callbackMethodId_ = env->GetMethodID(cls, "onRecvSubtitle", "([BI)V");
    reader->setListener(onOpenSubtitle, onRecvSubtitle);
    return (jlong)reader;
}

JNIEXPORT void JNICALL Java_Radix_Reader_SubtitleReader_destrory
  (JNIEnv* env, jobject jobj, jlong instance) {
    BaseSubtitleReader* reader = (BaseSubtitleReader*)instance;
    delete reader;
    if (callbackParam_ != NULL) {
        delete callbackParam_;
        callbackParam_ = NULL;
    }
}

JNIEXPORT void JNICALL Java_Radix_Reader_SubtitleReader_open
  (JNIEnv* env, jobject jobj, jlong instance, jstring filePath) {
    BaseSubtitleReader* reader = (BaseSubtitleReader*)instance;
    const char* newFilePath = env->GetStringUTFChars(filePath, 0);
    reader->open(newFilePath);
    env->ReleaseStringUTFChars(filePath, newFilePath);
}

JNIEXPORT void JNICALL Java_Radix_Reader_SubtitleReader_progress
  (JNIEnv* env, jobject jobj, jlong instance, jdouble playTime) {
    BaseSubtitleReader* reader = (BaseSubtitleReader*)instance;
    reader->progress(playTime);
}

JNIEXPORT void JNICALL Java_Radix_Reader_SubtitleReader_reset
  (JNIEnv* env, jobject jobj, jlong instance) {
    BaseSubtitleReader* reader = (BaseSubtitleReader*)instance;
    return reader->reset();
}