package Radix.Ui.FileExplorer;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class FileExploreAdapter extends BaseAdapter implements FileExploreSource.FileExplorerSourceListener {
    private FileExploreSource fileExploreSource_ = null;
    private ArrayList<FileExploreSource.FileItemInfo> fileList_ = null;
    private FileExploreAdapterListener listener_ = null;
    private int folderRessId_ = -1;
    private int fileResId_ = -1;
    private boolean isSourceLock_ = true;
    private int imgContentResId_ = -1;
    private int tvFileNameResId_ = -1;
    private int tvFileAttrResId_ = -1;
    private int viewItemResId_ = -1;

    public interface FileExploreAdapterListener {
        void onUpdatedFileList(ArrayList<FileExploreSource.FileItemInfo> fileItemInfoList);
        void onSelected(File selected);
    }

    public FileExploreAdapter(FileExploreAdapterListener listener, Context context) {
        super();
        listener_ = listener;
        fileExploreSource_ = new FileExploreSource(this);
        fileExploreSource_.start();

        imgContentResId_ = context.getResources().getIdentifier("imgContent", "id", context.getPackageName());
        tvFileNameResId_ = context.getResources().getIdentifier("tvFileName", "id", context.getPackageName());
        tvFileAttrResId_ = context.getResources().getIdentifier("tvFileAttr", "id", context.getPackageName());
        viewItemResId_ = context.getResources().getIdentifier("linearlayout_fileexplorerviewitem", "layout", context.getPackageName());
    }

    public void setDirectory(String directoryPath) {
        fileExploreSource_.explore(directoryPath);
    }

    public void release() {
        fileExploreSource_.release();
    }

    public void clickedItem(int position) {
        if (position < 0 || position >= fileList_.size())
            return;

        if (fileList_.get(position).file_.isDirectory())
            fileExploreSource_.explore(fileList_.get(position).file_.getAbsolutePath());
        else {
            if (listener_ != null)
                listener_.onSelected(fileList_.get(position).file_);
        }
    }

    public void setFolderResourceId(int resourceId) {
        folderRessId_ = resourceId;
    }

    public void setFileResourceId(int resourceId) {
        fileResId_ = resourceId;
    }

    public void setThumbnail(String filePath, Bitmap thumbnail) {
        for (FileExploreSource.FileItemInfo fileItemInfo : fileList_) {
            if (fileItemInfo.file_.getAbsolutePath().equals(filePath)) {
                fileItemInfo.thumbnail_ = thumbnail.copy(thumbnail.getConfig(), true);
                notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public int getCount() {
        if (fileList_ == null || isSourceLock_)
            return 0;

        return fileList_.size();
    }

    @Override
    public Object getItem(int position) {
        if (fileList_ == null)
            return null;

        if (position < fileList_.size())
            return fileList_.get(position);
        else
            return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (fileList_ == null || isSourceLock_)
            return convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(viewItemResId_, parent, false);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ListView lv = (ListView)parent;
                    int realPos = lv.getPositionForView(v);
                    clickedItem(realPos);
                }
            });
        }

        if (position >= fileList_.size())
            return convertView;
        FileExploreSource.FileItemInfo fileItemInfo = fileList_.get(position);
        if (fileItemInfo == null)
            return convertView;
        ImageView contentImage = convertView.findViewById(imgContentResId_);
        TextView tvFileName = convertView.findViewById(tvFileNameResId_);
        TextView tvFileAttr = convertView.findViewById(tvFileAttrResId_);
        if (fileItemInfo.file_.isDirectory()) {
            if (folderRessId_ != -1)
                contentImage.setImageResource(folderRessId_);
        }
        else {
            if (fileItemInfo.thumbnail_ != null)
                contentImage.setImageBitmap(fileItemInfo.thumbnail_);
            else if (fileResId_ != -1)
                contentImage.setImageResource(fileResId_);
        }
        if (fileItemInfo.isParentFolder_)
            tvFileName.setText("..");
        else
            tvFileName.setText(fileItemInfo.file_.getName());
        tvFileAttr.setText("");

        return convertView;
    }

    @Override
    public void onExploredDirectory(ArrayList<FileExploreSource.FileItemInfo> fileItemInfoList) {
        isSourceLock_ = true;
        try {
            fileList_ = fileItemInfoList;
            if (listener_ != null)
                listener_.onUpdatedFileList(fileList_);
        }
        finally {
            isSourceLock_ = false;
        }
    }
}
