package Radix.Ui.FileExplorer;

import android.graphics.Bitmap;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FileExploreSource extends Thread {
    private class FileComparator implements Comparator<FileItemInfo> {
        @Override
        public int compare(FileItemInfo o1, FileItemInfo o2) {
            if (o1.isParentFolder_)
                return -1;
            if (o2.isParentFolder_)
                return 1;
            if (o1.file_.isDirectory() && !o2.file_.isDirectory())
                return -1;
            if (!o1.file_.isDirectory() && o2.file_.isDirectory())
                return 1;

            return o1.file_.getName().compareTo(o2.file_.getName());
        }
    }

    public class FileItemInfo {
        public boolean isParentFolder_;
        public File file_;
        public Bitmap thumbnail_ = null;
    }

    public interface FileExplorerSourceListener {
        void onExploredDirectory(ArrayList<FileItemInfo> fileItemInfoList);
    }

    private boolean terminated_ = false;
    private Lock lock_ = new ReentrantLock();
    private Condition condition_ = lock_.newCondition();
    private String explorePath_ = null;
    private FileExplorerSourceListener listener_ = null;

    private void waitSignal() {
        lock_.lock();
        try {
            condition_.await();
        }
        catch (InterruptedException e) {
        }
        finally {
            lock_.unlock();
        }
    }

    private void signal() {
        lock_.lock();
        try {
            condition_.signal();
        }
        finally {
            lock_.unlock();
        }
    }

    public FileExploreSource(FileExplorerSourceListener listener) {
        listener_ = listener;
    }

    public void explore(String path) {
        explorePath_ = path;
        signal();
    }

    public void release() {
        try {
            this.join();
        }
        catch (InterruptedException e) {
        }
    }

    @Override
    public void run() {
        while (!terminated_) {
            if (explorePath_ == null)
                waitSignal();
            if (terminated_)
                break;
            if (explorePath_ == null || listener_ == null)
                continue;

            ArrayList<FileItemInfo> fileItemInfoList = new ArrayList<>();
            try {
                File exploreFile = new File(explorePath_);
                if (exploreFile == null)
                    continue;
                File parentFile = exploreFile.getParentFile();
                if (parentFile != null) {
                    FileItemInfo itemInfo = new FileItemInfo();
                    itemInfo.isParentFolder_ = true;
                    itemInfo.file_ = parentFile;
                    fileItemInfoList.add(itemInfo);
                }
                File[] listFiles = exploreFile.listFiles();
                if (listFiles == null)
                    continue;
                for (File listFile : listFiles) {
                    FileItemInfo itemInfo = new FileItemInfo();
                    itemInfo.isParentFolder_ = false;
                    itemInfo.file_ = listFile;
                    fileItemInfoList.add(itemInfo);
                }
                Collections.sort(fileItemInfoList, new FileComparator());
            }
            finally {
                listener_.onExploredDirectory(fileItemInfoList);
                explorePath_ = null;
            }
        }
    }
}
