package Radix.Ui.FileExplorer;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class FileExploreView extends ListView implements FileExploreAdapter.FileExploreAdapterListener {
    private FileExploreAdapter adapter_ = null;
    private FileExploreViewListener listener_ = null;

    public interface FileExploreViewListener {
        void onSelected(File selected);
        void onUpdatedFileList(ArrayList<FileExploreSource.FileItemInfo> fileItemInfoList);
    }

    private void initialize(Context context) {
        adapter_ = new FileExploreAdapter(this, context);
        setAdapter(adapter_);
    }

    public FileExploreView(Context context) {
        super(context);
        initialize(context);
    }

    public FileExploreView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public FileExploreView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    public void setListener(FileExploreViewListener listener) {
        listener_ = listener;
    }

    public void setDirectory(String directoryPath) {
        adapter_.setDirectory(directoryPath);
    }

    public void release() {
        adapter_.release();
    }

    public void setFolderResourceId(int resourceId) {
        adapter_.setFolderResourceId(resourceId);
    }

    public void setFileResourceId(int resourceId) {
        adapter_.setFileResourceId(resourceId);
    }

    public void setThumbnail(String filePath, Bitmap thumbnail) {
        adapter_.setThumbnail(filePath, thumbnail);
    }


    @Override
    public void onUpdatedFileList(ArrayList<FileExploreSource.FileItemInfo> fileItemInfoList) {
        if (listener_ != null)
            listener_.onUpdatedFileList(fileItemInfoList);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                adapter_.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onSelected(File selected) {
        if (listener_ != null)
            listener_.onSelected(selected);
    }
}
