package Radix.Ui.SeekBar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.SeekBar;

@SuppressLint("AppCompatCustomView")
public class RangeSeekBar extends SeekBar {
    private Paint rangePaint_ = null;
    private Rect rangeRect_ = null;
    private double rangeMin_ = -1;
    private double rangeMax_ = -1;

    private void updateRangeRect() {
        if (rangeMin_ == -1.0f || rangeMax_ == -1.0f) {
            rangeRect_ = null;
            return;
        }
        if (rangeRect_ == null)
            rangeRect_ = new Rect();
        int realWidth = getWidth();
        if (realWidth > 0)
            realWidth = realWidth - (getPaddingLeft() + getPaddingRight());
        rangeRect_.top = 1;
        rangeRect_.left = getPaddingLeft() + ((int)((rangeMin_ / getMax()) * realWidth));
        rangeRect_.right = getPaddingLeft() + ((int)((rangeMax_ / getMax()) * realWidth));
        rangeRect_.bottom = getHeight() - 1;
        invalidate();
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (rangeRect_ != null) {
            if (rangePaint_ == null) {
                rangePaint_ = new Paint();
                rangePaint_.setColor(Color.WHITE);
            }
            canvas.drawRect(rangeRect_, rangePaint_);
        }
    }

    public RangeSeekBar(Context context) {
        super(context);
    }

    public RangeSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RangeSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setRangeMin(int min) {
        rangeMin_ = min * 1.0f;
        updateRangeRect();
    }

    public void setRangeMax(int max) {
        rangeMax_ = max * 1.0f;
        updateRangeRect();
    }

    public int getRangeMin() {
        return (int)rangeMin_;
    }

    public int getRangeMax() {
        return (int)rangeMax_;
    }

    @Override
    public synchronized void setMax(int max) {
        super.setMax(max);
        updateRangeRect();
    }
}
