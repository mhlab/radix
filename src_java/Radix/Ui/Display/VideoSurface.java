package Radix.Ui.Display;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.SurfaceHolder;

public class VideoSurface extends GLSurfaceView {
    public interface VideoSurfaceListener {
        void surfaceCreated(SurfaceHolder holder);
        void surfaceChanged(SurfaceHolder holder, int format, int w, int h);
        void surfaceDestroyed(SurfaceHolder holder);
    }

    private VideoSurfaceListener listener_ = null;

    private void initialize() {
        setEGLContextClientVersion(2);
        setRenderer(new VideoSurfaceRenderer());
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }

    public VideoSurface(Context context) {
        super(context);
        initialize();
    }

    public VideoSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public void setListener(VideoSurfaceListener listener) {
        listener_ = listener;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        super.surfaceCreated(holder);
        if (listener_ != null)
            listener_.surfaceCreated(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        super.surfaceChanged(holder, format, w, h);
        if (listener_ != null)
            listener_.surfaceChanged(holder, format, w, h);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        super.surfaceDestroyed(holder);
        if (listener_ != null)
            listener_.surfaceDestroyed(holder);
    }
}
