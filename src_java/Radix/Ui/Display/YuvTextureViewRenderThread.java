package Radix.Ui.Display;

import android.graphics.SurfaceTexture;
import android.util.Log;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.EGL14.EGL_CONTEXT_CLIENT_VERSION;
import static android.opengl.EGL14.EGL_OPENGL_ES2_BIT;

public class YuvTextureViewRenderThread extends Thread {
    private EGL10 egl_ = null;
    private EGLDisplay eglDisplay_ = null;
    private EGLConfig eglConfig_ = null;
    private EGLContext eglContext_ = null;
    private EGLSurface eglSurface_ = null;
    private SurfaceTexture surfaceTexture_ = null;
    private GL gl_ = null;
    private VideoSurfaceRenderer renderer_ = null;

    private volatile boolean sizeChanged_ = true;
    private boolean terminated_ = false;
    private int renderWidth_ = 0;
    private int renderHeight_ = 0;

    private boolean createSurface() {
        if (egl_ == null || eglDisplay_ == null || eglConfig_ == null)
            return false;

        try {
            destroySurface();
            eglSurface_ = egl_.eglCreateWindowSurface(eglDisplay_, eglConfig_, surfaceTexture_, null);
            if (eglSurface_ == null)
                return false;
            if (!egl_.eglMakeCurrent(eglDisplay_, eglSurface_, eglSurface_, eglContext_)) // bind context to current thread and read/draw surface.
                return false;

            return true;
        }
        catch (Exception e) {
        }
        return false;
    }

    private void destroySurface() {
        if (eglSurface_ != null && eglSurface_ != EGL10.EGL_NO_SURFACE) {
            egl_.eglMakeCurrent(eglDisplay_, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
            egl_.eglDestroySurface(eglDisplay_, eglSurface_);
        }
        eglSurface_ = null;
    }

    private boolean initGL() {
        egl_ = (EGL10)EGLContext.getEGL();
        eglDisplay_ = egl_.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        if (eglDisplay_ == null)
            return false;
        int[] version = new int[2];
        if (!egl_.eglInitialize(eglDisplay_, version))
            return false;
        int[] configSpec = {
                EGL10.EGL_RENDERABLE_TYPE,
                EGL_OPENGL_ES2_BIT,
                EGL10.EGL_RED_SIZE, 8,
                EGL10.EGL_GREEN_SIZE, 8,
                EGL10.EGL_BLUE_SIZE, 8,
                EGL10.EGL_ALPHA_SIZE, 8,
                EGL10.EGL_DEPTH_SIZE, 0,
                EGL10.EGL_STENCIL_SIZE, 0,
                EGL10.EGL_NONE
        };
        EGLConfig[] configs = new EGLConfig[1];
        int[] configCount = new int[1];
        if (!egl_.eglChooseConfig(eglDisplay_, configSpec, configs, 1, configCount))
            return false;
        if (configCount[0] == 0)
            return false;
        eglConfig_ = configs[0];
        int[] attrib_list = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE };
        eglContext_ = egl_.eglCreateContext(eglDisplay_, eglConfig_, EGL10.EGL_NO_CONTEXT, attrib_list);
        if (!createSurface())
            return false;
        gl_ = eglContext_.getGL();

        return true;
    }

    private void finishGL() {
        if (egl_ == null || eglDisplay_ == null)
            return;

        if (eglContext_ != null) {
            egl_.eglDestroyContext(eglDisplay_, eglContext_);
            eglContext_ = null;
        }
        egl_.eglTerminate(eglDisplay_);
        destroySurface();
        eglDisplay_ = null;
        egl_ = null;
    }

    public YuvTextureViewRenderThread(SurfaceTexture surfaceTexture) {
        surfaceTexture_ = surfaceTexture;
        setName("YuvTextureRenderThread");
    }

    public void setRenderer(VideoSurfaceRenderer renderer) {
        renderer_ = renderer;
    }

    public void terminate() {
        terminated_ = true;
    }

    public void sizeChanged(int width, int height) {
        renderWidth_ = width;
        renderHeight_ = height;
        sizeChanged_ = true;
    }

    public void setYuv(byte[] ybuf, byte[] ubuf, byte[] vbuf, int yuvWidth, int yuvHeight) {
        if (renderer_ == null)
            return;

        renderer_.setYuv(ybuf, ubuf, vbuf, yuvWidth, yuvHeight);
    }

    @Override
    public void run() {
        if (!initGL())
            return;
        GL10 gl10 = (GL10)gl_;
        renderer_.onSurfaceCreated(gl10, eglConfig_);
        while (!terminated_) {
            if (!egl_.eglMakeCurrent(eglDisplay_, eglSurface_, eglSurface_, eglContext_)) {
                Log.e("TextureViewRenderThread", "Error-MakeCurrent");
                break;
            }
            if (sizeChanged_) {
                createSurface();
                renderer_.onSurfaceChanged(gl10, renderWidth_, renderHeight_);
                sizeChanged_ = false;
            }
            renderer_.onDrawFrame(gl10);
            if (!egl_.eglSwapBuffers(eglDisplay_, eglSurface_)) {
                Log.e("TextureViewRenderThread", "Error-eglSwapBuffer");
                break;
            }
            try {
                Thread.sleep(30);
            }
            catch (InterruptedException e) {
            }
        }
        finishGL();
    }
}
