package Radix.Ui.Display;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.util.AttributeSet;
import android.view.TextureView;

public class YuvTextureView extends TextureView implements TextureView.SurfaceTextureListener {
    private YuvTextureViewRenderThread renderThread_ = null;
    private SurfaceTextureListener listener_ = null;
    private boolean useRenderer_ = true;

    public YuvTextureView(Context context) {
        super(context);
        setSurfaceTextureListener(this);
    }

    public YuvTextureView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public YuvTextureView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setSurfaceTextureListener(this);
    }

    public void setGLTextureViewListener(SurfaceTextureListener listener) {
        listener_ = listener;
    }

    public void stopRender() {
        if (renderThread_ != null) {
            try {
                renderThread_.terminate();
                renderThread_.join();
            }
            catch (Exception e) {
            }
            renderThread_ = null;
        }
        useRenderer_ = false;
    }

    public void startRender() {
        if (!useRenderer_)
            useRenderer_ = true;
        if (isAvailable() && renderThread_ == null) {
            renderThread_ = new YuvTextureViewRenderThread(getSurfaceTexture());
            renderThread_.setRenderer(new VideoSurfaceRenderer());
            renderThread_.sizeChanged(getWidth(), getHeight());
            renderThread_.start();
        }
    }

    public void setYuv(byte[] ybuf, byte[] ubuf, byte[] vbuf, int yuvWidth, int yuvHeight) {
        startRender();
        if (renderThread_ != null)
            renderThread_.setYuv(ybuf, ubuf, vbuf, yuvWidth, yuvHeight);
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        if (useRenderer_) {
            renderThread_ = new YuvTextureViewRenderThread(surface);
            renderThread_.setRenderer(new VideoSurfaceRenderer());
            renderThread_.sizeChanged(width, height);
            renderThread_.start();
        }
        if (listener_ != null)
            listener_.onSurfaceTextureAvailable(surface, width, height);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        if (renderThread_ != null)
            renderThread_.sizeChanged(width, height);
        if (listener_ != null)
            listener_.onSurfaceTextureSizeChanged(surface, width, height);
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        boolean isDestroy = true;
        if (listener_ != null)
            isDestroy = listener_.onSurfaceTextureDestroyed(surface);
        if (isDestroy && renderThread_ != null) {
            renderThread_.terminate();
            renderThread_ = null;
        }
        return isDestroy;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        if (listener_ != null)
            listener_.onSurfaceTextureUpdated(surface);
    }
}
