package Radix.Ui.Display;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class YuvRenderShader {
    private static final String Vertex_Shader_Code =
            "attribute vec4 vPosition;" +
            "attribute vec2 i_texCoord;" +
            "varying vec2 texCoord;" +
            "void main() {" +
            "  gl_Position = vPosition;" +
            "  texCoord = i_texCoord;" +
            "}";
    private static final String Fragment_Shader_Code =
            "precision mediump float;" +
            "varying vec2 texCoord;" +
            "uniform vec4 vColor;" +
            "uniform sampler2D tex_y;" +
            "uniform sampler2D tex_u;" +
            "uniform sampler2D tex_v;" +
            "const mat3 yuv2rgb = mat3("+
            "	1, 0, 1.2802,"+
            "	1, -0.214821, -0.380589,"+
            "	1, 2.127982, 0"+
            "	);"+
            "void main() {" +
            "	vec3 yuv = vec3("+
            "	  1.1643 * (texture2D(tex_y, texCoord).r - 0.0627),"+
            "    texture2D(tex_u, texCoord).r - 0.5,"+
            "	  texture2D(tex_v, texCoord).r - 0.5"+
            "	);"+
            "	vec3 rgb = yuv * yuv2rgb;"+
            "	gl_FragColor = vec4(rgb, 1.0);"+
            "}";
    private static final int Vertex_Size = 2;
    private static float squreCoords[] = {-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f};
    private static float textureCoords[] = {0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f};
    private int program_ = 0;
    private FloatBuffer vertexBuffer_ = null;
    private FloatBuffer textureCoordBuffer_ = null;
    private int[] yuvTexture_ = {-1, -1, -1};

    private void initVertextBuffer() {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(squreCoords.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        vertexBuffer_ = byteBuffer.asFloatBuffer();
        vertexBuffer_.put(squreCoords);
        vertexBuffer_.position(0);

        byteBuffer = ByteBuffer.allocateDirect(textureCoords.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        textureCoordBuffer_ = byteBuffer.asFloatBuffer();
        textureCoordBuffer_.put(textureCoords);
        textureCoordBuffer_.position(0);
    }

    private int setYuvTexture(int pos, ByteBuffer yuvData, int yuvWidth, int yuvHeight) {
        if (yuvTexture_[pos] == -1) {
            int[] texture = new int[1];
            GLES20.glGenTextures(1, texture, 0);
            yuvTexture_[pos] = texture[0];
        }
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, yuvTexture_[pos]);
        if (pos == 0)
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, yuvWidth, yuvHeight, 0, GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, yuvData);
        else
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, yuvWidth / 2, yuvHeight / 2, 0, GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, yuvData);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        return yuvTexture_[pos];
    }

    public YuvRenderShader() {
        int vertexShader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
        GLES20.glShaderSource(vertexShader, Vertex_Shader_Code);
        GLES20.glCompileShader(vertexShader);
        int FragmentShader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
        GLES20.glShaderSource(FragmentShader, Fragment_Shader_Code);
        GLES20.glCompileShader(FragmentShader);

        program_ = GLES20.glCreateProgram();
        GLES20.glAttachShader(program_, vertexShader);
        GLES20.glAttachShader(program_, FragmentShader);
        GLES20.glLinkProgram(program_);

        initVertextBuffer();
    }

    public void draw(ByteBuffer yBuf, ByteBuffer uBuf, ByteBuffer vBuf, int yuvWidth, int yuvHeight) {
        GLES20.glUseProgram(program_);
        int positionHandle = GLES20.glGetAttribLocation(program_, "vPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, Vertex_Size, GLES20.GL_FLOAT, false, 0, vertexBuffer_);
        int coordHandle = GLES20.glGetAttribLocation(program_, "i_texCoord");
        GLES20.glEnableVertexAttribArray(coordHandle);
        GLES20.glVertexAttribPointer(coordHandle, Vertex_Size, GLES20.GL_FLOAT, false, 0, textureCoordBuffer_);

        int yTexture = setYuvTexture(0, yBuf, yuvWidth, yuvHeight);
        int uTexture = setYuvTexture(1, uBuf, yuvWidth, yuvHeight);
        int vTexture = setYuvTexture(2, vBuf, yuvWidth, yuvHeight);

        int yLocation = GLES20.glGetUniformLocation(program_, "tex_y");
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, yTexture);
        GLES20.glUniform1i(yLocation, 0);
        int uLocation = GLES20.glGetUniformLocation(program_, "tex_u");
        GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, uTexture);
        GLES20.glUniform1i(uLocation, 1);
        int vLocation = GLES20.glGetUniformLocation(program_, "tex_v");
        GLES20.glActiveTexture(GLES20.GL_TEXTURE2);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, vTexture);
        GLES20.glUniform1i(vLocation, 2);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, squreCoords.length / Vertex_Size);
        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(coordHandle);
    }
}
