package Radix.Ui.Display;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class VideoSurfaceRenderer implements GLSurfaceView.Renderer {
    private YuvRenderShader shader_ = null;
    private ByteBuffer yBuf_ = null;
    private ByteBuffer uBuf_ = null;
    private ByteBuffer vBuf_ = null;
    private Lock yuvLock_ = new ReentrantLock();
    private int yuvWidth_ = 0;
    private int yuvHeight_ = 0;

    public void setYuv(byte[] ybuf, byte[] ubuf, byte[] vbuf, int yuvWidth, int yuvHeight) {
        if (!yuvLock_.tryLock())
            return;

        if (yBuf_ == null || yBuf_.capacity() < ybuf.length)
            yBuf_ = ByteBuffer.allocateDirect(ybuf.length);
        if (uBuf_ == null || uBuf_.capacity() < ubuf.length)
            uBuf_ = ByteBuffer.allocateDirect(ubuf.length);
        if (vBuf_ == null || vBuf_.capacity() < vbuf.length)
            vBuf_ = ByteBuffer.allocateDirect(vbuf.length);
        yBuf_.clear();
        uBuf_.clear();
        vBuf_.clear();
        yBuf_.put(ybuf);
        uBuf_.put(ubuf);
        vBuf_.put(vbuf);
        yBuf_.position(0);
        uBuf_.position(0);
        vBuf_.position(0);
        yuvWidth_ = yuvWidth;
        yuvHeight_ = yuvHeight;

        yuvLock_.unlock();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        shader_ = new YuvRenderShader();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        gl.glViewport(0, 0, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        yuvLock_.lock();
        try {
            if (yBuf_ != null && uBuf_ != null && vBuf_ != null)
                shader_.draw(yBuf_, uBuf_, vBuf_, yuvWidth_, yuvHeight_);
        }
        finally {
            yuvLock_.unlock();
        }
    }
}
