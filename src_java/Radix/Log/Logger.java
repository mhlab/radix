package Radix.Log;

import android.util.Log;

public class Logger {
    public static void writeDebugMessageInDebugMode(String tag, String message) {
        Log.d(tag, message);
    }
}
