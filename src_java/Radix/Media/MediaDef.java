package Radix.Media;

public class MediaDef {
    public static class MediaCodec {
        public static final int MEDIA_CODEC_UNKNOWN = 0;
        public static final int MEDIA_CODEC_MJPEG = 8;
        public static final int MEDIA_CODEC_H264 = 28;
        public static final int MEDIA_CODEC_MP3 = 86017;
        public static final int MEDIA_CODEC_AAC = 86018;
        public static final int MEDIA_CODEC_AC3 = 86019;

        public static final int MEDIA_PIXELFORMAT_YUV420 = 0;

        public static final String MEDIA_ANDROID_CODEC_MJPEG = "video/mjpeg";
        public static final String MEDIA_ANDROID_CODEC_H264 = "video/avc";
        public static final String MEDIA_ANDROID_CODEC_MP3 = "audio/mpeg";
        public static final String MEDIA_ANDROID_CODEC_AAC = "audio/mp4a-latm";
        public static final String MEDIA_ANDROID_CODEC_AC3 = "audio/ac3";

        public static boolean isVideoCodec(String androidCodec) {
            return (androidCodec.equals(MEDIA_ANDROID_CODEC_H264) || androidCodec.equals(MEDIA_ANDROID_CODEC_MJPEG));
        }

        public static boolean isVideoCodec(int mediaCodec) {
            return (mediaCodec <= MEDIA_CODEC_H264);
        }

        public static boolean isAudioCodec(int mediaCodec) {
            return (mediaCodec >= MEDIA_CODEC_MP3);
        }

        public static String mediaCodecToAndroidMediaCodec(int mediaCodec) {
            switch (mediaCodec) {
                case MEDIA_CODEC_MJPEG:
                    return MEDIA_ANDROID_CODEC_MJPEG;
                case MEDIA_CODEC_H264:
                    return MEDIA_ANDROID_CODEC_H264;
                case MEDIA_CODEC_MP3:
                    return MEDIA_ANDROID_CODEC_MP3;
                case MEDIA_CODEC_AAC:
                    return MEDIA_ANDROID_CODEC_AAC;
                case MEDIA_CODEC_AC3:
                    return MEDIA_ANDROID_CODEC_AC3;
            }

            return "";
        }

        public static int androidCodecToMediaCodec(String androidCodec) {
            if (androidCodec.equals(MEDIA_ANDROID_CODEC_MJPEG))
                return MEDIA_CODEC_MJPEG;
            else if (androidCodec.equals(MEDIA_ANDROID_CODEC_H264))
                return MEDIA_CODEC_H264;
            else if (androidCodec.equals(MEDIA_ANDROID_CODEC_MP3))
                return MEDIA_CODEC_MP3;
            else if (androidCodec.equals(MEDIA_ANDROID_CODEC_AAC))
                return MEDIA_CODEC_AAC;
            else if (androidCodec.equals(MEDIA_ANDROID_CODEC_AC3))
                return MEDIA_CODEC_AC3;

            return MEDIA_CODEC_UNKNOWN;
        }
    }
}
