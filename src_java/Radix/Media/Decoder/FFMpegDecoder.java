package Radix.Media.Decoder;

public class FFMpegDecoder {
    public static final int FFMPEG_DECODER_SUCCESS = 0;
    public static final int FFMPEG_DECODER_FAIL = -1;
    public static final int FFMPEG_DECODER_NOT_SUPPORT_PIXELFORMAT = -2;
    public static final int FFMPEG_DECODER_NOT_INITIALIZE = -3;
    public static final int FFMPEG_DECODER_INVALID_ARGUMENTS = -4;

    private native long create();
    private native void destroy(long instance);
    private native boolean openVideoCodec(long instance, int codec);
    private native boolean openAudioCodec(long instance, int codec, int sampleRate, int channels, int channelLayout);
    private native void closeCodec(long instance);
    private native int decodeVideoToYuv(long instance, byte[] src, int srcsize, byte[] dstY, byte[] dstU, byte[] dstV, int[] resolution, int pixelformat);
    private native int decodeAudio(long instance, byte[] src, int srcsize, byte[] dst, int[] dstsize);
    private native boolean isOpen(long instance);
    private long instance_ = 0;

    public FFMpegDecoder() {
        instance_ = create();
    }

    public void destroy() {
        if (instance_ == 0)
            return;

        destroy(instance_);
    }

    public boolean openVideoCodec(int codec) {
        if (instance_ == 0)
            return false;

        return openVideoCodec(instance_, codec);
    }

    public boolean openAudioCodec(int codec, int sampleRate, int channels, int channelLayout) {
        if (instance_ == 0)
            return false;

        return openAudioCodec(instance_, codec, sampleRate, channels, channelLayout);
    }

    public void closeCodec() {
        if (instance_ == 0)
            return;

        closeCodec(instance_);
    }

    public int decodeVideoToYuv(byte[] src, int srcsize, byte[] dstY, byte[] dstU, byte[] dstV, int[] resolution, int pixelformat) {
        if (instance_ == 0)
            return FFMPEG_DECODER_NOT_INITIALIZE;

        return decodeVideoToYuv(instance_, src, srcsize, dstY, dstU, dstV, resolution, pixelformat);
    }

    public int decodeAudio(byte[] src, int srcsize, byte[] dst, int[] dstsize) {
        if (instance_ == 0)
            return FFMPEG_DECODER_NOT_INITIALIZE;

        return decodeAudio(instance_, src, srcsize, dst, dstsize);
    }

    public boolean isOpen() {
        if (instance_ == 0)
            return false;

        return isOpen(instance_);
    }

    static {
        System.loadLibrary("FFMpegDecoder");
    }
}
