package Radix.Media.Decoder;

import android.media.MediaFormat;
import android.view.Surface;

import java.nio.ByteBuffer;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import Radix.Media.MediaDef;
import Radix.Media.Parser.VideoStreamParser;

import static Radix.Media.MediaDef.MediaCodec.MEDIA_PIXELFORMAT_YUV420;

public class MediaDecodeThread extends Thread implements MediaCodecDecoder.MediaCodecDecoderListener{
    private MediaCodecDecoder videoDecoder_ = new MediaCodecDecoder();
    private MediaCodecDecoder audioDecoder_ = new MediaCodecDecoder();
    private FFMpegDecoder ffmpegVideoDecoder_ = new FFMpegDecoder();
    private FFMpegDecoder ffmpegAudioDecoder_ = new FFMpegDecoder();
    private Queue<MediaQueueItem> mediaQueue_ = new ConcurrentLinkedQueue<>();
    private MediaDecodeThreadListener listener_ = null;
    private VideoStreamParser videoStreamParser_ = new VideoStreamParser();
    private boolean useHwDecode_ = true;
    private byte[] yBuf_ = null;
    private byte[] uBuf_ = null;
    private byte[] vBuf_ = null;

    private int videoWidth_ = -1;
    private int videoHeight_ = -1;
    private Surface videoSurface_ = null;

    private int audioSampleRate_ = -1;
    private int audioChannels_ = -1;
    private int audioProfile_ = -1;
    private int audioChannelLayout_ = -1;
    private byte[] audioDecodeBuf_ = null;

    private boolean terminated_ = false;
    private boolean closeCodec_ = false;
    private boolean needKeyFrame_ = false;

    private class MediaQueueItem {
        public byte[] mediaData_ = null;
        public int codec_ = Radix.Media.MediaDef.MediaCodec.MEDIA_CODEC_UNKNOWN;
        public long presentationTime_ = 0;
    }

    public interface MediaDecodeThreadListener {
        void onDecodedFrame(boolean isVideo, byte[] decodeStream, long presentationTime);
        void onDecodedFrame(byte[] ybuf, byte[] ubuf, byte[] vbuf, int yuvWidth, int yuvHeight, long presentationTime);
        void onChangedAudioOutputFormat(MediaFormat format);
        void onChangeDecodeMode(boolean isVideoCodec, boolean useHwDecode);
        void onChangedDecodeMode(boolean isVideoCodec, boolean useHwDecode);
    }

    private void sleep() {
        try {
            Thread.sleep(1);
        }
        catch (InterruptedException e) {
        }
    }

    private boolean setVideoResolution(int codec, byte[] media) {
        VideoStreamParser.VideoResolution videoResolution = videoStreamParser_.getVideoResolution(media, codec);
        if (videoResolution == null || videoResolution.videoWidth_ == 0 || videoResolution.videoHeight_ == 0)
            return false;
        videoWidth_ = videoResolution.videoWidth_;
        videoHeight_ = videoResolution.videoHeight_;
        return true;
    }

    public MediaDecodeThread() {
        audioDecoder_.setListener(this);
    }

    public void setListener(MediaDecodeThreadListener listener) {
        listener_ = listener;
    }

    public void setVideoCodecParam(int videoWidth, int videoHeight, Surface surface) {
        videoWidth_ = videoWidth;
        videoHeight_ = videoHeight;
        videoSurface_ = surface;
        if (videoSurface_ == null)
            throw new RuntimeException("Surface is null");
    }

    public void setAudioCodecParam(int sampleRate, int channels, int profile, int channelLayout) {
        audioSampleRate_ = sampleRate;
        audioChannels_ = channels;
        audioProfile_ = profile;
        audioChannelLayout_ = channelLayout;
    }

    public boolean isVideoCodec(int codec) {
        return (MediaDef.MediaCodec.isVideoCodec(codec));
    }

    public boolean isAudioCodec(int codec) {
        return (MediaDef.MediaCodec.isAudioCodec(codec));
    }

    public boolean openVideoCodec(int codec, byte[] codecData, int videoWidth, int videoHeight, Surface surface) {
        if (videoDecoder_.isOpen())
            videoDecoder_.close();
        if (ffmpegVideoDecoder_.isOpen())
            ffmpegVideoDecoder_.closeCodec();
        needKeyFrame();
        setVideoCodecParam(videoWidth, videoHeight, surface);
        if (useHwDecode_) {
            if (surface == null)
                return false;
            return videoDecoder_.openVideo(MediaDef.MediaCodec.mediaCodecToAndroidMediaCodec(codec), codecData, videoWidth, videoHeight, surface);
        }
        else
            return ffmpegVideoDecoder_.openVideoCodec(codec);
    }

    public boolean openAudioCodec(int codec, byte[] codecData, int samplerate, int channels, int profile, int channelLayout) {
        if (audioDecoder_.isOpen())
            audioDecoder_.close();

        audioDecodeBuf_ = null;
        setAudioCodecParam(samplerate, channels, profile, channelLayout);
        return audioDecoder_.openAudio(MediaDef.MediaCodec.mediaCodecToAndroidMediaCodec(codec), codecData, samplerate, channels, profile);
    }

    public void sendMediaFrame(byte[] media, int mediaSize, int codec, long presentationTime) {
        if (needKeyFrame_) {
            if (!MediaCodecDecoder.isKeyFrame(MediaDef.MediaCodec.mediaCodecToAndroidMediaCodec(codec), media))
                return;
            needKeyFrame_ = false;
        }
        MediaQueueItem queueItem = new MediaQueueItem();
        queueItem.mediaData_ = new byte[mediaSize];
        System.arraycopy(media, 0, queueItem.mediaData_, 0, mediaSize);
        queueItem.codec_ = codec;
        queueItem.presentationTime_ = presentationTime;
        mediaQueue_.add(queueItem);
    }

    public boolean decodeVideo(byte[] media, int mediaSize, int codec, long presentationTime) {
        if (useHwDecode_) {
            if (!videoDecoder_.isOpen()) {
                if (videoWidth_ == 0 || videoHeight_ == 0) {
                    if (!setVideoResolution(codec, media))
                        return false;
                }
                if (!openVideoCodec(codec, media, videoWidth_, videoHeight_, videoSurface_))
                    return false;
            }
            videoDecoder_.push(media, mediaSize, presentationTime);
            if (!videoDecoder_.decode(true))
                return false;
            if (listener_ != null)
                listener_.onDecodedFrame(true, null, videoDecoder_.mediaPresentationTime());
            return true;
        }
        else {
            if (!ffmpegVideoDecoder_.isOpen()) {
                if (videoWidth_ != 0 && videoHeight_ != 0) {
                    if (!setVideoResolution(codec, media))
                        return false;
                }
                if (!ffmpegVideoDecoder_.openVideoCodec(codec))
                    return false;
            }
            int ySize = videoWidth_ * videoHeight_;
            int uvSize = (videoWidth_ / 2) * (videoHeight_ / 2);
            if (yBuf_ == null || yBuf_.length != ySize)
                yBuf_ = new byte[ySize];
            if (uBuf_ == null || uBuf_.length != uvSize)
                uBuf_ = new byte[uvSize];
            if (vBuf_ == null || vBuf_.length != uvSize)
                vBuf_ = new byte[uvSize];
            if (ffmpegVideoDecoder_.decodeVideoToYuv(media, mediaSize, yBuf_, uBuf_, vBuf_, null, MEDIA_PIXELFORMAT_YUV420) != FFMpegDecoder.FFMPEG_DECODER_SUCCESS)
                return false;
            if (listener_ != null)
                listener_.onDecodedFrame(yBuf_, uBuf_, vBuf_, videoWidth_, videoHeight_, presentationTime);
            return true;
        }
    }

    public boolean decodeAudio(byte[] media, int mediaSize, int codec, long presentationTime) {
        if (useHwDecode_) {
            if (!audioDecoder_.isOpen()) {
                if (!openAudioCodec(codec, null, audioSampleRate_, audioChannels_, audioProfile_, audioChannelLayout_))
                    return false;
            }
            if (!audioDecoder_.push(media, mediaSize, presentationTime))
                return false;
            ByteBuffer audioDecodeBuf = audioDecoder_.decode();
            if (audioDecodeBuf == null)
                return false;
            byte[] audioStream = new byte[audioDecoder_.mediaBufferSize()];
            audioDecodeBuf.get(audioStream);
            audioDecodeBuf.clear();
            if (listener_ != null)
                listener_.onDecodedFrame(false, audioStream, audioDecoder_.mediaPresentationTime());
            audioDecoder_.releaseBuffer(false);
        }
        else {
            if (!ffmpegAudioDecoder_.isOpen()) {
                if (!ffmpegAudioDecoder_.openAudioCodec(codec, audioSampleRate_, audioChannels_, audioChannelLayout_))
                    return false;
                MediaFormat mediaFormat = new MediaFormat();
                mediaFormat.setInteger(MediaFormat.KEY_SAMPLE_RATE, audioSampleRate_);
                if (listener_ != null)
                    listener_.onChangedAudioOutputFormat(mediaFormat);
            }
            int[] audioDecodeSize = {0};
            if (audioDecodeBuf_ == null) {
                if (ffmpegAudioDecoder_.decodeAudio(media, mediaSize, null, audioDecodeSize) != FFMpegDecoder.FFMPEG_DECODER_SUCCESS)
                    return false;
                audioDecodeBuf_ = new byte[audioDecodeSize[0]];
            }
            if (ffmpegAudioDecoder_.decodeAudio(media, mediaSize, audioDecodeBuf_, audioDecodeSize) != FFMpegDecoder.FFMPEG_DECODER_SUCCESS)
                return false;
            if (listener_ != null)
                listener_.onDecodedFrame(false, audioDecodeBuf_, presentationTime);
        }

        return true;
    }

    public boolean decode(byte[] media, int mediaSize, int codec, long presentationTime) {
        if (isVideoCodec(codec)) {
            return decodeVideo(media, mediaSize, codec, presentationTime);
        }
        else {
            return decodeAudio(media, mediaSize, codec, presentationTime);
        }
    }

    public void terminate() {
        terminated_ = true;
    }

    public void closeCodec() {
        closeCodec_ = true;
    }

    public boolean isOpenCodec() {
        return ((videoDecoder_ != null) && (videoDecoder_.isOpen())) || ((audioDecoder_ != null) && (audioDecoder_.isOpen()));
    }

    public void needKeyFrame() {
        needKeyFrame_ = true;
    }

    public void clearFrames() {
        mediaQueue_.clear();
    }

    public void useHwDecode(boolean use) {
        useHwDecode_ = use;
    }

    @Override
    public void run() {
        MediaQueueItem queueItem;
        boolean curUseHwDecode = useHwDecode_;
        while (!terminated_) {
            if (terminated_)
                break;

            if (closeCodec_) {
                mediaQueue_.clear();
                videoDecoder_.close();
                audioDecoder_.close();
                if (ffmpegVideoDecoder_ != null)
                    ffmpegVideoDecoder_.closeCodec();
                closeCodec_ = false;
                continue;
            }

            queueItem = mediaQueue_.poll();
            if (queueItem == null) {
                sleep();
                continue;
            }
            if (curUseHwDecode != useHwDecode_) {
                if (isVideoCodec(queueItem.codec_)) {
                    if (!MediaCodecDecoder.isKeyFrame(MediaDef.MediaCodec.mediaCodecToAndroidMediaCodec(queueItem.codec_), queueItem.mediaData_))
                        continue;
                    if (listener_ != null)
                        listener_.onChangeDecodeMode(true, curUseHwDecode);
                    if (useHwDecode_) {
                        if (ffmpegVideoDecoder_ != null)
                            ffmpegVideoDecoder_.closeCodec();
                    }
                    else
                        videoDecoder_.close();
                    if (listener_ != null)
                        listener_.onChangedDecodeMode(true, useHwDecode_);
                }
                else {
                    if (listener_ != null)
                        listener_.onChangeDecodeMode(false, curUseHwDecode);
                    if (useHwDecode_) {
                        if (ffmpegAudioDecoder_ != null)
                            ffmpegAudioDecoder_.closeCodec();
                    }
                    else
                        audioDecoder_.close();
                    if (listener_ != null)
                        listener_.onChangedDecodeMode(false, useHwDecode_);
                }
                curUseHwDecode = useHwDecode_;
            }
            decode(queueItem.mediaData_, queueItem.mediaData_.length, queueItem.codec_, queueItem.presentationTime_);
        }
        mediaQueue_.clear();
        videoDecoder_.close();
        audioDecoder_.close();
        if (ffmpegVideoDecoder_ != null) {
            ffmpegVideoDecoder_.closeCodec();
            ffmpegVideoDecoder_.destroy();
        }
    }

    @Override
    public void onChangedOutputFormatChanged(MediaFormat format) {
        if (listener_ != null)
            listener_.onChangedAudioOutputFormat(format);
    }
}
