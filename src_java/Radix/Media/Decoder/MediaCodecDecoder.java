package Radix.Media.Decoder;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.Surface;

import java.nio.ByteBuffer;
import java.util.Arrays;

import static Radix.Media.MediaDef.MediaCodec.MEDIA_ANDROID_CODEC_AAC;
import static Radix.Media.MediaDef.MediaCodec.MEDIA_ANDROID_CODEC_H264;

public class MediaCodecDecoder {
    public interface MediaCodecDecoderListener {
        void onChangedOutputFormatChanged(MediaFormat format);
    }

    private MediaCodec decoder_ = null;
    private ByteBuffer[] inputBuffer_ = null;
    private MediaCodec.BufferInfo outputBufferInfo_ = null;
    private boolean opened_ = false;
    private int outputBufferId_ = -1;
    private MediaCodecDecoderListener listener_ = null;
    private String codecName_ = "";

    private int findH264StartCode(byte[] h264Frame, int startIndex) {
        for (int i = startIndex; i < h264Frame.length; i++) {
            if (i + 3 < h264Frame.length) {
                if (h264Frame[i] == 0 && h264Frame[i+1] == 0 && h264Frame[i+2] == 0 && h264Frame[i+3] == 1)
                    return i;
            }
            if (i + 2 < h264Frame.length) {
                if (h264Frame[i] == 0 && h264Frame[i+1] == 0 && h264Frame[i+2] == 1)
                    return i;
            }
        }
        return -1;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private boolean setMediaFormatByH264(MediaFormat format, byte[] frame) {
        if (frame == null)
            return false;

        int spsIndex = -1;
        int ppsIndex = -1;
        int nalIndex = 0;
        while (nalIndex < frame.length) {
            nalIndex = findH264StartCode(frame, nalIndex);
            if (spsIndex > -1 && ppsIndex > -1) {
                if (nalIndex == -1)
                    nalIndex = frame.length;
                break;
            }
            if (spsIndex == -1 && (((frame[nalIndex + 4] & 0x1f) == 0x07) || ((frame[nalIndex + 3] & 0x1f) == 0x07)))
                spsIndex = nalIndex;
            if (ppsIndex == -1 && (((frame[nalIndex + 4] & 0x1f) == 0x08) || ((frame[nalIndex + 3] & 0x1f) == 0x08)))
                ppsIndex = nalIndex;
            if (nalIndex > -1)
                nalIndex += 3;
        }
        if (spsIndex == -1 || ppsIndex == -1)
            return false;

        byte[] sps = Arrays.copyOfRange(frame, spsIndex, ppsIndex);
        byte[] pps = Arrays.copyOfRange(frame, ppsIndex, nalIndex);

        format.setString("KEY_MIME", MEDIA_ANDROID_CODEC_H264);
        format.setByteBuffer("csd-0", ByteBuffer.wrap(sps));
        format.setByteBuffer("csd-1", ByteBuffer.wrap(pps));

        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private boolean setMediaFormatByAAC(MediaFormat format, int profile, int samplerate, int channels) {
        int samplerateArr[] = {96000, 88200, 64000, 48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000};
        int samplerateIndex = -1;
        for (int i = 0; i < samplerateArr.length; i++) {
            if (samplerateArr[i] == samplerate) {
                samplerateIndex = i;
                break;
            }
        }
        if (samplerateIndex == -1)
            return false;

        /*
         CSD-0 is ADTS header. Refer below site.
         https://wiki.multimedia.cx/index.php/ADTS
         */
        profile++;
        ByteBuffer csd = ByteBuffer.allocate(2);
        csd.put(0, (byte)((profile << 3) | (samplerateIndex >> 1)));
        csd.put(1, (byte)((samplerate << 7) & 0x80 | channels << 3));
        format.setByteBuffer("csd-0", csd);

        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private boolean startDecoder(MediaFormat format, Surface surface) {
        if (decoder_ == null)
            return false;

        decoder_.configure(format, surface, null, 0);
        decoder_.start();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            inputBuffer_ = decoder_.getInputBuffers();
            if (inputBuffer_ == null)
                return false;
        }
        codecName_ = format.getString(MediaFormat.KEY_MIME);
        opened_ = true;
        return true;
    }

    public void setListener(MediaCodecDecoderListener listener) {
        listener_ = listener;
    }

    // 1. if codec is h.264, frame must contain sps, pps
    public boolean openVideo(String codec, byte[] frame, int videoWidth, int videoHeight, Surface surface) {
        close();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                decoder_ = MediaCodec.createDecoderByType(codec);
                outputBufferInfo_ = new MediaCodec.BufferInfo();
            }
            else
                return false;

            MediaFormat format = MediaFormat.createVideoFormat(codec, videoWidth, videoHeight);
            if (format == null)
                return false;
            format.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, videoWidth * videoHeight);
            if (codec.equals(MEDIA_ANDROID_CODEC_H264)) {
                if (!setMediaFormatByH264(format, frame))
                    return false;
            }

            return startDecoder(format, surface);
        }
        catch (Exception e) {
        }

        return false;
    }

    public boolean openAudio(String codec, byte[] codecData, int samplerate, int channels, int profile) {
        close();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                decoder_ = MediaCodec.createDecoderByType(codec);
                outputBufferInfo_ = new MediaCodec.BufferInfo();
            }
            else
                return false;

            MediaFormat format = MediaFormat.createAudioFormat(codec, samplerate, channels);
            if (format == null)
                return false;

            format.setString(MediaFormat.KEY_MIME, codec);
            format.setInteger(MediaFormat.KEY_SAMPLE_RATE, samplerate);
            format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, channels);
            if (codecData != null) {
                ByteBuffer codecBuffer = ByteBuffer.wrap(codecData);
                format.setByteBuffer("csd-0", codecBuffer);
            }
            else {
                if (codec.equals(MEDIA_ANDROID_CODEC_AAC))
                    setMediaFormatByAAC(format, profile, samplerate, channels);
            }

            return startDecoder(format, null);
        }
        catch (Exception e) {
        }

        return false;
    }

    public void close() {
        opened_ = false;
        if (decoder_ == null)
            return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            decoder_.stop();
            decoder_.release();
        }
        codecName_ = "";
        decoder_ = null;
        inputBuffer_ = null;
        outputBufferId_ = -1;
    }

    public boolean push(byte[] frame, int framesize, long presentationTime) {
        if (decoder_ == null)
            return false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            int inputBufferId;
            inputBufferId = decoder_.dequeueInputBuffer(1);
            if (inputBufferId < 0)
                return false;

            ByteBuffer inputBuffer;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                inputBuffer = decoder_.getInputBuffer(inputBufferId);
            else
                inputBuffer = inputBuffer_[inputBufferId];

            inputBuffer.clear();
            inputBuffer.put(frame, 0, framesize);
            decoder_.queueInputBuffer(inputBufferId, 0, framesize, presentationTime, 0);

            return true;
        }

       return false;
    }

    public boolean decode(boolean render) {
        if (decoder_ == null)
            return false;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            outputBufferId_ = decoder_.dequeueOutputBuffer(outputBufferInfo_, 100);
            if (outputBufferId_ < 0) {
                if (outputBufferId_ == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    MediaFormat format = decoder_.getOutputFormat();
                    if (listener_ != null)
                        listener_.onChangedOutputFormatChanged(format);
                }
                Log.d("[Decode]", String.format("Decode Fail!!: %d", outputBufferId_));
                return false;
            }

            decoder_.releaseOutputBuffer(outputBufferId_, render);
            return true;
        }

        return false;
    }

    public ByteBuffer decode() {
        if (decoder_ == null)
            return null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            outputBufferId_ = decoder_.dequeueOutputBuffer(outputBufferInfo_, 100);
            if (outputBufferId_ < 0) {
                if (outputBufferId_ == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    MediaFormat format = decoder_.getOutputFormat();
                    if (listener_ != null)
                        listener_.onChangedOutputFormatChanged(format);
                }
                return null;
            }

            ByteBuffer decodeBuf = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                decodeBuf = decoder_.getOutputBuffer(outputBufferId_);
            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                decodeBuf = decoder_.getOutputBuffers()[outputBufferId_];

            return decodeBuf;
        }

        return null;
    }

    public int mediaBufferSize() {
        if (outputBufferInfo_ == null)
            return 0;

        return outputBufferInfo_.size;
    }

    public long mediaPresentationTime() {
        if (outputBufferInfo_ == null)
            return 0;

        return outputBufferInfo_.presentationTimeUs;
    }

    public void releaseBuffer(boolean render) {
        if (decoder_ == null || outputBufferId_ == -1)
            return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            decoder_.releaseOutputBuffer(outputBufferId_, render);
    }

    public boolean isOpen() {
        return opened_;
    }

    public boolean isSameCodec(String codecName) {
        if (codecName == null)
            return false;

        return codecName_.equals(codecName);
    }

    public static boolean isKeyFrame(String codec, byte[] framedata) {
        if (!codec.equals(MEDIA_ANDROID_CODEC_H264))
            return true;
        if (framedata.length < 4)
            return false;

        int len = framedata.length;
        int nalType;
        for (int i = 0 ; i < len; i++) {
            if (len - i >= 4 && framedata[i] == 0 && framedata[i+1] == 0 && framedata[i+2] == 0 && framedata[i+3] == 1) {
                nalType = framedata[i + 4] & 0x1f;
                if (nalType == 0x05 || nalType == 0x07)
                    return true;
            }
            else if (len - i >= 3 && framedata[i] == 0 && framedata[i+1] == 0 && framedata[i+2] == 1) {
                nalType = framedata[i + 3] & 0x1f;
                if (nalType == 0x05 || nalType == 0x07)
                    return true;
            }
        }

        return false;
    }
}
