package Radix.Media.Parser;

public class VideoStreamParser {
    public class VideoResolution {
        public int videoWidth_ = 0;
        public int videoHeight_ = 0;
    }

    private native void getVideoResolution(byte[] videoFrame, int codec, int[] videoResolution);

    public VideoResolution getVideoResolution(byte[] videoFrame, int codec) {
        int[] videoResolutionArr = {0, 0};
        getVideoResolution(videoFrame, codec, videoResolutionArr);
        VideoResolution ret = new VideoResolution();
        ret.videoWidth_ = videoResolutionArr[0];
        ret.videoHeight_ = videoResolutionArr[1];

        return ret;
    }

    static {
        System.loadLibrary("VideoStreamParser");
    }
}
