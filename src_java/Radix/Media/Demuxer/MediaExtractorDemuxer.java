package Radix.Media.Demuxer;

import android.media.MediaExtractor;
import android.media.MediaFormat;

import org.json.JSONObject;

import java.nio.ByteBuffer;

import Radix.Media.MediaDef;
import Radix.Thread.SignalThread;

import static Radix.Media.Demuxer.DemuxerDef.DEMUXER_COMMAND_NONE;
import static Radix.Media.Demuxer.DemuxerDef.DEMUXER_COMMAND_OPEN;
import static Radix.Media.Demuxer.DemuxerDef.DEMUXER_COMMAND_PLAY;

public class MediaExtractorDemuxer {
    private DemuxerListener listener_ = null;
    private String fileName_ = "";
    private MediaExtractor mediaExtractor_ = new MediaExtractor();
    private SignalThread signalThread_ = null;
    private int videoTrackIndex_ = -1;
    private int audioTrackIndex_ = -1;
    private int currentDemuxCommand_ = DEMUXER_COMMAND_NONE;
    private int audioCodec_ = -1;

    public MediaExtractorDemuxer(DemuxerListener listener) {
        listener_ = listener;
    }

    public void destroy() {
        if (signalThread_ != null)
            signalThread_.destroy();
    }

    public void open(String fileName) {
        try {
            mediaExtractor_.setDataSource(fileName);
            if (mediaExtractor_.getTrackCount() == 0)
                throw new RuntimeException("no track");
            MediaFormat mediaFormat;
            String formatType;
            JSONObject mediaAttrJson = new JSONObject();
            for (int i = 0; i < mediaExtractor_.getTrackCount(); i++) {
                mediaFormat = mediaExtractor_.getTrackFormat(i);
                formatType = mediaFormat.getString(MediaFormat.KEY_MIME);
                if (MediaDef.MediaCodec.isVideoCodec(formatType)) {
                    videoTrackIndex_ = i;
                    mediaAttrJson.put("video_codec", MediaDef.MediaCodec.androidCodecToMediaCodec(formatType));
                    mediaAttrJson.put("video_width", mediaFormat.getInteger(MediaFormat.KEY_WIDTH));
                    mediaAttrJson.put("video_height", mediaFormat.getInteger(MediaFormat.KEY_HEIGHT));
                    if (mediaFormat.containsKey(MediaFormat.KEY_FRAME_RATE))
                        mediaAttrJson.put("video_framerate", mediaFormat.getInteger(MediaFormat.KEY_FRAME_RATE));
                    if (mediaFormat.containsKey("csd-0") && mediaFormat.containsKey("csd-1")) {
                        ByteBuffer csd0 = mediaFormat.getByteBuffer("csd-0");
                        ByteBuffer csd1 = mediaFormat.getByteBuffer("csd-1");
                        byte[] csd0Arr = csd0.array();
                        byte[] csd1Arr = csd1.array();
                        byte[] videoCodec = new byte[csd0Arr.length + csd1Arr.length];
                        System.arraycopy(csd0Arr, 0, videoCodec, 0, csd0Arr.length);
                        System.arraycopy(csd1Arr, 0, videoCodec, csd0Arr.length, csd1Arr.length);
                        if (listener_ != null)
                            listener_.onCodecData_Demuxer(videoCodec, null);
                    }
                }
                else {
                    audioTrackIndex_ = i;
                    long duration = mediaFormat.getLong(MediaFormat.KEY_DURATION);
                    audioCodec_ = MediaDef.MediaCodec.androidCodecToMediaCodec(formatType);
                    mediaAttrJson.put("duration", duration / 1000000);
                    mediaAttrJson.put("audio_codec", audioCodec_);
                    mediaAttrJson.put("audio_sample_rate", mediaFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE));
                    mediaAttrJson.put("audio_channels", mediaFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT));
                    mediaAttrJson.put("audio_channelLayout", 1);
                    if (mediaFormat.containsKey(MediaFormat.KEY_AAC_PROFILE))
                        mediaAttrJson.put("audio_profile", mediaFormat.getInteger(MediaFormat.KEY_AAC_PROFILE) - 1);
                    if (mediaFormat.containsKey("csd-0")) {
                        ByteBuffer audioCodec = mediaFormat.getByteBuffer("csd-0");
                        if (listener_ != null)
                            listener_.onCodecData_Demuxer(null, audioCodec.array());
                    }
                }
            }
            fileName_ = fileName;
            if (listener_ != null)
                listener_.onCommandResult_Demuxer(DEMUXER_COMMAND_OPEN, true, mediaAttrJson.toString());
        }
        catch (Exception e) {
            if (listener_ != null)
                listener_.onCommandResult_Demuxer(DEMUXER_COMMAND_OPEN, false, "");
        }
    }

    public String getFileName() {
        return fileName_;
    }

    public void play() {
        if (videoTrackIndex_ > -1)
            mediaExtractor_.selectTrack(videoTrackIndex_);
        if (signalThread_ == null) {
            signalThread_ = new SignalThread(new SignalThread.SignalThreadFuncListener() {
                @Override
                public void onSignalThreadFunc(SignalThread.LockCondition lockCondition, Boolean isTerminated) {
                    ByteBuffer buf = ByteBuffer.allocate(1024 * 1024);
                    int sampleSize;
                    while (!isTerminated) {
                        if (currentDemuxCommand_ != DEMUXER_COMMAND_PLAY)
                            lockCondition.waitFor();
                        if (isTerminated)
                            break;
                        switch (currentDemuxCommand_) {
                            case DEMUXER_COMMAND_PLAY:
                                sampleSize = mediaExtractor_.readSampleData(buf, 0);
                                if (sampleSize > 0) {
                                    if (listener_ != null) {
                                        byte[] mediaArr = buf.array();
                                        listener_.onRecvStream_Demuxer(mediaArr, sampleSize, audioCodec_, mediaExtractor_.getSampleTime() / 1000);
                                    }
                                    mediaExtractor_.advance();
                                    lockCondition.set();
                                }
                                break;
                        }
                    }
                }
            });
        }
        currentDemuxCommand_ = DEMUXER_COMMAND_PLAY;
        signalThread_.set();
    }

    public void stop() {
    }

    public void pause() {
    }

    public void seek(int seekSeconds) {

    }
}
