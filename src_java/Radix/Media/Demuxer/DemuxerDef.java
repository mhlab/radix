package Radix.Media.Demuxer;

public class DemuxerDef {
    public static final int DEMUXER_COMMAND_NONE = -1;
    public static final int DEMUXER_COMMAND_OPEN = 0;
    public static final int DEMUXER_COMMAND_PLAY = 1;
    public static final int DEMUXER_COMMAND_STOP = 2;
    public static final int DEMUXER_COMMAND_PAUSE = 3;
    public static final int DEMUXER_COMMAND_SEEK = 4;
}
