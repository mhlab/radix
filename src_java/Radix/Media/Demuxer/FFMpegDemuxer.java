package Radix.Media.Demuxer;

public class FFMpegDemuxer {
    private native long initInstance();
    private native void freeInstance(long instance);
    private native void open(long instance, String fileName);
    private native void close(long instance);
    private native void play(long instance);
    private native void stop(long instance);
    private native void pause(long instance);
    private native boolean isSupportFile(long instance, String fileName);
    private long instance_ = 0;
    private DemuxerListener listener_ = null;
    private String fileName_ = null;

    public FFMpegDemuxer(DemuxerListener listener) {
        instance_ = initInstance();
        listener_ = listener;
    }

    public void release() {
        if (instance_ == 0)
            return;

        close(instance_);
        freeInstance(instance_);
    }

    public void open(String fileName) {
        if (instance_ == 0)
            return;

        fileName_ = fileName;
        open(instance_, fileName);
    }

    public String getFileName() {
        return fileName_;
    }

    public void play() {
        if (instance_ == 0)
            return;

        play(instance_);
    }

    public void stop() {
        if (instance_ == 0)
            return;

        stop(instance_);
    }

    public void pause() {
        if (instance_ == 0)
            return;

        pause(instance_);
    }

    public boolean isSupportFile(String fileName) {
        if (instance_ == 0)
            return false;

        return isSupportFile(instance_, fileName);
    }

    public void onCommandResult(int command, boolean ret, String commandAttr) {
        if (listener_ != null)
            listener_.onCommandResult_Demuxer(command, ret, commandAttr);
    }

    public void onCodecData(byte[] videoCodecData, byte[] audioCodecData) {
        if(listener_ != null)
            listener_.onCodecData_Demuxer(videoCodecData, audioCodecData);
    }

    public void onRecvMediaStream(byte[] mediaData, int mediaSize, int codec, int presentationTime) {
        if (listener_ != null)
            listener_.onRecvStream_Demuxer(mediaData, mediaSize, codec, presentationTime);
    }

    static {
        System.loadLibrary("FFMpegDemuxer");
    }
}
