package Radix.Media.Demuxer;

public interface DemuxerListener {
    void onCommandResult_Demuxer(int command, boolean ret, String commandAttr);
    void onRecvStream_Demuxer(byte[] mediaData, int mediaSize, int codec, long presentationTime);
    void onCodecData_Demuxer(byte[] videoCodecData, byte[] audioCodecData);
    void onUpdatedPlayTime_Demuxer(long playTime);
}
