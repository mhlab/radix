package Radix.Media.Demuxer;

public class SyncDemuxer {
    private native long create();
    private native void destroy(long instance);
    private native void open(long instance, String fileName);
    private native void play(long instance);
    private native void stop(long instance);
    private native void pause(long instance);
    private native boolean isSupportFile(long instance, String fileName);
    private native void seek(long instance, int seekSeconds);
    private long instance_ = 0;
    private DemuxerListener listener_ = null;

    public String fileName_ = "";

    public SyncDemuxer(DemuxerListener listener) {
        instance_ = create();
        listener_ = listener;
    }

    public void destroy() {
        if (instance_ == 0)
            return;

        destroy(instance_);
        instance_ = 0;
    }

    public void open(String fileName) {
        if (instance_ == 0)
            return;

        fileName_ = fileName;
        open(instance_, fileName);
    }

    public void play() {
        if (instance_ == 0)
            return;

        play(instance_);
    }

    public void pause() {
        if (instance_ == 0)
            return;

        pause(instance_);
    }

    public void stop() {
        if (instance_ == 0)
            return;

        stop(instance_);
    }

    public boolean isSupportFile(String fileName) {
        if (instance_ == 0)
            return false;

        return isSupportFile(instance_, fileName);
    }

    public void seek(int seekSeconds) {
        if (instance_ == 0)
            return;

        seek(instance_, seekSeconds);
    }

    public String getFileName() {
        return fileName_;
    }

    public void onCommandResult(int command, boolean ret, String commandAttr) {
        if (listener_ != null)
            listener_.onCommandResult_Demuxer(command, ret, commandAttr);
    }

    public void onCodecData(byte[] videoCodecData, byte[] audioCodecData) {
        if(listener_ != null)
            listener_.onCodecData_Demuxer(videoCodecData, audioCodecData);
    }

    public void onRecvMediaStream(byte[] mediaData, int mediaSize, int codec, long presentationTime) {
        if (listener_ != null)
            listener_.onRecvStream_Demuxer(mediaData, mediaSize, codec, presentationTime);
    }

    public void onUpdatedPlayTime(long playTime) {
        if (listener_ != null)
            listener_.onUpdatedPlayTime_Demuxer(playTime);
    }

    static {
        System.loadLibrary("SyncDemuxer");
    }
}
