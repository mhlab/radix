package Radix.Network.Rtsp;

import Radix.Network.Rtsp.RTSPDef.*;

public class RTSPClient {
    private native long create();
    private native void destroy(long instance);
    private native void startLiveVideo(long instance, int connectionId, String ip, int port, String abs_path, boolean useTcp);
    private native void stopLiveVideo(long instance, int connectionId);

    private long instance_ = 0;
    private RTSPClientListener listener_ = null;

    public RTSPClient(RTSPClientListener listener) {
        instance_ = create();
        listener_ = listener;
    }

    public void destroy() {
        if (instance_ == 0)
            return;

        destroy(instance_);
    }

    public void startLiveVideo(int connectionId, String ip, int port, String abs_path, boolean useTcp) {
        if (instance_ == 0)
            return;

        startLiveVideo(instance_, connectionId, ip, port, abs_path, useTcp);
    }

    public void stopLiveVideo(int connectionId) {
        if (instance_ == 0)
            return;

        stopLiveVideo(instance_, connectionId);
    }

    public void onCommandResult(int connectionId, int command, boolean ret, String param) {
        if (listener_ != null)
            listener_.onCommandResult_RTSPClient(connectionId, RTSPCommand.values()[command], ret, param);
    }

    public void onRecvMediaStream(int connectionId, byte[] media, int size, int codec, long timestamp) {
        if (listener_ != null)
            listener_.onRecvMediaStream_RTSPClient(connectionId, media, size, codec, timestamp);
    }

    static {
        System.loadLibrary("RTSPClient");
    }
}
