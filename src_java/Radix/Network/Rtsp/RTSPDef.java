package Radix.Network.Rtsp;

public class RTSPDef {
    public enum RTSPCommand {
        COMMAND_UNKNOWN,
        COMMAND_CONNECTION ,
        COMMAND_DESCRIBE,
        COMMAND_SETUP,
        COMMAND_PLAY,
        COMMAND_TEARDOWN
    }

    public class RTSPCodec {
        public static final int CODEC_UNKNOWN = -1;
        public static final int CODEC_H264 = 96;
        public static final int CODEC_JPEG = 26;
    };

    public interface RTSPClientListener {
        void onCommandResult_RTSPClient(int connectionId, RTSPCommand command, boolean ret, String param);
        void onRecvMediaStream_RTSPClient(int connectionId, byte[] media, int size, int codec, long timestamp);
    }
}
