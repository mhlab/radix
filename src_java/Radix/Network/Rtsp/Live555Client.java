package Radix.Network.Rtsp;

public class Live555Client {
    private native long create();
    private native void destroy(long instance);
    private native void startLiveVideo(long instance, int connectionId, String url, String userId, String userPass);
    private native void stopLiveVideo(long instance, int connectionId);
    private native void sendDescribe(long instance, int connectionId, String url, String userId, String userPass);

    private long instance_ = 0;
    private RTSPDef.RTSPClientListener listener_ = null;

    public Live555Client(RTSPDef.RTSPClientListener listener) {
        instance_ = create();
        listener_ = listener;
    }

    public void destroy() {
        if (instance_ != 0)
            destroy(instance_);
        instance_ = 0;
    }

    public void startLiveVideo(int connectionId, String url, String userId, String userPass) {
        if (instance_ == 0)
            return;

        startLiveVideo(instance_, connectionId, url, userId, userPass);
    }

    public void stopLiveVideo(int connectionId) {
        if (instance_ == 0)
            return;

        stopLiveVideo(instance_, connectionId);
    }

    public void sendDescribe(int connectionId, String url, String userId, String userPass) {
        if (instance_ == 0)
            return;

        sendDescribe(instance_, connectionId, url, userId, userPass);
    }

    public void onCommandResult(int connectionId, int command, boolean ret, String param) {
        if (listener_ != null)
            listener_.onCommandResult_RTSPClient(connectionId, RTSPDef.RTSPCommand.values()[command], ret, param);
    }

    public void onRecvMediaStream(int connectionId, byte[] media, int size, int codec, long timestamp) {
        if (listener_ != null)
            listener_.onRecvMediaStream_RTSPClient(connectionId, media, size, codec, timestamp);
    }

    static {
        System.loadLibrary("Live555RtspClient");
    }
}
