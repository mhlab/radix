package Radix.Reader;

import android.util.Log;

import java.io.UnsupportedEncodingException;

public class SubtitleReader {
    public interface SubtitleReaderListener {
        void onRecvSubtitle(String subtitle);
    }

    public enum SubTitleFileType {
        SUBTITLE_FILE_SMI,
        SUBTITLE_FILE_SRT
    }

    private native long create(int subtitleType);
    private native void destrory(long instance);
    private native boolean open(long instance, String filePath);
    private native void progress(long instance, double playTime);
    private native void reset(long instance);

    private long instance_ = 0;
    private SubtitleReaderListener listener_ = null;

    public SubtitleReader(SubtitleReaderListener listener) {
        listener_ = listener;
    }

    public boolean open(SubTitleFileType fileType, String filePath) {
        instance_ = create(fileType.ordinal());
        if (instance_ == 0)
            return false;

        return open(instance_, filePath);
    }

    public void close() {
        if (instance_ == 0)
            destrory(instance_);
        instance_ = 0;
    }

    public void progress(double playTime) {
        if (instance_ == 0)
            return;

        progress(instance_, playTime);
    }

    public void reset() {
        if (instance_ == 0)
            return;

        reset(instance_);
    }

    public void onRecvSubtitle(byte[] subtitle, int subtitleLength) {
        String subtitleStr = "";
        if (subtitleLength > 0)
            try {
                byte[] realdata = new byte[subtitleLength];
                System.arraycopy(subtitle, 0, realdata, 0, subtitleLength);
                subtitleStr = new String(realdata, "UTF-16");
            }
            catch (UnsupportedEncodingException e) {
            }
        if (listener_ != null)
            listener_.onRecvSubtitle(subtitleStr);
    }

    static {
        System.loadLibrary("SubtitleReader");
    }
}
