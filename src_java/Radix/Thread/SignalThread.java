package Radix.Thread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SignalThread implements Runnable {
    private Thread thread_ = null;
    private SignalThreadFuncListener threadFunc_ = null;
    private Boolean isTerminated_ = false;
    private LockCondition lockCondition_ = new LockCondition();

    public interface SignalThreadFuncListener {
        void onSignalThreadFunc(LockCondition lockCondition, Boolean isTerminated);
    }

    public class LockCondition {
        private Lock lock_ = new ReentrantLock();
        private Condition condition_ = lock_.newCondition();

        public void set() {
            lock_.lock();
            condition_.signal();
            lock_.unlock();
        }

        public void waitFor() {
            lock_.lock();
            try {
                condition_.await();
            }
            catch (InterruptedException e) {
            }
            lock_.unlock();
        }
    }

    public SignalThread(SignalThreadFuncListener threadFunc) {
        threadFunc_ = threadFunc;
        thread_ = new Thread(this);
        thread_.start();
    }

    public void destroy() {
        try {
            isTerminated_ = true;
            lockCondition_.set();
            thread_.join();
        }
        catch (InterruptedException e) {
        }
    }

    public void set() {
        lockCondition_.set();
    }

    @Override
    public void run() {
        if (threadFunc_ != null)
            threadFunc_.onSignalThreadFunc(lockCondition_, isTerminated_);
    }
}
