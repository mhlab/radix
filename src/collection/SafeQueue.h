#pragma once

#include <queue>
#include "thread/Lock.h"

namespace Radix {
	namespace Collection {
		template<typename T>
		class SafeQueue
		{
		private:
			std::queue<T> queue_;
			Radix::Thread::Lock lock_;

		public:
			void push(T item) {
				Radix::Thread::Locker locker(&lock_); 
				queue_.push(item);
			}
			T pop() {
				Radix::Thread::Locker locker(&lock_);
				if (queue_.size() == 0)
					return NULL;

				T item = queue_.front();
				queue_.pop();

				return item;
			}
			unsigned int count() {
				Radix::Thread::Locker locker(&lock_);
				return queue_.size();
			}
		};
	}
}

