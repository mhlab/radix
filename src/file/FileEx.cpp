#include "stdafx.h"
#include "FileEx.h"
#include <cstdio>

char* Radix::File::getContentFromFile(const char* filePath) {
	FILE* file = fopen(filePath, "rb");
	if (file == NULL)
		return NULL;
	if (fseek(file, 0, SEEK_END) != 0) {
		fclose(file);
		return NULL;
	}
	int fileSize = ftell(file);
	fseek(file, 0, SEEK_SET);
	char* ret = new char[fileSize];	
	fread(ret, fileSize, 1, file);
	fclose(file);

	return ret;
}

wchar_t* Radix::File::getUTFContentFromFile(const char* filePath) {
	FILE* file = fopen(filePath, "rb");
	if (file == NULL)
		return NULL;
	if (fseek(file, 0, SEEK_END) != 0) {
		fclose(file);
		return NULL;
	}
	int fileSize = ftell(file);
	fseek(file, 0, SEEK_SET);
	wchar_t* ret = new wchar_t[fileSize];
	fread(ret, fileSize, 1, file);
	fclose(file);

	return ret;
}