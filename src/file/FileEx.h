#pragma once

#ifdef WIN32
#pragma warning(disable:4996)
#endif

namespace Radix {
	namespace File {
		char* getContentFromFile(const char* filePath);
		wchar_t* getUTFContentFromFile(const char* filePath);
	}
}

