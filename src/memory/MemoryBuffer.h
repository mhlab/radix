#pragma once

namespace Radix {
	namespace Memory {
		class MemoryBuffer
		{
		private:
			char* buffer_;
			unsigned int bufferSize_;
			unsigned int pos_;

		public:
			MemoryBuffer();
			~MemoryBuffer();

		public:
			void write(const char* data, unsigned int size);
			void setPosition(unsigned int pos);
			unsigned int getPosition();
			char* memory();
		};
	}
}

