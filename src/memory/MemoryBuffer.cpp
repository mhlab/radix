#include "StdAfx.h"
#include "MemoryBuffer.h"
#ifdef _WIN32
#include <Windows.h>
#else
#include <cstring>
#endif

Radix::Memory::MemoryBuffer::MemoryBuffer():
	buffer_(NULL),
	bufferSize_(0),
	pos_(0) {
}

Radix::Memory::MemoryBuffer::~MemoryBuffer() {
	if (buffer_ != NULL)
		delete[] buffer_;
}

void Radix::Memory::MemoryBuffer::write(const char* data, unsigned int size) {
	if (buffer_ == NULL) {
		buffer_ = new char[size];
		bufferSize_ = size;
	}
	else if (pos_ + size > bufferSize_) {
		char* tmp = new char[bufferSize_];
		memcpy(tmp, buffer_, bufferSize_);
		delete[] buffer_;

		buffer_ = new char[pos_ + size];
		memcpy(buffer_, tmp, bufferSize_);
		delete[] tmp;

		bufferSize_ = pos_ + size;
	}
	memcpy((buffer_ + pos_), data, size);
	pos_ += size;
}

void Radix::Memory::MemoryBuffer::setPosition(unsigned int pos) {
	if (pos > bufferSize_)
		return;

	pos_ = pos;
}

unsigned int Radix::Memory::MemoryBuffer::getPosition() {
	return pos_;
}

char* Radix::Memory::MemoryBuffer::memory() {
	return buffer_;
}