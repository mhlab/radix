#include "StdAfx.h"
#include "DateTime.h"
#include <ctime>
#ifdef _WIN32
#include <Windows.h>
#else
#include <sys/time.h>
#endif

long Radix::DateTime::getCurrentUnixTimeMiliseconds() {
	std::time_t time = std::time(NULL);
#ifdef _WIN32
    SYSTEMTIME systemTime = { 0, };
	GetSystemTime(&systemTime);

	return (long)time * 1000 + systemTime.wMilliseconds;
#else
    timeval systemTime = {};
	gettimeofday(&systemTime, NULL);

	return (long)time * 1000 + (systemTime.tv_usec / 1000);
#endif
}
