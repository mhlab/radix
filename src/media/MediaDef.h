#pragma once

namespace Radix {
	namespace Media {
		static int H264CODEC_NALTYPE_SPS = 7;
		static int H264CODEC_NALTYPE_PPS = 8;
		static int H264CODEC_NALTYPE_IDR = 5;
		static int H264CODEC_NALTYPE_NONIDR = 1;
		static int H264CODEC_NALTYPE_SEI = 6;
		static int H264CODEC_NALTYPE_FRAGMENTA = 28;

		static int H264CODEC_STARTCODE_1_SIZE = 4;
		static int H264CODEC_STARTCODE_2_SIZE = 3;

		// defined by avcodec definition
		enum MediaCodec {
			MEDIA_CODEC_UNKNOWN = 0,
			MEDIA_CODEC_MJPEG = 8,
			MEDIA_CODEC_H264 = 28,
			MEDIA_CODEC_MP3 = 86017,
			MEDIA_CODEC_AAC = 86018,
			MEDIA_CODEC_AC3 = 86019
		};

		// defined by pixfmt.h definition(ffmpeg)
		enum MediaPixelFormat {
			MEDIA_PIXELFORMAT_YUV420P = 0
		};

		static bool isVideoCodec(MediaCodec codec) {
			return (codec == MEDIA_CODEC_H264);
		}

                static bool isVideoKeyFrame(MediaCodec codec, const char* media, int mediaSize) {
                        if (codec != MEDIA_CODEC_H264)
                            return true;
			char* findPos = (char*)media;
			int nalType;
			for (int i = 0; i < mediaSize; i++) {
				if ((mediaSize - i >= H264CODEC_STARTCODE_1_SIZE) && (*findPos == 0 && *(findPos + 1) == 0 && *(findPos + 2) == 0 && *(findPos + 3) == 1)) {
					nalType = *(findPos + H264CODEC_STARTCODE_1_SIZE) & 0x1F;
					if (nalType == H264CODEC_NALTYPE_IDR || nalType == H264CODEC_NALTYPE_SPS)
						return true;
				}
				else if ((mediaSize - i >= H264CODEC_STARTCODE_2_SIZE) && (*findPos == 0 && *(findPos + 1) == 0 && *(findPos + 2) == 1)) {
					nalType = *(findPos + H264CODEC_STARTCODE_2_SIZE) & 0x1F;
					if (nalType == H264CODEC_NALTYPE_IDR || nalType == H264CODEC_NALTYPE_SPS)
						return true;
				}
				findPos++;
			}

			return false;
		}
	}
}
