#include "StdAfx.h"
#include "H264VideoParser.h"

#include <math.h>
#include <assert.h>

Radix::Media::H264VideoParser::H264VideoParser() {
}

Radix::Media::H264VideoParser::~H264VideoParser() {
}

bool Radix::Media::H264VideoParser::isKeyFrame(const void* data, int dataSize) {
	char* curPos = (char*)data;
	for (int i = 0; i < 250; i++) {
		if (*curPos == 0x65 || *curPos == 0x25 || *curPos == 0x45) {
			if (*(curPos-3) == 0x00 && *(curPos-2) == 0x00 && *(curPos-1) == 0x01)
				return true;
		}
		curPos++;
	}

	return false;
}

void Radix::Media::H264VideoParser::scalingList(CBitStreamReader& bitStreamReader, int loopIndex, int sizeScaling)
{
	unsigned int lastScale = 8;
	unsigned int nextScale = 8;
	int deltaScale;

	for (int i = 0; i < sizeScaling; i++) {
		if (nextScale != 0) {
			deltaScale = bitStreamReader.sev();
			nextScale = (lastScale + deltaScale + 256) % 256;
		}
		if (nextScale == 0)
			lastScale = lastScale;
		else
			lastScale = nextScale;
	}
}

// Resolution is getted from SPS unit.
void Radix::Media::H264VideoParser::getResolution(const void* data, int dataSize, int* width, int* height)
{
	*width = 0;
	*height = 0;

	char* curPos = (char*)data;
	for (int i = 0; i < 250; i++) {
		if (*curPos == 0x67 || *curPos == 0x47 || *curPos == 0x27) {
			if (*(curPos-3) == 0x00 && *(curPos-2) == 0x00 && *(curPos-1) == 0x01) {
				curPos++;
				CBitStreamReader bitStreamReader((unsigned char*)curPos);
				int profile = bitStreamReader.u(8); 
				bitStreamReader.u(8); 
				bitStreamReader.u(8); 
				bitStreamReader.uev();

				if (profile == 100 || profile == 110 || profile == 122 || profile == 144) {
					int chromaIdc = bitStreamReader.uev();
					if (chromaIdc == 3)
						bitStreamReader.u(1);

					bitStreamReader.uev();
					bitStreamReader.uev();
					bitStreamReader.u(1);
					int scaling_flag = bitStreamReader.u(1);
					if (scaling_flag > 0) {
						int temp;
						for (int i = 0; i < 8; i++) {
							temp = bitStreamReader.u(1);
							if (temp > 0)
								scalingList(bitStreamReader, i, i < 6 ? 16 : 64);
						}
					}
				}

				bitStreamReader.uev();
				int picOrderType = bitStreamReader.uev();
				if (picOrderType == 0)
					bitStreamReader.uev();
				else if (picOrderType == 1) {
					bitStreamReader.u(1);
					bitStreamReader.sev();
					bitStreamReader.sev();
					int numRefFrames = bitStreamReader.uev();
					for (int i = 0; i < numRefFrames; i++)
						bitStreamReader.sev();
				}

				bitStreamReader.uev();
				bitStreamReader.u(1);

				*width = (bitStreamReader.uev() + 1) * 16;
				*height = (bitStreamReader.uev() + 1) * 16;
			}
		}
		curPos++;
	}
}

bool Radix::Media::H264VideoParser::isIncludeSPS(const void* data, int dataSize)
{
	char* pos = (char*)data;

	return ((*(pos + 4) & 0x1f) == 0x07);
}

void Radix::Media::H264VideoParser::getSPSPPS(const void* srcData, int srcDataSize, char** dstData, int& dstSize)
{
	bool isFindSPS = false;
	bool isFindPPS = false;
	*dstData = NULL;

	char* pos = (char*)srcData;
	int curPosSize = 0;
	int nalUnitType;
	while (curPosSize < srcDataSize) {
		if (*pos == 0x00 && *(pos+1) == 0x00 && *(pos+2) == 0x00 && *(pos+3) ==0x01) {
			if (!isFindSPS || !isFindPPS) {
				nalUnitType = (*(pos + 4) & 0x1f);
				if (nalUnitType == 0x07)
					isFindSPS = true;
				else if (nalUnitType == 0x08)
					isFindPPS = true;
			}
			else 
				break;
		}
		pos++;
		curPosSize++;
	}

	*dstData = new char[curPosSize];
	memcpy(*dstData, srcData, curPosSize);
	dstSize = curPosSize;
}

void Radix::Media::H264VideoParser::getNextStartCode(const char* data, int dataSize, char** dstPos, int& dstSize)
{
	*dstPos = (char*)data;
	int posSize = 0;
	int val = 0;

	while (posSize < dataSize) {
		val = (val << 8 | **dstPos);
		if (val == 0x00000001) {
			*dstPos -= 3;
			dstSize = dataSize - (posSize - 3);
			return;
		}
		posSize++;
		(*dstPos)++;
	}

	dstSize = 0;
	*dstPos = NULL;
}