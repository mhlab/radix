#pragma once

#include "BaseVideoParser.h"
#include "H264BitStreamReader.h"

namespace Radix {
	namespace Media {
		class H264VideoParser : public BaseVideoParser {
		private:
			void scalingList(CBitStreamReader& bitStreamReader, int loopIndex, int sizeScaling);

		public:
			H264VideoParser();
			virtual ~H264VideoParser();

		public:
			virtual bool isKeyFrame(const void* data, int dataSize);
			virtual void getResolution(const void* data, int dataSize, int* width, int* height);
			bool isIncludeSPS(const void* data, int dataSize);
			void getSPSPPS(const void* srcData, int srcDataSize, char** dstData, int& dstSize);
			void getNextStartCode(const char* data, int dataSize, char** dstPos, int& dstSize);
		};
	}
}
