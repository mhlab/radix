#pragma once

namespace Radix {
	namespace Media {
		class BaseVideoParser {
		public:
			BaseVideoParser();
			virtual ~BaseVideoParser();

		public:
			virtual bool isKeyFrame(const void* data, int dataSize) { return false; }
			virtual void getResolution(const void* data, int dataSize, int* width, int* height) {}
		};
	}
}

