#pragma once

#include "BaseVideoParser.h"

namespace Radix {
	namespace Media {
		class JPEGVideoParser : public BaseVideoParser {
		public:
			JPEGVideoParser();
			virtual ~JPEGVideoParser();

		public:
			virtual bool isKeyFrame(const void* data, int dataSize) { return true; }
			virtual void getResolution(const void* data, int dataSize, int* width, int* height);
		};
	}
}

