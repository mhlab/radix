#include "StdAfx.h"
#include "JPEGVideoParser.h"

#include <math.h>
#include <assert.h>

Radix::Media::JPEGVideoParser::JPEGVideoParser() {
}

Radix::Media::JPEGVideoParser::~JPEGVideoParser() {
}

void Radix::Media::JPEGVideoParser::getResolution(const void* data, int dataSize, int* width, int* height) {
	short* curData = (short*)data;
	unsigned char* firstByte;
	for (int i = 0; i < (dataSize / 2); i++) {
		firstByte = (unsigned char*)curData;
		if (*firstByte == 0xFF && *(firstByte + 1) == 0xC0) {
			firstByte += 5;
			*height = *(firstByte + 1) + ((*firstByte) << 8);
			firstByte += 2;
			*width = *(firstByte + 1) + ((*firstByte) << 8);
			break;
		}
		curData++;
	}
}