#pragma once

#include <iostream>
#include <sstream>
#include <string>

class CBitStreamReader
{
private:
	unsigned char* binaryData;
	int position;

	int getBit()
	{
		int mask = 1 << (7 - (position & 7));
		int index = position >> 3;
		position++;
		return ((binaryData[index] & mask) == 0) ? 0 : 1;
	}

	void skipBit()
	{
		position++;
	}

	int ev(bool isItSigned)
	{
		int bitCount = 0;

		while (getBit() == 0)
			bitCount++;

		int result = 1;
		for (int i = 0; i < bitCount; i++)
		{
			int b = getBit();
			result = result * 2 + b;
		}

		result--;
		if (isItSigned)
			result = (result + 1) / 2 * (result % 2 == 0 ? -1 : 1);

		return result;
	}

public:
	CBitStreamReader(unsigned char* dataToRead)
	{
		position = 0;
		binaryData = dataToRead;
	}

	~CBitStreamReader()
	{
	}

	void skipBits(int n)
	{
		for (int i = 0; i < n; i++)
			skipBit();
	}

	int getBits(int n)
	{
		int result = 0;
		for (int i = 0; i < n; i++)
			result = result * 2 + getBit();

		return result;
	}

	int u(int n)
	{
		int result = 0;
		for (int i = 0; i < n; i++)
			result = result * 2 +getBit();

		return result;
	}

	int uev() 
	{
		return ev(false);
	}

	int sev()
	{
		return ev(true);
	}
};