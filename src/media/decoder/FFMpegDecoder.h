#pragma once

#include "media/MediaDef.h"
extern "C" {
#include "libavcodec/avcodec.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavutil/opt.h"
}

namespace Radix {
	namespace Media {
		const int FFMPEG_DECODER_SUCCESS = 0;
		const int FFMPEG_DECODER_FAIL = -1;
		const int FFMPEG_DECODER_NOT_SUPPORT_PIXELFORMAT = -2;
		const int FFMPEG_DECODER_NOT_INITIALIZE = -3;
		const int FFMPEG_DECODER_INVALID_ARGUMENTS = -4;

		class FFMpegDecoder	{
		private:
			AVCodecContext* context_;
			AVPacket pkt_;
			AVFrame* frame_;
			SwsContext* swContext_;
			SwrContext* swrContext_;

			bool convertToYuv420(char* dstY, char* dstU, char* dstV, int width, int height);

		public:
			FFMpegDecoder();
			~FFMpegDecoder();

		public:
			bool openVideo(MediaCodec codec);
			bool openAudio(MediaCodec codec, int samplerate, int channels, int channelLayout);
			void close();
			int decodeVideoToYuv(const char* src, int srcSize, char* dstY, char* dstU, char* dstV, int* width, int* height, MediaPixelFormat pixelfmt);
			int decodeAudio(const char* src, int srcSize, char* dst, int* dstsize, int newChannels, int newChannelLayout);
			bool isOpen();
		};
	}
}

