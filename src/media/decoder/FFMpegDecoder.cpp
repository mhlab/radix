#include "StdAfx.h"
#include "FFMpegDecoder.h"
#include "log/Log.h"
#include "libavutil/error.h"

static void ffmpeg_log(void* ptr, int level, const char* fmt, va_list vargs) {
	char log[4096] = {0};
#ifdef _WIN32
	vsprintf_s(log, fmt, vargs);
#else
	vsprintf(log, fmt, vargs);
#endif
	Radix::Log::debugOutputA(log);
}

Radix::Media::FFMpegDecoder::FFMpegDecoder():
	context_(NULL),
	frame_(NULL),
	swContext_(NULL),
	swrContext_(NULL) {
	avcodec_register_all();

#if defined(_WIN32) && defined(_DEBUG)
	av_log_set_level(99);
	av_log_set_callback(ffmpeg_log);
#endif
}

Radix::Media::FFMpegDecoder::~FFMpegDecoder() {
	close();
}

bool Radix::Media::FFMpegDecoder::openVideo(MediaCodec codec) {
	close();
	AVCodecID codecId = (AVCodecID)codec;
	AVCodec* findCodec = avcodec_find_decoder(codecId);
	if (findCodec == NULL)
		return false;
	context_ = avcodec_alloc_context3(findCodec);
	if (context_ == NULL)
		return false;
	if (codecId == AV_CODEC_ID_H264)
		context_->thread_type = FF_THREAD_SLICE;
	context_->thread_count = 0;
	context_->flags2 |= CODEC_FLAG2_FAST;
	if (avcodec_open2(context_, findCodec, NULL) < 0) {
		close();
		return false;
	}

	frame_ = av_frame_alloc();
	if (frame_ == NULL) {
		close();
		return false;
	}

	return true;
}

bool Radix::Media::FFMpegDecoder::openAudio(MediaCodec codec, int samplerate, int channels, int channelLayout) {
	close();
	AVCodecID codecId = (AVCodecID)codec;
	AVCodec* findCodec = avcodec_find_decoder(codecId);
	if (findCodec == NULL)
		return false;
	context_ = avcodec_alloc_context3(findCodec);
	if (context_ == NULL)
		return false;

	context_->sample_rate = samplerate;
	context_->channel_layout = channelLayout;
	context_->channels = channels;
	if (avcodec_open2(context_, findCodec, NULL) < 0) {
		close();
		return false;
	}

	frame_ = av_frame_alloc();
	if (frame_ == NULL) {
		close();
		return false;
	}

	return true;
}

void Radix::Media::FFMpegDecoder::close() {
	if (frame_ != NULL) {
		av_frame_free(&frame_);
		frame_= NULL;
	}
	if (swContext_ != NULL) {
		sws_freeContext(swContext_);
		swContext_ = NULL;
	}
	if (swrContext_ != NULL) {
		swr_free(&swrContext_);
		swrContext_ = NULL;
	}
	if (context_ != NULL) {
		avcodec_close(context_);
		av_free(context_);
		context_ = NULL;
	}
}

int Radix::Media::FFMpegDecoder::decodeVideoToYuv(const char* src, int srcSize, char* dstY, char* dstU, char* dstV, int* width, int* height, MediaPixelFormat pixelfmt) {
	if (context_ == NULL || frame_ == NULL)
		return FFMPEG_DECODER_NOT_INITIALIZE;

	av_init_packet(&pkt_);
	pkt_.data = (uint8_t*)src;
	pkt_.size = srcSize;
	int ret = avcodec_send_packet(context_, &pkt_);
	if (ret < 0)
		return FFMPEG_DECODER_FAIL;

	while (ret >= 0) {
		ret = avcodec_receive_frame(context_, frame_);
		if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
			return FFMPEG_DECODER_SUCCESS;
		if (ret < 0)
			return FFMPEG_DECODER_FAIL;
		if (width != NULL && height != NULL) {
			if (*width <= 0 || *height <= 0) {
				*width = frame_->width;
				*height = frame_->height;
			}
		}
		if (dstY != NULL && dstU != NULL && dstV != NULL) {
			AVPixelFormat avPixFmt = (AVPixelFormat)pixelfmt;
			if (context_->pix_fmt == avPixFmt && avPixFmt == AV_PIX_FMT_YUV420P && *width == frame_->width && *height == frame_->height) {
				int ysize = frame_->width * frame_->height;
				int uvsize = (frame_->width / 2) * (frame_->height / 2);
				if (frame_->width == frame_->linesize[0]) {
					memcpy(dstY, frame_->data[0], ysize);
					memcpy(dstU, frame_->data[1], uvsize);
					memcpy(dstV, frame_->data[2], uvsize);
				}
				else {
					char* newDstY = dstY;
					char* newDstU = dstU;
					char* newDstV = dstV;
					int halfWidth = frame_->width / 2;
					int uvIndex = 0;
					for (int i = 0; i < frame_->height; i++) {
						memcpy(newDstY, frame_->data[0] + (i * frame_->linesize[0]), frame_->width);
						if (i % 2 == 0) {
							memcpy(newDstU, frame_->data[1] + (uvIndex * frame_->linesize[1]), halfWidth);
							memcpy(newDstV, frame_->data[2] + (uvIndex * frame_->linesize[2]), halfWidth);
							newDstU += halfWidth;
							newDstV += halfWidth;
							uvIndex++;
						}
						newDstY += frame_->width;
					}
				}
			}
			else {
				if (avPixFmt != AV_PIX_FMT_YUV420P)
					return FFMPEG_DECODER_NOT_SUPPORT_PIXELFORMAT;
				if (!convertToYuv420(dstY, dstU, dstV, *width, *height))
					return FFMPEG_DECODER_FAIL;
			}
		}
//		break;
	}

	return FFMPEG_DECODER_SUCCESS;
}

bool Radix::Media::FFMpegDecoder::convertToYuv420(char* dstY, char* dstU, char* dstV, int width, int height) {
	if (frame_ == NULL || dstU == NULL || dstV == NULL)
		return false;

	swContext_ = sws_getCachedContext(swContext_, frame_->width, frame_->height, context_->pix_fmt, width, height, AV_PIX_FMT_YUV420P, SWS_BICUBIC, NULL, NULL, NULL);
	if (swContext_ == NULL)
		return false;

	uint8_t* out[3] = {(uint8_t*)dstY, (uint8_t*)dstU, (uint8_t*)dstV};
	int half_width = width >> 1;
	int outlines[3] = { width, half_width, half_width };
	sws_scale(swContext_, frame_->data, frame_->linesize, 0, frame_->height, out, outlines);

	return true;
}

int Radix::Media::FFMpegDecoder::decodeAudio(const char* src, int srcSize, char* dst, int* dstsize, int newChannels, int newChannelLayout) {
	if (context_ == NULL || frame_ == NULL)
		return FFMPEG_DECODER_NOT_INITIALIZE;

	av_init_packet(&pkt_);
	pkt_.data = (uint8_t*)src;
	pkt_.size = srcSize;
	int ret = avcodec_send_packet(context_, &pkt_);
	if (ret < 0)
		return FFMPEG_DECODER_FAIL;

	int bytePerSample;
	while (ret >= 0) {
		ret = avcodec_receive_frame(context_, frame_);
		if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
			return FFMPEG_DECODER_SUCCESS;
		if (ret < 0)
			return FFMPEG_DECODER_FAIL;
		bytePerSample = av_get_bytes_per_sample(AV_SAMPLE_FMT_S16);
		if (bytePerSample < 0)
			return FFMPEG_DECODER_FAIL;
		if (dstsize != NULL)
			*dstsize = bytePerSample * (frame_->nb_samples * newChannels);
		if (dst == NULL)
			return FFMPEG_DECODER_SUCCESS;
		if (newChannels == -1)
			newChannels = context_->channels;
		if (newChannelLayout == -1)
			newChannelLayout = context_->channel_layout;
		if (context_->sample_fmt == AV_SAMPLE_FMT_S16 && newChannels == context_->channels && newChannelLayout == context_->channel_layout) {
			char* dstPos = dst;
			for (int i = 0; i < frame_->nb_samples; i++) {
				for (int ch = 0; ch < context_->channels; ch++) {
					memcpy(dstPos, frame_->data[ch] + bytePerSample * i, bytePerSample);
					dstPos += bytePerSample;
				}
			}
		}
		else {
			if (swrContext_ == NULL) {
				swrContext_ = swr_alloc();
				av_opt_set_int(swrContext_, "in_channel_layout", context_->channel_layout, 0);
				av_opt_set_int(swrContext_, "in_sample_rate", context_->sample_rate, 0);
				av_opt_set_int(swrContext_, "in_sample_fmt", context_->sample_fmt, 0);
				av_opt_set_int(swrContext_, "in_channel_count", context_->channels, 0);
				av_opt_set_int(swrContext_, "out_channel_layout", newChannelLayout, 0);
				av_opt_set_int(swrContext_, "out_sample_rate", context_->sample_rate, 0);
				av_opt_set_int(swrContext_, "out_sample_fmt", AV_SAMPLE_FMT_S16, 0);
				av_opt_set_int(swrContext_, "out_channel_count", newChannels, 0);
				swr_init(swrContext_);
			}
			swr_convert(swrContext_, (uint8_t**)&dst, *dstsize, (const uint8_t**)frame_->data, frame_->nb_samples);
		}
	}

	return FFMPEG_DECODER_SUCCESS;
}

bool Radix::Media::FFMpegDecoder::isOpen() {
	return (context_ != NULL && frame_ != NULL);
}