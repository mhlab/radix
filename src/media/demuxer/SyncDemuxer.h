#pragma once

#include "FFMpegDemuxer.h"

namespace Radix {
	namespace Media {
		typedef void(*FOnUpdatedTime)(long playTime);

		class SyncDemuxerClock {
		private:
			double startPts_;
			unsigned long clock_;

		public:
			SyncDemuxerClock();
			~SyncDemuxerClock();

		public:
			void start(int startPts);
			void stop();
			bool isStart();
			double getClockMiliseconds();
		};

		class SyncDemuxer {
		private:
			FFMpegDemuxer audioDemuxer_;
			FFMpegDemuxer videoDemuxer_;
			FOnCommandResult onCommandResult_;
			FOnRecvMediaStream videoRecvMediaStream_;
			FOnRecvMediaStream audioRecvMediaStream_;
			FOnCodecData onCodecData_;
			FOnUpdatedTime onUpdatedTime_;
			void* callbackObj_;
			MediaAttribution mediaAttribution_;
			char* videoCodec_;
			char* audioCodec_;
			long startUnixTime_;
			long playTime_;
			SyncDemuxerClock clock_;
			int openDemuxerCount_;
			int processCommandCount_;
			Thread::Lock commandLock_;

			static void onCommandResult(DemuxerCommand command, bool ret, void* param, void* obj);
			static void onRecvMediaStream(const char* media, int mediaSize, int mediaCodec, int64_t presentationTime, void* obj);
			static void onCodecData(const char* videoCodecData, int videoCodecDataSize, char* audioCodecData, int audioCodecDataSize, void* obj);
			DemuxerCommand cuurentCommand();

		public:
			SyncDemuxer();
			~SyncDemuxer();
		public:
			void setListener(FOnCommandResult onCommandResult, FOnRecvMediaStream videoRecvMediaStream, FOnRecvMediaStream audioRecvMediaStream, FOnCodecData onCodecData, FOnUpdatedTime onUpdatedTime, void* obj);
			void open(const char* fileName);
			void close();
			void play();
			void stop();
			void pause();
			void seek(int seekSeconds);
			bool isSupportFile(const char* fileName);
		};
	}
}

