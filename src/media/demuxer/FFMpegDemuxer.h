#pragma once

extern "C" {
#include "libavformat/avformat.h"
}
#include "thread/ThreadRunner.h"
#include "collection/SafeQueue.h"
#include "string/StringEx.h"
#include "media/MediaDef.h"

namespace Radix {
	namespace Media {
		enum DemuxerCommand {
			DEMUXER_COMMAND_NONE = -1,
			DEMUXER_COMMAND_OPEN = 0,
			DEMUXER_COMMAND_PLAY = 1,
			DEMUXER_COMMAND_STOP = 2,
			DEMUXER_COMMAND_PAUSE = 3,
			DEMUXER_COMMAND_SEEK = 4
		};

		enum DemuxerOpenParam {
			DEMUXER_OPEN_PARAM_ALL = 0,
			DEMUXER_OPEN_PARAM_AUDIO = 1,
			DEMUXER_OPEN_PARAM_VIDEO = 2
		};

		struct MediaAttribution {
			unsigned int duration;
			int video_codec;
			int video_width;
			int video_height;
			int video_timebase_den;
			int video_timebase_num;
			int video_bitrate;
			int audio_codec;
			int audio_sample_rate;
			int audio_channelLayout;
			int audio_channels;
			int audio_profile;
			int audio_timebase_den;
			int audio_timebase_num;

			MediaAttribution() {
				reset();
			}

			void reset() {
				duration = 0;
				video_bitrate = 0;
				video_codec = (int)MEDIA_CODEC_UNKNOWN;
				audio_codec = (int)MEDIA_CODEC_UNKNOWN;
			}

			void copyVideoAttrTo(MediaAttribution* dst) {
				dst->video_codec = video_codec;
				dst->video_timebase_den = video_timebase_den;
				dst->video_timebase_num = video_timebase_num;
				dst->video_width = video_width;
				dst->video_height = video_height;
				dst->video_bitrate = video_bitrate;
			}

			void copyAudioAttrTo(MediaAttribution* dst) {
				dst->audio_codec = audio_codec;
				dst->audio_channelLayout = audio_channelLayout;
				dst->audio_channels = audio_channels;
				dst->audio_profile = audio_profile;
				dst->audio_sample_rate = audio_sample_rate;
				dst->audio_timebase_den = audio_timebase_den;
				dst->audio_timebase_num = audio_timebase_num;
			}

			std::string toJson() {
				return Radix::String::format("{\"duration\":%d, \"video_codec\":%d, \"video_width\": %d, \"video_height\": %d, \"video_timebase_den\":%d, \"video_timebase_num\":%d, "
					"\"video_bitrate\": %d, \"audio_codec\":%d, \"audio_sample_rate\":%d, \"audio_channelLayout\":%d, \"audio_channels\":%d, \"audio_profile\":%d, \"audio_timebase_den\": %d, \"audio_timebase_num\": %d}", 
					duration, video_codec, video_width, video_height, video_timebase_den, video_timebase_num, video_bitrate, audio_codec, audio_sample_rate, audio_channelLayout, 
					audio_channels, audio_profile, audio_timebase_den, audio_timebase_num);
			}
		};

		typedef void (*FOnCommandResult)(DemuxerCommand command, bool ret, void* param, void* obj);
		typedef void (*FOnCodecData)(const char* videoCodecData, int videoCodecDataSize, char* audioCodecData, int audioCodecDataSize, void* obj);
		typedef void (*FOnRecvMediaStream)(const char* media, int mediaSize, int mediaCodec, int64_t presentationTime, void* obj);

		class FFMpegDemuxer: public Thread::IThreadRunner {
		private:
			struct DemuxerQueueItem {
				DemuxerCommand command_;
				void* param_;

				DemuxerQueueItem() {
					param_ = NULL;
				}
			};
			AVFormatContext* context_;
			int videoStreamIdx_;
			int audioStreamIdx_;
			Thread::ThreadRunner* demuxerThread_;
			Collection::SafeQueue<DemuxerQueueItem*> demuxerQueue_;
			AVBitStreamFilterContext* aacFilterContext_;
			AVBSFContext* h264BsfContext_;
			AVBSFContext* aacBsfContext_;
			FOnCommandResult cmdResultCallback_;
			FOnRecvMediaStream recvMediaCallback_;
			FOnCodecData codecDataCallback_;
			void* callbackObj_;
			DemuxerOpenParam openParam_;
			DemuxerCommand currentCommand_;
			int64_t seekPts_;

			virtual void onThreadFunc(Thread::ThreadRunner* threadRunner, Thread::Signal& signal, bool& terminate);
			void h264_mp4extra_to_spspps(const char* extra, int extraSize, char** out, int* outSize);
			bool isValidStream(int streamIndex);

		public:
			FFMpegDemuxer();
			~FFMpegDemuxer();

		public:
			void setListener(FOnCommandResult cmdResultCallback, FOnRecvMediaStream recvMediaCallback, FOnCodecData codecDataCallback, void* obj);
			void open(const char* fileName, DemuxerOpenParam param);
			bool isOpen();
			void close();
			void play();
			void stop();
			void pause();
			void seek(int presentationTime);
			bool isSupportFile(const char* fileName);
			DemuxerCommand currentCommand();
		};
	}
}

