#include "StdAfx.h"
#include "SyncDemuxer.h"
#include "datetime/DateTime.h"
#include "log/Log.h"

Radix::Media::SyncDemuxerClock::SyncDemuxerClock() :
	clock_(0),
	startPts_(0.0f) {
}

Radix::Media::SyncDemuxerClock::~SyncDemuxerClock() {
}

void Radix::Media::SyncDemuxerClock::start(int startPts) {
	startPts_ = startPts;
	clock_ = Radix::DateTime::getCurrentUnixTimeMiliseconds();
}

void Radix::Media::SyncDemuxerClock::stop() {
	clock_ = 0;
	startPts_ = 0;
}

double Radix::Media::SyncDemuxerClock::getClockMiliseconds() {
	return startPts_ + (Radix::DateTime::getCurrentUnixTimeMiliseconds() - clock_);
}

bool Radix::Media::SyncDemuxerClock::isStart() {
	return (clock_ > 0);
}


Radix::Media::SyncDemuxer::SyncDemuxer():
	onCommandResult_(NULL),
	videoRecvMediaStream_(NULL),
	audioRecvMediaStream_(NULL),
	onCodecData_(NULL),
	onUpdatedTime_(NULL),
	callbackObj_(NULL),
	playTime_(0),
	openDemuxerCount_(0) {
	audioDemuxer_.setListener(onCommandResult, onRecvMediaStream, onCodecData, this);
	videoDemuxer_.setListener(onCommandResult, onRecvMediaStream, onCodecData, this);
}

Radix::Media::SyncDemuxer::~SyncDemuxer() {
	audioDemuxer_.close();
	videoDemuxer_.close();
}

void Radix::Media::SyncDemuxer::setListener(FOnCommandResult onCommandResult, FOnRecvMediaStream videoRecvMediaStream, FOnRecvMediaStream audioRecvMediaStream, FOnCodecData onCodecData,
	FOnUpdatedTime onUpdatedTime, void* obj) {
	onCommandResult_ = onCommandResult;
	videoRecvMediaStream_ = videoRecvMediaStream;
	audioRecvMediaStream_ = audioRecvMediaStream;
	onCodecData_ = onCodecData;
	onUpdatedTime_ = onUpdatedTime;
	callbackObj_ = obj;
}

void Radix::Media::SyncDemuxer::open(const char* fileName) {
	close();

	mediaAttribution_.video_codec = MEDIA_CODEC_UNKNOWN;
	mediaAttribution_.audio_codec = MEDIA_CODEC_UNKNOWN;
	mediaAttribution_.duration = 0;
	openDemuxerCount_ = 2;
	processCommandCount_ = openDemuxerCount_;

	audioDemuxer_.open(fileName, DEMUXER_OPEN_PARAM_AUDIO);
	videoDemuxer_.open(fileName, DEMUXER_OPEN_PARAM_VIDEO);
}

void Radix::Media::SyncDemuxer::close() {
	audioDemuxer_.close();
	videoDemuxer_.close();
	openDemuxerCount_ = 0;
}

void Radix::Media::SyncDemuxer::onCommandResult(DemuxerCommand command, bool ret, void* param, void* obj) {
	if (obj == NULL)
		return;

	SyncDemuxer* syncDemuxer = (SyncDemuxer*)obj;
	Thread::Locker locker(&syncDemuxer->commandLock_);
	syncDemuxer->processCommandCount_--;

	if (command == DEMUXER_COMMAND_OPEN) {
		MediaAttribution* mediaAttr = NULL;
		if (param != NULL)
			mediaAttr = (MediaAttribution*)param;
		if (ret && mediaAttr->video_codec != MEDIA_CODEC_UNKNOWN) {
			mediaAttr->copyVideoAttrTo(&syncDemuxer->mediaAttribution_);
		}
		else if (ret && mediaAttr->audio_codec != MEDIA_CODEC_UNKNOWN)
			mediaAttr->copyAudioAttrTo(&syncDemuxer->mediaAttribution_);
		if (mediaAttr != NULL && mediaAttr->duration > 0)
			syncDemuxer->mediaAttribution_.duration = mediaAttr->duration;
		if (syncDemuxer->processCommandCount_ == 0) {
			syncDemuxer->openDemuxerCount_ = 0;
			if (syncDemuxer->mediaAttribution_.video_codec != MEDIA_CODEC_UNKNOWN)
				syncDemuxer->openDemuxerCount_++;
			if (syncDemuxer->mediaAttribution_.audio_codec != MEDIA_CODEC_UNKNOWN)
				syncDemuxer->openDemuxerCount_++;
			syncDemuxer->processCommandCount_ = syncDemuxer->openDemuxerCount_;
			ret = (syncDemuxer->mediaAttribution_.video_codec != MEDIA_CODEC_UNKNOWN || syncDemuxer->mediaAttribution_.audio_codec != MEDIA_CODEC_UNKNOWN);
			if (syncDemuxer->onCommandResult_ != NULL)
				syncDemuxer->onCommandResult_(command, ret, &syncDemuxer->mediaAttribution_, syncDemuxer->callbackObj_);
		}
	}
	else {
		if (command == DEMUXER_COMMAND_STOP || command == DEMUXER_COMMAND_PAUSE || command == DEMUXER_COMMAND_SEEK)
			syncDemuxer->clock_.stop();
		if (syncDemuxer->processCommandCount_ == 0) {
			if (syncDemuxer->onCommandResult_ != NULL)
				syncDemuxer->onCommandResult_(command, ret, param, syncDemuxer->callbackObj_);
			syncDemuxer->processCommandCount_ = syncDemuxer->openDemuxerCount_;
		}
	}
}

void Radix::Media::SyncDemuxer::onRecvMediaStream(const char* media, int mediaSize, int mediaCodec, int64_t presentationTime, void* obj) {
	if (obj == NULL)
		return;
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)obj;
	bool isVideo = isVideoCodec((MediaCodec)mediaCodec);
	if (isVideo) {
		if (syncDemuxer->videoRecvMediaStream_ != NULL)
			syncDemuxer->videoRecvMediaStream_(media, mediaSize, mediaCodec, presentationTime, syncDemuxer->callbackObj_);
	}
	else {
		if (syncDemuxer->audioRecvMediaStream_ != NULL)
			syncDemuxer->audioRecvMediaStream_(media, mediaSize, mediaCodec, presentationTime, syncDemuxer->callbackObj_);
	}
	if (syncDemuxer->cuurentCommand() != DEMUXER_COMMAND_PLAY || presentationTime < 0) {
		Thread::ThreadRunner::sleep(1);
		return;
	}

	double newPresentationTime;
	if (isVideo) {
		newPresentationTime = (double)presentationTime * 1000 * ((double)syncDemuxer->mediaAttribution_.video_timebase_num / syncDemuxer->mediaAttribution_.video_timebase_den);
		if (syncDemuxer->mediaAttribution_.audio_codec == Media::MEDIA_CODEC_UNKNOWN && !syncDemuxer->clock_.isStart())
			syncDemuxer->clock_.start(newPresentationTime);
	}
	else {
		newPresentationTime = (double)presentationTime * 1000 * ((double)syncDemuxer->mediaAttribution_.audio_timebase_num / syncDemuxer->mediaAttribution_.audio_timebase_den);
		if (!syncDemuxer->clock_.isStart())
			syncDemuxer->clock_.start(newPresentationTime);
	}
	if (!syncDemuxer->clock_.isStart()) {
		Thread::ThreadRunner::sleep(1);
		return;
	}
	double curMiliseconds = syncDemuxer->clock_.getClockMiliseconds();
	int syncTime = round(newPresentationTime - curMiliseconds);
	if (syncTime > 0)
		Thread::ThreadRunner::sleep(syncTime);
	else
		Thread::ThreadRunner::sleep(1);
}

void Radix::Media::SyncDemuxer::onCodecData(const char* videoCodecData, int videoCodecDataSize, char* audioCodecData, int audioCodecDataSize, void* obj) {
	if (obj == NULL)
		return;
	SyncDemuxer* syncDemuxer = (SyncDemuxer*)obj;
	if (syncDemuxer->onCodecData_ != NULL)
		syncDemuxer->onCodecData_(videoCodecData, videoCodecDataSize, audioCodecData, audioCodecDataSize, syncDemuxer->callbackObj_);
}

void Radix::Media::SyncDemuxer::play() {
	if (!audioDemuxer_.isOpen() && !videoDemuxer_.isOpen()) {
		if (onCommandResult_ != NULL)
			onCommandResult_(DEMUXER_COMMAND_PLAY, false, NULL, NULL);
	}
	if (videoDemuxer_.isOpen())
		videoDemuxer_.play();
	if (audioDemuxer_.isOpen())
		audioDemuxer_.play();
}

void Radix::Media::SyncDemuxer::stop() {
	if (!audioDemuxer_.isOpen() && !videoDemuxer_.isOpen()) {
		if (onCommandResult_ != NULL)
			onCommandResult_(DEMUXER_COMMAND_STOP, false, NULL, NULL);
	}
	if (videoDemuxer_.isOpen())
		videoDemuxer_.stop();
	if (audioDemuxer_.isOpen())
		audioDemuxer_.stop();
}

void Radix::Media::SyncDemuxer::pause() {
	if (!audioDemuxer_.isOpen() && !videoDemuxer_.isOpen()) {
		if (onCommandResult_ != NULL)
			onCommandResult_(DEMUXER_COMMAND_PAUSE, false, NULL, NULL);
	}
	if (videoDemuxer_.isOpen())
		videoDemuxer_.pause();
	if (audioDemuxer_.isOpen())
		audioDemuxer_.pause();
}

void Radix::Media::SyncDemuxer::seek(int seekSeconds) {
	if (videoDemuxer_.isOpen())
		videoDemuxer_.seek(seekSeconds);
	if (audioDemuxer_.isOpen())
		audioDemuxer_.seek(seekSeconds);
}

bool Radix::Media::SyncDemuxer::isSupportFile(const char* fileName) {
	return videoDemuxer_.isSupportFile(fileName);
}

Radix::Media::DemuxerCommand Radix::Media::SyncDemuxer::cuurentCommand() {
	if (videoDemuxer_.isOpen())
		return videoDemuxer_.currentCommand();

	if (audioDemuxer_.isOpen())
		return audioDemuxer_.currentCommand();

	return DEMUXER_COMMAND_NONE;
}
