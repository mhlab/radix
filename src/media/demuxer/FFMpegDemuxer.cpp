#include "StdAfx.h"
#include "FFMpegDemuxer.h"
#include "log/Log.h"

Radix::Media::FFMpegDemuxer::FFMpegDemuxer():
	context_(NULL),
	videoStreamIdx_(-1),
	audioStreamIdx_(-1),
	demuxerThread_(NULL),
	aacBsfContext_(NULL),
	h264BsfContext_(NULL),
	cmdResultCallback_(NULL),
	recvMediaCallback_(NULL),
	codecDataCallback_(NULL),
	callbackObj_(NULL),
	currentCommand_(DEMUXER_COMMAND_NONE),
	seekPts_(-1) {
	av_register_all();
	av_log_set_level(AV_LOG_QUIET);
	av_log_set_callback(NULL);
}

Radix::Media::FFMpegDemuxer::~FFMpegDemuxer() {
	close();
}

void Radix::Media::FFMpegDemuxer::setListener(FOnCommandResult cmdResultCallback, FOnRecvMediaStream recvMediaCallback, FOnCodecData codecDataCallback, void* obj) {
	cmdResultCallback_ = cmdResultCallback;
	recvMediaCallback_ = recvMediaCallback;
	codecDataCallback_ = codecDataCallback;
	callbackObj_ = obj;
}

void Radix::Media::FFMpegDemuxer::h264_mp4extra_to_spspps(const char* extra, int extraSize, char** out, int* outSize) {
	int spsindex = -1;
	int ppsindex = -1;
	for (int i = 0; i < extraSize; i++) {
		if ((*(extra + i) & 0x1f) == H264CODEC_NALTYPE_SPS)
			spsindex = i;
		if ((*(extra + i) & 0x1f) == H264CODEC_NALTYPE_PPS)
			ppsindex = i;
	}
	if (spsindex == -1 || ppsindex == -1) {
		*out = new char[extraSize];
		*outSize = extraSize;
		memcpy(*out, extra, extraSize);
	}
	else {
		char startcode[4] = {0, 0, 0, 1};
		int spsSize = (ppsindex - spsindex) + 4;
		int ppsSize = (extraSize - ppsindex) + 4;
		*out = new char[spsSize + ppsSize];
		*outSize = spsSize + ppsSize;
		memcpy(*out, startcode, 4);
		memcpy((*out + 4), (extra + spsindex), spsSize - 4);
		memcpy((*out + spsSize), startcode, 4);
		memcpy((*out + spsSize + 4), (extra + ppsindex), ppsSize - 4);
	}
}

bool Radix::Media::FFMpegDemuxer::isValidStream(int streamIndex) {
	if (openParam_ == DEMUXER_OPEN_PARAM_ALL)
		return true;

	if (openParam_ == DEMUXER_OPEN_PARAM_AUDIO && streamIndex == audioStreamIdx_)
		return true;

	if (openParam_ == DEMUXER_OPEN_PARAM_VIDEO && streamIndex == videoStreamIdx_)
		return true;

	return false;
}

void Radix::Media::FFMpegDemuxer::open(const char* fileName, DemuxerOpenParam param) {
	close();
	try {
		long duration = 0;
		char* videoCodecData = NULL;
		int videoCodecDataSize = 0;
		char* audioCodecData = NULL;
		int audioCodecDataSize = 0;
		videoStreamIdx_ = -1;
		audioStreamIdx_ = -1;
		if (avformat_open_input(&context_, fileName, NULL, NULL) < 0)
			throw 0;
		if (avformat_find_stream_info(context_, NULL) < 0)
			throw 1;

		int ret = -1;
		if (param == DEMUXER_OPEN_PARAM_ALL || param == DEMUXER_OPEN_PARAM_VIDEO)
			ret = av_find_best_stream(context_, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
		if (ret >= 0) {
			videoStreamIdx_ = ret;
			if (context_->streams[ret]->codecpar->codec_id == AV_CODEC_ID_H264) {
				const AVBitStreamFilter* bsf = av_bsf_get_by_name("h264_mp4toannexb");
				if (bsf != NULL) {
					int bsfret = av_bsf_alloc(bsf, &h264BsfContext_);
					if (bsfret == 0) {
						avcodec_parameters_copy(h264BsfContext_->par_in, context_->streams[ret]->codecpar);
						av_bsf_init(h264BsfContext_);
					}
				}
			}
			duration = (long)(context_->duration / 1000000);
			if (codecDataCallback_ != NULL && context_->streams[ret]->codecpar->extradata != NULL) {
				videoCodecData = (char*)context_->streams[ret]->codecpar->extradata;
				videoCodecDataSize = context_->streams[ret]->codecpar->extradata_size;
				if (context_->streams[ret]->codecpar->codec_id == AV_CODEC_ID_H264)
					h264_mp4extra_to_spspps(videoCodecData, videoCodecDataSize, &videoCodecData, &videoCodecDataSize);
			}
		}
		ret = -1;
		if (param == DEMUXER_OPEN_PARAM_ALL || param == DEMUXER_OPEN_PARAM_AUDIO)
			ret = av_find_best_stream(context_, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);
		if (ret >= 0) {
			audioStreamIdx_ = ret;
			if (context_->streams[ret]->codecpar->codec_id == AV_CODEC_ID_AAC) {
				const AVBitStreamFilter* bsf = av_bsf_get_by_name("aac_adtstoasc");
				if (bsf != NULL) {
					int bsfret = av_bsf_alloc(bsf, &aacBsfContext_);
					if (bsfret == 0) {
						avcodec_parameters_copy(aacBsfContext_->par_in, context_->streams[ret]->codecpar);
						av_bsf_init(aacBsfContext_);
					}
				}
			}
			if (duration == 0)
				duration = (long)(context_->duration / 1000000);
			if (codecDataCallback_ != NULL && context_->streams[ret]->codecpar->extradata != NULL) {
				audioCodecData = (char*)context_->streams[ret]->codecpar->extradata;
				audioCodecDataSize = context_->streams[ret]->codecpar->extradata_size;
			}
		}
		MediaAttribution mediaInfo;
		mediaInfo.duration = duration;
		if (videoStreamIdx_ > -1) {
			mediaInfo.video_codec = context_->streams[videoStreamIdx_]->codecpar->codec_id;
			mediaInfo.video_width = context_->streams[videoStreamIdx_]->codecpar->width;
			mediaInfo.video_height = context_->streams[videoStreamIdx_]->codecpar->height;
			mediaInfo.video_timebase_num = context_->streams[videoStreamIdx_]->time_base.num;
			mediaInfo.video_timebase_den = context_->streams[videoStreamIdx_]->time_base.den;
			mediaInfo.video_bitrate = (int)context_->streams[videoStreamIdx_]->codecpar->bit_rate;
		}
		if (audioStreamIdx_ > -1) {
			mediaInfo.audio_codec = context_->streams[audioStreamIdx_]->codecpar->codec_id;
			mediaInfo.audio_sample_rate = context_->streams[audioStreamIdx_]->codecpar->sample_rate;
			mediaInfo.audio_channels = context_->streams[audioStreamIdx_]->codecpar->channels;
			mediaInfo.audio_channelLayout = (int)context_->streams[audioStreamIdx_]->codecpar->channel_layout;
			mediaInfo.audio_profile = context_->streams[audioStreamIdx_]->codecpar->profile;
			mediaInfo.audio_timebase_num = context_->streams[audioStreamIdx_]->time_base.num;
			mediaInfo.audio_timebase_den = context_->streams[audioStreamIdx_]->time_base.den;
		}
		openParam_ = param;
		if (demuxerThread_ == NULL)
			demuxerThread_ = new Thread::ThreadRunner(this);
		if (codecDataCallback_ != NULL)
			codecDataCallback_(videoCodecData, videoCodecDataSize, audioCodecData, audioCodecDataSize, callbackObj_);
		if (cmdResultCallback_ != NULL)
			cmdResultCallback_(DEMUXER_COMMAND_OPEN, (videoStreamIdx_ > -1 || audioStreamIdx_ > -1), &mediaInfo, callbackObj_);
		return;
	}
	catch(...) {
	}

	close();
	if (cmdResultCallback_ != NULL)
		cmdResultCallback_(DEMUXER_COMMAND_OPEN, false, NULL, callbackObj_);
}

bool Radix::Media::FFMpegDemuxer::isOpen() {
	return (context_ != NULL);
}

void Radix::Media::FFMpegDemuxer::close() {
	if (demuxerThread_ != NULL) {
		delete demuxerThread_;
		demuxerThread_ = NULL;
	}
	else {
		if (context_ != NULL) {
			avformat_close_input(&context_);
			context_ = NULL;
		}
	}
	if (aacBsfContext_ != NULL) {
		av_bsf_free(&aacBsfContext_);
		aacBsfContext_ = NULL;
	}
	if (h264BsfContext_ != NULL) {
		av_bsf_free(&h264BsfContext_);
		h264BsfContext_ = NULL;
	}
	videoStreamIdx_ = -1;
	audioStreamIdx_ = -1;
}

void Radix::Media::FFMpegDemuxer::onThreadFunc(Thread::ThreadRunner* threadRunner, Thread::Signal& signal, bool& terminate) {
	AVPacket pkt = { 0, };
    av_init_packet(&pkt);
	DemuxerCommand oldCommand_ = DEMUXER_COMMAND_NONE;
	while (!terminate) {
		signal.wait();
		if (terminate)
			break;
		if (oldCommand_ != currentCommand_ && currentCommand_ == DEMUXER_COMMAND_PLAY) {
			if (cmdResultCallback_ != NULL)
				cmdResultCallback_(DEMUXER_COMMAND_PLAY, true, NULL, callbackObj_);
		}
		oldCommand_ = currentCommand_;
		switch (currentCommand_) {
		case DEMUXER_COMMAND_PLAY:
		{
			pkt.data = NULL;
			pkt.size = 0;
			int ret = av_read_frame(context_, &pkt);
			if (ret < 0) {
				if (recvMediaCallback_ != NULL)
					recvMediaCallback_(NULL, 0, (int)MEDIA_CODEC_UNKNOWN, -1, callbackObj_);
				continue;
			}
			if (!isValidStream(pkt.stream_index)) {
				if (currentCommand_ == DEMUXER_COMMAND_PLAY)
					signal.set();
				continue;
			}
			uint8_t* out = pkt.data;
			int outsize = pkt.size;
			int64_t pts = pkt.pts;
			if (pkt.stream_index == videoStreamIdx_)
				pts = pkt.dts;
			AVBSFContext* bsfContext = NULL;
			if (pkt.stream_index == videoStreamIdx_ && h264BsfContext_ != NULL)
				bsfContext = h264BsfContext_;
			else if (pkt.stream_index == audioStreamIdx_ && aacBsfContext_ != NULL)
				bsfContext = aacBsfContext_;
			if (bsfContext != NULL) {
				AVPacket filterPacket = { 0, };
				AVPacket filteredPacket = { 0, };
				av_packet_ref(&filterPacket, &pkt);
				int ret = av_bsf_send_packet(bsfContext, &filterPacket);
				if (ret == 0) {
					ret = av_bsf_receive_packet(bsfContext, &filteredPacket);
					while (ret == 0) {
						out = filteredPacket.data;
						outsize = filteredPacket.size;
						if (recvMediaCallback_ != NULL)
							recvMediaCallback_((const char*)out, outsize, context_->streams[pkt.stream_index]->codecpar->codec_id, pts, callbackObj_);
						av_packet_unref(&filteredPacket);
						ret = av_bsf_receive_packet(bsfContext, &filteredPacket);
					}
				}
				av_packet_unref(&filterPacket);
				av_packet_unref(&filteredPacket);
			}
			else {
				if (recvMediaCallback_ != NULL)
					recvMediaCallback_((const char*)out, outsize, context_->streams[pkt.stream_index]->codecpar->codec_id, pts, callbackObj_);
			}
			av_packet_unref(&pkt);
			if (currentCommand_ == DEMUXER_COMMAND_PLAY)
				signal.set();
			break;
		}
		case DEMUXER_COMMAND_STOP:
		{
			if (videoStreamIdx_ >= 0)
				av_seek_frame(context_, videoStreamIdx_, 0, AVSEEK_FLAG_BACKWARD);
			if (audioStreamIdx_ >= 0)
				av_seek_frame(context_, audioStreamIdx_, 0, AVSEEK_FLAG_BACKWARD);
			if (cmdResultCallback_ != NULL)
				cmdResultCallback_(DEMUXER_COMMAND_STOP, true, NULL, callbackObj_);
			break;
		}
		case DEMUXER_COMMAND_PAUSE:
		{
			if (cmdResultCallback_ != NULL)
				cmdResultCallback_(DEMUXER_COMMAND_PAUSE, true, NULL, callbackObj_);
			break;
		}
		case DEMUXER_COMMAND_SEEK:
		{
			currentCommand_ = DEMUXER_COMMAND_NONE;
			av_seek_frame(context_, -1, seekPts_, AVSEEK_FLAG_BACKWARD);
			if (cmdResultCallback_ != NULL)
				cmdResultCallback_(DEMUXER_COMMAND_SEEK, true, NULL, callbackObj_);
			break;
		}
		}
	}
}

void Radix::Media::FFMpegDemuxer::play() {
	if (demuxerThread_ == NULL)
		return;
	if (!isOpen())
		return;

	currentCommand_ = DEMUXER_COMMAND_PLAY;
	demuxerThread_->setEvent();
}

void Radix::Media::FFMpegDemuxer::stop() {
	if (demuxerThread_ == NULL)
		return;
	if (!isOpen())
		return;

	currentCommand_ = DEMUXER_COMMAND_STOP;
	demuxerThread_->setEvent();
}

void Radix::Media::FFMpegDemuxer::pause() {
	if (demuxerThread_ == NULL)
		return;
	if (!isOpen())
		return;

	currentCommand_ = DEMUXER_COMMAND_PAUSE;
	demuxerThread_->setEvent();
}

void Radix::Media::FFMpegDemuxer::seek(int presentationTime) {
	if (demuxerThread_ == NULL)
		return;
	if (!isOpen() || (videoStreamIdx_ == -1 && audioStreamIdx_ == -1))
		return;

	seekPts_ = (int64_t)presentationTime * AV_TIME_BASE;
	currentCommand_ = DEMUXER_COMMAND_SEEK;
	demuxerThread_->setEvent();	
}

bool Radix::Media::FFMpegDemuxer::isSupportFile(const char* fileName) {
	AVFormatContext* context = NULL;
	if (avformat_open_input(&context, fileName, NULL, NULL) < 0)
		return false;
	if (context->nb_streams == 0 || context->streams == NULL) {
	 	avformat_close_input(&context);
		return false;
	}
	AVStream* stream = *(context->streams);
	bool findStream = false;
	for (unsigned int i = 0; i < context->nb_streams; i++) {
		if (stream->index >= 0 && stream->codecpar != NULL) {
			if (stream->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
				if (stream->codecpar->width > 0 && stream->codecpar->height > 0) {
					findStream = true;
					break;
				}
			}
			else if (stream->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
				findStream = true;
				break;
			}
		}
		stream++;
	}
	if (context != NULL)
		avformat_close_input(&context);
	return findStream;
}

Radix::Media::DemuxerCommand Radix::Media::FFMpegDemuxer::currentCommand() {
	return currentCommand_;
}
