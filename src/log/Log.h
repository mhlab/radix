#pragma once

namespace Radix {
	namespace Log {
		void debugOutputA(char* format, ...);
		void debugOutputW(wchar_t* format, ...);
	}
}