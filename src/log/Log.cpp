#include "StdAfx.h"
#include "Log.h"
#include <stdarg.h>
#include <stdio.h>
#ifdef _WIN32
#include <Windows.h>
#elif __ANDROID__
#include <android/log.h>
#endif

void Radix::Log::debugOutputA(char* format, ...) {
	va_list args;
	va_start(args, format);
#ifdef _WIN32
	char Buf[1024];
	int nBuf = _vsnprintf_s(Buf, 1024, format, args);
	OutputDebugStringA(Buf);
#elif __APPLE__
	va_list args;
	va_start(args, format);
    vfprintf(stderr, format, args);
#elif __ANDROID__
    __android_log_vprint(ANDROID_LOG_DEBUG, "Tag", format, args);
#endif
	va_end(args);
}

void Radix::Log::debugOutputW(wchar_t* format, ...) {
	va_list args;
	va_start(args, format);
#ifdef _WIN32
	wchar_t Buf[1024];
	int nBuf = _vsnwprintf_s(Buf, 1024, format, args);
	OutputDebugStringW(Buf);
#elif __APPLE__
//	va_list args;
//	va_start(args, format);
//	vfprintf(stderr, format, args);
#elif __ANDROID__
//	__android_log_vprint(ANDROID_LOG_DEBUG, "Tag", format, args);
#endif
	va_end(args);
}