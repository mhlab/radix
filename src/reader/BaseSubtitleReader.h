#pragma once

#include <string>
#include "thread/ThreadRunner.h"

namespace Radix {
	namespace Reader {
		typedef void (*FOnRecvSubtitle)(const char* subtitle, int length);
		typedef void (*FOnOpenSubtitle)(const char* subtitleFile, bool ret);

		enum SubTitleFileType {
			SUBTITLE_FILE_SMI = 0,
			SUBTITLE_FILE_SRT = 1
		};

		class BaseSubtitleReader : public Thread::IThreadRunner {
		private:
			FOnRecvSubtitle onRecvSubtitle_;
			FOnOpenSubtitle onOpenSubtitle_;
			Thread::ThreadRunner* openThread_;
			std::string subtitleFile_;
			bool isOpen_;
			double prevPlaytime_;

			virtual void onThreadFunc(Thread::ThreadRunner* threadRunner, Thread::Signal& signal, bool& terminate);

		protected:
			struct SubTitleInfo {
				double beginSeconds_;
				double endSeconds_;
				char* subTitle_;
				int subtitleLength_;
			};
			SubTitleInfo curSubTitleInfo_;

			virtual void next();
			virtual bool internalOpen(const char* subtitleFile) { return false; }
			virtual bool move(double playTime) { return false; }

		public:
			BaseSubtitleReader();
			virtual ~BaseSubtitleReader();

		public:
			void setListener(FOnOpenSubtitle onOpenSubtitle, FOnRecvSubtitle onRecvSubtitles);
			void open(const char* subtitleFile);
			void progress(double playTime);
			void reset();
			static BaseSubtitleReader* createSubtitleReader(SubTitleFileType fileType);
		};
	}
}

