#include "stdafx.h"
#include "BaseSubtitleReader.h"
#include "SmiFileReader.h"
#include "string/StringEx.h"
#include "log/Log.h"

Radix::Reader::BaseSubtitleReader::BaseSubtitleReader() :
	onRecvSubtitle_(NULL),
	onOpenSubtitle_(NULL),
	openThread_(NULL),
	isOpen_(false),
	prevPlaytime_(-1) {
	curSubTitleInfo_.beginSeconds_ = -1;
	curSubTitleInfo_.endSeconds_ = -1;
}

Radix::Reader::BaseSubtitleReader::~BaseSubtitleReader() {
	if (openThread_ != NULL)
		delete openThread_;
}

void Radix::Reader::BaseSubtitleReader::setListener(FOnOpenSubtitle onOpenSubtitle, FOnRecvSubtitle onRecvSubtitles) {
	onOpenSubtitle_ = onOpenSubtitle;
	onRecvSubtitle_ = onRecvSubtitles;
}

void Radix::Reader::BaseSubtitleReader::open(const char* subtitleFile) {
	subtitleFile_ = subtitleFile;
	if (openThread_ != NULL) {
		if (!openThread_->isTerminated())
			return;
		delete openThread_;
	}
	openThread_ = new Thread::ThreadRunner(this);
}

void Radix::Reader::BaseSubtitleReader::onThreadFunc(Thread::ThreadRunner* threadRunner, Thread::Signal& signal, bool& terminate) {
	const char* subtitleFile = subtitleFile_.c_str();
	isOpen_ = internalOpen(subtitleFile);
	if (onOpenSubtitle_ != NULL)
		onOpenSubtitle_(subtitleFile, isOpen_);
}

void Radix::Reader::BaseSubtitleReader::progress(double playTime) {
	if (!isOpen_)
		return;
	if (prevPlaytime_ == -1 || abs(prevPlaytime_ - playTime) > 1.0f || playTime > curSubTitleInfo_.endSeconds_) {
		if (!move(playTime))
			return;
	}
	prevPlaytime_ = playTime;
#ifdef _RADIX_READER_DEBUG
	Log::debugOutputA("[BaseSubtitleReader::progress] playTime: %f\n, beginSeconds: %f, endSeconds: %f\n", playTime, curSubTitleInfo_.beginSeconds_, curSubTitleInfo_.endSeconds_);
#endif
	if (playTime >= curSubTitleInfo_.beginSeconds_ && playTime < curSubTitleInfo_.endSeconds_) {
#ifdef _RADIX_READER_DEBUG
		Log::debugOutputA("[BaseSubtitleReader::progress] Send SubTitle and Next\n");
#endif
		if (onRecvSubtitle_ != NULL)
			onRecvSubtitle_(curSubTitleInfo_.subTitle_, curSubTitleInfo_.subtitleLength_);
		next();
	}
}

void Radix::Reader::BaseSubtitleReader::next() {
}

Radix::Reader::BaseSubtitleReader* Radix::Reader::BaseSubtitleReader::createSubtitleReader(SubTitleFileType fileType) {
	switch (fileType)
	{
	case Radix::Reader::SUBTITLE_FILE_SMI:
		return new SmiFileReader();
	case Radix::Reader::SUBTITLE_FILE_SRT:
		return NULL;
	default:
		return NULL;
	}
}

void Radix::Reader::BaseSubtitleReader::reset() {
	curSubTitleInfo_.beginSeconds_ = -1;
	curSubTitleInfo_.endSeconds_ = -1;
	prevPlaytime_ = -1;
}