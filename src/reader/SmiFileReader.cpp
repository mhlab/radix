#include "stdafx.h"
#include "SmiFileReader.h"
#include "string/StringEx.h"
#include "file/FileEx.h"
#include "log/Log.h"

Radix::Reader::SmiFileReader::SmiFileReader(): BaseSubtitleReader() {
}

Radix::Reader::SmiFileReader::~SmiFileReader() {
	for (auto it = timeSubtitleVec_.begin(); it != timeSubtitleVec_.end(); it++)
		delete (*it);
}

bool Radix::Reader::SmiFileReader::internalOpen(const char* subtitleFile) {
	if (subtitleFile == NULL)
		return false;
	std::string ext = Radix::String::getFileExtension(subtitleFile);
	if (strcmp(ext.c_str(), ".smi") != 0 && strcmp(ext.c_str(), ".sami") != 0)
		return false;
	char* content = Radix::File::getContentFromFile(subtitleFile);
	if (content == NULL)
		return false;

	std::string smicontent = content;
	std::regex reg("<SYNC Start=(\\d+)><P Class=(\\w+)>", std::regex_constants::ECMAScript | std::regex_constants::icase);
	std::sregex_iterator iter(smicontent.begin(), smicontent.end(), reg);
	std::sregex_iterator end;
	if (iter == end) {
		delete[] content;
		return false;
	}

	initCodepage();
	SmiTimeSubtitle* smiTimeSubtitle;
	while (iter != end) {
		smiTimeSubtitle = new SmiTimeSubtitle();
		parseTimeSubtitle(smiTimeSubtitle, iter, smicontent);
		timeSubtitleVec_.push_back(smiTimeSubtitle);
		iter++;
	}
	insertDummyifNeed();
	delete[] content;
	return true;
}

void Radix::Reader::SmiFileReader::initCodepage() {
	smiEncodeMap_.clear();
	smiEncodeMap_["KRCC"] = "euc-kr";
}

void Radix::Reader::SmiFileReader::parseTimeSubtitle(SmiTimeSubtitle* smiTimeSubtitle, std::sregex_iterator& iter, std::string& smicontent) {
	smiTimeSubtitle->time_ = (double)(atoi(iter->str(1).c_str())) / 1000;
	
	std::string localeKey = iter->str(2);
	std::string srcEncode = "ascii";
	auto it = smiEncodeMap_.find(localeKey);
	if (it != smiEncodeMap_.end())
		srcEncode = it->second.c_str();

	std::string findStr = iter->str(0);
	std::size_t stPos = iter->position(0) + findStr.length();
	std::size_t edPos = smicontent.find("<SYNC", stPos);
	if (edPos == std::string::npos) {
		edPos = smicontent.find("</BODY>", stPos);
		if (edPos == std::string::npos)
			return;
	}
	std::string subtitle = smicontent.substr(stPos, edPos - stPos);
	Radix::String::replaceTo(subtitle, "\r\n", "");
	Radix::String::replaceTo(subtitle, "&nbsp;", "");

	char* subtitlePtr = (char*)subtitle.c_str();
	size_t subtitleLength = subtitle.length();
	size_t utfSubtitleSize = subtitleLength * 2;
	if (smiTimeSubtitle->subtitle_ == NULL || strlen(smiTimeSubtitle->subtitle_) < utfSubtitleSize) {
		if (smiTimeSubtitle->subtitle_ != NULL)
			delete[] smiTimeSubtitle->subtitle_;
		smiTimeSubtitle->subtitle_ = new char[utfSubtitleSize];
	}
	memset(smiTimeSubtitle->subtitle_, 0x00, utfSubtitleSize);
	smiTimeSubtitle->subtitleLength_ = utfSubtitleSize;
	char* utfPtr = smiTimeSubtitle->subtitle_;
	size_t utfSize = utfSubtitleSize;
	iconvImpl.convert(srcEncode.c_str(), "utf-16", &subtitlePtr, subtitleLength, &utfPtr, utfSize);
	smiTimeSubtitle->subtitleLength_ -= utfSize;
}

bool Radix::Reader::SmiFileReader::move(double playTime) {
	double beginSeconds = -1;
	double endSeconds = -1;
	for (currentIt = timeSubtitleVec_.begin(); currentIt != timeSubtitleVec_.end(); currentIt++) {
		beginSeconds = endSeconds;
		endSeconds = (*currentIt)->time_;
		if (beginSeconds > -1 && playTime >= beginSeconds && playTime <= endSeconds) {
			currentIt--;
			break;
		}
	}
	curSubTitleInfo_.beginSeconds_ = beginSeconds;
	curSubTitleInfo_.endSeconds_ = endSeconds;
	if (currentIt == timeSubtitleVec_.end())
		return false;
	curSubTitleInfo_.subTitle_ = (*currentIt)->subtitle_;
	curSubTitleInfo_.subtitleLength_ = (*currentIt)->subtitleLength_;
	return true;
}

void Radix::Reader::SmiFileReader::insertDummyifNeed() {
	auto it = timeSubtitleVec_.begin();
	if (it == timeSubtitleVec_.end())
		return;
	if ((*it)->time_ > 0) {
		SmiTimeSubtitle* smiTimeSubtitle = new SmiTimeSubtitle();
		smiTimeSubtitle->time_ = 0;
		smiTimeSubtitle->subtitle_ = new char[4];
		smiTimeSubtitle->subtitle_[0] = 0xFE;
		smiTimeSubtitle->subtitle_[1] = 0xFF;
		smiTimeSubtitle->subtitle_[2] = 0x00;
		smiTimeSubtitle->subtitle_[3] = 0x00;
		timeSubtitleVec_.insert(it, smiTimeSubtitle);
	}
}

void Radix::Reader::SmiFileReader::next() {
	if (currentIt == timeSubtitleVec_.end())
		return;

	currentIt++;
	if (currentIt == timeSubtitleVec_.end())
		return;
	curSubTitleInfo_.beginSeconds_ = (*currentIt)->time_;
	curSubTitleInfo_.subTitle_ = (*currentIt)->subtitle_;
	curSubTitleInfo_.subtitleLength_ = (*currentIt)->subtitleLength_;
	currentIt++;
	if (currentIt == timeSubtitleVec_.end())
		return;
	curSubTitleInfo_.endSeconds_ = (*currentIt)->time_;
	currentIt--;
}