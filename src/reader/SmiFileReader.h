#pragma once

#include <regex>
#include <map>
#include "BaseSubtitleReader.h"
#include "string/IconvImpl.h"

namespace Radix {
	namespace Reader {
		class SmiFileReader : public BaseSubtitleReader {
		private:
			struct SmiTimeSubtitle {
				double time_;
				char* subtitle_;
				int subtitleLength_;
				SmiTimeSubtitle() {
					subtitle_ = NULL;
					subtitleLength_ = 0;
				}
				~SmiTimeSubtitle() {
					if (subtitle_ != NULL)
						delete[] subtitle_;
				}
			};
			std::vector<SmiTimeSubtitle*> timeSubtitleVec_;
			std::vector<SmiTimeSubtitle*>::iterator currentIt;
			std::map<std::string, std::string> smiEncodeMap_;
			String::IconvImpl iconvImpl;

			void parseTimeSubtitle(SmiTimeSubtitle* smiTimeSubtitle, std::sregex_iterator& iter, std::string& smicontent);
			void initCodepage();
			void insertDummyifNeed();

		protected:
			virtual void next();
			virtual bool internalOpen(const char* subtitleFile);
			virtual bool move(double playTime);

		public:
			SmiFileReader();
			virtual ~SmiFileReader();
		};
	}
}
