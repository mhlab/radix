#pragma once

#include <string>
#include "string/StringEx.h"

namespace Radix {
	namespace Network {
		enum RTSPCommand {
			COMMAND_UNKNOWN = 0,
			COMMAND_CONNECTION = 1,
			COMMAND_DESCRIBE = 2,
			COMMAND_SETUP = 3,
			COMMAND_PLAY = 4,
			COMMAND_TEARDOWN = 5
		};

		enum RTSPCodec {
			CODEC_UNKNOWN = -1,
			CODEC_MPA = 14,
			CODEC_H264 = 96,
			CODEC_JPEG = 26
		};

		struct SDPInformation {
			std::string mediaType_;
			int transPort_;
			std::string protocol_;
			RTSPCodec codec_;
			int payloadType_;
			std::string control_;
			bool isAbsoluteControlUrl_;
			int videoWidth_;
			int videoHeight_;

			SDPInformation() {
				codec_ = CODEC_UNKNOWN;
				payloadType_ = -1;
				control_ = "";
				videoWidth_ = -1;
				videoHeight_ = -1;
				isAbsoluteControlUrl_ = true;
			}

			std::string toJson() {
				return Radix::String::format("{\"codec\": %d, \"video_width\": %d, \"video_height\": %d}", (int)codec_, videoWidth_, videoHeight_);
			}
		};

		inline std::string RTSPCommandToString(RTSPCommand command) {
			switch (command) {
			case COMMAND_DESCRIBE: return "DESCRIBE";
			case COMMAND_SETUP: return "SETUP";
			case COMMAND_PLAY: return "PLAY";
			case COMMAND_TEARDOWN: return "TEARDOWN";
            default: return "";
			}
		}

		inline static bool isVideoCodec(RTSPCodec codec) {
			return (codec == CODEC_H264 || codec == CODEC_JPEG);
		}

		inline static RTSPCodec live555CodecToRtspCodec(const char* codec) {
			if (strcmp(codec, "H264") == 0)
				return CODEC_H264;
			else if (strcmp(codec, "JPEG") == 0)
				return CODEC_JPEG;

			return CODEC_UNKNOWN;
		}
	}
}
