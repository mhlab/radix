#pragma once

#include "RTSPDef.h"

namespace Radix {
	namespace Network {
		struct FrameInfo {
			char* frame_;
			int size_;

			FrameInfo() {
				frame_ = NULL;
				size_ = 0;
			}
		};

		class MediaFrameAssembler {
		protected:
			FrameInfo frameInfo_;

		public:
			MediaFrameAssembler();
			virtual ~MediaFrameAssembler();

		public:
			virtual void put(const char* frame, int size);
			virtual FrameInfo& get();
			static MediaFrameAssembler* createFrameAssembler(RTSPCodec codec);
		};
	}
}

