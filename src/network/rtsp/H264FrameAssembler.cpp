#include "stdafx.h"
#include "H264FrameAssembler.h"

Radix::Network::NalunitContext::NalunitContext() {
}

Radix::Network::NalunitContext::~NalunitContext() {
}

int Radix::Network::NalunitContext::write(char* data, int dataSize) {
	if (!isStartMarker(data))
		return 0;

	context_.setPosition(0);
	int readSize = startCodeSize();
	char* readPos = data + readSize;
	while (readSize < dataSize)
	{
		if (isStartMarker(readPos))
			break;
		readPos++;
		readSize++;
	}

	context_.write(data, readSize);
	return readSize;
}

void Radix::Network::NalunitContext::reset() {
	context_.setPosition(0);
}

bool Radix::Network::NalunitContext::isReady() {
	return context_.getPosition() > startCodeSize();
}

int Radix::Network::NalunitContext::getSize() {
	return context_.getPosition();
}

char* Radix::Network::NalunitContext::getMemory() {
	return context_.memory();
}

bool Radix::Network::NalunitContext::isStartMarker(char* pos) {
	return (*pos == 0x00 && *(pos + 1) == 0x00 && *(pos + 2) == 0x00 && *(pos + 3) == 0x01);
}

int Radix::Network::NalunitContext::startCodeSize() {
	return 4;
}

Radix::Network::H264FrameAssembler::H264FrameAssembler():
	MediaFrameAssembler() {
}

Radix::Network::H264FrameAssembler::~H264FrameAssembler() {
}

void Radix::Network::H264FrameAssembler::put(const char* frame, int size) {
	if (frame == NULL || size == 0)
		return;

	int nalUnitType = *(frame + 4) & 0x1f;
	switch (nalUnitType)
	{
	case naltype_sps:
	{
		spsContext_.reset();
		ppsContext_.reset();
		fullFrame_.setPosition(0);

		char* framePos = (char*)frame;
		int readSize = spsContext_.write(framePos, size);
		int remainSize = size - readSize;
		if (remainSize > NalunitContext::startCodeSize())
			put(frame + readSize, remainSize);
		return;
	}
	case naltype_pps:
	{
		char* framePos = (char*)frame;
		int readSize = ppsContext_.write(framePos, size);
		int remainSize = size - readSize;
		if (remainSize > NalunitContext::startCodeSize())
			put(frame + readSize, remainSize);
		return;
	}
	case naltype_sei:
	{
		NalunitContext seiNalUnit;
		char* framePos = (char*)frame;
		int readSize = seiNalUnit.write(framePos, size);
		int remainSize = size - readSize;
		if (remainSize > NalunitContext::startCodeSize())
			put(frame + readSize, remainSize);
		else {
			frameInfo_.frame_ = NULL;
			frameInfo_.size_ = 0;
		}
		return;
	}
	}
	if (fullFrame_.getPosition() == 0) {
		frameInfo_.frame_ = NULL;
		frameInfo_.size_ = 0;
		if (!spsContext_.isReady() || !ppsContext_.isReady() || (nalUnitType != naltype_idr))
			return;
		fullFrame_.write(spsContext_.getMemory(), spsContext_.getSize());
		fullFrame_.write(ppsContext_.getMemory(), ppsContext_.getSize());
		fullFrame_.write(frame, size);

		frameInfo_.frame_ = fullFrame_.memory();
		frameInfo_.size_ = fullFrame_.getPosition();
	}
	else {
		MediaFrameAssembler::put(frame, size);
	}
}