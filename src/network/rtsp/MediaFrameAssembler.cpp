#include "stdafx.h"
#include "MediaFrameAssembler.h"
#include "H264FrameAssembler.h"

Radix::Network::MediaFrameAssembler::MediaFrameAssembler() {
}

Radix::Network::MediaFrameAssembler::~MediaFrameAssembler() {
}

void Radix::Network::MediaFrameAssembler::put(const char* frame, int size) {
	frameInfo_.frame_ = (char*)frame;
	frameInfo_.size_ = size;
}

Radix::Network::FrameInfo& Radix::Network::MediaFrameAssembler::get() {
	return frameInfo_;
}

Radix::Network::MediaFrameAssembler* Radix::Network::MediaFrameAssembler::createFrameAssembler(RTSPCodec codec) {
	if (codec == CODEC_H264)
		return new H264FrameAssembler();

	return new MediaFrameAssembler();
}