#include "StdAfx.h"
#include "RTSPConnection.h"
#include "network/rtsp/RTSPDef.h"
#include "string/StringEx.h"
#include "network/NetworkEx.h"
#include "UdpRtpSession.h"
#include "log/Log.h"

Radix::Network::RTSPConnection::RTSPConnection(int connectionId, const char* ip, int port, const char* abs_path):
	connectionId_(connectionId),
	ip_(ip),
	port_(port),
	abs_path_(abs_path),
	listener_(NULL),
	useRtpTcp_(false),
	rtpSession_(NULL) {
		url_ = Radix::String::format("rtsp://%s:%d/%s", ip, port, abs_path);
		rtspSocket_.setListener(this);
}

Radix::Network::RTSPConnection::~RTSPConnection() {
	sendTeardown();
	rtspSocket_.disconnect();
	clearSdp();
}

void Radix::Network::RTSPConnection::setListener(IRTSPConnection* listener) {
	listener_ = listener;
}

bool Radix::Network::RTSPConnection::isSameConnectionId(int connectionId) {
	return (connectionId_ == connectionId);
}

Radix::Network::TCPSocket* Radix::Network::RTSPConnection::rtspSocket() {
	return &rtspSocket_;
}

void Radix::Network::RTSPConnection::addToSelector(SocketSelector* selector) {
	if (selector == NULL)
		return;
	selector->addSocket(&rtspSocket_);
	if (rtpSession_ != NULL)
		selector->addSocket(rtpSession_->rtpSocket());
}

bool Radix::Network::RTSPConnection::isSetSocket(SocketSelector* selector) {
	if (selector == NULL)
		return false;

	bool ret = selector->isSet(&rtspSocket_);
	if (rtpSession_ != NULL)
		ret |= selector->isSet(rtpSession_->rtpSocket());
	if (rtcpSession_.isOpen())
		ret |= selector->isSet(rtcpSession_.rtcpSocket());

	return ret;
}

void Radix::Network::RTSPConnection::clearSdp() {
	for (auto it = sdpVec_.begin(); it != sdpVec_.end(); it++)
		delete *it;
	sdpVec_.clear();
}

void Radix::Network::RTSPConnection::createSdpInformation(const char* sdp) {
	clearSdp();
	std::vector<std::string> sdpLines;
	Radix::String::tokenize(sdp, "\r\n", sdpLines);

	char* key_rtpmap = "a=rtpmap:";
	char* key_framesize = "a=framesize:";
	char* key_control = "a=control:";
	char* key_media = "m=";
	unsigned int key_rtpmap_len = strlen(key_rtpmap);
	unsigned int key_framesize_len = strlen(key_framesize);
	unsigned int key_control_len = strlen(key_control);
	SDPInformation* sdpInfo = NULL;

	for (auto it = sdpLines.begin(); it != sdpLines.end(); it++) {
#ifdef _RADIX_RTSP_DEBUG
		Log::debugOutputA("[RTSPConnection::createSdpInformation] sdpLines - %s\n", it->c_str());
#endif
		if (it->find(key_media) != std::string::npos) {
			std::string media = it->substr(strlen(key_media), std::string::npos);
			std::vector<std::string> mediaToken;
			sdpInfo = new SDPInformation();
			String::tokenize(media.c_str(), " ", mediaToken);
			int tokenSize = mediaToken.size();
			if (tokenSize > 0)
				sdpInfo->mediaType_ = mediaToken[0];
			if (tokenSize > 2)
				sdpInfo->protocol_ = mediaToken[2];
		}
		else if (it->find(key_rtpmap) != std::string::npos) {
			if (sdpInfo == NULL)
				continue;
			int payloadType = 0;
			char codecName[50] = {0, };
			sscanf(it->substr(key_rtpmap_len, std::string::npos).c_str(), "%d %[^/]s", &payloadType, codecName);

//			sdpInfo = new SDPInformation();
			sdpInfo->payloadType_ = payloadType;
			sdpInfo->codec_ = CODEC_UNKNOWN;
			if (strcmp(codecName, "H264") == 0)
				sdpInfo->codec_ = CODEC_H264;
			sdpVec_.push_back(sdpInfo);
		}
		else if (it->find(key_framesize) != std::string::npos) {
			if (sdpInfo == NULL)
				continue;
			int payloadType = 0;
			int videoWidth = 0;
			int videoHeight = 0;
			sscanf(it->substr(key_framesize_len, std::string::npos).c_str(), "%d %d-%d", &payloadType, &videoWidth, &videoHeight);
			sdpInfo->videoWidth_ = videoWidth;
			sdpInfo->videoHeight_ = videoHeight;
		}
		else if (it->find(key_control) != std::string::npos) {
			if (sdpInfo == NULL)
				continue;
			sdpInfo->control_ = it->substr(key_control_len, std::string::npos);
			if (sdpInfo->control_.find("rtsp://") != std::string::npos)
				sdpInfo->isAbsoluteControlUrl_ = true;
			else
				sdpInfo->isAbsoluteControlUrl_ = false;
		}
	}
}

std::string Radix::Network::RTSPConnection::sdpinfoJson() {
	std::string ret = "[";
	for (auto it = sdpVec_.begin(); it != sdpVec_.end(); it++) {
		ret.append((*it)->toJson());
		ret.append(",");
	}
	if (sdpVec_.size() > 0)
		ret.erase(ret.length() - 1, 1);
	ret.append("]");
	return ret;
}

bool Radix::Network::RTSPConnection::processRtspRecv() {
	if (!rtspSocket_.isConnected())
		return false;

	std::stringstream packetstream;
	if (!rtspSocket_.read(packetstream, "\r\n\r\n", 4))
		return false;

#ifdef _RADIX_RTSP_DEBUG
	Log::debugOutputA("[RTSPConnection::processRtspRecv] Recv Packet: %s\n", packetstream.str().c_str());
#endif
	if (!packetAgent_.parseHeader(packetstream.str().c_str())) {
		if (listener_ != NULL)
			listener_->onCommandResult(connectionId_, packetAgent_.lastCommand(), false, NULL);
		return false;
	}

	std::string paramJson;
	switch (packetAgent_.lastCommand()) {
	case COMMAND_DESCRIBE:
		{
			int contentlength = packetAgent_.contentLength();
			if (contentlength > 0) {
				char* sdp = new char[contentlength];
				if (!rtspSocket_.read(sdp, contentlength))
					return false;
				createSdpInformation(sdp);
				delete[] sdp;
				writeCmdQueue_.push((int)COMMAND_SETUP);
				paramJson = sdpinfoJson();
			}
			break;
		}
	case COMMAND_SETUP:
		{
			sessionName_ = packetAgent_.sessionName();
			rtcpSession_.open(rtcpPort_);
			writeCmdQueue_.push((int)COMMAND_PLAY);
			break;
		}
	case COMMAND_PLAY:
		{
			if (rtpSession_ != NULL)
				break;
			if (!useRtpTcp_) {
				rtpSession_ = new UdpRtpSession();
				rtpSession_->setListener(this);
				if (!((UdpRtpSession*)rtpSession_)->open(rtpPort_))
					return false;
			}
			break;
		}
	case COMMAND_TEARDOWN:
		{
			if (rtpSession_ != NULL) {
				delete rtpSession_;
				rtpSession_ = NULL;
			}
			break;
		}
	}

	if (listener_ != NULL)
		listener_->onCommandResult(connectionId_, packetAgent_.lastCommand(), true, (char*)paramJson.c_str());
	return true;
}

void Radix::Network::RTSPConnection::onTCPSocketConnection(bool ret) {
#ifdef _RADIX_RTSP_DEBUG
	Log::debugOutputA("[RTSPConnection::onTCPSocketConnection] Socket Connection Result: %d\n", (int)ret);
#endif
	if (listener_ != NULL)
		listener_->onCommandResult(connectionId_, COMMAND_CONNECTION, ret, NULL);

	if (ret) {
		rtspSocket_.setAsyncMode(true);
		writeCmdQueue_.push((int)COMMAND_DESCRIBE);
	}
}

bool Radix::Network::RTSPConnection::turnToWrite() {
	if (writeCmdQueue_.count() == 0)
		return true;

	RTSPCommand command = (RTSPCommand)writeCmdQueue_.pop();
#ifdef _RADIX_RTSP_DEBUG
	Log::debugOutputA("[RTSPConnection::turnToWrite] Pop-Command: %d\n", command);
#endif
	switch (command) {
	case COMMAND_DESCRIBE: {
		if (!rtspSocket_.isConnected()) {
#ifdef _RADIX_RTSP_DEBUG
			Log::debugOutputA("[RTSPConnection::turnToWrite] Connect To Server. ip - %s, port - %d\n", ip_.c_str(), port_);
#endif
			rtspSocket_.connectAsync(ip_.c_str(), port_, 10);
			return true;
		}
		return sendDescribe();
	}
	case COMMAND_SETUP: {
		int sdpIndex = -1;
		for (int i = 0; i < sdpVec_.size(); i++) {
			if (sdpVec_.at(i)->codec_ == CODEC_H264) {
				sdpIndex = i;
				break;
			}
		}
		if (sdpIndex == -1) {
			if (listener_ != NULL)
				listener_->onCommandResult(connectionId_, COMMAND_SETUP, false, NULL);
			return false;
		}
		return sendSetup(sdpIndex);
	}
	case COMMAND_PLAY: return sendPlay();
	case COMMAND_TEARDOWN: return sendTeardown();
	}

	return false;
}

bool Radix::Network::RTSPConnection::turnToRead(SocketSelector* selector) {
	if (selector == NULL)
		return false;

	bool ret = false;
	if (selector->isSet(&rtspSocket_)) {
		ret = processRtspRecv();
#ifdef _RADIX_RTSP_DEBUG
		Log::debugOutputA("[RTSPConnection::turnToRead] ProcessRtspRecv Result: %d\n", (int)ret);
#endif
	}
	if (rtpSession_ != NULL) {
		if (selector->isSet(rtpSession_->rtpSocket())) {
			rtpSession_->processRtpRecv();
			ret = true;
		}
	}
	if (rtcpSession_.isOpen()) {
		if (selector->isSet(rtcpSession_.rtcpSocket())) {
			rtcpSession_.readRtcpPacket();
		}
	}

	return ret;
}

bool Radix::Network::RTSPConnection::sendDescribe() {
	if (!rtspSocket_.isConnected())
		return false;

	std::string packet = packetAgent_.createDescribePacket(url_.c_str(), "", "");
	return rtspSocket_.write(packet.c_str(), packet.length());
}

bool Radix::Network::RTSPConnection::sendSetup(int sdpIndex) {
	if (!rtspSocket_.isConnected())
		return false;

	rtpPort_ = findFreePort(62000);
	rtcpPort_ = rtpPort_ + 1;

	std::string url = url_.c_str();
	if (sdpVec_.size() > 0 && sdpIndex < sdpVec_.size()) {
		if (sdpVec_.at(sdpIndex)->isAbsoluteControlUrl_)
			url = sdpVec_.at(sdpIndex)->control_;
		else {
			url.append("/");
			url.append(sdpVec_.at(sdpIndex)->control_);
		}
	}

	std::string packet = packetAgent_.createSetupPacket(url.c_str(), rtpPort_);
#ifdef _RADIX_RTSP_DEBUG
	Log::debugOutputA("[RTSPConnection::sendSetup] Send Message: %s\n", packet.c_str());
#endif
	return rtspSocket_.write(packet.c_str(), packet.length());
}

bool Radix::Network::RTSPConnection::sendPlay() {
	if (!rtspSocket_.isConnected())
		return false;

	std::string packet = packetAgent_.createPlayPacket(url_.c_str(), sessionName_.c_str());
#ifdef _RADIX_RTSP_DEBUG
	Log::debugOutputA("[RTSPConnection::sendPlay] Send Message: %s\n", packet.c_str());
#endif
	return rtspSocket_.write(packet.c_str(), packet.length());
}

bool Radix::Network::RTSPConnection::sendTeardown() {
	if (!rtspSocket_.isConnected())
		return false;

	std::string packet = packetAgent_.createTeardownPacket(url_.c_str(), sessionName_.c_str());
	return rtspSocket_.write(packet.c_str(), packet.length());
}

void Radix::Network::RTSPConnection::onRecvMediaStream(const char* media, int size, RTSPCodec codec, unsigned int timestamp) {
	if (listener_ != NULL)
		listener_->onRecvMediaStream(connectionId_, media, size, codec, timestamp);
}

void Radix::Network::RTSPConnection::startLiveVideo(bool useRtpTcp) {
	clearSdp();
	useRtpTcp_ = useRtpTcp;
	writeCmdQueue_.push((int)COMMAND_DESCRIBE);
}

void Radix::Network::RTSPConnection::stopLiveVideo() {
	writeCmdQueue_.push((int)COMMAND_TEARDOWN);
}
