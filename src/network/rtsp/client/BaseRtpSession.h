#pragma once

#include "network/NetworkDef.h"
#include "network/rtp/BaseRtpTo.h"
#include "network/rtsp/RTSPDef.h"

using Radix::Network::BaseRtpTo;

namespace Radix {
	namespace Network {
		struct IRtpSession {
			virtual void onRecvMediaStream(const char* media, int size, RTSPCodec codec, unsigned int timestamp) = 0;
		};

		class BaseRtpSession {
		private:
			BaseRtpTo* rtpConverter_;
			IRtpSession* listener_;

			BaseRtpTo* createRtpConverter(int payloadType);

		protected:
			void processRtpPacket(const char* packet, unsigned int packetSize);

		public:
			BaseRtpSession();
			virtual ~BaseRtpSession();

		public:
			void setListener(IRtpSession* listener);
			virtual SOCKET rtpSocket() { return INVALID_SOCKET; }
			virtual void processRtpRecv() {}
		};
	}
}

