#pragma once

#include <string>
#include <vector>
#include "network/TCPSocket.h"
#include "network/SocketSelector.h"
#include "collection/SafeQueue.h"
#include "RTSPPacketAgent.h"
#include "BaseRtpSession.h"
#include "RTCPSession.h"

namespace Radix {
	namespace Network {
		struct IRTSPConnection {
			virtual void onCommandResult(int connectionId, RTSPCommand command, bool ret, char* param) = 0;
			virtual void onRecvMediaStream(int connectionId, const char* media, int size, RTSPCodec codec, unsigned int timestamp) = 0;
		};

		class RTSPConnection: public ITCPSocket, IRtpSession
		{
		private:
			int connectionId_;
			std::string ip_;
			int port_;
			std::string abs_path_;
			std::string url_;
			TCPSocket rtspSocket_;
			Radix::Collection::SafeQueue<int> writeCmdQueue_;
			RTSPPacketAgent packetAgent_;
			IRTSPConnection* listener_;
			std::vector<Radix::Network::SDPInformation*> sdpVec_;
			int rtpPort_;
			int rtcpPort_;
			std::string sessionName_;
			bool useRtpTcp_;
			BaseRtpSession* rtpSession_;
			RTCPSession rtcpSession_;

			virtual void onTCPSocketConnection(bool ret);
			void clearSdp();
			void createSdpInformation(const char* sdp);
			std::string sdpinfoJson();
			bool processRtspRecv();
			bool sendDescribe();
			bool sendSetup(int sdpIndex);
			bool sendPlay();
			bool sendTeardown();

			virtual void onRecvMediaStream(const char* media, int size, RTSPCodec codec, unsigned int timestamp);

		public:
			RTSPConnection(int connectionId, const char* ip, int port, const char* abs_path);
			~RTSPConnection();

		public:
			void setListener(IRTSPConnection* listener);
			bool isSameConnectionId(int connectionId);
			void addToSelector(SocketSelector* selector);
			bool isSetSocket(SocketSelector* selector);
			TCPSocket* rtspSocket();
			bool turnToWrite();
			bool turnToRead(SocketSelector* selector);
			void startLiveVideo(bool useRtpTcp);
			void stopLiveVideo();
		};
	}
}
