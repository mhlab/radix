#include "StdAfx.h"
#include "UdpRtpSession.h"

Radix::Network::UdpRtpSession::UdpRtpSession():
	BaseRtpSession(),
	udpSocket_(INVALID_SOCKET) {
}

Radix::Network::UdpRtpSession::~UdpRtpSession() {
	if (udpSocket_ != INVALID_SOCKET) {
#ifdef _WIN32
		closesocket(udpSocket_);
#else
		close(udpSocket_);
#endif
	}
}

bool Radix::Network::UdpRtpSession::open(int port) {
	try {
		if (port <= 0 || port > 65535)
			return false;

		udpSocket_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (udpSocket_ == INVALID_SOCKET)
			return false;

		sockaddr_in addr = {0, };
		addr.sin_family = PF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr.s_addr = htonl(INADDR_ANY);
		if (bind(udpSocket_, (sockaddr*)&addr, sizeof(addr)) != 0)
			return false;

		return true;
	}
	catch(...) {
	}

	return false;
}

SOCKET Radix::Network::UdpRtpSession::rtpSocket() {
	return udpSocket_;
}

void Radix::Network::UdpRtpSession::processRtpRecv() {
	if (udpSocket_ == INVALID_SOCKET)
		return;

	sockaddr_in addr = {0, };
	int addrlen = sizeof(addr);
	char buf[1500];
	unsigned int recvsize = recvfrom(udpSocket_, buf, 1500, 0, (sockaddr*)&addr, (socklen_t*)&addrlen);
	if (recvsize > 0)
		processRtpPacket(buf, recvsize);
}
