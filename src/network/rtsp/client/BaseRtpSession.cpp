#include "StdAfx.h"
#include "BaseRTPSession.h"
#include "network/rtp/RTPDef.h"
#include "network/rtp/RtpToH264Stream.h"
#include <ctime>

Radix::Network::BaseRtpSession::BaseRtpSession():
	rtpConverter_(NULL),
	listener_(NULL) {
}

Radix::Network::BaseRtpSession::~BaseRtpSession() {
}

void Radix::Network::BaseRtpSession::setListener(IRtpSession* listener) {
	listener_ = listener;
}

BaseRtpTo* Radix::Network::BaseRtpSession::createRtpConverter(int payloadType) {
	switch (payloadType) {
	case 96: return new RtpToH264Stream(payloadType);
	default: return NULL;
	}
}

void Radix::Network::BaseRtpSession::processRtpPacket(const char* packet, unsigned int packetSize) {
	if (packet == NULL || packetSize < RTPHEADER_SIZE)
		return;

	RTPHeader* rtpHeader = (RTPHeader*)packet;
	if (rtpConverter_ == NULL) {
		rtpConverter_ = createRtpConverter(rtpHeader->payloadType);
		if (rtpConverter_ == NULL)
			return;
	}

	rtpConverter_->putRtp(packet + RTPHEADER_SIZE, packetSize - RTPHEADER_SIZE, (unsigned int)std::time(NULL));
	RtpStream rtpStream;
	if (!rtpConverter_->read(rtpStream))
		return;

	if (listener_ != NULL)
		listener_->onRecvMediaStream(rtpStream.data_, rtpStream.size_, (RTSPCodec)rtpConverter_->payloadType(), rtpStream.timestamp_);
}
