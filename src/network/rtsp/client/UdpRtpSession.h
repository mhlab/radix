#pragma once

#include "BaseRtpSession.h"
#include "network/NetworkDef.h"

namespace Radix {
	namespace Network {
		class UdpRtpSession: public BaseRtpSession {
		private:
			SOCKET udpSocket_;

		public:
			UdpRtpSession();
			virtual ~UdpRtpSession();

		public:
			bool open(int port);
			virtual SOCKET rtpSocket();
			virtual void processRtpRecv();
		};
	}
}

