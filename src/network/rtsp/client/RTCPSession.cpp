#include "stdafx.h"
#include "RTCPSession.h"

Radix::Network::RTCPSession::RTCPSession():
	socket_(INVALID_SOCKET) {
}

Radix::Network::RTCPSession::~RTCPSession() {
	if (socket_ != INVALID_SOCKET) {
#ifdef _WIN32
		closesocket(socket_);
#else
		close(socket_);
#endif
	}
}

bool Radix::Network::RTCPSession::open(int port) {
	try {
		if (port <= 0 || port > 65535)
			return false;

		socket_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (socket_ == INVALID_SOCKET)
			return false;

		sockaddr_in addr = { 0, };
		addr.sin_family = PF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr.s_addr = htonl(INADDR_ANY);
		if (bind(socket_, (sockaddr*)&addr, sizeof(addr)) != 0)
			return false;

		return true;
	}
	catch (...) {
	}

	return false;
}

bool Radix::Network::RTCPSession::isOpen() {
	return (socket_ != INVALID_SOCKET);
}

SOCKET Radix::Network::RTCPSession::rtcpSocket() {
	return socket_;
}

bool Radix::Network::RTCPSession::readRtcpPacket() {
	if (socket_ == INVALID_SOCKET)
		return false;

	sockaddr_in addr = { 0, };
	int addrlen = sizeof(addr);
	char buf[1500];
	packetHeader_.packetLength = 0;
	unsigned int recvsize = recvfrom(socket_, buf, 1500, 0, (sockaddr*)&addr, (socklen_t*)&addrlen);
	if (recvsize < 0)
		return false;

	return true;
}