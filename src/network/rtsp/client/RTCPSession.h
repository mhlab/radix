#pragma once

#include "network/NetworkDef.h"
#include "memory/MemoryBuffer.h"

namespace Radix {
	namespace Network {
		struct RTCPPacketHeader {
			unsigned char reportCount : 5;
			unsigned char padding : 1;
			unsigned char version : 2;
			unsigned char payloadType : 8;
			int packetLength : 16;
		};

		class RTCPSession {
		private:
			SOCKET socket_;
			RTCPPacketHeader packetHeader_;
			Memory::MemoryBuffer packetContent_;

		public:
			RTCPSession();
			~RTCPSession();

		public:
			bool open(int port);
			bool isOpen();
			SOCKET rtcpSocket();
			bool readRtcpPacket();
		};
	}
}
