#include "StdAfx.h"
#include "RTSPClient.h"
#include "network/SocketSelector.h"
#include "string/StringEx.h"
#include "log/Log.h"

Radix::Network::RTSPClient::RTSPClient():
	commandRetCallback_(NULL),
	recvStreamCallback_(NULL) {
	rtspThread_ = new ThreadRunner(this);
}

Radix::Network::RTSPClient::~RTSPClient() {
	delete rtspThread_;
	for (auto it = connectionVec_.begin(); it != connectionVec_.end(); it++)
		delete *it;
}

Radix::Network::RTSPConnection* Radix::Network::RTSPClient::findConnection(int connectionId) {
	for (auto it = connectionVec_.begin(); it != connectionVec_.end(); it++) {
		if ((*it)->isSameConnectionId(connectionId))
			return *it;
	}

	return NULL;
}

void Radix::Network::RTSPClient::onThreadFunc(ThreadRunner* threadRunner, Signal& signal, bool& terminate) {
	SocketSelector selector;
	bool ret;
	while (!terminate) {
		if (connectionVec_.size() == 0) {
			ThreadRunner::sleep(1);
			continue;
		}
		selector.initialize();
		auto it = connectionVec_.begin();
		while (it != connectionVec_.end()) {
			ret = (*it)->turnToWrite();
			if (!ret)
				it = connectionVec_.erase(it);
			else {
				(*it)->addToSelector(&selector);
				it++;
			}
		}
		selector.selectEx(1);
		it = connectionVec_.begin();
		while (it != connectionVec_.end()) {
			if (!(*it)->isSetSocket(&selector)) { 
				it++;
				continue;
			}
			ret = (*it)->turnToRead(&selector);
			if (!ret)
				it = connectionVec_.erase(it);
			else
				it++;
		}
	}
}

void Radix::Network::RTSPClient::onCommandResult(int connectionId, RTSPCommand command, bool ret, char* param) {
	if (commandRetCallback_ != NULL)
		commandRetCallback_(connectionId, command, ret, param);
}

void Radix::Network::RTSPClient::onRecvMediaStream(int connectionId, const char* media, int size, RTSPCodec codec, unsigned int timestamp) {
	if (recvStreamCallback_ != NULL)
		recvStreamCallback_(connectionId, media, size, codec, timestamp);
}

void Radix::Network::RTSPClient::setListener(FOnCommandResult commandRet, FOnRecvMediaStream recvStream) {
	commandRetCallback_ = commandRet;
	recvStreamCallback_ = recvStream;
}

void Radix::Network::RTSPClient::startLiveVideo(int connectionId, const char* ip, int port, const char* abs_path, bool useTCP) {
	RTSPConnection* connection = findConnection(connectionId);
	if (connection != NULL) {
		connection->startLiveVideo(useTCP);
		return;
	}

	connection = new RTSPConnection(connectionId, ip, port, abs_path);
	connection->setListener(this);
	connection->startLiveVideo(useTCP);
	connectionVec_.push_back(connection);
}

void Radix::Network::RTSPClient::stopLiveVideo(int connectionId) {
	RTSPConnection* connection = findConnection(connectionId);
	if (connection == NULL)
		return;

	connection->stopLiveVideo();
}