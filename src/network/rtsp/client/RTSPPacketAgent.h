#pragma once

#include <string>
#include <map>
#include "network/rtsp/RTSPDef.h"

namespace Radix {
	namespace Network {
		class RTSPPacketAgent {
		private:
			static int cseq_;
			RTSPCommand lastCommand_;
			std::map<std::string, std::string> headers_;

			void createDefaultPacket(std::string& packet, Radix::Network::RTSPCommand command, const char* url);

		public:
			RTSPPacketAgent();
			~RTSPPacketAgent();

		public:
			RTSPCommand lastCommand();
			std::string createDescribePacket(const char* url, const char* id, const char* pass);
			std::string createSetupPacket(const char* url, int rtpPort);
			std::string createPlayPacket(const char* url, const char* sessionName);
			std::string createTeardownPacket(const char* url, const char* sessionName);
			bool parseHeader(const char* msg);
			int contentLength();
			std::string sessionName();
		};
	}
}

