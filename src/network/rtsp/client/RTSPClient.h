#pragma once

#include <vector>
#include "RTSPConnection.h"
#include "thread/ThreadRunner.h"

using Radix::Thread::ThreadRunner;
using Radix::Thread::Signal;

namespace Radix {
	namespace Network {
		typedef void (*FOnCommandResult)(int connectionId, RTSPCommand command, bool ret, char* param);
		typedef void (*FOnRecvMediaStream)(int connectionId, const char* media, int size, RTSPCodec codec, unsigned int timestamp);

		class RTSPClient: public Thread::IThreadRunner, IRTSPConnection {
		private:
			std::vector<RTSPConnection*> connectionVec_;
			ThreadRunner* rtspThread_;
			FOnCommandResult commandRetCallback_;
			FOnRecvMediaStream recvStreamCallback_;

			RTSPConnection* findConnection(int connectionId);
			virtual void onThreadFunc(ThreadRunner* threadRunner, Signal& signal, bool& terminate);
			virtual void onCommandResult(int connectionId, RTSPCommand command, bool ret, char* param);
			virtual void onRecvMediaStream(int connectionId, const char* media, int size, RTSPCodec codec, unsigned int timestamp);

		public:
			RTSPClient();
			~RTSPClient();
			
		public:
			void setListener(FOnCommandResult commandRet, FOnRecvMediaStream recvStream);
			void startLiveVideo(int connectionId, const char* ip, int port, const char* abs_path, bool useTCP);
			void stopLiveVideo(int connectionId);
		};
	}
}

