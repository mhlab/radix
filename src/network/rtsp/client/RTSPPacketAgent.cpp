#include "StdAfx.h"
#include "RTSPPacketAgent.h"
#include "string/StringEx.h"
#include <cstdlib>
#include <locale>

int Radix::Network::RTSPPacketAgent::cseq_ = 0;

Radix::Network::RTSPPacketAgent::RTSPPacketAgent():
	lastCommand_(COMMAND_UNKNOWN) {
}

Radix::Network::RTSPPacketAgent::~RTSPPacketAgent() {
}

Radix::Network::RTSPCommand Radix::Network::RTSPPacketAgent::lastCommand() {
	return lastCommand_;
}

void Radix::Network::RTSPPacketAgent::createDefaultPacket(std::string& packet, Radix::Network::RTSPCommand command, const char* url) {
	cseq_++;
	packet = Radix::String::format("%s %s RTSP/1.0\r\n", RTSPCommandToString(command).c_str(), url);
	packet.append(Radix::String::format("CSeq: %d\r\n", cseq_));
	packet.append("User-Agent: Radix Media Streaming Agent\r\n");
}

std::string Radix::Network::RTSPPacketAgent::createDescribePacket(const char* url, const char* id, const char* pass) {
	std::string packet = "";
	createDefaultPacket(packet, COMMAND_DESCRIBE, url);
	packet.append("Accept: application/sdp\r\n\r\n");
	lastCommand_ = COMMAND_DESCRIBE;

	return packet;
}

std::string Radix::Network::RTSPPacketAgent::createSetupPacket(const char* url, int rtpPort) {
	std::string packet = "";
	createDefaultPacket(packet, COMMAND_SETUP, url);
	packet.append(Radix::String::format("Transport: RTP/AVP;unicast;client_port=%d-%d\r\n\r\n", rtpPort, rtpPort + 1));
	lastCommand_ = COMMAND_SETUP;
	
	return packet;
}

std::string Radix::Network::RTSPPacketAgent::createPlayPacket(const char* url, const char* sessionName) {
	std::string packet = "";
	createDefaultPacket(packet, COMMAND_PLAY, url);
	packet.append(Radix::String::format("Session: %s\r\n\r\n", sessionName));
	lastCommand_ = COMMAND_PLAY;

	return packet;
}

std::string Radix::Network::RTSPPacketAgent::createTeardownPacket(const char* url, const char* sessionName) {
	std::string packet = "";
	createDefaultPacket(packet, COMMAND_TEARDOWN, url);
	packet.append(Radix::String::format("Session: %s\r\n\r\n", sessionName));
	lastCommand_ = COMMAND_TEARDOWN;

	return packet;

}

bool Radix::Network::RTSPPacketAgent::parseHeader(const char* msg) {
	try {
		std::string header = msg;
		std::string response = Radix::String::seperate(header, "\r\n");
		std::vector<std::string> responseTokens;
		Radix::String::tokenize(response.c_str(), " ", responseTokens);
		if (responseTokens.size() < 3)
			return false;

		int code = atoi(responseTokens.at(1).c_str());
		if (code != 200)
			return false;

		std::vector<std::string> headerTokens;
		std::vector<std::string> fieldTokens;
		Radix::String::tokenize(header.c_str(), "\r\n", headerTokens);
		for (auto it = headerTokens.begin(); it != headerTokens.end(); it++) {
			Radix::String::tokenize(it->c_str(), ":", fieldTokens);
			if (fieldTokens.size() < 2)
				return false;
			headers_[String::toLower(fieldTokens.at(0))] = fieldTokens.at(1);
		}

		auto it = headers_.find("cseq");
		if (it == headers_.end())
			return false;

		int seq = atoi(it->second.c_str());
		if (seq != cseq_)
			return false;

		return true;
	}
	catch(...) {
	}

	return false;
}

int Radix::Network::RTSPPacketAgent::contentLength() {
	try {
		auto it = headers_.find("content-length");
		if (it == headers_.end())
			return 0;

		return atoi(it->second.c_str());
	}
	catch(...) {
	}

	return 0;
}

std::string Radix::Network::RTSPPacketAgent::sessionName() {
	auto it = headers_.find("session");
	if (it == headers_.end())
		return "";

	std::string sessionName = it->second;
	unsigned int index = sessionName.find(";");
	if (index == std::string::npos)
		return sessionName;

    sessionName = sessionName.substr(0, index);
	return Radix::String::trim(sessionName);
}
