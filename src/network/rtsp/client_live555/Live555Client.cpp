#include "stdafx.h"
#include "Live555Client.h"
#include "string/StringEx.h"
#ifdef _RADIX_RTSP_DEBUG
#include "log/Log.h"
#endif

#define MEDIUM_CLOSE(x) {if (x != NULL) {Medium::close(x); x = NULL;}}

Radix::Network::Live555Client::Live555Client(UsageEnvironment& env, const char* url, int connectionId) :
	RTSPClient(env, url, 1, NULL, 0, -1),
	connectionId_(connectionId),
	listener_(NULL),
	session_(NULL),
	videoSession_(NULL), 
	audioSession_(NULL),
	allowVideoCallback_(false), 
	allowAudioCallback_(false),
	sendSetupCommand_(false) {
	responseBufferSize = 2000000;
}

Radix::Network::Live555Client::~Live555Client() {
	sendTearDown();
	MEDIUM_CLOSE(session_);
}

void Radix::Network::Live555Client::setListener(Live555ClientListener* listener) {
	listener_ = listener;
}

void Radix::Network::Live555Client::call_onCommandResult(RTSPCommand command, bool result, char* param) {
	if (listener_ != NULL)
		listener_->onCommandResult(connectionId_, command, result, param);
}

std::string Radix::Network::Live555Client::subSessionToJson(MediaSubsession* subSession) {
	if (subSession == NULL)
		return "";

	return Radix::String::format("{\"codec\": %d, \"video_width\": %d, \"video_height\": %d}", (int)live555CodecToRtspCodec(subSession->codecName()), 
			subSession->videoWidth(), subSession->videoHeight());
}

void Radix::Network::Live555Client::setAccount(const char* userId, const char* userPass) {
	auth_.setUsernameAndPassword(userId, userPass);
}

void Radix::Network::Live555Client::startLiveVideo() {
	if (session_ == NULL)
		sendDescribe(true);
	else
		sendSetup();
	allowVideoCallback_ = true;
}

void Radix::Network::Live555Client::stopLiveVideo() {
	allowVideoCallback_ = false;
	if (!allowAudioCallback_) {
		sendTearDown();
	}
}

void Radix::Network::Live555Client::sendDescribe(bool sendSetupCommand) {
	MEDIUM_CLOSE(session_);
	sendSetupCommand_ = sendSetupCommand;
	sendDescribeCommand(recvfromDescribe, &auth_);
}

void Radix::Network::Live555Client::recvfromDescribe(RTSPClient* rtspClient, int resultCode, char* resultString) {
	Live555Client* client = (Live555Client*)rtspClient;
	try {
		if (resultCode != 0)
			throw 0;
		client->session_ = MediaSession::createNew(rtspClient->envir(), (const char*)resultString);
		if (!client->session_->hasSubsessions()) {
			MEDIUM_CLOSE(client->session_);
			throw 1;
		}
		MediaSubsessionIterator* iter = new MediaSubsessionIterator(*(client->session_));
		MediaSubsession* subSession = iter->next();
		std::string sdpjson = "[";
		while (subSession != NULL)
		{
			if (strcmp(subSession->mediumName(), "video") == 0) {
				client->videoSession_ = subSession;
				sdpjson.append(client->subSessionToJson(subSession));
				sdpjson.append(",");
			}
			else if (strcmp(subSession->mediumName(), "audio") == 0) {
				client->audioSession_ = subSession;
				sdpjson.append(client->subSessionToJson(subSession));
				sdpjson.append(",");
			}
			subSession = iter->next();
		}
		sdpjson.erase(sdpjson.length() - 1, 1);
		sdpjson.append("]");
		if (client->videoSession_ == NULL && client->audioSession_ == NULL)
			throw 2;
		if (client->sendSetupCommand_)
			client->sendSetup();
		client->call_onCommandResult(COMMAND_DESCRIBE, true, (char*)sdpjson.c_str());
	}
	catch(...) {
		if (resultCode != 0)
			client->call_onCommandResult(COMMAND_DESCRIBE, false, NULL);
	}
	if (resultString != NULL)
		delete[] resultString;
}

void Radix::Network::Live555Client::sendSetup() {
	if (videoSession_ != NULL)
		sendSetupVideo();
	else if (audioSession_ != NULL)
		sendSetupAudio();
}

void Radix::Network::Live555Client::sendSetupVideo() {
	if (videoSession_ == NULL && audioSession_ == NULL) {
		call_onCommandResult(COMMAND_SETUP, false, NULL);
		return;
	}
	if (!videoSession_->initiate() && audioSession_ == NULL) {
		call_onCommandResult(COMMAND_SETUP, false, NULL);
		return;
	}
	bool streamUsingTcp = false;
	sendSetupCommand(*videoSession_, recvfromSetupVideo, false, streamUsingTcp);
}

void Radix::Network::Live555Client::recvfromSetupVideo(RTSPClient* rtspClient, int resultCode, char* resultString) {
	Live555Client* client = (Live555Client*)rtspClient;
	if (resultCode != 0) {
		if (client->audioSession_ == NULL) {
			client->call_onCommandResult(COMMAND_SETUP, false, NULL);
			return;
		}
	}
	client->videoSession_->sink = new Live555Sink(rtspClient->envir(), client->videoSession_, client);
	client->videoSession_->sink->startPlaying(*(client->videoSession_->readSource()), videoSubSessionAfterPlaying, client);
	if (client->videoSession_->rtcpInstance() != NULL)
		client->videoSession_->rtcpInstance()->setByeHandler(videoSubSessionByeHandler, client);
	if (client->audioSession_ == NULL) {
		client->call_onCommandResult(COMMAND_SETUP, true, NULL);
		client->sendPlay();
	}
	else
		client->sendSetupAudio();
}

void Radix::Network::Live555Client::sendSetupAudio() {
	if (videoSession_ == NULL && audioSession_ == NULL) {
		call_onCommandResult(COMMAND_SETUP, false, NULL);
		return;
	}
	if (!audioSession_->initiate() && videoSession_ == NULL) {
		call_onCommandResult(COMMAND_SETUP, false, NULL);
		return;
	}
	bool streamUsingTcp = false;
	sendSetupCommand(*audioSession_, recvfromSetupAudio, false, streamUsingTcp);
}

void Radix::Network::Live555Client::recvfromSetupAudio(RTSPClient* rtspClient, int resultCode, char* resultString) {
	Live555Client* client = (Live555Client*)rtspClient;
	if (resultCode != 0) {
		client->call_onCommandResult(COMMAND_SETUP, false, NULL);
		return;
	}
	client->audioSession_->sink = new Live555Sink(rtspClient->envir(), client->audioSession_, client);
	client->audioSession_->sink->startPlaying(*(client->audioSession_->readSource()), audioSubSessionAfterPlaying, client);
	if (client->audioSession_->rtcpInstance() != NULL)
		client->audioSession_->rtcpInstance()->setByeHandler(audioSubSessionByeHandler, client);
	client->call_onCommandResult(COMMAND_SETUP, true, NULL);
	client->sendPlay();
}

void Radix::Network::Live555Client::videoSubSessionAfterPlaying(void* clientData) {
	Live555Client* client = (Live555Client*)clientData;
	MEDIUM_CLOSE(client->videoSession_->sink);
}

void Radix::Network::Live555Client::audioSubSessionAfterPlaying(void* clientData) {
	Live555Client* client = (Live555Client*)clientData;
	MEDIUM_CLOSE(client->audioSession_->sink);
}

void Radix::Network::Live555Client::videoSubSessionByeHandler(void* clientData) {
}

void Radix::Network::Live555Client::audioSubSessionByeHandler(void* clientData) {
}

void Radix::Network::Live555Client::sendPlay() {
	sendPlayCommand(*session_, recvfromPLAY);
}

void Radix::Network::Live555Client::recvfromPLAY(RTSPClient* rtspClient, int resultCode, char* resultString) {
	Live555Client* client = (Live555Client*)rtspClient;
	if (resultCode != 0) {
		client->call_onCommandResult(COMMAND_PLAY, false, NULL);
		return;
	}

	client->call_onCommandResult(COMMAND_PLAY, true, NULL);
}

void Radix::Network::Live555Client::sendTearDown() {
	if (videoSession_ != NULL) {
		MEDIUM_CLOSE(videoSession_->sink);
		if (videoSession_->rtcpInstance() != NULL)
			videoSession_->rtcpInstance()->setByeHandler(NULL, NULL);
	}
	if (audioSession_ != NULL) {
		MEDIUM_CLOSE(audioSession_->sink);
		if (audioSession_->rtcpInstance() != NULL)
			audioSession_->rtcpInstance()->setByeHandler(NULL, NULL);
	}
	if (session_ != NULL)
		sendTeardownCommand(*session_, NULL);
	call_onCommandResult(COMMAND_TEARDOWN, true, NULL);
	videoSession_ = NULL;
	audioSession_ = NULL;
}

void Radix::Network::Live555Client::onRecvMediaStream(RTSPCodec codec, char* media, int mediaSize, int64_t presentationTime) {
#ifdef _RADIX_RTSP_DEBUG
	Log::debugOutputA("[Live555Client::onRecvMediaStream] codec: %d, mediaSize: %d, presentationTime: %d\n", codec, mediaSize, presentationTime);
	Log::debugOutputA("[Live555Client::onRecvMediaStream] %x-%x-%x-%x-%x\n", (char)*media, (char)*(media + 1), (char)*(media + 2), (char)*(media + 3), (char)*(media + 4));
#endif
	bool isVideo = isVideoCodec(codec);
	if (allowVideoCallback_ && !isVideo)
		return;
	if (allowAudioCallback_ && isVideo)
		return;
	if (listener_ != NULL)
		listener_->onRecvMediaStream(connectionId_, codec, media, mediaSize, presentationTime);
}
