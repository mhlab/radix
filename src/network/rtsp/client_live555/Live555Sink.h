#pragma once

#include "live555/MediaSink.hh"
#include "live555/MediaSession.hh"
#include "network/rtsp/RTSPDef.h"
#include "network/rtsp/MediaFrameAssembler.h"

namespace Radix {
	namespace Network {
		struct Live555SinkListener {
			virtual void onRecvMediaStream(RTSPCodec codec, char* media, int mediaSize, int64_t presentationTime) = 0;
		};

		class Live555Sink: public MediaSink {
		private:
			u_int8_t* buf_;
			u_int8_t* payloadInBuf_;
			int payloadSize_;
			MediaFrameAssembler* frameAssembler_;
			Live555SinkListener* listener_;
			RTSPCodec codec_;

		protected:
			virtual Boolean continuePlaying();
			static void afterGettingFrame(void* clientData, unsigned frameSize, unsigned numTruncatedBytes, struct timeval presentationTime,
				unsigned durationInMicroseconds);

		public:
			Live555Sink(UsageEnvironment& env, MediaSubsession* subSession, Live555SinkListener* listener);
			virtual ~Live555Sink();
		};
	}
}

