#pragma once

#include "live555/liveMedia.hh"
#include "network/rtsp/RTSPDef.h"
#include "Live555Sink.h"

namespace Radix {
	namespace Network {
		struct Live555ClientListener {
			virtual void onCommandResult(int streamNo, RTSPCommand command, bool cmdResult, char* param) = 0;
			virtual void onRecvMediaStream(int streamNo, RTSPCodec codec, char* media, int mediaSize, int64_t presentationTime) = 0;
		};

		class Live555Client: public RTSPClient, Live555SinkListener {
		private:
			Authenticator auth_;
			int connectionId_;
			Live555ClientListener* listener_;
			bool allowVideoCallback_;
			bool allowAudioCallback_;
			bool sendSetupCommand_;

			MediaSession* session_;
			MediaSubsession* videoSession_;
			MediaSubsession* audioSession_;

			void sendSetup();
			void sendSetupVideo();
			void sendSetupAudio();
			void sendPlay();
			void sendTearDown();

			void call_onCommandResult(RTSPCommand command, bool result, char* param);
			std::string subSessionToJson(MediaSubsession* subSession);
			static void recvfromDescribe(RTSPClient* rtspClient, int resultCode, char* resultString);
			static void recvfromSetupVideo(RTSPClient* rtspClient, int resultCode, char* resultString);
			static void recvfromSetupAudio(RTSPClient* rtspClient, int resultCode, char* resultString);
			static void recvfromPLAY(RTSPClient* rtspClient, int resultCode, char* resultString);

			static void videoSubSessionAfterPlaying(void* clientData);
			static void audioSubSessionAfterPlaying(void* clientData);
			static void videoSubSessionByeHandler(void* clientData);
			static void audioSubSessionByeHandler(void* clientData);

			virtual void onRecvMediaStream(RTSPCodec codec, char* media, int mediaSize, int64_t presentationTime);

		public:
			Live555Client(UsageEnvironment& env, const char* url, int connectionId);
			virtual ~Live555Client();

		public:
			void setListener(Live555ClientListener* listener);
			void setAccount(const char* userId, const char* userPass);
			void startLiveVideo();
			void stopLiveVideo();
			void sendDescribe(bool sendSetupCommand);
		};
	}
}

