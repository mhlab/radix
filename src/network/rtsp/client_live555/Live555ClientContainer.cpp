#include "stdafx.h"
#include "Live555ClientContainer.h"

Radix::Network::Live555ClientContainer::Live555ClientContainer():
	onCommandResult_(NULL), 
	onRecvMediaStream_(NULL) {
	scheduler_ = BasicTaskScheduler::createNew();
	env_ = BasicUsageEnvironment::createNew(*scheduler_);
	rtspThread_ = new Thread::ThreadRunner(this);
	rtspThread_->setThreadName("RTSP-Thread");
}

Radix::Network::Live555ClientContainer::~Live555ClientContainer() {
	for (auto it = clientMap_.begin(); it != clientMap_.end(); it++)
		Medium::close(it->second);
	rtspThreadSignal_ = 1;
	delete rtspThread_;
	env_->reclaim();
	env_ = NULL;
	delete scheduler_;
}

void Radix::Network::Live555ClientContainer::setListener(FOnCommandResultLive555Client onCommandResult, FOnRecvMediaStreamLive555Client onRecvMediaStream) {
	onCommandResult_ = onCommandResult;
	onRecvMediaStream_ = onRecvMediaStream;
}

void Radix::Network::Live555ClientContainer::onThreadFunc(Thread::ThreadRunner* threadRunner, Thread::Signal& signal, bool& terminate) {
	rtspThreadSignal_ = 0;
	env_->taskScheduler().doEventLoop(&rtspThreadSignal_);
}

void Radix::Network::Live555ClientContainer::startLiveVideo(int connectionId, const char* url, const char* id, const char* pass) {
	RTSPConnection* rtspConnection = new RTSPConnection();
	rtspConnection->url_ = url;
	if (id != NULL)
		rtspConnection->userId_ = id;
	if (pass != NULL)
		rtspConnection->userPass_ = pass;
	rtspConnection->connectionId_ = connectionId;
	rtspConnection->container_ = this;
	rtspConnection->triggerId_ = env_->taskScheduler().createEventTrigger(internalStartLiveVideo);
	env_->taskScheduler().triggerEvent(rtspConnection->triggerId_, rtspConnection);
}

void Radix::Network::Live555ClientContainer::internalStartLiveVideo(void* argu) {
	RTSPConnection* rtspConnection = (RTSPConnection*)argu;
	Live555ClientContainer* container = (Live555ClientContainer*)rtspConnection->container_;
	auto it = container->clientMap_.find(rtspConnection->connectionId_);
	Live555Client* client;
	if (it == container->clientMap_.end()) {
		client = new Live555Client(*container->env_, rtspConnection->url_.c_str(), rtspConnection->connectionId_);
		if (rtspConnection->userId_.length() > 0 && rtspConnection->userPass_.length() > 0)
			client->setAccount(rtspConnection->userId_.c_str(), rtspConnection->userPass_.c_str());
		client->setListener(container);
		container->clientMap_[rtspConnection->connectionId_] = client;
	}
	else
		client = it->second;
	client->startLiveVideo();

	delete rtspConnection;
}

void Radix::Network::Live555ClientContainer::stopLiveVideo(int connectionId) {
	TriggerInfo* triggerInfo = new TriggerInfo();
	triggerInfo->container_ = this;
	triggerInfo->connectionId_ = connectionId;
	triggerInfo->triggerId_ = env_->taskScheduler().createEventTrigger(internalStopLiveVideo);
	env_->taskScheduler().triggerEvent(triggerInfo->triggerId_, triggerInfo);
}

void Radix::Network::Live555ClientContainer::internalStopLiveVideo(void* argu) {
	TriggerInfo* triggerInfo = (TriggerInfo*)argu;
	Live555ClientContainer* container = (Live555ClientContainer*)triggerInfo->container_;
	auto it = container->clientMap_.find(triggerInfo->connectionId_);
	if (it == container->clientMap_.end())
		return;
	it->second->stopLiveVideo();
}

void Radix::Network::Live555ClientContainer::sendDescribe(int connectionId, const char* url, const char* id, const char* pass) {
	RTSPConnection* rtspConnection = new RTSPConnection();
	rtspConnection->url_ = url;
	if (id != NULL)
		rtspConnection->userId_ = id;
	if (pass != NULL)
		rtspConnection->userPass_ = pass;
	rtspConnection->connectionId_ = connectionId;
	rtspConnection->container_ = this;
	rtspConnection->triggerId_ = env_->taskScheduler().createEventTrigger(internalSendDescribe);
	env_->taskScheduler().triggerEvent(rtspConnection->triggerId_, rtspConnection);
}

void Radix::Network::Live555ClientContainer::internalSendDescribe(void* argu) {
	RTSPConnection* rtspConnection = (RTSPConnection*)argu;
	Live555ClientContainer* container = (Live555ClientContainer*)rtspConnection->container_;
	auto it = container->clientMap_.find(rtspConnection->connectionId_);
	Live555Client* client;
	if (it == container->clientMap_.end()) {
		client = new Live555Client(*container->env_, rtspConnection->url_.c_str(), rtspConnection->connectionId_);
		if (rtspConnection->userId_.length() > 0 && rtspConnection->userPass_.length() > 0)
			client->setAccount(rtspConnection->userId_.c_str(), rtspConnection->userPass_.c_str());
		client->setListener(container);
		container->clientMap_[rtspConnection->connectionId_] = client;
	}
	else
		client = it->second;
	client->sendDescribe(false);

	delete rtspConnection;
}

void Radix::Network::Live555ClientContainer::onCommandResult(int streamNo, RTSPCommand command, bool cmdResult, char* param) {
	if (onCommandResult_ != NULL)
		onCommandResult_(streamNo, command, cmdResult, param);
}

void Radix::Network::Live555ClientContainer::onRecvMediaStream(int streamNo, RTSPCodec codec, char* media, int mediaSize, int64_t presentationTime) {
	if (onRecvMediaStream_ != NULL)
		onRecvMediaStream_(streamNo, media, mediaSize, codec, presentationTime);
}
