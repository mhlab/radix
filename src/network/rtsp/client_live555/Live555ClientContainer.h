#pragma once

#include "live555/BasicUsageEnvironment.hh"
#include "thread/ThreadRunner.h"
#include "Live555Client.h"
#include <map>

#ifdef _WIN32 
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "live555.lib")
#endif

namespace Radix {
	namespace Network {
		typedef void(*FOnCommandResultLive555Client)(int streamNo, RTSPCommand command, bool cmdResult, char* param);
		typedef void(*FOnRecvMediaStreamLive555Client)(int streamNo, const char* media, int mediaSize, RTSPCodec codec, int64_t presentationTime);

		class Live555ClientContainer: public Thread::IThreadRunner, Live555ClientListener {
		private:
			TaskScheduler* scheduler_;
			UsageEnvironment* env_;
			Thread::ThreadRunner* rtspThread_;
			char rtspThreadSignal_;
			std::map<int, Live555Client*> clientMap_;
			FOnCommandResultLive555Client onCommandResult_;
			FOnRecvMediaStreamLive555Client onRecvMediaStream_;

			virtual void onThreadFunc(Thread::ThreadRunner* threadRunner, Thread::Signal& signal, bool& terminate);
			static void internalStartLiveVideo(void* argu);
			static void internalStopLiveVideo(void* argu);
			static void internalSendDescribe(void* argu);

			virtual void onCommandResult(int streamNo, RTSPCommand command, bool cmdResult, char* param);
			virtual void onRecvMediaStream(int streamNo, RTSPCodec codec, char* media, int mediaSize, int64_t presentationTime);

		public:
			Live555ClientContainer();
			~Live555ClientContainer();

		public:
			void setListener(FOnCommandResultLive555Client onCommandResult, FOnRecvMediaStreamLive555Client onRecvMediaStream);
			void startLiveVideo(int connectionId, const char* url, const char* id, const char* pass);
			void stopLiveVideo(int connectionId);
			void sendDescribe(int connectionId, const char* url, const char* id, const char* pass);
		};

		struct TriggerInfo {
			EventTriggerId triggerId_;
			int connectionId_;
			Live555ClientContainer* container_;
		};

		struct RTSPConnection : TriggerInfo {
			std::string url_;
			std::string userId_;
			std::string userPass_;

			RTSPConnection() {
				userId_ = "";
				userPass_ = "";
			}
		};
	}
}
