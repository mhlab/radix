#include "stdafx.h"
#include "Live555Sink.h"
#include "live555/H264VideoRTPSource.hh"

//#define _DUMP_STREAM
#ifdef _DUMP_STREAM
FILE* dumpStream_ = NULL;
#endif

Radix::Network::Live555Sink::Live555Sink(UsageEnvironment& env, MediaSubsession* subSession, Live555SinkListener* listener) :
	MediaSink(env),
	listener_(listener) {
	payloadSize_ = 1000000;
	buf_ = new u_int8_t[payloadSize_];
	payloadInBuf_ = buf_;
	codec_ = live555CodecToRtspCodec(subSession->codecName());
	frameAssembler_ = MediaFrameAssembler::createFrameAssembler(codec_);
	if (codec_ == CODEC_H264) {
		char streamHeader[4] = { 0x00, 0x00, 0x00, 0x01 };
		int streamHeaderSize = sizeof(streamHeader);
		memcpy(buf_, streamHeader, streamHeaderSize);
		payloadInBuf_ += streamHeaderSize;
		payloadSize_ -= streamHeaderSize;

		unsigned int len;
		SPropRecord* prop = parseSPropParameterSets(subSession->fmtp_spropparametersets(), len);
		char* nalUnit;
		for (int i = 0; i < (int)len; i++)
		{
			nalUnit = new char[prop[i].sPropLength + streamHeaderSize];
			memcpy(nalUnit, streamHeader, streamHeaderSize);
			memcpy(nalUnit + streamHeaderSize, prop[i].sPropBytes, prop[i].sPropLength);
#ifdef _DUMP_STREAM
	if (dumpStream_ == NULL)
		dumpStream_ = fopen("DUMP_STREAM.dat", "wb");
	fwrite(nalUnit, prop[i].sPropLength + streamHeaderSize, 1, dumpStream_);
#endif
			frameAssembler_->put(nalUnit, prop[i].sPropLength + streamHeaderSize);
			delete[] nalUnit;
		}
		delete[] prop;
	}
}

Radix::Network::Live555Sink::~Live555Sink() {
#ifdef _DUMP_STREAM
	if (dumpStream_ != NULL) {
		fclose(dumpStream_);
		dumpStream_ = NULL;
	}
#endif
	delete[] buf_;
}

Boolean Radix::Network::Live555Sink::continuePlaying() {
	if (fSource == NULL)
		return false;

	fSource->getNextFrame(payloadInBuf_, payloadSize_, afterGettingFrame, this, onSourceClosure, this);
	return true;
}

void Radix::Network::Live555Sink::afterGettingFrame(void* clientData, unsigned frameSize, unsigned numTruncatedBytes, struct timeval presentationTime,
	unsigned durationInMicroseconds) {
	Live555Sink* sink = (Live555Sink*)clientData;
	int realFrameSize = frameSize - numTruncatedBytes + ((long)sink->payloadInBuf_ - (long)sink->buf_);
#ifdef _DUMP_STREAM
	if (dumpStream_ != NULL)
		fwrite(sink->buf_, realFrameSize, 1, dumpStream_);
#endif
	sink->frameAssembler_->put((char*)sink->buf_, realFrameSize);
	FrameInfo& frameInfo = sink->frameAssembler_->get();
	if (frameInfo.frame_ != NULL && frameInfo.size_ > 0) {
		if (sink->listener_ != NULL)
			sink->listener_->onRecvMediaStream(sink->codec_, frameInfo.frame_, frameInfo.size_, presentationTime.tv_sec * 1000 + presentationTime.tv_usec);
	}
	sink->continuePlaying();
}