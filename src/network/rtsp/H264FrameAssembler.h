#pragma once

#include "MediaFrameAssembler.h"
#include "memory/MemoryBuffer.h"

namespace Radix {
	namespace Network {
		class NalunitContext {
		private:
			Memory::MemoryBuffer context_;

			bool isStartMarker(char* pos);

		public:
			NalunitContext();
			~NalunitContext();

		public:
			int write(char* data, int dataSize);
			void reset();
			bool isReady();
			int getSize();
			char* getMemory();
			static int startCodeSize();
		};

		class H264FrameAssembler: public MediaFrameAssembler {
		private:
			enum H264NALTYPE
			{
				naltype_sps = 0x07,
				naltype_pps = 0x08,
				naltype_sei = 0x06,
				naltype_idr = 0x05,
				naltype_nonidr = 0x01
			};
			NalunitContext spsContext_;
			NalunitContext ppsContext_;
			Memory::MemoryBuffer fullFrame_;

		public:
			H264FrameAssembler();
			virtual ~H264FrameAssembler();

		public:
			virtual void put(const char* frame, int size);
		};
	}
}
