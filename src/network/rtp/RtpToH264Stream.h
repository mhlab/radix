#pragma once

#include "BaseRtpTo.h"

namespace Radix {
	namespace Network {
		class RtpToH264Stream: public BaseRtpTo {
		private:
			static int startCode_;
			static unsigned int startCodeSize_;
			unsigned int timestamp_;

		public:
			RtpToH264Stream(int payloadType);
			virtual ~RtpToH264Stream();

		public:
			virtual void putRtp(const char* rtpData, unsigned int size, unsigned int timestamp);
			virtual bool read(RtpStream& stream);
		};
	}
}

