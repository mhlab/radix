#pragma once

namespace Radix {
	namespace Network {
		struct RTPHeader
		{
			// 1Byte
			unsigned char csrcCount : 4;
			unsigned char extension : 1;
			unsigned char padding : 1;
			unsigned char version : 2;
			// 1Byte
			unsigned char payloadType : 7;
			unsigned char marker : 1;
			// 2Byte
			unsigned short seqNumber : 16;
			// 4Byte
			unsigned int timestamp : 32;
			// 4Byte
			unsigned int ssrcId : 32;
		};
		static unsigned int RTPHEADER_SIZE = sizeof(RTPHeader);

		/*
		f: forbidden_zero_bit.  A value of 0 indicates that the NAL unit type
	       octet and payload should not contain bit errors or other syntax
           violations.  A value of 1 indicates that the NAL unit type octet
           and payload may contain bit errors or other syntax violations.
		nri: nal_ref_idc.
		type: NAL unit types 
		*/
		struct H264FUIdentifier
		{
			unsigned char type : 5; 
			unsigned char nri : 2;
			unsigned char f : 1; 
		};
		static unsigned int H264FUIDENTIFIER_SIZE = sizeof(H264FUIdentifier);

		struct H264FUHeader
		{
			unsigned char type : 5;
			unsigned char forbiddenBit : 1;
			unsigned char endBit : 1;
			unsigned char startBit : 1;
		};
		static unsigned int H264FUHEADER_SIZE = sizeof(H264FUHeader);
	}
}