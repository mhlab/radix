#include "StdAfx.h"
#include "BaseRtpTo.h"

Radix::Network::BaseRtpTo::BaseRtpTo(int payloadType):
	payloadType_(payloadType) {
}

Radix::Network::BaseRtpTo::~BaseRtpTo() {
}

int Radix::Network::BaseRtpTo::payloadType() {
	return payloadType_;
}