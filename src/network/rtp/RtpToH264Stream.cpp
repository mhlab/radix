#include "StdAfx.h"
#include "RtpToH264Stream.h"
#include "RTPDef.h"
#include "media/MediaDef.h"
#include <cstring>

unsigned int Radix::Network::RtpToH264Stream::startCodeSize_ = 4;
int Radix::Network::RtpToH264Stream::startCode_ = 0x01000000;

Radix::Network::RtpToH264Stream::RtpToH264Stream(int payloadType):
	BaseRtpTo(payloadType),
	timestamp_(-1) {
}

Radix::Network::RtpToH264Stream::~RtpToH264Stream() {
}

void Radix::Network::RtpToH264Stream::putRtp(const char* rtpData, unsigned int size, unsigned int timestamp) {
	if (rtpData == NULL || size < H264FUHEADER_SIZE)
		return;
	H264FUIdentifier* identifier = (H264FUIdentifier*)rtpData;
	H264FUHeader* header = (H264FUHeader*)(rtpData + H264FUIDENTIFIER_SIZE);
	char* mediaData = (char*)(rtpData + H264FUIDENTIFIER_SIZE + H264FUHEADER_SIZE);
	unsigned int mediaSize = (size - (H264FUIDENTIFIER_SIZE + H264FUHEADER_SIZE));
	H264FUIdentifier nalType;
	nalType.f = identifier->f;
	nalType.nri = identifier->nri;
	nalType.type = header->type;
	if (identifier->f != 0)
		return;
	if (identifier->type == Media::H264CODEC_NALTYPE_SPS) {
		timestamp_ = -1;
		buffer_.setPosition(0);
		buffer_.write((char*)&startCode_, startCodeSize_);
		buffer_.write(rtpData, size);
	}
	else if (identifier->type == Media::H264CODEC_NALTYPE_PPS) {
		buffer_.write((char*)&startCode_, startCodeSize_);
		buffer_.write(rtpData, size);
	}
	else if (identifier->type == Media::H264CODEC_NALTYPE_FRAGMENTA && (nalType.type == Media::H264CODEC_NALTYPE_IDR || nalType.type == Media::H264CODEC_NALTYPE_NONIDR)) {
		if (header->startBit == 1) {
			timestamp_ = -1;
			buffer_.setPosition(0);
			buffer_.write((char*)&startCode_, startCodeSize_);
			buffer_.write((const char*)&nalType, H264FUIDENTIFIER_SIZE);
		}
		buffer_.write(mediaData, mediaSize);
		if (header->endBit == 1)
			timestamp_ = timestamp;
	}
	else if (identifier->type > 0 && identifier->type < 24 && identifier->type != Media::H264CODEC_NALTYPE_SEI) {
		timestamp_ = -1;
		buffer_.setPosition(0);
		buffer_.write((char*)&startCode_, startCodeSize_);
		H264FUIdentifier nalType;
		nalType.f = identifier->f;
		nalType.nri = identifier->nri;
		nalType.type = header->type;
		buffer_.write((const char*)&nalType, H264FUIDENTIFIER_SIZE);
		buffer_.write(mediaData, mediaSize);
		timestamp_ = timestamp;
	}
}

bool Radix::Network::RtpToH264Stream::read(RtpStream& stream) {
	if (timestamp_ == -1)
		return false;

	stream.data_ = buffer_.memory();
	stream.size_ = buffer_.getPosition();
	stream.timestamp_ = timestamp_;

	return true;
}