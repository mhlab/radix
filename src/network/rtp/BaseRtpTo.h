#pragma once

#ifdef _WIN32
#include <Windows.h>
#endif
#include "memory/MemoryBuffer.h"

namespace Radix {
	namespace Network {
		struct RtpStream {
			const char* data_;
			unsigned int size_;
			unsigned int timestamp_;
		};

		class BaseRtpTo	{
		private:
			int payloadType_;

		protected:
			Memory::MemoryBuffer buffer_;

		public:
			BaseRtpTo(int payloadType);
			virtual ~BaseRtpTo();

		public:
			virtual void putRtp(const char* rtpData, unsigned int size, unsigned int timestamp) = 0;
			virtual bool read(RtpStream& stream) { return false; }
			int payloadType();
		};
	}
}

