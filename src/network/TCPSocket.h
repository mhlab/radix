#pragma once

#include <string>
#include <sstream>
#include "NetworkDef.h"
#include "thread/ThreadRunner.h"

using Radix::Thread::ThreadRunner;
using Radix::Thread::Signal;

namespace Radix {
	namespace Network {
		struct ITCPSocket {
			virtual void onTCPSocketConnection(bool ret) = 0;
		};

		class TCPSocket: public Radix::Thread::IThreadRunner {
		private:
			std::string ip_;
			int port_;
			int connectionTimeout_;
			Radix::Thread::ThreadRunner* connectThread_;
			SOCKET socket_;
			ITCPSocket* listener_;

			int lastError();
			bool isWouldBlock();
			virtual void onThreadFunc(ThreadRunner* threadRunner, Signal& signal, bool& terminate);

		public:
			TCPSocket();
			~TCPSocket();

		public:
			void setListener(ITCPSocket* listener);
			void connectAsync(const char* ip, int port, int connectionTimeout);
			void disconnect();
			void setAsyncMode(bool value);
			bool isConnected();
			bool write(const char* data, int dataSize);
			bool read(char* data, int dataSize);
			bool read(std::stringstream& buf, const char* lastChar, int lastCharLen);

			friend class SocketSelector;
		};
	}
}

