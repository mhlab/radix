#pragma once

#include "TCPSocket.h"

namespace Radix {
	namespace Network {
		class SocketSelector {
		private:
			SOCKET maxFd_;
#ifdef _WIN32
			FD_SET readSet_;
#else
			fd_set readSet_;
#endif

		public:
			SocketSelector();
			~SocketSelector();

		public:
			void initialize();
			void addSocket(TCPSocket* socket);
			void addSocket(SOCKET socket);
			bool selectEx(int timeout);
			bool isSet(TCPSocket* socket);
			bool isSet(SOCKET socket);
		};
	}
}

