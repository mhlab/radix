#pragma once

#ifdef _WIN32
#include <WinSock2.h>
#endif

namespace Radix {
	namespace Network {
		inline int findFreePort(int from = -1) {
			if (from == -1)
				from = 1;

		#ifdef _WIN32
			WSADATA wsaData;
			WSAStartup(MAKEWORD(2, 2), &wsaData);
		#endif
			unsigned int socketfd = socket(AF_INET, SOCK_STREAM, 0);
			if (socketfd == -1)
				return -1;

			sockaddr_in addr = {0,};
			addr.sin_family = AF_INET;
			addr.sin_addr.s_addr = inet_addr("127.0.0.1");
			int len = sizeof(addr);
	
			int retport = -1;
			for (int i = from; i < 65535; i++)
			{
				addr.sin_port = htons(i);
				if (connect(socketfd, (sockaddr*)&addr, len) < 0)
				{
					retport = i;
					break;
				}
			}

		#ifdef _WIN32
			closesocket(socketfd);
			WSACleanup();
		#else
			close(socketfd);
		#endif

			return retport;
		}
	}
}