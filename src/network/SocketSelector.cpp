#include "StdAfx.h"
#include "SocketSelector.h"

Radix::Network::SocketSelector::SocketSelector():
	maxFd_(NULL) {
	FD_ZERO(&readSet_);
}

Radix::Network::SocketSelector::~SocketSelector() {
}

void Radix::Network::SocketSelector::initialize() {
	maxFd_ = NULL;
	FD_ZERO(&readSet_);
}

void Radix::Network::SocketSelector::addSocket(TCPSocket* socket) {
	if (socket == NULL)
		return;

	addSocket(socket->socket_);
}

void Radix::Network::SocketSelector::addSocket(SOCKET socket) {
    if (socket == INVALID_SOCKET)
        return;
    
	FD_SET(socket, &readSet_);
	if (maxFd_ == NULL) {
		maxFd_ = socket;
		return;
	}
	if (socket > maxFd_)
		maxFd_ = socket;
}

bool Radix::Network::SocketSelector::selectEx(int timeout) {
	if (maxFd_ == NULL)
		return false;

	timeval tv;
	tv.tv_sec = timeout / 1000;
	tv.tv_usec = timeout % 1000;

#ifdef _WIN32
	return (select(0, &readSet_, NULL, NULL, &tv) > -1);
#else
	return (select(maxFd_ + 1, &readSet_, NULL, NULL, &tv) > -1);
#endif
}

bool Radix::Network::SocketSelector::isSet(TCPSocket* socket) {
	if (socket == NULL || !socket->isConnected())
		return false;

	return isSet(socket->socket_);
}

bool Radix::Network::SocketSelector::isSet(SOCKET socket) {
	if (socket == INVALID_SOCKET)
		return false;

	return (FD_ISSET(socket, &readSet_) > 0);
}
