#include "StdAfx.h"
#include "TCPSocket.h"
#ifdef _WIN32
#else
#include <errno.h>
#endif

Radix::Network::TCPSocket::TCPSocket():
	connectThread_(NULL),
	ip_(""),
	port_(0),
	connectionTimeout_(-1),
	socket_(INVALID_SOCKET),
	listener_(NULL)
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif
}

Radix::Network::TCPSocket::~TCPSocket()
{
	disconnect();
#ifdef _WIN32
	WSACleanup();
#endif
}

void Radix::Network::TCPSocket::onThreadFunc(ThreadRunner* threadRunner, Signal& signal, bool& terminate) {
	disconnect();
	try {
		if (port_ <= 0 || port_ > 65535)
			throw 0;
		SOCKET connectionSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (connectionSocket == INVALID_SOCKET)
			throw 1;
		setAsyncMode(true);

		sockaddr_in serverAddr = {0,};
		serverAddr.sin_family = AF_INET;
		serverAddr.sin_addr.s_addr = inet_addr(ip_.c_str());
		serverAddr.sin_port = htons(port_);
		int ret = connect(connectionSocket, (struct sockaddr*)&serverAddr, sizeof(struct sockaddr_in));
	#ifdef _WIN32
		if (ret == -1 && GetLastError() != WSAEWOULDBLOCK)
	#else
		if (ret == -1 && errno != EINPROGRESS)
	#endif
			throw 3;

		setAsyncMode(false);
		fd_set writeEvent, errEvent;
		FD_ZERO(&writeEvent);
		FD_ZERO(&errEvent);
		FD_SET(connectionSocket, &writeEvent);
		FD_SET(connectionSocket, &errEvent);
		timeval timeoutVal;
		timeoutVal.tv_sec = connectionTimeout_;
		timeoutVal.tv_usec = 0;
		select(0, NULL, &writeEvent, &errEvent, &timeoutVal);

		bool connectionRet = (FD_ISSET(connectionSocket, &writeEvent) > 0);
		if (!connectionRet)
			throw 4;

		socket_ = connectionSocket;
		if (listener_ != NULL)
			listener_->onTCPSocketConnection(true);
		return;
	}
	catch(...) {
	}

	disconnect();
	if (listener_ != NULL)
		listener_->onTCPSocketConnection(false);
}

void Radix::Network::TCPSocket::setListener(ITCPSocket* listener) {
	listener_ = listener;
}

int Radix::Network::TCPSocket::lastError() {
#ifdef _WIN32
	return WSAGetLastError();
#else
	return errno;
#endif
}

bool Radix::Network::TCPSocket::isWouldBlock() {
#ifdef _WIN32
	return (lastError() == WSAEWOULDBLOCK);
#else
	return (lastError() == EWOULDBLOCK);
#endif

}

void Radix::Network::TCPSocket::connectAsync(const char* ip, int port, int connectionTimeout) {
	if (connectThread_ != NULL) {
		if (!connectThread_->isTerminated())
			return;

		delete connectThread_;
	}
	ip_ = ip;
	port_ = port;
	connectionTimeout_ = connectionTimeout;

	connectThread_ = new ThreadRunner(this);
}

void Radix::Network::TCPSocket::disconnect() {
	if (socket_ == INVALID_SOCKET)
		return;

#ifdef _WIN32
	closesocket(socket_);
#else
	close(socket_);
#endif
	socket_ = INVALID_SOCKET;
}

void Radix::Network::TCPSocket::setAsyncMode(bool value) {
	if (socket_ == INVALID_SOCKET)
		return;

#ifdef _WIN32
	u_long mode = 1;
	if (!value)
		mode = 0;
	ioctlsocket(socket_, FIONBIO, &mode);
#else
	int flags;
	flags = fcntl(socket_, F_GETFL, 0);
	if (value)
		fcntl(socket_, F_SETFL, flags | O_NONBLOCK);
	else
		fcntl(socket_, F_SETFL, flags & (~O_NONBLOCK));
#endif
}

bool Radix::Network::TCPSocket::isConnected() {
	return (socket_ != INVALID_SOCKET);
}

bool Radix::Network::TCPSocket::write(const char* data, int dataSize) {
	if (socket_ == INVALID_SOCKET)
		return false;

	int totalsize = 0;
	int sendsize = 0;
	char* datapos = (char*)data;
	while (totalsize < dataSize) {
		datapos = datapos + sendsize;
		sendsize = send(socket_, data, dataSize, 0);
		if (sendsize == -1) {
			if (!isWouldBlock())
				return false;
			continue;
		}
		totalsize += sendsize;
	}

	return true;
}

bool Radix::Network::TCPSocket::read(char* data, int dataSize) {
	if (socket_ == INVALID_SOCKET)
		return false;

	int totalsize = 0;
	int recvsize = 0;
	char* datapos = (char*)data;
	while (totalsize < dataSize) {
		datapos = datapos + recvsize;
		recvsize = recv(socket_, data, dataSize, 0);
		if (recvsize == 0)
			return false;
		if (recvsize == -1) {
			if (!isWouldBlock())
				return false;
			continue;
		}
		totalsize += recvsize;
	}

	return true;
}

bool Radix::Network::TCPSocket::read(std::stringstream& buf, const char* lastChar, int lastCharLen) {
	buf.str("");
	if (socket_ == INVALID_SOCKET)
		return false;

	char data;
	int recvsize;
	std::string bufstr;

	while (true) {
		recvsize = recv(socket_, &data, 1, 0);
		if (recvsize <= 0)
			return false;
		buf << data;
		bufstr = buf.str();
		if ((int)bufstr.length() >= lastCharLen) {
			if (strcmp(((char*)bufstr.c_str() + bufstr.length() - lastCharLen), lastChar) == 0)
				return true;
		}
	}

	return true;
}
