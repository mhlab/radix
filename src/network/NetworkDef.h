#pragma once

#ifdef _WIN32
#include <WinSock2.h>
typedef int socklen_t;
#else
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
typedef unsigned int SOCKET;
#define INVALID_SOCKET (SOCKET)(~0)
#endif
