#pragma once

#ifdef _WIN32
#include <Windows.h>
#else
#include <pthread.h>
#endif

namespace Radix {
	namespace Thread {
		class Signal {
		private:
#ifdef _WIN32
			HANDLE event_;
#else
			pthread_mutex_t lock_;
			pthread_cond_t signal_;
			bool isSet_;
#endif
		public:
			Signal();
			~Signal();

		public:
			void set();
			void wait();
		};
	}
}


