#include "StdAfx.h"
#include "ThreadRunner.h"
#ifdef _WIN32
#include <process.h>
#else
#include <unistd.h>
#endif

#ifdef _WIN32
const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push,8)
typedef struct tagTHREADNAME_INFO
{
    DWORD dwType; // Must be 0x1000.
    LPCSTR szName; // Pointer to name (in user addr space).
    DWORD dwThreadID; // Thread ID (-1=caller thread).
    DWORD dwFlags; // Reserved for future use, must be zero.
 } THREADNAME_INFO;
#pragma pack(pop)
#endif

Radix::Thread::ThreadRunner::ThreadRunner(IThreadRunner* listener):
	listener_(listener),
	terminate_(false) {
#ifdef _WIN32
	threadHandle_ = (HANDLE)_beginthreadex(NULL, 0, threadFunc, this, 0, NULL);
#else
	pthread_create(&threadHandle_, NULL, threadFunc, this);
#endif
}

Radix::Thread::ThreadRunner::~ThreadRunner() {
	terminate_ = true;
	signal_.set();
	Locker locker(&lock_);

#ifdef _WIN32
	WaitForSingleObject(threadHandle_, INFINITE);
	CloseHandle(threadHandle_);
#else
	pthread_join(threadHandle_, NULL);
#endif
}

#ifdef _WIN32
unsigned int Radix::Thread::ThreadRunner::threadFunc(void* argu) {
	ThreadRunner* runner = (ThreadRunner*)argu;
	if (runner->terminate_)
		return 0;

	runner->internalSetThreadName();
	Locker locker(&(runner->lock_));
	if (runner->listener_ != NULL)
		runner->listener_->onThreadFunc(runner, runner->signal_, runner->terminate_);

	runner->terminate_ = true;

	return 0;
}
#else
void* Radix::Thread::ThreadRunner::threadFunc(void* argu) {
	ThreadRunner* runner = (ThreadRunner*)argu;
	if (runner->terminate_)
		return NULL;

	runner->internalSetThreadName();
	Locker locker(&(runner->lock_));
	if (runner->listener_ != NULL)
		runner->listener_->onThreadFunc(runner, runner->signal_, runner->terminate_);

	runner->terminate_ = true;

	return NULL;
}
#endif

void Radix::Thread::ThreadRunner::setThreadName(const char* name) {
	name_ = name;
}

bool Radix::Thread::ThreadRunner::isTerminated() {
	return terminate_;
}

void Radix::Thread::ThreadRunner::setEvent() {
	signal_.set();
}

void Radix::Thread::ThreadRunner::internalSetThreadName() {
	if (name_.length() == 0)
		return;

#ifdef _WIN32
	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = name_.c_str();
	info.dwThreadID = GetCurrentThreadId();
	info.dwFlags = 0;
	#pragma warning(push)
	#pragma warning(disable: 6320 6322)
		__try{
			RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
	#pragma warning(pop)
#elif __APPLE__
    pthread_setname_np(name_.c_str());
#else
	pthread_setname_np(threadHandle_, name_.c_str());
#endif
}

void Radix::Thread::ThreadRunner::sleep(int ms) {
#ifdef _WIN32
	Sleep(ms);
#else
    usleep(ms * 1000);
#endif
}
