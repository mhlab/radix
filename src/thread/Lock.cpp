#include "StdAfx.h"
#include "Lock.h"

Radix::Thread::Lock::Lock() {
#ifdef _WIN32
	InitializeCriticalSection(&lock_);
#else
	pthread_mutex_init(&lock_, NULL);	
#endif
}

Radix::Thread::Lock::~Lock() {
#ifdef _WIN32
	DeleteCriticalSection(&lock_);
#else
	pthread_mutex_destroy(&lock_);	
#endif
}

void Radix::Thread::Lock::lock() {
#ifdef _WIN32
	EnterCriticalSection(&lock_);
#else
	pthread_mutex_lock(&lock_);
#endif
}

void Radix::Thread::Lock::unlock() {
#ifdef _WIN32
	LeaveCriticalSection(&lock_);
#else
	pthread_mutex_unlock(&lock_);
#endif
}

Radix::Thread::Locker::Locker(Lock* lock)
{
	lock_ = lock;
	lock_->lock();
}

Radix::Thread::Locker::~Locker()
{
	lock_->unlock();
}
