#pragma once

#ifdef _WIN32
#include <Windows.h>
#else
#include <pthread.h> 
#endif

namespace Radix {
	namespace Thread {
		class Lock {
		private:
			#ifdef _WIN32
			CRITICAL_SECTION lock_;
			#else
			pthread_mutex_t lock_;
			#endif

		public:
			Lock();
			~Lock();

		public:
			void lock();
			void unlock();
		};

		class Locker {
		private:
			Lock* lock_;

		public:
			Locker(Lock* lock);
			~Locker();
		};
	};
};