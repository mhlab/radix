#include "StdAfx.h"
#include "Signal.h"

Radix::Thread::Signal::Signal()
{
#ifdef _WIN32
	event_ = CreateEvent(NULL, FALSE, FALSE, NULL);
#else
    pthread_mutex_init(&lock_, NULL);
    pthread_cond_init(&signal_, NULL);
	isSet_ = false;
#endif
}

Radix::Thread::Signal::~Signal()
{
#ifdef _WIN32 
	CloseHandle(event_);
#else
    pthread_cond_destroy(&signal_);
    pthread_mutex_destroy(&lock_);  
#endif
}

void Radix::Thread::Signal::set() {
#ifdef _WIN32
	SetEvent(event_);
#else
  pthread_mutex_lock(&lock_);
  isSet_ = true;
  pthread_mutex_unlock(&lock_);
  pthread_cond_signal(&signal_);
#endif
}

void Radix::Thread::Signal::wait() {
#ifdef _WIN32
	WaitForSingleObject(event_, INFINITE);
#else
  pthread_mutex_lock(&lock_);
  while (!isSet_)
    pthread_cond_wait(&signal_, &lock_);
  isSet_ = false;
  pthread_mutex_unlock(&lock_);
#endif
}
