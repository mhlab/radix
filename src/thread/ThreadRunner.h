#pragma once

#include "Signal.h"
#include "Lock.h"
#include <string>

namespace Radix {
	namespace Thread {
		class ThreadRunner;

		struct IThreadRunner {
			virtual void onThreadFunc(Thread::ThreadRunner* threadRunner, Thread::Signal& signal, bool& terminate) = 0;
		};

		class ThreadRunner {
		private:
			IThreadRunner* listener_;
			bool terminate_;
			Signal signal_;
			Lock lock_;
			std::string name_;

#ifdef _WIN32
			HANDLE threadHandle_;
			static unsigned int __stdcall threadFunc(void* argu);
#else
			pthread_t threadHandle_;
			static void* threadFunc(void* argu);
#endif
			void internalSetThreadName();

		public:
			ThreadRunner(IThreadRunner* listener);
			~ThreadRunner();

		public:
			void setThreadName(const char* name);
			bool isTerminated();
			void setEvent();
			static void sleep(int ms);
		};
	}
}

