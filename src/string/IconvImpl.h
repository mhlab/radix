#pragma once

#ifdef _WIN32
#pragma comment(lib, "libiconv.lib")
#endif

#include "iconv/iconv.h"

namespace Radix {
	namespace String {
		class IconvImpl	{
		public:
			IconvImpl();
			~IconvImpl();

		public:
			bool convert(const char* srccode, const char* dstcode, char** src, size_t& srcsize, char** dst, size_t& dstsize);
		};
	}
}
