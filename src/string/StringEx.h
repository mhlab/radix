#pragma once

#include <string>
#include <vector>

namespace Radix {
	namespace String {
		std::string wtoa(const wchar_t* src);
		std::wstring atow(const char* src);
		std::string format(char* format, ...);
		std::string seperate(std::string& src, const char* delimiter);
		std::string trim(std::string& src);
		std::string toLower(std::string& src);
		void tokenize(const char* src, const char* delimiter, std::vector<std::string>& out);
		void replaceTo(std::string& content, const char* src, const char* dst);
		void replaceTo(std::wstring& content, const wchar_t* src, const wchar_t* dst);
		std::string getFileExtension(const char* filePath);
	}
}
