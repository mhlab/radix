#include "StdAfx.h"
#include "StringEx.h"
#include <stdarg.h>
#include <cstdlib>
#include <algorithm>

std::string Radix::String::wtoa(const wchar_t* src) {
	return "";
}

std::wstring Radix::String::atow(const char* src) {
	size_t srclen = strlen(src);
	if (srclen == 0)
		return L"";

	size_t buflen = srclen * 2;
	size_t retlen = 0;
	wchar_t* buf = new wchar_t[srclen * 2];
//	std::mbstowcs(buf, src, buflen);
	std::wstring ret(buf);
	delete[] buf;

	return ret;
}

std::string Radix::String::format(char* format, ...) {
	va_list args;
	va_start(args, format);

	char Buf[4096] = {0,};
	int nBuf = 0;
#ifdef _WIN32
	nBuf = _vsnprintf_s(Buf, 8192, format, args);
#else
	nBuf = vsnprintf(Buf, 8192, format, args);
#endif	
	va_end(args);

	std::string retBuf(Buf);

	return retBuf;
}

std::string Radix::String::seperate(std::string& src, const char* delimiter) {
	unsigned int pos = src.find(delimiter);
	if (pos == std::string::npos)
		return "";

	std::string ret = src.substr(0, pos);
	src.erase(0, pos + 2);

	return ret;
}

std::string Radix::String::trim(std::string& src) {
    unsigned int left = src.find_first_not_of(" ");
    unsigned int right = src.find_last_not_of(" ");
    return src.substr(left, (right - left + 1));
}

std::string Radix::String::toLower(std::string& src) {
	std::string ret = src;
	std::transform(ret.begin(), ret.end(), ret.begin(), ::tolower);

	return ret;
}

void Radix::String::tokenize(const char* src, const char* delimiter, std::vector<std::string>& out) {
	out.clear();
	unsigned int delimiterlen = strlen(delimiter);
	unsigned int srclen = strlen(src);
	if (delimiterlen > srclen)
		return;

	std::string srcstr = src;
	int pos = srcstr.find(delimiter);
	unsigned int srcpos = 0;
	if (pos == std::string::npos)
		return;

	std::string outstr;
	while (pos != std::string::npos)
	{
		outstr = srcstr.substr(srcpos, pos - srcpos);
		if (outstr.length() > 0)
			out.push_back(outstr);
		srcpos = pos + delimiterlen;
		pos = srcstr.find(delimiter, srcpos);
	}
	outstr = srcstr.substr(srcpos, srclen - srcpos);
	if (outstr.length() > 0)
		out.push_back(outstr);
}

void Radix::String::replaceTo(std::string& content, const char* src, const char* dst) {
	std::size_t pos = content.find(src, 0);
	while (pos != std::string::npos) {
		content.replace(pos, strlen(src), dst);
		pos = content.find(src, 0);
	}
}

void Radix::String::replaceTo(std::wstring& content, const wchar_t* src, const wchar_t* dst) {
	std::size_t pos = content.find(src, 0);
	while (pos != std::string::npos) {
		content.replace(pos, wcslen(src), dst);
		pos = content.find(src, 0);
	}
}

std::string Radix::String::getFileExtension(const char* filePath) {
	std::string path = filePath;
	std::size_t pos = path.find_last_of(".");
	if (pos == std::string::npos)
		return "";

	return path.substr(pos);
}