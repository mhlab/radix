#include "stdafx.h"
#include "IconvImpl.h"

Radix::String::IconvImpl::IconvImpl() {
}

Radix::String::IconvImpl::~IconvImpl() {
}

bool Radix::String::IconvImpl::convert(const char* srccode, const char* dstcode, char** src, size_t& srcsize, char** dst, size_t& dstsize) {
	iconv_t convenDiscriptor_ = iconv_open(dstcode, srccode);
	if (convenDiscriptor_ == iconv_t(-1))
		return false;

	size_t ret = iconv(convenDiscriptor_, src, &srcsize, dst, &dstsize);
	iconv_close(convenDiscriptor_);

	return (ret >= 0);
}
